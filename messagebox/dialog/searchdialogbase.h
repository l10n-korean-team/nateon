/****************************************************************************
** Form interface generated from reading ui file 'searchdialogbase.ui'
**
** Created: 수  6월 13 18:36:23 2007
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.7   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef SEARCHFORM_H
#define SEARCHFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QComboBox;
class QLineEdit;
class QPushButton;

class SearchForm : public QDialog {
    Q_OBJECT

public:
    SearchForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~SearchForm();
    void init();

    QComboBox* searchComboBox;
    QLineEdit* searchLineEdit;
    QPushButton* searchButton;
    QPushButton* cancelButton;

protected:
    QHBoxLayout* SearchFormLayout;

protected slots:
    virtual void languageChange();
    void accept();
    void slotEnableSearch( const QString & s );

private:
    QPixmap image0;
    QPixmap image1;
signals:
    void searchArg( int, const QString & );
};

#endif // SEARCHFORM_H
