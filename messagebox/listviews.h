/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LISTVIEWS_H
#define LISTVIEWS_H

#include <qtextedit.h>
#include <qsplitter.h>
#include <qstring.h>
#include <qobject.h>
#include <qdatetime.h>
#include <qptrlist.h>
#include <qlistview.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kglobal.h>
#include <kdebug.h>
#include <kmessagebox.h>
#include <kaboutdata.h>

class QListView;
class QLabel;
class QPainter;
class QColorGroup;
class QObjectList;
class QPopupMenu;

// -----------------------------------------------------------------

class MessageHeader {
public:
    MessageHeader( const QString &_id, const QString &_sender, const QString &_receivers, const QString &_subject, const QString &_datetime )
            : mid(_id),
            msender( _sender ),
            mreceivers( _receivers ),
            msubject( _subject ),
            mdatetime( _datetime ) {}

    MessageHeader( const MessageHeader &mh );
    MessageHeader &operator=( const MessageHeader &mh );

    QString id() {
        return mid;
    }
    QString sender() {
        return msender;
    }
    QString receivers() {
        return mreceivers;
    }
    QString subject() {
        return msubject;
    }
    QString datetime() {
        return mdatetime;
    }

protected:
    QString mid, msender, mreceivers, msubject;
    QString mdatetime;

};

// -----------------------------------------------------------------

class Message {
public:
    enum State { Read = 0,
                 Unread
               };

    Message( const MessageHeader &mh, const QString &_body )
            : mheader( mh ), mbody( _body ), mstate( Unread ) {}

    Message( const Message &m )
            : mheader( m.mheader ), mbody( m.mbody ), mstate( m.mstate ) {}

    MessageHeader header() {
        return mheader;
    }
    QString body() {
        return mbody;
    }

    void setState( const State &s ) {
        mstate = s;
    }
    State state() {
        return mstate;
    }

protected:
    MessageHeader mheader;
    QString mbody;
    State mstate;

};

// -----------------------------------------------------------------

class Folder : public QObject {
    Q_OBJECT

public:
    Folder( Folder *parent, const QString &name );
    ~Folder() {}

    void addMessage( Message *m ) {
        lstMessages.append( m );
    }

    QString folderName() {
        return fName;
    }
    void clear() {
        lstMessages.clear();
    }

    Message *firstMessage() {
        return lstMessages.first();
    }
    Message *nextMessage() {
        return lstMessages.next();
    }

protected:
    QString fName;
    QPtrList<Message> lstMessages;

};

// -----------------------------------------------------------------

class FolderListItem : public QListViewItem {
public:
    FolderListItem( QListView *parent, Folder *f );
    FolderListItem( FolderListItem *parent, Folder *f );

    void insertSubFolders( const QObjectList *lst );

    Folder *folder() {
        return myFolder;
    }

protected:
    Folder *myFolder;

};

// -----------------------------------------------------------------

class MessageListItem : public QListViewItem {
public:
    MessageListItem( QListView *parent, Message *m );

    virtual void paintCell( QPainter *p, const QColorGroup &cg,
                            int column, int width, int alignment );

    Message *message() {
        return myMessage;
    }

protected:
    Message *myMessage;

};

// -----------------------------------------------------------------

class ListViews : public QSplitter {
    Q_OBJECT
public:
    ListViews( QWidget *parent = 0, const char *name = 0 );
    ~ListViews() {}
    void createMemoTree();
    void createChatTree();
    void addMemoInbox(const QString &sID, const QString &sSender, const QString &sReceivers, const QString & sSubject, const QString &sBody, const QString &sDateTime, bool bReaded);
    void addMemoOutbox(const QString &sID, const QString &sSender, const QString &sReceivers, const QString & sSubject, const QString &sBody, const QString &sDateTime, bool bReaded);
    void addChatBox(const QString &sID, const QString &sSender, const QString &sReceivers, const QString & sSubject, const QString &sBody, const QString &sDateTime, bool bReaded);
    void initFolders();
    int getFolderIndex();
    void clearList();
    void refresh();
    QString getText() {
        return message->text();
    }
    QListViewItem* getCurrentItem();
    void removeCurrentItem();
    void removeItem(QListViewItem *item);

    void createMemoMouseMenu();
    void createChatMouseMenu();
    QPopupMenu* menu;
    QListView *messages;
    QListView *folders;
    // QLabel *message;
    QTextEdit *message;

    int getSelectedItemCount();

protected:
    void initFolder( Folder *folder, unsigned int &count );
    void setupFolders();


    QPtrList<Folder> lstFolders;

    KStandardDirs   *dirs; /*   = KGlobal::dirs();*/
    QString         sPicsPath;

    void resizeEvent ( QResizeEvent * e );
    void showEvent ( QShowEvent * e );

private:
    Folder *fMemoInbox;
    Folder *fMemoOutbox;
    Folder *fChatBox;
    FolderListItem *fMemoInboxItem;
    FolderListItem *fMemoOutboxItem;
    FolderListItem *fChatBoxItem;
    int m_nOpenMenuID;
    int m_nSaveMenuID;
    int m_nReplyMenuID;
    int m_nReplyAllMenuID;
    int m_nForwardMenuID;
    int m_nDeleteMenuID;
    int m_nChatSaveMenuID;
    int m_nChatDeleteMenuID;
protected slots:
    void slotFolderChanged( QListViewItem* );
    void slotMessageChanged();
    void slotRMB( QListViewItem*, const QPoint &, int );
    // virtual void slotDoubleClick( QListViewItem *pItem, const QPoint &, int nPoint);
    void slotUNREAD();
    void slotReply();
    void slotReplyAll();
    void slotForward();
    void slotDelete();
    void slotOpen();
    void slotSave();
    void slotChatSave();
    void slotChatDelete();

signals:
    void setREAD( QListViewItem* );
    void setUNREAD( QListViewItem* );
    void sendReply( QListViewItem* );
    void sendReplyAll( QListViewItem* );
    void sendForward( QListViewItem* );
    void setDelete( QListViewItem* );
    void showMemoInbox();
    void showMemoOutbox();
    void showChatBox();
    void multiItemsSelected( bool );
};

#endif
