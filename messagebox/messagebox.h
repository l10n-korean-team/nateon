/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _MESSAGEBOX_H_
#define _MESSAGEBOX_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <kapp.h>
#include <kdebug.h>
#include <kmainwindow.h>
#include <kstandarddirs.h>
#include <kpopupmenu.h>
#include <kmenubar.h>
#include <dcopclient.h>
#include <klocale.h>
#include <kaction.h>
#include <kstdaction.h>
#include <kactionclasses.h>
#include <khelpmenu.h>
#include <kaboutdata.h>

#include "messageboxdcop.h"
#include "messageboxwidget.h"
#include "utils.h"


class MessageBoxDCOP;


/**
 * @short Application Main Window
 * @author 장두현 <ring0320@nate.com>
 * @version 0.1
 */
class messagebox : public KMainWindow, virtual public MessageBoxDCOP {
    Q_OBJECT
public:
    /**
      * Default Constructor
      */
    messagebox();
    /**
      * Default Destructor
      */
    // virtual ~messagebox();
    // ~messagebox();

    void createMenu();
    // void setUserID( QString _UserID ) { sUserID = _UserID; }

    QString getUserID() {
        return sUserID;
    }

    void setUserID( const QString s );
    void setDBPath( const QString s );
    void setDefaultBrowser( const QString s );
    void dcopCommand( const QString s );
    void openDB( const QString& sPath );

private:
    KPopupMenu  *pFile;
    KPopupMenu  *pMessage;
    KPopupMenu  *pTool;
    KPopupMenu  *pView;
    KPopupMenu  *pHelp;

    KAction *pOpenAction;
    KAction *pSaveAction;
    KAction *pQuitAction;

    KAction *pNewAction;
    KAction *pReplyMemoAction;
    KAction *pReplyAllMemoAction;
    KAction *pForwardMemoAction;
    KAction *pDeleteAction;

    KToggleAction *pAlwaysTopAction;
    KAction *pFindAction;
    KAction *pSeletAllAction;
    KAction *pSetupAction;

    KAction *pRefreshAction;

    KAction *pNateDotComAction;
    KAction *pHelpAction;
    KAction *pHotTipAction;
    KAction *pFaqAction;
    KAction *pAboutAction;

    QString     sUserID;
    QString     sDBPath;
    messageboxWidget *pMessageboxWidget;

    KHelpMenu*			helpMenu_;

protected slots:
    void slotQuit();
    void slotOpenMenu();
    void slotSaveMenu();
    void slotNewMenu();
    void slotAlwaysTop( bool bAlwaysTop );
    void slotSetup();
    void slotFindMenu();
    void slotSelectAll();
    void slotRefreshMenu();
    void slotGoNateDotComMenu();
    void slotGoHelpMenu();
    void slotGoHotTipMenu();
    void slotGoFaqMenu();
    void slotGoAboutMenu();
    void slotDelete();
    void slotChangeTab( int nTab );

signals:
    void gotDBPath(const QString &sPath);
};

#endif // _MESSAGEBOX_H_
