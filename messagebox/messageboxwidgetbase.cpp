#include <kdialog.h>
#include <klocale.h>
/****************************************************************************
** Form implementation generated from reading ui file '/home/x/devel/messagebox/src/messageboxwidgetbase.ui'
**
** Created: 월  6월 11 19:21:20 2007
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.7   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "messageboxwidgetbase.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

/*
 *  Constructs a messageboxWidgetBase as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
messageboxWidgetBase::messageboxWidgetBase( QWidget* parent, const char* name, WFlags fl )
        : QWidget( parent, name, fl ) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString sPicsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" );

    if ( !name )
        setName( "messagebox" );
    setIcon( QPixmap( sPicsPath + "title_icon_tmb.bmp" ) );
    messageboxwidgetbaseLayout = new QVBoxLayout( this, 0, 0, "messageboxwidgetbaseLayout");

    tabWidget2 = new QTabWidget( this, "tabWidget2" );

    tab = new QWidget( tabWidget2, "tab" );
    tabLayout = new QVBoxLayout( tab, 0, 0, "tabLayout");

    memoToolBar = new QFrame( tab, "memoToolBar" );
    memoToolBar->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 58, memoToolBar->sizePolicy().hasHeightForWidth() ) );
    memoToolBar->setMinimumSize( QSize( 0, 58 ) );
    memoToolBar->setMaximumSize( QSize( 32767, 58 ) );
    memoToolBar->setPaletteBackgroundPixmap( QPixmap( sPicsPath + "tmh_iconset_bg.bmp" ) );
    memoToolBar->setFrameShape( QFrame::StyledPanel );
    memoToolBar->setFrameShadow( QFrame::Plain );
    memoToolBarLayout = new QHBoxLayout( memoToolBar, 0, 0, "memoToolBarLayout");

    memoWriteButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_01_nor.bmp", 16777215 );
    memoWriteButton->setMinimumSize( QSize( 56, 55 ) );
    memoWriteButton->setMaximumSize( QSize( 56, 55 ) );
    memoWriteButton->setMouseOverShape( sPicsPath + "tmh_iconset_01_ov.bmp", 16777215 );
    memoWriteButton->setPressedShape( sPicsPath + "tmh_iconset_01_down.bmp", 16777215 );
    // memoWriteButton->setPixmap( QPixmap( sPicsPath + "tmh_iconset_01_nor.bmp" ) );
    memoToolBarLayout->addWidget( memoWriteButton );

    memoReplyButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_02_nor.bmp", 16777215 );
    memoReplyButton->setMinimumSize( QSize( 56, 55 ) );
    memoReplyButton->setMaximumSize( QSize( 56, 55 ) );
    memoReplyButton->setMouseOverShape( sPicsPath + "tmh_iconset_02_ov.bmp", 16777215 );
    memoReplyButton->setPressedShape( sPicsPath + "tmh_iconset_02_down.bmp", 16777215 );
    // memoReplyButton->setPixmap( QPixmap( sPicsPath + "tmh_iconset_02_nor.bmp" ) );
    memoToolBarLayout->addWidget( memoReplyButton );

    memoReplyAllButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_03_nor.bmp", 16777215 );
    memoReplyAllButton->setMinimumSize( QSize( 56, 55 ) );
    memoReplyAllButton->setMaximumSize( QSize( 56, 55 ) );
    memoReplyAllButton->setMouseOverShape( sPicsPath + "tmh_iconset_03_ov.bmp", 16777215 );
    memoReplyAllButton->setPressedShape( sPicsPath + "tmh_iconset_03_down.bmp", 16777215 );
    // memoReplyAllButton->setPixmap( XXXXXX(  ) );
    memoToolBarLayout->addWidget( memoReplyAllButton );

    memoForwardButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_04_nor.bmp", 16777215 );
    memoForwardButton->setMinimumSize( QSize( 56, 55 ) );
    memoForwardButton->setMaximumSize( QSize( 56, 55 ) );
    // memoForwardButton->setPixmap( XXXXXX(  ) );
    memoForwardButton->setMouseOverShape( sPicsPath + "tmh_iconset_04_ov.bmp", 16777215 );
    memoForwardButton->setPressedShape( sPicsPath + "tmh_iconset_04_down.bmp", 16777215 );
    memoToolBarLayout->addWidget( memoForwardButton );

    memoSaveButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_05_nor.bmp", 16777215 );
    memoSaveButton->setMinimumSize( QSize( 56, 55 ) );
    memoSaveButton->setMaximumSize( QSize( 56, 55 ) );
    memoSaveButton->setMouseOverShape( sPicsPath + "tmh_iconset_05_ov.bmp", 16777215 );
    memoSaveButton->setPressedShape( sPicsPath + "tmh_iconset_05_down.bmp", 16777215 );
    // memoSaveButton->setPixmap( XXXXXX(  ) );
    memoToolBarLayout->addWidget( memoSaveButton );

    memoDeleteButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_06_nor.bmp", 16777215 );
    memoDeleteButton->setMinimumSize( QSize( 56, 55 ) );
    memoDeleteButton->setMaximumSize( QSize( 56, 55 ) );
    memoDeleteButton->setMouseOverShape( sPicsPath + "tmh_iconset_06_ov.bmp", 16777215 );
    memoDeleteButton->setPressedShape( sPicsPath + "tmh_iconset_06_down.bmp", 16777215 );
    // memoDeleteButton->setPixmap( XXXXXX(  ) );
    memoToolBarLayout->addWidget( memoDeleteButton );

    memoFindButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_07_nor.bmp", 16777215 );
    memoFindButton->setMinimumSize( QSize( 56, 55 ) );
    memoFindButton->setMaximumSize( QSize( 56, 55 ) );
    memoFindButton->setMouseOverShape( sPicsPath + "tmh_iconset_07_ov.bmp", 16777215 );
    memoFindButton->setPressedShape( sPicsPath + "tmh_iconset_07_down.bmp", 16777215 );
    // memoFindButton->setPixmap( XXXXXX(  ) );
    memoToolBarLayout->addWidget( memoFindButton );

    memoRefreshButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_08_nor.bmp", 16777215 );
    memoRefreshButton->setMinimumSize( QSize( 56, 55 ) );
    memoRefreshButton->setMaximumSize( QSize( 56, 55 ) );
    memoRefreshButton->setMouseOverShape( sPicsPath + "tmh_iconset_08_ov.bmp", 16777215 );
    memoRefreshButton->setPressedShape( sPicsPath + "tmh_iconset_08_down.bmp", 16777215 );
    // memoRefreshButton->setPixmap( XXXXXX(  ) );
    memoToolBarLayout->addWidget( memoRefreshButton );

    memoHelpButton = new ShapeButton( memoToolBar, sPicsPath + "tmh_iconset_09_nor.bmp", 16777215 );
    memoHelpButton->setMinimumSize( QSize( 56, 55 ) );
    memoHelpButton->setMaximumSize( QSize( 56, 55 ) );
    memoHelpButton->setMouseOverShape( sPicsPath + "tmh_iconset_09_ov.bmp", 16777215 );
    memoHelpButton->setPressedShape( sPicsPath + "tmh_iconset_09_down.bmp", 16777215 );
    // memoHelpButton->setPixmap( XXXXXX(  ) );
    memoToolBarLayout->addWidget( memoHelpButton );
    spacer1 = new QSpacerItem( 80, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    memoToolBarLayout->addItem( spacer1 );

    LogoLabel1 = new QLabel( memoToolBar, "LogoLabel1" );
    LogoLabel1->setPixmap( QPixmap( sPicsPath + "tmh_iconset_nateon_logo.bmp" ) );
    LogoLabel1->setMinimumSize( QSize( 165, 55 ) );
    LogoLabel1->setMaximumSize( QSize( 165, 55 ) );
    LogoLabel1->setScaledContents( TRUE );
    memoToolBarLayout->addWidget( LogoLabel1 );
    tabLayout->addWidget( memoToolBar );

    memoFrame = new QFrame( tab, "memoFrame" );
    memoFrame->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, memoFrame->sizePolicy().hasHeightForWidth() ) );
    memoFrame->setFrameShape( QFrame::StyledPanel );
    memoFrame->setFrameShadow( QFrame::Raised );
    tabLayout->addWidget( memoFrame );
    tabWidget2->insertTab( tab, QString::fromLatin1("") );

    tab_2 = new QWidget( tabWidget2, "tab_2" );
    tabLayout_2 = new QVBoxLayout( tab_2, 0, 0, "tabLayout_2");

    chatToolBar = new QFrame( tab_2, "chatToolBar" );
    chatToolBar->setMinimumSize( QSize( 0, 58 ) );
    chatToolBar->setMaximumSize( QSize( 32767, 58 ) );
    chatToolBar->setPaletteBackgroundPixmap( QPixmap( sPicsPath + "tmh_iconset_bg.bmp" ) );
    chatToolBar->setFrameShape( QFrame::StyledPanel );
    chatToolBar->setFrameShadow( QFrame::Raised );
    chatToolBarLayout = new QHBoxLayout( chatToolBar, 0, 0, "chatToolBarLayout");

    chatGoChatButton = new ShapeButton( chatToolBar, sPicsPath + "tmh_d_iconset_01_nor.bmp", 16777215 );
    chatGoChatButton->setMinimumSize( QSize( 56, 55 ) );
    chatGoChatButton->setMaximumSize( QSize( 56, 55 ) );
    // chatGoChatButton->setPixmap( sPicsPath + "tmh_d_iconset_01_nor.bmp" );
    chatGoChatButton->setMouseOverShape( sPicsPath + "tmh_d_iconset_01_ov.bmp", 16777215 );
    chatGoChatButton->setPressedShape( sPicsPath + "tmh_d_iconset_01_down.bmp", 16777215 );
    chatToolBarLayout->addWidget( chatGoChatButton );

    chatSaveButton = new ShapeButton( chatToolBar, sPicsPath + "tmh_d_iconset_02_nor.bmp", 16777215 );
    chatSaveButton->setMinimumSize( QSize( 56, 55 ) );
    chatSaveButton->setMaximumSize( QSize( 56, 55 ) );
    chatSaveButton->setMouseOverShape( sPicsPath + "tmh_d_iconset_02_ov.bmp", 16777215 );
    chatSaveButton->setPressedShape( sPicsPath + "tmh_d_iconset_02_down.bmp", 16777215 );
    chatToolBarLayout->addWidget( chatSaveButton );

    chatDeleteButton = new ShapeButton( chatToolBar, sPicsPath + "tmh_d_iconset_03_nor.bmp", 16777215 );
    chatDeleteButton->setMinimumSize( QSize( 56, 55 ) );
    chatDeleteButton->setMaximumSize( QSize( 56, 55 ) );
    chatDeleteButton->setMouseOverShape( sPicsPath + "tmh_d_iconset_03_ov.bmp", 16777215 );
    chatDeleteButton->setPressedShape( sPicsPath + "tmh_d_iconset_03_down.bmp", 16777215 );
    chatToolBarLayout->addWidget( chatDeleteButton );

    chatFindButton = new ShapeButton( chatToolBar, sPicsPath + "tmh_d_iconset_04_nor.bmp", 16777215 );
    chatFindButton->setMinimumSize( QSize( 56, 55 ) );
    chatFindButton->setMaximumSize( QSize( 56, 55 ) );
    chatFindButton->setMouseOverShape( sPicsPath + "tmh_d_iconset_04_ov.bmp", 16777215 );
    chatFindButton->setPressedShape( sPicsPath + "tmh_d_iconset_04_down.bmp", 16777215 );
    chatToolBarLayout->addWidget( chatFindButton );

    chatRefreshButton = new ShapeButton( chatToolBar, sPicsPath + "tmh_d_iconset_05_nor.bmp", 16777215 );
    chatRefreshButton->setMinimumSize( QSize( 56, 55 ) );
    chatRefreshButton->setMaximumSize( QSize( 56, 55 ) );
    chatRefreshButton->setMouseOverShape( sPicsPath + "tmh_d_iconset_05_ov.bmp", 16777215 );
    chatRefreshButton->setPressedShape( sPicsPath + "tmh_d_iconset_05_down.bmp", 16777215 );
    chatToolBarLayout->addWidget( chatRefreshButton );

    chatHelpButton = new ShapeButton( chatToolBar, sPicsPath + "tmh_d_iconset_06_nor.bmp", 16777215 );
    chatHelpButton->setMinimumSize( QSize( 56, 55 ) );
    chatHelpButton->setMaximumSize( QSize( 56, 55 ) );
    chatHelpButton->setMouseOverShape( sPicsPath + "tmh_d_iconset_06_ov.bmp", 16777215 );
    chatHelpButton->setPressedShape( sPicsPath + "tmh_d_iconset_06_down.bmp", 16777215 );
    chatToolBarLayout->addWidget( chatHelpButton );
    spacer2 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    chatToolBarLayout->addItem( spacer2 );

    LogoLabel2 = new QLabel( chatToolBar, "LogoLabel2" );
    LogoLabel2->setPixmap( QPixmap( sPicsPath + "tmh_iconset_nateon_logo.bmp" ) );
    LogoLabel2->setScaledContents( TRUE );
    LogoLabel2->setMinimumSize( QSize( 165, 55 ) );
    LogoLabel2->setMaximumSize( QSize( 165, 55 ) );
    chatToolBarLayout->addWidget( LogoLabel2 );
    tabLayout_2->addWidget( chatToolBar );

    chatFrame = new QFrame( tab_2, "chatFrame" );
    chatFrame->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, chatFrame->sizePolicy().hasHeightForWidth() ) );
    chatFrame->setFrameShape( QFrame::StyledPanel );
    chatFrame->setFrameShadow( QFrame::Raised );
    tabLayout_2->addWidget( chatFrame );
    tabWidget2->insertTab( tab_2, QString::fromLatin1("") );
    messageboxwidgetbaseLayout->addWidget( tabWidget2 );
    languageChange();
    resize( QSize(848, 525).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
messageboxWidgetBase::~messageboxWidgetBase() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void messageboxWidgetBase::languageChange() {
    setCaption( tr2i18n( "통합메시지함" ) );
    /*
        memoWriteButton->setText( QString::null );
        memoReplyButton->setText( QString::null );
        memoReplyAllButton->setText( QString::null );
        memoForwardButton->setText( QString::null );
        memoSaveButton->setText( QString::null );
        memoDeleteButton->setText( QString::null );
        memoFindButton->setText( QString::null );
        memoRefreshButton->setText( QString::null );
        memoHelpButton->setText( QString::null );
    */
    tabWidget2->changeTab( tab, tr2i18n( "쪽지함" ) );
    /*
        chatGoChatButton->setText( QString::null );
        chatSaveButton->setText( QString::null );
        chatDeleteButton->setText( QString::null );
        chatFindButton->setText( QString::null );
        chatRefreshButton->setText( QString::null );
        chatHelpButton->setText( QString::null );
    */
    tabWidget2->changeTab( tab_2, tr2i18n( "대화함" ) );
}

void messageboxWidgetBase::button_clicked() {
    qWarning( "messageboxWidgetBase::button_clicked(): Not implemented yet" );
}

#include "messageboxwidgetbase.moc"
