/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _MESSAGEBOXWIDGET_H_
#define _MESSAGEBOXWIDGET_H_

#include <kapp.h>
#include <qlayout.h>
#include <qlabel.h>
#include <dcopclient.h>
#include <kdebug.h>
#include <qtabwidget.h>
#include <qfiledialog.h>
#include <qinputdialog.h>
#include <qcombobox.h>
#include <klocale.h>
#include <krun.h>
#include <qpopupmenu.h>
#include <kmessagebox.h>

#include "messageboxwidgetbase.h"
#include "listviews.h"
#include "dialog/searchdialogbase.h"
#include "utils.h"

class SearchForm;
class SQLiteDB;

class messageboxWidget : public messageboxWidgetBase {
    Q_OBJECT
public:
    messageboxWidget(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~messageboxWidget();
    /*$PUBLIC_FUNCTIONS$*/

    void requireDBPath();

    ListViews*    pListViews;
    ListViews*    pListViews_2;
    QVBoxLayout*  Widget8Layout;
    QVBoxLayout*  Widget9Layout;
    SQLiteDB      *pDB;
    QString       sDBPath;
    QString       sMemoInboxDBFilePath;
    QString       sMemoOutboxDBFilePath;
    QString       sChatBoxDBFilePath;
    QString       sUserDBFilePath;

    SearchForm    *pMemoSearch;
    SearchForm    *pChatSearch;

    void setUNREAD( QListViewItem* item );
    void setREAD( QListViewItem* item );
    QString getNameByID( QString sID );
    void showMemoAllInbox();
    void showMemoAllOutbox();
    void showChatAllList();

    void showMemoInbox( const QCString & sQuery );
    void showMemoOutbox( const QCString & sQuery );
    void showChatList( const QCString & sQuery );
    void dcopGetCommand();


protected:
    int getNewMemoCount();

public slots:
    virtual void button_clicked();
    void slotChatSearch( int nIdx, const QString & sKey );
    void slotMouseRight( int nId );

    /*! 메모 툴바 */
    void slotMemoWrite();
    void slotMemoReply();
    void slotMemoReplyAll();
    void slotMemoForward();
    void slotMemoDelete();
    void slotMemoFind();
    void slotMemoRefresh();
    void slotMemoHelp();
    void slotMemoSave();

    /*! 대화 툴바 */
    void slotGoChat();
    void slotChatSave();
    void slotChatDelete();
    void slotChatFind();
    void slotChatRefresh();
    void slotChatHelp();

protected slots:
    void slotMemoSearch( int nIdx, const QString & sKey );
    void slotListing( const QString &sDBPath );



    void slotShowMemoInbox();
    void slotShowMemoOutbox();
    void slotShowChatBox();

    void slotSetUNREAD( QListViewItem* item );
    void slotSetREAD( QListViewItem* item );
    void slotChangeTab( QWidget *wg );
    void slotChatMouseRight( int nId );
    void slotDoubleClick( QListViewItem *pItem, const QPoint & mPoint, int nID);
    void slotMemoView( const QString &sUUID );

    void slotMultiItemsSelectedEnv( bool bMultiSelected );

signals:
    void doubleClicked( const QString & );
    void changeShowTab( int );
};

#endif

