/****************************************************************************
** Form interface generated from reading ui file 'messageboxwidgetbase.ui'
**
** Created: 월  6월 11 19:22:41 2007
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.7   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef MESSAGEBOXWIDGETBASE_H
#define MESSAGEBOXWIDGETBASE_H

#include <kglobal.h>
#include <klocale.h>
#include <qvariant.h>
#include <qwidget.h>
#include <qpixmap.h>
#include <kstandarddirs.h>
#include <kaboutdata.h>

#include "shapewidget.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QTabWidget;
class QFrame;
class QLabel;
class ShapeButton;
class ShapeForm;
class ShapeWidget;

class messageboxWidgetBase : public QWidget {
    Q_OBJECT
public:
    messageboxWidgetBase( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~messageboxWidgetBase();

    QTabWidget* tabWidget2;
    QWidget* tab;
    QFrame* memoToolBar;
    ShapeButton* memoWriteButton;
    ShapeButton* memoReplyButton;
    ShapeButton* memoReplyAllButton;
    ShapeButton* memoForwardButton;
    ShapeButton* memoSaveButton;
    ShapeButton* memoDeleteButton;
    ShapeButton* memoFindButton;
    ShapeButton* memoRefreshButton;
    ShapeButton* memoHelpButton;
    QLabel* LogoLabel1;
    QFrame* memoFrame;
    QWidget* tab_2;
    QFrame* chatToolBar;
    ShapeButton* chatGoChatButton;
    ShapeButton* chatSaveButton;
    ShapeButton* chatDeleteButton;
    ShapeButton* chatFindButton;
    ShapeButton* chatRefreshButton;
    ShapeButton* chatHelpButton;
    QLabel* LogoLabel2;
    QFrame* chatFrame;

public slots:
    virtual void button_clicked();

protected:
    QVBoxLayout* messageboxwidgetbaseLayout;
    QVBoxLayout* tabLayout;
    QHBoxLayout* memoToolBarLayout;
    QSpacerItem* spacer1;
    QVBoxLayout* tabLayout_2;
    QHBoxLayout* chatToolBarLayout;
    QSpacerItem* spacer2;

protected slots:
    virtual void languageChange();

};

#endif // MESSAGEBOXWIDGETBASE_H
