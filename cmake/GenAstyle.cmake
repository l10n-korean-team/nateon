#
# Generate astyle
#
# Usage :
#   set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
#   INCLUDE( GenAstyle )
#
# Copyright (c) 2009, Doo-Hyun Jang, <dh.jang@gmail.com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

FIND_PROGRAM(ASTYLE_BIN
	astyle
	/bin
	/usr/bin
	/usr/local/bin
	/sbin
)
if(ASTYLE_BIN)
	ADD_CUSTOM_TARGET(
		gen_astyle 
		COMMAND ${ASTYLE_BIN} --style=kr -vR *.h *.cpp *.c
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		)
endif(ASTYLE_BIN)

