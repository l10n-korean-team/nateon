# - Find xdg-utils
# command line mime executer of Freedesktop.org
#  XDG_OPEN_EXECUTABLE: xdg-open
#  XDG_OPEN_FOUND: True if xdg-open has been found.
#  XDG_EMAIL_EXECUTABLE: xdg-email
#  XDG_EMAIL_FOUND: True if xdg-email has been found.

FIND_PROGRAM(XDG_OPEN_EXECUTABLE
  xdg-open
  /bin
  /usr/bin
  /usr/local/bin
)

IF (XDG_OPEN_EXECUTABLE)
  SET(XDG_OPEN_FOUND "Yes")
  MESSAGE(STATUS "Found xdg-utils:${XDG_OPEN_EXECUTABLE}")
ELSE (XDG_OPEN_EXECUTABLE)
  SET(XDG_OPEN_FOUND "No")
  MESSAGE(FATAL_ERROR "Not found xdg-utils(xdg-open):${XDG_OPEN_EXECUTABLE}")
ENDIF (XDG_OPEN_EXECUTABLE)


MARK_AS_ADVANCED(
  XDG_OPEN_EXECUTABLE
)

# XDG_OPEN option is deprecated.
# use XDG_OPEN_EXECUTABLE instead.
SET (XDG_OPEN ${XDG_OPEN_EXECUTABLE} )

FIND_PROGRAM(XDG_EMAIL_EXECUTABLE
  xdg-email
  /bin
  /usr/bin
  /usr/local/bin
)

IF (XDG_EMAIL_EXECUTABLE)
  SET(XDG_EMAIL_FOUND "Yes")
  MESSAGE(STATUS "Found xdg-utils:${XDG_EMAIL_EXECUTABLE}")
ELSE (XDG_EMAIL_EXECUTABLE)
  SET(XDG_EMAIL_FOUND "No")
  MESSAGE(FATAL_ERROR "Not found xdg-utils(xdg-email):${XDG_EMAIL_EXECUTABLE}")
ENDIF (XDG_EMAIL_EXECUTABLE)


MARK_AS_ADVANCED(
  XDG_EMAIL_EXECUTABLE
)

# XDG_EMAIL option is deprecated.
# use XDG_EMAIL_EXECUTABLE instead.
SET (XDG_EMAIL ${XDG_EMAIL_EXECUTABLE} )
