/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KNATEONMAINVIEW_H
#define KNATEONMAINVIEW_H

#include <kmessagebox.h>
#include <qpushbutton.h>
#include <qimage.h>
#include <kstandarddirs.h>
#include <qlabel.h>
#include <qlistview.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <kdebug.h>
#include <kaction.h>
#include <kactionclasses.h>
#include <kpopupmenu.h>
#include <qscrollview.h>
#include <qcursor.h>
#include <qeuckrcodec.h>

#include "knateonmainviewinterface.h"
#include "chat/chatview.h"
#include "currentaccount.h"
#include "buddy/buddybase.h"
#include "buddy/buddy.h"
#include "buddy/group.h"
#include "buddy/buddylist.h"
#include "buddy/grouplist.h"
#include "contactlist.h"
#include "contactroot.h"
#include "dialog/addbuddyselector.h"
#include "util/emoticon.h"
// #include "lib/shapewidget.h"
#include "dialog/webviewer.h"
#include "util/common.h"

//extern nmconfig stConfig;

class QListView;
class QListViewItem;
class Buddy;
class BuddyList;
class Group;
class GroupList;
class CurrentAccount;
class QLabel;
class AddBuddySelector;
class Emoticon;
class KAction;
class KActionMenu;
class KPopupMenu;
class QScrollView;
class ShapeWidget;
class ShapeButton;
class ChatView;
class WebViewer;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class KNateonMainview : public knateonmainviewinterface {
    Q_OBJECT

public:
    KNateonMainview(QWidget* parent = 0, const char* name = 0, WFlags fl = 0);

    ~KNateonMainview();


    bool initialize();
    // void reloadList(int type=0);
    Buddy* getContactByItem(QListViewItem* item);
    const BuddyList* getBuddyList() {
        return pBuddyList;
    }
    const GroupList* getGroupList() {
        return pGroupList;
    }
    QListViewItem* getCurrentItem() {
        return listView3->currentItem();
    };

	QListViewItem* getGroupItem( const QString &gID ); 
	
    void removeCurrentItem() {
        listView3->removeItem( listView3->currentItem() );
    };
	void removeItem( QListViewItem *item ) {
		listView3->removeItem( item );
	}
    void clearList() {
        listView3->clear();
    };
    void setView1(int nID);
    void setView2(int nID);
    void setViewEmo(bool bView);

    QListViewItem* getGroupItemByName( QString sName );

    int getView1();
    int getView2();
    bool isViewEmo();

    void sortGroupName(bool bOnlyOnline);
    void sortOnOffline();

    KSelectAction*  pCopybuddyAction;       // 친구 복사
    KSelectAction*  pMovebuddyAction;       // 친구 이동
    KAction*        pDeletebuddyAction;     // 친구 삭제slotPBMenu
    KAction*        pBlockbuddyAction;      // 친구 차단/해재
    KAction*        pSendmailAction;        // 메일 보내기
    KAction*        pSendfileAction;        // 파일 보내기
    KAction*        pViewmessageboxAction;  // 지난 대화 보기
    KAction*        pChangenickAction;      // 닉 바꾸기.

    KRadioAction*   pViewNameAction;
    KRadioAction*   pViewNickAction;
    KRadioAction*   pViewNameIDAction;
    KRadioAction*   pViewNameNickAction;
    KRadioAction*   pViewAllAction;
    KRadioAction*   pViewOnlineAction;
    KRadioAction*   pViewOnOffAction;
    KAction*        pViewprofileAction;     // 내 프로필 보기
    KAction*        pEditprofileAction;     // 내 프로필 설정
	
	QTimer *		pTimer;

    QString getBuddyListName(Buddy* pBuddy, bool bEmoticon, unsigned short int nTypeOfBuddyList );
    bool updateBuddy( Buddy* pBuddy );

    bool isMultiSelected();


    /*! 메모 개수 업데이트 */
    void slotUpdateMemoCount(int nMemoCount);

    int getSelectedBuddyCount() {
        return nSelectBuddy;
    }

    /*! 상태 변경 */
    void changeStatusUI( int nID );

    /*! 아이콘 변경 */
    void setMemoIcon( bool isNew );
    void setHompyIcon( bool isNew );
	void setMultiIcon( bool isMulti );

public slots:
    /// 상태변경.Buddy *pBuddy = pBuddyList->getBuddyByHandle( slCommand[1] );
    void slotChangeStatusOnline();
    void slotChangeStatusAway();
    void slotChangeStatusBusy();
    void slotChangeStatusPhone();
    void slotChangeStatusMeeting();
    void slotChangeStatusOffline();

    /// 목록 확장/비확장
    void slotCloseAllGroup();
    void slotOpenAllGroup();

    /*! 친구추가요청을 수락했을때 목록에 사용자를 추가하기윈한 함수 */
    void slotAddBuddy( QListViewItem* pGroup, Buddy* pBuddy );

    /*! 리스팅 - 버디 보이기 방법 */
    void slotListOnlyName();
    void slotListOnlyNick();
    void slotListNameID();
    void slotListNameNick();

    /*! 리스팅 - 정렬 */
    void slotResetBuddyList();			// added by luciferX2@gmail.com 20081103, for search
    void slotSortNormal();
    void slotSortOnlyOnline();
    void slotSortOnOffline();
    void slotEmoticonList( bool bEmoticon );
    void slotGotINFY(const QStringList & slCommand);
    void slotFreeSMS();

    void slotHompyNew();

    void slotSetGroupList(QStringList &Group);

	void slotMultiIconAnimate();
	
	void slotReceivedADDBFL( const QStringList &slCommand );

private:
    void groupOpenClose( QListViewItem * m_pSelectQLVI );
    void selectChanged( QListViewItem * pSelectedItem );
    bool createStatusMenu();
    bool createGroupRightClickMenu();
    bool createBuddyRightClickMenu();
    bool createListingMenu();
    void initIcons();
    QPixmap*        getStatusPixmap(QString sStatus);
    QString         getStatusHTML(QString sStatus);
    QString         sPicsPath;

    QPixmap*        mOnlineQI;
    QPixmap*        mAwayQI;
    QPixmap*        mBusyQI;
    QPixmap*        mPhoneQI;
    QPixmap*        mMeetingQI;
    QPixmap*        mOfflineQI;

    // 메인 상태변경 아이콘
    KAction*        pOnlineAction;          // 온라인
    KAction*        pAwayAction;            // 자리비움
    KAction*        pBusyAction;            // 다른용무중
    KAction*        pOnphoneAction;         // 통화중
    KAction*        pMeetingAction;         // 회의중
    KAction*        pOfflineAction;         // 오프라인으로 표시

    // 버디리스트에서 그룹 오른쪽 버튼
    KAction*        pGroupchatAction;       // 그룹으로 대화하기
    KAction*        pGroupsendmemoAction;   // 그룹으로 쪽지 보내기
    KAction*        pAddgroupAction;        // 그룹 추가
    KAction*        pRenamegroupAction;     // 그룹명 변경
    KAction*        pBlockgroupAction;      // 그룹 차단
    KAction*        pUnblockgroupAction;    // 그룹 차단해제
    KAction*        pDeletegroupAction;     // 그룹삭제
    KAction*        pUnfoldinggroupAction;  // 그룹 전체 열기
    KAction*        pFoldinggroupAction;    // 그룹 전체 닫기

    // 버디리스트에서 버디 오른쪽 버튼
    KAction*        pViewbuddyprofileAction;// 프로필 보기
    KAction*        pChatAction;            // 대화 하기
    KAction*        pSendmemoAction;        // 쪽지 보내기
    KAction*        pViewminihompyAction;   // 미니홈피 보기

    KPopupMenu*     pStatusClickMenu;           // 메인 상태 아이콘 클릭메뉴
    KPopupMenu*     pGroupRightClickMenu;       // 그룹 오른쪽 마우스 클릭메뉴
    KPopupMenu*     pBuddyRightClickMenu;       // 버디 오른쪽 마우스 클릭메뉴

    CurrentAccount* m_pCurrentAccount;
    ChatView*       m_pChatView;
    BuddyList*      pBuddyList;
    GroupList*      pGroupList;
    Emoticon*       myEmoticon;

    /// 리스트 메뉴
    KPopupMenu*     pListringMenu;
    KAction*        pAllGroupOpenAction;
    KAction*        pAllGroupCloseAction;
    KToggleAction*  pViewEmoticonAction;

    /// 웹뷰어
    WebViewer* pWebViewer;

    KConfig *config;

    // Emoticon *pEmoticon;

    int nSelectBuddy;
    int nSelectGroup;

private slots:
    void slotBuddyAdded(Buddy *pBuddy);
    void slotGroupAdded(Group *pGroup);
    void slotViewChat(QListViewItem* m_pSelectQLVI, const QPoint& m_cPointQP, int m_nID);

    /// 대화하기
    void slotGoChat();
    void slotViewMemo();
    void slotViewGroupMemo();
    void slotAddGroup();
    void slotRenameGroup();
    void slotBlockGroup();
    void slotUnblockGroup();
    void slotDeleteGroup();
    void slotAddFriend( const QStringList& slCommand );
    void slotAddBuddySync( const QStringList& slCommand );

    void slotItemExecuted( QListViewItem* item);

    void slotPBMenu(int notUse);
    void slotPBSortList();
    void slotPBMemo();

    void slotGotoMinihompy(QListViewItem* item);
    /*! 미니홈페이지 보이기 */
    void slotViewMinihompy();

    /*! 버디 프로필 보기 */
    void slotViewProfile();
    /*! 버디 리스트 클릭 슬롯 */
    void slotBuddyListClicked( QListViewItem * pSelectedItem );

protected:
#if 0
    void resizeEvent(QResizeEvent *e);
#endif

protected slots:
    virtual void slotRightClickMenu( QListViewItem * item, const QPoint & point, int col );
    void slotChangeNickName(QString sNick);
    void slotMemoAddBuddy(AddBuddySelector *pBuddySelector);
    // void slotListing();
    /*!
      메인창에 있는 친구검색에 글자가 변경되면...
    */
    void slotListSearch( const QString & sKeyword );
    void slotGroupChat();
    void slotSendMail();


signals:
    void sendToDP(const QString &);
    void receFromDP(const QString &);
    /// 목록에서 채팅 이벤트가 발생하면...
    void startChat( Buddy* );
    void startChat( QPtrList<Buddy>& );
    /// 쪽지 보내기.
    void startMemoView(const QString&);
    void addGroup();
    void renameGroup();
    void deleteGroup();
    /// 상태변경
    void statusOnline();
    void statusAway();
    void statusBusy();
    void statusPhone();
    void statusMeeting();
    void statusOffline();

    /// 통합메세지함 보이기.
    void viewMemoBox();
    void changeStatus( const char & );
    void changeStatusNumber( int );
    void startGroupChat( const ContactRoot * );
    void blockGroup( const QString & );
    void unblockGroup( const QString & );
};
#endif
