/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kdebug.h>

#include "toastwindow.h"

/*! ClickLabel ---------------------------------------------------------- */
void ClickLabel::mousePressEvent( QMouseEvent *) {
    grabMouse();
    emit clicked();
}

void ClickLabel::mouseMoveEvent(QMouseEvent *) {
}

void ClickLabel::mouseReleaseEvent(QMouseEvent *) {
    releaseMouse();
}


/*! TostWindow ---------------------------------------------------------- */
ToastWindow::ToastWindow(QWidget *parent, const char *name)
        : ShapeWidget( parent, name, WStyle_StaysOnTop | WStyle_Customize |
                       WStyle_NoBorder | WStyle_Tool | WX11BypassWM ), pLogo(0), pClose(0), pCyLogo(0),
        pText(0), pTimer(0) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    sPicsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" );
    setShape(sPicsPath + "popup_notice_cy_bg.bmp" );

    pLogo = new ShapeWidget( this, sPicsPath + "popup_notice_cy_natelogo.bmp", 16777215 );
    pLogo->move(4,0);

    pClose= new ShapeButton( this, sPicsPath + "popup_notice_cy_close.bmp", 16777215 );
    pClose->move(145, 0);

    pCyLogo= new ShapeWidget( this, sPicsPath + "popup_notice_cy_cylogo.bmp", 16777215 );
    pCyLogo->move(5, 40);
    pCyLogo->setMinimumSize( QSize( 44, 44 ) );
    pCyLogo->setMaximumSize( QSize( 44, 44 ) );

    pText = new ClickLabel( this, "text", 0);
    // pText->move(52, 40);
    pText->setGeometry( QRect( 55, 30, 110, 70 ) );
    // pText->setGeometry( QRect( 55, 40, 120, 50 ) );
    pText->setBackgroundMode( QLabel::PaletteLink );
    pText->setBackgroundOrigin( QLabel::ParentOrigin );
    pText->setCursor( QCursor( 13 ) );
    pText->setLineWidth( 0 );
    pText->setIndent( 0 );
    pText->setText( QString::null );
    pText->setAlignment( int( QLabel::WordBreak | QLabel::AlignTop | QLabel::AlignLeft ) );

    connect(pClose, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(pText, SIGNAL( clicked() ), this, SLOT( slotClicked() ) );
}

ToastWindow::~ToastWindow() {
}

void ToastWindow::setAnchor(const QPoint& anchor) {
    m_anchor = anchor;
    updateMask();
}

void ToastWindow::show() {
    if ( !pTimer ) {
        pTimer = new QTimer(this, "timer");
        connect( pTimer, SIGNAL( timeout() ), this, SLOT( slotHide() ) );
    }

    pTimer->start(5000, TRUE);
    QWidget::show();
    updateMask();
}

void ToastWindow::updateMask() {
#ifdef NETDEBUG
    kdDebug() << "TostWindow Height : " << height() << endl;
#endif
    move(m_anchor.x() - ( width() / 2 ) + 46, m_anchor.y() - height() );
}

void ToastWindow::slotClicked() {
    emit textClicked(  m_sUrl );
#ifdef NETDEBUG
    kdDebug() << "XXX Clicked" << endl;
#endif
}

void ToastWindow::slotHide() {
    hide();
}

void ToastWindow::setText(QString sMsg) {
    pText->setText( sMsg );
    updateMask();
}



/*! PopupWindow --------------------------------------------------------- */
PopupWindow::PopupWindow(bool bMulti, QWidget * parent, const char * name)
        : ShapeWidget( parent, name, WStyle_StaysOnTop | WStyle_Customize | WStyle_NoBorder | WStyle_Tool | WX11BypassWM ), pLogo(0), pClose(0), pCyLogo(0),
        pText(0), pTimer(0) {
    // QWidget::setWFlags( WStyle_StaysOnTop | WStyle_Customize | WStyle_NoBorder | WStyle_Tool | WX11BypassWM );
    KStandardDirs   *dirs   = KGlobal::dirs();
    sPicsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" );

    // setShape(sPicsPath + "popup_notice_cy_bg.bmp");
    pLogo = new ShapeWidget( this, sPicsPath + "popup_notice_title.bmp", 16777215 );
    pLogo->move(0,0);

    pClose= new ShapeButton( this, sPicsPath + "popup_notice_close.bmp", 16777215 );

	if ( bMulti ) {
		setShape( sPicsPath + "popup_notice_bg_large.bmp", 16777215 );
		pClose->move(170, 0);
	}
	else {
		setShape( sPicsPath + "popup_notice_bg.bmp", 16777215 );
		pClose->move(140, 0);
	}

    // pCyLogo= new ShapeWidget( this, 0, 16777215 );
#if 0
    // pCyLogo= new ShapeWidget( this, sPicsPath + "slidewnd_login.bmp", 16777215 );
    pCyLogo->move(5, 40);
    pCyLogo->setMinimumSize( QSize( 44, 44 ) );
    pCyLogo->setMaximumSize( QSize( 44, 44 ) );
#endif

    pText = new ClickLabel( this, "text", Qt::WStyle_NoBorder);
	pText->setFixedHeight( 90 );
    pText->setAlignment( int( QLabel::WordBreak | QLabel::AlignTop | QLabel::AlignLeft ) );
    // pText->setGeometry( QRect( 55, 30, 0, 0 ) );
    // pText->setText("<br><p>aaa<br>");

	// default timeout
	nTimeout = 5000;

    connect(pText, SIGNAL( clicked() ), this, SLOT( slotTextClick() ) );
    connect(pClose, SIGNAL( clicked() ), this, SLOT( close() ) );
}

PopupWindow::~ PopupWindow() {
}

void PopupWindow::updateMask() {
#if 0
    // Added/Updated by Scott Morgan - 2002-10-16
    int screen = kapp->desktop()->screenNumber( m_anchor );
    QRect screenSize = kapp->desktop()->screenGeometry(screen);

    bool bottom = (m_anchor.y() + height()) > (screenSize.height() - 48);
    bool right  = (m_anchor.x() + width())  > (screenSize.width() - 48);
    // End of changes


    move(right ? m_anchor.x() - width() : m_anchor.x() - 48,
         bottom ? m_anchor.y() - height() + 96 : m_anchor.y() + 96 );

    // move(100, 300);
    kdDebug() << "Hight : [" << QString::number( m_anchor.y() ) << "]" << endl;
    kdDebug() << "Width : [" << QString::number( m_anchor.x() ) << "]" << endl;
#endif
    move(m_anchor.x() - ( width() / 2 ), m_anchor.y() - height() );
}

void PopupWindow::setAnchor(const QPoint & anchor) {
    m_anchor = anchor;
    updateMask();
}

void PopupWindow::setTimeout(int timeout) {
	nTimeout = timeout;	
}

void PopupWindow::show() {
    if ( !pTimer ) {
        pTimer = new QTimer(this, "timer");
        connect( pTimer, SIGNAL( timeout() ), this, SLOT( slotHide() ) );
    }
	
	if (nTimeout > 0) {
	    pTimer->start(nTimeout, TRUE);
	}
    QWidget::show();
    // updateMask();
}

void PopupWindow::slotTextClick() {
    QWidget::hide();
    emit clickText( nType, sID );
    // kdDebug() << "XXX Clicked" << endl;
}

void PopupWindow::setText(QString sText) {
    pText->setBackgroundMode( QLabel::PaletteLink );
    pText->setBackgroundOrigin( QLabel::ParentOrigin );
    pText->setCursor( QCursor( 13 ) );
    pText->setLineWidth( 0 );
    pText->setIndent( 0 );
    pText->setTextFormat ( Qt::RichText );
    pText->setText( sText );
    pText->setGeometry( QRect( 55, 30, 110, 70 ) );
}

void PopupWindow::setLogo(const QImage &img, long ln) {
    if ( !pCyLogo )
        pCyLogo= new ShapeWidget( this, img, ln);
    pCyLogo->move(5, 40);
    pCyLogo->setMinimumSize( QSize( 44, 44 ) );
    pCyLogo->setMaximumSize( QSize( 44, 44 ) );
    // pCyLogo->setShape(img, 16777215);
}

void PopupWindow::setLogo(const QString &sImg, long ln) {
#if 1
    if ( !pCyLogo ) {
        pCyLogo= new ShapeWidget( this, sImg, ln);
        // kdDebug() << "UUUUUUUUUUUUUUU" << endl;
    }
#endif
    pCyLogo->move(5, 40);
    pCyLogo->setMinimumSize( QSize( 44, 44 ) );
    pCyLogo->setMaximumSize( QSize( 44, 44 ) );
    pCyLogo->setShape(sImg, 16777215);
    // kdDebug() << sImg << endl;
}

void PopupWindow::slotHide() {
    hide();
    emit hidePopup();
}


#include "toastwindow.moc"
