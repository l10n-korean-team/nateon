/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "loginview.h"
#include <kmdcodec.h>
#include <gcrypt.h>

LoginView::LoginView(QWidget* parent, const char* name, WFlags fl)
        : loginviewinterface(parent,name,fl),
        kLineEdit1(0),
        kHistoryCombo1(0),
        kPasswordEdit1(0),
        kPushButton1(0),
        pAccount(0),
        config(0),
        isEnableCancel( FALSE ) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString         sPicsPath( dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" ) );

    config = kapp->config();
    /*! XXXXX */
    // setMinimumSize( QSize( 200, 400 ) );

    layout26 = new QHBoxLayout( 0, 0, 6, "layout26");
    spacer34_2 = new QSpacerItem( 139, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout26->addItem( spacer34_2 );

    layout25 = new QVBoxLayout( 0, 0, 6, "layout25");
    spacer14 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout25->addItem( spacer14 );

    layout16 = new QHBoxLayout( 0, 0, /*!6*/2, "layout16");
    spacer24_2 = new QSpacerItem( 20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout16->addItem( spacer24_2 );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1->sizePolicy().hasHeightForWidth() ) );
    textLabel1->setMinimumSize( QSize( 60, 0 ) );
    textLabel1->setMaximumSize( QSize( 60, 32767 ) );
    textLabel1->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    layout16->addWidget( textLabel1 );

    kLineEdit1 = new KLineEdit( this, "kLineEdit1" );
    kLineEdit1->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    // kLineEdit1->setMinimumSize( QSize( 90, 0 ) );
    // kLineEdit1->setMaximumSize( QSize( 90, 32767 ) );
    layout16->addWidget( kLineEdit1 );

    textLabel3 = new QLabel( this, "textLabel3" );
    textLabel3->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    textLabel3->setMinimumSize( QSize( 12, 0 ) );
    textLabel3->setMaximumSize( QSize( 12, 32767 ) );

    layout16->addWidget( textLabel3 );

    kHistoryCombo1 = new KHistoryCombo( this, "kHistoryCombo1" );
    kHistoryCombo1->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    kHistoryCombo1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, kHistoryCombo1->sizePolicy().hasHeightForWidth() ) );
    // kHistoryCombo1->setMinimumSize( QSize( 120, 0 ) );
    // kHistoryCombo1->setMaximumSize( QSize( 120, 32767 ) );
    kHistoryCombo1->setBackgroundOrigin( KHistoryCombo::ParentOrigin );
    layout16->addWidget( kHistoryCombo1 );

    spacer24 = new QSpacerItem( 20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout16->addItem( spacer24 );

    layout25->addLayout( layout16 );

    spacer15 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout25->addItem( spacer15 );

    layout15 = new QHBoxLayout( 0, 0, 6, "layout15");
    spacer22_2 = new QSpacerItem( 20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout15->addItem( spacer22_2 );

    textLabel2 = new QLabel( this, "textLabel2" );
    textLabel2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel2->sizePolicy().hasHeightForWidth() ) );
    textLabel2->setMinimumSize( QSize( 60, 0 ) );
    textLabel2->setMaximumSize( QSize( 60, 32767 ) );
    textLabel2->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    layout15->addWidget( textLabel2 );

    kPasswordEdit1 = new MyPasswordEdit( this, "kPasswordEdit1" );
    kPasswordEdit1->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    // kPasswordEdit1->setMinimumSize( QSize( 222, 0 ) );
    // kPasswordEdit1->setMaximumSize( QSize( 222, 32767 ) );

    kPasswordEdit1->setFrameShape( KPasswordEdit::LineEditPanel );
    kPasswordEdit1->setFrameShadow( KPasswordEdit::Sunken );
    kPasswordEdit1->setLineWidth( 2 );
    kPasswordEdit1->setFrame( TRUE );
    kPasswordEdit1->setEchoMode( KPasswordEdit::Password );
    kPasswordEdit1->setCursorPosition( 0 );
    kPasswordEdit1->setAlignment( int( KPasswordEdit::AlignAuto ) );
    layout15->addWidget( kPasswordEdit1 );
    spacer22 = new QSpacerItem( 20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout15->addItem( spacer22 );
    layout25->addLayout( layout15 );
    spacer16 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout25->addItem( spacer16 );

    layout24 = new QHBoxLayout( 0, 0, 6, "layout24");
    spacer13 = new QSpacerItem( 200, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout24->addItem( spacer13 );

    hideLoginCheckBox = new QCheckBox( this, "hideLoginCheckBox" );
    // hideLoginCheckBox->setMinimumSize( QSize( 140, 0 ) );
    // hideLoginCheckBox->setMaximumSize( QSize( 140, 32767 ) );
    hideLoginCheckBox->setCursor(Qt::PointingHandCursor);
    hideLoginCheckBox->setPaletteForegroundColor( QColor( 34, 102, 153 ) );
    QFont hideLoginCheckBox_font(  hideLoginCheckBox->font() );
    hideLoginCheckBox_font.setBold( TRUE );
    hideLoginCheckBox->setFont( hideLoginCheckBox_font );
    layout24->addWidget( hideLoginCheckBox );

    kPushButton1 = new ShapeButton( this, sPicsPath + "rogin_bt_login_nor.bmp" );
    kPushButton1->setMouseOverShape( sPicsPath + "rogin_bt_login_ov.bmp" );
    kPushButton1->setPressedShape( sPicsPath + "rogin_bt_login_down.bmp" );
    // kPushButton1 = new KPushButton( this, "kPushButton1" );
    kPushButton1->setMinimumSize( QSize( 96, 37 ) );
    kPushButton1->setMaximumSize( QSize( 96, 37 ) );
    // kPushButton1->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    layout24->addWidget( kPushButton1 );
    spacer22_3 = new QSpacerItem( 20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout24->addItem( spacer22_3 );
    layout25->addLayout( layout24 );
    spacer17 = new QSpacerItem( 20, 32, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout25->addItem( spacer17 );

    layout19 = new QHBoxLayout( 0, 0, 6, "layout19");
    spacer29 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout19->addItem( spacer29 );

    pixmapLabel1_2 = new QLabel( this, "pixmapLabel1_2" );
    pixmapLabel1_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, pixmapLabel1_2->sizePolicy().hasHeightForWidth() ) );
    pixmapLabel1_2->setPixmap( image3 );
    pixmapLabel1_2->setScaledContents( TRUE );
    layout19->addWidget( pixmapLabel1_2 );
    spacer28 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout19->addItem( spacer28 );
    layout25->addLayout( layout19 );
    spacer19 = new QSpacerItem( 20, 32, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout25->addItem( spacer19 );

    layout20 = new QHBoxLayout( 0, 0, 6, "layout20");
    spacer30 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout20->addItem( spacer30 );

    autoLoginCheckBox = new QCheckBox( this, "autoLoginCheckBox" );
    autoLoginCheckBox->setMinimumSize( QSize( 140, 0 ) );
    autoLoginCheckBox->setMaximumSize( QSize( 140, 32767 ) );
    autoLoginCheckBox->setCursor(Qt::PointingHandCursor);
    // autoLoginCheckBox->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    autoLoginCheckBox->setPaletteForegroundColor( QColor( 34, 102, 153 ) );
    QFont autoLoginCheckBox_font(  autoLoginCheckBox->font() );
    // autoLoginCheckBox_font.setFamily( "DejaVu Sans" );
    autoLoginCheckBox_font.setBold( TRUE );
    autoLoginCheckBox->setFont( autoLoginCheckBox_font );
    layout20->addWidget( autoLoginCheckBox );
    spacer31 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout20->addItem( spacer31 );
    layout25->addLayout( layout20 );
    spacer20 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout25->addItem( spacer20 );

    layout17 = new QHBoxLayout( 0, 0, 6, "layout17");
    spacer26 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout17->addItem( spacer26 );

    findIDPWLabel = new LinkLabel( this, "findIDPWLabel" );
    findIDPWLabel->setMinimumSize( QSize( 130, 0 ) );
    findIDPWLabel->setMaximumSize( QSize( 130, 32767 ) );
    findIDPWLabel->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    findIDPWLabel->setMouseOverColor( QColor( 140, 140, 140) );
    findIDPWLabel->setMouseLeaveColor( QColor( 255, 255, 255 ) );
    layout17->addWidget( findIDPWLabel );

    regLabel = new LinkLabel( this, "regLabel" );
    regLabel->setMinimumSize( QSize( 130, 0 ) );
    regLabel->setMaximumSize( QSize( 130, 32767 ) );
    regLabel->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    regLabel->setMouseOverColor( QColor( 140, 140, 140) );
    regLabel->setMouseLeaveColor( QColor( 255, 255, 255 ) );
    layout17->addWidget( regLabel );

    spacer27 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout17->addItem( spacer27 );
    layout25->addLayout( layout17 );
    spacer21 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout25->addItem( spacer21 );

    layout26->addLayout( layout25 );
    spacer34 = new QSpacerItem( 138, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout26->addItem( spacer34 );

    loginviewinterfaceLayout->addLayout( layout26 );
    /*! XXXXX */
    // signals and slots connections
    connect( kPushButton1, SIGNAL( clicked() ), SLOT( slotStartLogin() ) );
    connect( kPasswordEdit1, SIGNAL( pressReturn() ), SLOT( slotStartLogin() ) );

    // tab order
    setTabOrder( kLineEdit1, kHistoryCombo1 );
    setTabOrder( kHistoryCombo1, kPasswordEdit1 );
    setTabOrder( kPasswordEdit1, hideLoginCheckBox );
    setTabOrder( hideLoginCheckBox, autoLoginCheckBox );
    setTabOrder( autoLoginCheckBox, kPushButton1 );
    /*! XXXXX */

    textLabel1->setText( UTF8( "\x3c\x62\x3e\xec\x95\x84\xec\x9d\xb4\xeb\x94\x94\x3c\x2f\x62\x3e" ) );
    kLineEdit1->setText( QString::null );
    textLabel3->setText( UTF8( "<b>@</b>" ) );
    textLabel2->setText( UTF8( "\x3c\x62\x3e\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88\xed\x98\xb8\x3c\x2f\x62\x3e" ) );
    kPasswordEdit1->setText( QString::null );
    autoLoginCheckBox->setText( UTF8( "\xec\x9e\x90\xeb\x8f\x99\xec\x9c\xbc\xeb\xa1\x9c\x20\xeb\xa1\x9c\xea\xb7\xb8\xec\x9d"
                                      "\xb8\xed\x95\x98\xea\xb8\xb0" ) );
    autoLoginCheckBox->setAccel( QKeySequence( QString::null ) );

    hideLoginCheckBox->setText( UTF8( "남몰래 들어가기" ) );
    hideLoginCheckBox->setAccel( QKeySequence( QString::null ) );

    findIDPWLabel->setText( UTF8( "\x3c\x75\x3e\xec\x95\x84\xec\x9d\xb4\xeb\x94\x94\x2f\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2"
                                  "\x88\xed\x98\xb8\x20\xec\xb0\xbe\xea\xb8\xb0\x3c\x2f\x75\x3e" ) );
    regLabel->setText( UTF8( "\x3c\x75\x3e\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xec\x98\xa8\x20\xeb\xac\xb4\xeb\xa3"
                             "\x8c\xed\x9a\x8c\xec\x9b\x90\x20\xea\xb0\x80\xec\x9e\x85\x3c\x2f\x75\x3e" ) );
    /*! XXXXX */


    checkLoginButton();
    //	m_pDplcon = NULL;
    connect(kPasswordEdit1, SIGNAL( textChanged( const QString & ) ), this, SLOT( slotLoginButtonStatusPasswd( const QString & ) ) );
    connect(kLineEdit1, SIGNAL( textChanged( const QString & ) ), this, SLOT( slotLoginButtonStatusLogin( const QString & ) ) );
    connect(kHistoryCombo1, SIGNAL( textChanged( const QString & ) ), this, SLOT( slotLoginButtonStatusLogin( const QString & ) ) );

    connect(findIDPWLabel, SIGNAL( clicked() ), this, SLOT( slotRunFindPasswordWeb() ) );
    connect(regLabel, SIGNAL( clicked() ), this, SLOT( slotRunRegWeb() ) );
}


LoginView::~LoginView() {
    //	if (m_pDplcon) delete m_pDplcon;
}


/*$SPECIALIZATION$*/

/*!
  \fn LoginView::initialize
*/
void LoginView::initialize() {
    // frame3->setStaticBackground(true);
    // this->setStaticBackground(true);
    //	if (!m_pDplcon) m_pDplcon = new NateonDPLConnection;
    // kHistoryCombo1->setCurrentText("nate.com");

    QStringList myList;
    myList.append("nate.com");
    myList.append("empas.com");
    myList.append("lycos.co.kr");
    myList.append("netsgo.com");
    myList.append("cyworld.com");
    myList.append("empal.com");
    myList.append("hanmail.net");
    myList.append("naver.com");
    myList.append("yahoo.co.kr");
    myList.append("hotmail.com");
    myList.append("dreamwiz.com");
    myList.append( QString::fromUtf8("직접입력") );
    kHistoryCombo1->setHistoryItems( myList );
    kHistoryCombo1->setCurrentItem ( 0 );

    connect( kHistoryCombo1, SIGNAL( activated(int) ), this, SLOT( slotSelectCombo(int) ) );
}


void LoginView::connectToDPLserver() {
    /*! "로그인취소" 버튼을 활성화 */
    setCancel( TRUE ); // setEnable( false );

    kPasswordEdit1->setFocus();

    pAccount = new Account();
    if ( pAccount ) {
        QString sID = kLineEdit1->text() + "@" + kHistoryCombo1->currentText();
        pAccount->setLoginInformation( sID, QString( kPasswordEdit1->text() ));
        emit changeLoginID( sID );
    }

    config->setGroup("Login");
    config->writeEntry( "ID", getID() );
    config->writeEntry( "Domain", getDomain() );

    // encryption
    gcry_cipher_hd_t hd;
    gcry_error_t err;

    /*! TODO: exception */
    err = gcry_cipher_open(&hd, GCRY_CIPHER_ARCFOUR, GCRY_CIPHER_MODE_STREAM, 0);
    err = gcry_cipher_setkey(hd, KNATEON_SALT_KEY, strlen(KNATEON_SALT_KEY));
    char ciphertext[128];
    memset(ciphertext, 0, sizeof(ciphertext));
    err = gcry_cipher_encrypt(hd, ciphertext, sizeof(ciphertext), getPasswd().ascii(), sizeof(ciphertext));
    gcry_cipher_close(hd);

    QString encPasswd = KCodecs::base64Encode( ciphertext );
    config->writeEntry( "Password", encPasswd);

    config->writeEntry( "AutoLogin", isAutoLogin() );
    config->writeEntry( "HidenLogin", hideLoginCheckBox->isChecked() );
    config->sync();

    emit connectWithAccount( pAccount );
}

/*
  const QString LoginView::getID() const
  {
  return pAccount->getID();
  }
*/

void LoginView::slotSelectCombo(int nNum) {
    if ( (unsigned int)nNum == kHistoryCombo1->historyItems().size() - 1 ) {
        kHistoryCombo1->clearEdit();
    }
}

void LoginView::checkLoginButton() {
    if ( ( kLineEdit1->text().length() > 0 ) && (kHistoryCombo1->currentText().length() > 0) ) {
        if ( kPasswordEdit1->text().length() > 0 ) {
            kPushButton1->setEnabled();
            kPushButton1->setCursor(Qt::PointingHandCursor);
        } else {
            kPushButton1->setDisabled();
            kPushButton1->setCursor(Qt::ArrowCursor);
        }
        kPasswordEdit1->setEnabled( true );
        autoLoginCheckBox->setEnabled( true );
    } else {
        kPasswordEdit1->setEnabled( false );
        autoLoginCheckBox->setEnabled( false );
        kPushButton1->setDisabled();
        kPushButton1->setCursor(Qt::ArrowCursor);
    }
}

void LoginView::slotLoginButtonStatusPasswd(const QString & sPasswd) {
    if ( sPasswd.length() > 0 ) {
        kPushButton1->setEnabled();
        kPushButton1->setCursor(Qt::PointingHandCursor);
    } else {
        kPushButton1->setDisabled();
        kPushButton1->setCursor(Qt::ArrowCursor);
    }
}

void LoginView::slotLoginButtonStatusLogin(const QString & sLogin) {
    Q_UNUSED( sLogin );

    if ( ( kLineEdit1->text().length() > 0 ) && (kHistoryCombo1->currentText().length() > 0) ) {
        if ( kPasswordEdit1->text().length() > 0 ) {
            kPushButton1->setEnabled();
            kPushButton1->setCursor(Qt::PointingHandCursor);
        } else {
            kPushButton1->setDisabled();
            kPushButton1->setCursor(Qt::ArrowCursor);
        }
        kPasswordEdit1->setEnabled( true );
        // kHistoryCombo1->setEnabled( true );
        autoLoginCheckBox->setEnabled( true );
        // kPasswordEdit1->setFocus ();
    } else {
        kPasswordEdit1->setEnabled( false );
        // kHistoryCombo1->setEnabled( false );
        autoLoginCheckBox->setEnabled( false );
        kPushButton1->setDisabled();
        kPushButton1->setCursor(Qt::ArrowCursor);
    }
}

void LoginView::slotRunFindPasswordWeb() {
    QString sURL("http://br.nate.com/index.php");
    sURL += "?code=A018";
    LNMUtils::openURL( sURL );
}

void LoginView::slotRunRegWeb() {
    QString sURL("http://br.nate.com/index.php");
    sURL += "?code=A019";
    LNMUtils::openURL( sURL );
}

/*
void LoginView::setEnable(bool bEnable)
{
	if ( bEnable ) {
		kLineEdit1->setEnabled(true);
		kPasswordEdit1->setEnabled(true);
		kHistoryCombo1->setEnabled(true);
		kPushButton1->setEnabled();
	} else {
		kLineEdit1->setEnabled(false);
		kPasswordEdit1->setEnabled(false);
		kHistoryCombo1->setEnabled(false);
		kPushButton1->setDisabled();
	}
}
*/

void LoginView::setCancel(bool bEnable) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString         sPicsPath( dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" ) );

    if ( bEnable ) {
        isEnableCancel = TRUE;
        kPushButton1->setShape( sPicsPath + "rogin_bt_login_cancel_nor.bmp" );
        kPushButton1->setMouseOverShape( sPicsPath + "rogin_bt_login_cancel_ov.bmp" );
        kPushButton1->setPressedShape( sPicsPath + "rogin_bt_login_cancel_down.bmp" );
    } else {
        isEnableCancel = FALSE;
        kPushButton1->setShape( sPicsPath + "rogin_bt_login_nor.bmp" );
        kPushButton1->setMouseOverShape( sPicsPath + "rogin_bt_login_ov.bmp" );
        kPushButton1->setPressedShape( sPicsPath + "rogin_bt_login_down.bmp" );
    }
}

void LoginView::slotStartLogin() {
    if ( isEnableCancel ) {
        emit disconnectFromServer();
    } else {
        setCancel( TRUE );
        connectToDPLserver();
    }
}

void LoginView::show() {
    setCancel( FALSE ); // setEnable( true );
    loginviewinterface::show();
}

void LoginView::emptyPasswordShow() {
    this->show();
    kPasswordEdit1->clear();
    kPasswordEdit1->setFocus();
}

void LoginView::emptyAllShow() {
    this->show();
    kLineEdit1->clear();
    kLineEdit1->setFocus();
    kHistoryCombo1->setCurrentItem( 0 );
    kPasswordEdit1->clear();
}

#include "loginview.moc"
