/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "account.h"
#include <qstring.h>

Account::Account():
        m_proxyType(TYPE_NONE) {
    m_sAuthTicket = QString::null;
    m_sOTP = QString::null;
}


Account::~Account() {
}


bool Account::getUseProxy() const {
    return m_bUseProxy;
}


// Return the proxy password
const QString& Account::getProxyPassword() const {
    return  m_sProxyPassword;
}


// Return the proxy port
const int& Account::getProxyPort()  const {
    return  m_nProxyPort;
}


// Return the proxy server IP
const QString& Account::getProxyServer() const {
    return  m_sProxyServer;
}


// Return the proxy server type
Account::PROXYTYPE Account::getProxyType() const { // const int& Account::getProxyType() const
    return m_proxyType; // return  (int)m_proxyType;
}


// Return the user's proxy user ID
const QString& Account::getProxyUID() const {
    return  m_sProxyUID;
}

const QString Account::getOTP() const {
    return m_sOTP;
}
/*!
  \fn Account::setLoginInformation(QString sID, QString sPassword)
*/
void Account::setLoginInformation(QString sID, QString sPassword) {
    m_sID = sID;
    m_sPassword = sPassword;
}

void Account::setOTP( QString sOTP ) {
    m_sOTP = sOTP;
}


/*!
  \fn Account::copyAccount(Account *pAccount)
*/
void Account::copyAccount(const Account *pAccount) {
    QString sID = pAccount->getID();
    QString sPass = pAccount->getPassword();

    setLoginInformation( sID, sPass );

    QString sDPipt = pAccount->getDPip();
    int nDPport = pAccount->getDPport();

    setDPInformation( sDPipt, nDPport );
}


/*!
  \fn Account::getID()
*/
const QString Account::getID() const {
    return m_sID;
}


/*!
  \fn Account::getPassword()
*/
const QString& Account::getPassword() const {
    return m_sPassword;
}


/*!
  \fn Account::getDPip() const
*/
const QString Account::getDPip() const {
    return m_sDPip;
}


/*!
  \fn Account::getDPport() const
*/
const int Account::getDPport() const {
    return m_nDPport;
}

const QString Account::getAuthTicket() const {
    return m_sAuthTicket;
}

/*!
  \fn Account::setDPInformation(QString sIP, int nPort)
*/
void Account::setDPInformation(QString sIP, int nPort) {
    m_sDPip = sIP;
    m_nDPport = nPort;
}


#include "account.moc"


void Account::setIdleTime(int idleTime) {
    idleTime_ = idleTime;
    emit changedTimerSettings();
}
