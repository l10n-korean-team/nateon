/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LOGINVIEW_H
#define LOGINVIEW_H

#include <kapplication.h>
#include <kconfig.h>
#include <krun.h>
#include <kmessagebox.h>
#include <klineedit.h>
#include <kcombobox.h>
#include <kpassdlg.h>
#include <kdebug.h>
#include <kpushbutton.h>
#include <qcheckbox.h>
#include <qsound.h>
// #include <arts.h>

#include "loginviewinterface.h"
#include "account.h"
#include "util/common.h"
#include "util/sound.h"

#include "utils.h"

#if 0
using namespace std;
using namespace Arts;
#endif
//extern nmconfig stConfig;

class Account;
class QCheckBox;

class LoginView : public loginviewinterface {
    Q_OBJECT
public:
    LoginView(QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~LoginView();

    void initialize();

    QString getID() {
        return kLineEdit1->text();
    }
    QString getDomain() {
        return kHistoryCombo1->currentText();
    }
    QString getPasswd() {
        return kPasswordEdit1->text();
    }
    bool isAutoLogin() {
        return autoLoginCheckBox->isChecked();
    }

    void setID(QString sID) {
        kLineEdit1->setText(sID);
    }
    void setDomain(QString sDomain) {
        kHistoryCombo1->setCurrentText(sDomain);
    }
    void setPasswd(QString sPasswd) { /* kPasswordEdit1->setText(sPasswd); */
        kPasswordEdit1->insert(sPasswd);
    }
    void setAutoLogin(bool bBool) {
        autoLoginCheckBox->setChecked(bBool);
    }
    void checkLoginButton();
    virtual void connectToDPLserver();
    // void setEnable( bool bEnable );
    void setCancel( bool bEnable );
    void show();
    void emptyPasswordShow();
    void emptyAllShow();

    /*! XXXX */
    QLabel* textLabel1;
    KLineEdit* kLineEdit1;
    QLabel* textLabel3;
    KHistoryCombo* kHistoryCombo1;
    QLabel* textLabel2;
    MyPasswordEdit* kPasswordEdit1;
    ShapeButton* kPushButton1;
    QLabel* pixmapLabel1_2;
    QCheckBox* autoLoginCheckBox;
    QCheckBox* hideLoginCheckBox;
    LinkLabel* findIDPWLabel;
    LinkLabel* regLabel;
    /*! XXXX */

protected:
    QHBoxLayout* layout26;
    QSpacerItem* spacer34_2;
    QSpacerItem* spacer34;
    QVBoxLayout* layout25;
    QSpacerItem* spacer14;
    QSpacerItem* spacer15;
    QSpacerItem* spacer16;
    QSpacerItem* spacer17;
    QSpacerItem* spacer19;
    QSpacerItem* spacer20;
    QSpacerItem* spacer21;
    QHBoxLayout* layout16;
    QSpacerItem* spacer24_2;
    QSpacerItem* spacer24;
    QHBoxLayout* layout15;
    QSpacerItem* spacer22_2;
    QSpacerItem* spacer22;
    QHBoxLayout* layout24;
    QSpacerItem* spacer13;
    QSpacerItem* spacer22_3;
    QHBoxLayout* layout19;
    QSpacerItem* spacer29;
    QSpacerItem* spacer28;
    QHBoxLayout* layout20;
    QSpacerItem* spacer30;
    QSpacerItem* spacer31;
    QHBoxLayout* layout17;
    QSpacerItem* spacer26;
    QSpacerItem* spacer27;

private slots:
    void slotSelectCombo(int nNum);
    void slotLoginButtonStatusPasswd( const QString & sPasswd);
    void slotLoginButtonStatusLogin( const QString & sLogin );
    void slotRunFindPasswordWeb();
    void slotRunRegWeb();
    void slotStartLogin();

private:
    Account *pAccount;
    KConfig *config;
    bool isEnableCancel;

signals:
    // Connect with the given account
    void connectWithAccount(Account *pAccount);
    void changeLoginID( const QString & );
    void disconnectFromServer();
};
#endif
