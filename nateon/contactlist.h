/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CONTENTLIST_H
#define CONTENTLIST_H

#include <qstylesheet.h>
#include <qlistview.h>
#include <qcolor.h>
#include <qmime.h>
#include <qpainter.h>
#include <qregexp.h>
#include <qsimplerichtext.h>
#include <kdebug.h>
#include <qpushbutton.h>
#include <kaboutdata.h>

#include "contactbase.h"
#include "contactroot.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
// class ContactList : public KMessListViewItem
class ContactList : public ContactBase {
public:

    ContactList( QListView *parent = 0, const QString &s1 = 0  );
    ContactList( QListViewItem *parent = 0, const QString &s1 = 0 );
    ~ContactList();

    void paintCell( QPainter * painter, const QColorGroup & colourGroup, int column, int width, int align );
    void setHTMLText( const QString &text );

    void setListParent(QListView* p) {
        listParent_ = p;
    }
    QSimpleRichText* richText() {
        return richText_;
    }

    void setHasHompy(bool f);
    bool hasHompy() {
        return hasHompy_;
    }
    QListViewItem *getGroupItem() {
        return pContactRoot;
    }

protected:
    void resortParent();

private:
    void recreateRichText( const QString &text );
    int width( const QFontMetrics & fm, const QListView * lv, int c ) const;

    QSimpleRichText *richText_;
    QListView *listParent_;
    bool hasHompy_;
    QListViewItem *pContactRoot;
};
#endif
