/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "buddylist.h"

BuddyList::BuddyList() : QPtrList<Buddy>() {
}

BuddyList::~BuddyList() {
}

Buddy * BuddyList::getBuddyByHandle( const QString sHandle ) const {
    QPtrListIterator<Buddy> iterator(*this);
    Buddy *pBuddy;

    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        if (pBuddy->getHandle() == sHandle) {
            return pBuddy;
        }
        ++iterator;
    }
    return 0;
}


QString BuddyList::getBuddyNickByHandle( const QString sHandle ) const {
    Buddy *pBuddy;
    pBuddy = getBuddyByHandle(sHandle);

    return pBuddy->getNick();
}

Buddy* BuddyList::addBuddy(Buddy* pBuddy) {
    append(pBuddy);

    return pBuddy;
}

Buddy* BuddyList::getBuddyByID(QString sID) const {
    QPtrListIterator<Buddy> iterator(*this);
    Buddy *pBuddy;

	QString	sOnlyID = sID.left( sID.find( "|" ) );
    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        if (pBuddy->getUID() == sOnlyID) {
            return pBuddy;
        }
        ++iterator;
    }

    return 0;
}

QString BuddyList::getBuddyIDByHandle(const QString sHandle) const {
    Buddy *pBuddy;
    pBuddy = getBuddyByHandle(sHandle);

    return pBuddy->getUID();
}

Buddy * BuddyList::getBuddyByCyworldCMN(QString sCMN) const {
    QPtrListIterator<Buddy> iterator(*this);
    Buddy *pBuddy;

    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        if ( pBuddy->getCyworld_CMN() == sCMN ) {
            return pBuddy;
        }
        ++iterator;
    }
    return 0;
}

Buddy * BuddyList::getBuddyByHompyMajorCMN(QString sCMN) const {
    QPtrListIterator<Buddy> iterator(*this);
    Buddy *pBuddy;

    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        if ( pBuddy->getMajorCMN() == sCMN ) {
            return pBuddy;
        }
        ++iterator;
    }
    return 0;
}

#include "buddylist.moc"
