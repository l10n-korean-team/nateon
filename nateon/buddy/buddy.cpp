/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdlib.h>
#include "buddy.h"
#include "../util/common.h"

// extern nmconfig stConfig;

Buddy::Buddy(QString sHandle, QString sNick, QString sGID) {
    m_sHandle = sHandle;
    m_sNick = sNick;
    setGID( sGID );
}


Buddy::~Buddy() {
}


#include "buddy.moc"


/*!
  \fn Buddy::setBuddyData(const QStringList& slCommand)
*/
void Buddy::setBuddyData(const QStringList& slCommand) {
    Common sC;
    setBuddyFlag( slCommand[4] );
    m_sUID = slCommand[5];
    m_sHandle = slCommand[6];
    m_sName = slCommand[7];
    sC.removePercents( m_sName );
    if (slCommand[8] == "%00")
        m_sNick = m_sName;
    else {
        m_sNick = slCommand[8];
        sC.removePercents( m_sNick );
    }

    m_sMobile = slCommand[9];
    m_sEmail = slCommand[10];

    int m_nYear = 0;
    int m_nMonth = 0;
    int m_nDay = 0;

    if (slCommand[11].length() == 8) {
        m_nYear = atoi(slCommand[11].left(4));
        m_nMonth = atoi(slCommand[11].mid(4,2));
        m_nDay = atoi(slCommand[11].right(2));
        m_Birthday.setYMD(m_nYear, m_nMonth, m_nDay);
    }

    m_sCyworld_CMN = slCommand[12];
    m_sMajorCMN = slCommand[12];
    m_eHompyType = Buddy::Cyworld;
    m_sHome2CMN = QString::null;

    m_nBirthday_TP_CD = atoi(slCommand[13]);
    m_bAuth = false; // <<< check!!!
    m_sMIMID = slCommand[15];

    if (slCommand[16].length() == 8) {
        m_nYear = atoi(slCommand[16].left(4));
        m_nMonth = atoi(slCommand[16].mid(4,2));
        m_nDay = atoi(slCommand[16].right(2));
        m_MusicDate.setYMD(m_nYear, m_nMonth, m_nDay);
    }

    if (slCommand[17] == "Y")
        m_bUseTong = true;
    else
        m_bUseTong = false;

    m_bHompyNew = FALSE;

    return;
}
