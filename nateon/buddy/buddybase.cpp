/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "buddybase.h"

BuddyBase::BuddyBase(QObject *parent, const char *name)
        : MyListItem(parent, name), m_cStatus("X"), bFL(TRUE), bAL(TRUE), bBL(FALSE), bRL(TRUE) {
    setType( MyListItem::tBuddy );
}


BuddyBase::~BuddyBase() {
}


bool BuddyBase::isOffline( ) const {
    if (m_bOnline_YN)
        return false;
    else
        return true;
}


bool BuddyBase::isOnline( ) const {
    if (m_bOnline_YN)
        return true;
    else
        return false;
}


void BuddyBase::setBuddyFlag(QString sFlag) {
    if ( sFlag[0] == '1' )
        bFL = TRUE;
    else
        bFL = FALSE;

    if ( sFlag[1] == '1' ) {
        bAL = TRUE;
        bBL = FALSE;
    } else {
        bAL = FALSE;
        bBL = TRUE;
    }

    if ( sFlag[3] == '1' )
        bRL = TRUE;
    else
        bRL = FALSE;
}

#include "buddybase.moc"


