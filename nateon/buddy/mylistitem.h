/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MYLISTITEM_H
#define MYLISTITEM_H

#include <qobject.h>

/**
	@author Doo-Hyun Jang <ring0320@nate.com>
*/
class MyListItem : public QObject {
    Q_OBJECT
public:
    MyListItem(QObject *parent = 0, const char *name = 0);
    ~MyListItem();

    enum ItemType { tGroup, tBuddy };

    virtual void setGID( const QString &sID ) {
        m_sGID = sID;
    }
    virtual void setGName( const QString &sName ) {
        m_sGName = sName;
    }
    virtual const QString getGID() const {
        return m_sGID;
    }
    virtual const QString getGName() const {
        return m_sGName;
    }

    void setType( ItemType eType ) {
        m_Type = eType;
    }

protected:

private:
    QString m_sGID;     /*! 소속된 그룹ID */
    QString m_sGName;    /*! 그룹명 */
    ItemType m_Type;
};

#endif
