/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GROUPLIST_H
#define GROUPLIST_H

#include <qobject.h>
#include <qptrlist.h>

#include "group.h"
#include "buddy.h"

class Group;
class Buddy;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class GroupList : public QPtrList<Group> {
public:
    GroupList();
    ~GroupList();

    // add a group to buddylist
    Group *addGroup(QString sGroupID, QString sGroupName);
    Group *addGroup(Group *pGroup);

    /// 그룹ID로 Group을 찾음.
    Group *getGroupByID(QString sGroupID) const;
    /// 그룹명으로 Group을 찾음.
    Group *getGroupByName(QString sName) const;
    // remove a group
    bool removeGroup(Group *pGroup);
    // 그룹에 버디를 담고있는지 여부
    bool isContain(Buddy* pBuddy);
    // 비교 함수
#if 0
    virtual int compareItems(QPtrCollection::Item item1, QPtrCollection::Item item2) {
        kdDebug() << "sort!" << endl;
        return 0;
    }
#endif
};

#endif
