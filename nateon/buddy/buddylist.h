/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef BUDDYLIST_H
#define BUDDYLIST_H

#include <qobject.h>
#include <qptrlist.h>
#include <kdebug.h>

#include "buddy.h"

class Buddy;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class BuddyList : public QPtrList<Buddy> {
public:
    BuddyList();
    ~BuddyList();

    /// 버디리스트에 추가.
    Buddy* addBuddy(Buddy *pBuddy);
    /// return the buddy with the given handle
    /// CMN번호로 Buddy 객체를 얻음.
    Buddy *getBuddyByHandle(const QString sHandle) const;
    /// Nateon ID(ex, xxx@nate.com)으로 Buddy 객체를 얻음.
    Buddy *getBuddyByID(QString sID) const;
    /// Cyworld CMN 으로 Buddy 객체를 얻음.
    Buddy *getBuddyByCyworldCMN(QString sCMN) const;
    /// Hompy Major CMN 으로 Buddy 객체를 얻음.
    Buddy *getBuddyByHompyMajorCMN(QString sCMN) const;
    /// CMN번로로 nick을 얻음.
    QString getBuddyNickByHandle(const QString sHandle) const;
    /// CMN번호로 Nateon ID를 얻음.
    QString getBuddyIDByHandle(const QString sHandle) const;

    // 비교 함수
#if 0
    virtual int compareItems(QPtrCollection::Item item1, QPtrCollection::Item item2) {
        kdDebug() << "sort!" << endl;
        return 0;
    }
#endif

};
#endif
