/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef BUDDYBASE_H
#define BUDDYBASE_H

#include <qobject.h>
#include <qdatetime.h>
#include <qstringlist.h>
#include "mylistitem.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class BuddyBase : public MyListItem {
    Q_OBJECT
public:
    enum HompyType { Cyworld, Home2 };
    BuddyBase(QObject *parent = 0, const char *name = 0);
    virtual ~BuddyBase();

    // QString getBuddyFlag() const { return m_sBuddyFlag; }
    QString getUID() const {
        return m_sUID;
    }
    QString getHandle() const {
        return m_sHandle;
    }
    QString getName() const {
        return m_sName;
    }
    QString getNick() const {
        return m_sNick;
    }
    QString getMobile() const {
        return m_sMobile;
    }
    QString getEmail() const {
        return m_sEmail;
    }
    QDate   getBirthday() const {
        return m_Birthday;
    }
    QString getCyworld_CMN() const {
        return m_sCyworld_CMN;
    }
    int     getBirthday_TP_CD() const {
        return m_nBirthday_TP_CD;
    }
    bool    isAuth() const {
        return m_bAuth;
    }
    QString getMIMID() const {
        return m_sMIMID;
    }
    QDate   getMusicDate() const {
        return m_MusicDate;
    }
    bool    isTong() const {
        return m_bUseTong;
    }
    bool    isMobileInvoke() {
        return m_bMobileInvoke;
    }
    QString getStatus() {
        return m_cStatus;
    }
    // QString getGID() const { return m_sGID; }
    bool    isOffline() const;
    bool    isOnline() const;
    bool    isQuit() {
        return m_bQuit;
    }
    QString getHome2CMN() {
        return m_sHome2CMN;    /*! Home2 */
    }
    // char getHompyType() { return m_cHompyType; } /*! 주홈피 - C: Cyworld, H: Home2 */
    HompyType getHompyType() {
        return m_eHompyType;    /*! 주홈피 타입 */
    }
    bool isHompyNew() {
        return m_bHompyNew;    /*! 새글이 있는가? */
    }
    bool isFL() {
        return bFL;
    }
    bool isAL() {
        return bAL;
    }
    bool isBL() {
        return bBL;
    }
    bool isRL() {
        return bRL;
    }
    const QString getMajorCMN() const {
        return m_sMajorCMN;    /*! 주사용계정 CMN */
    }

    void    setBuddyFlag( QString sFlag ); /* { m_sBuddyFlag = sFlag; } */
    void 		setFL( bool FL ) {
        bFL = FL;
    }
    void 		setAL( bool AL ) {
        bAL = AL;
        bBL = not AL;
    }
    void 		setBL( bool BL ) {
        bBL = BL;
        bAL = not BL;
    }
    void 		setRL( bool RL ) {
        bRL = RL;
    }
    void    setUID( QString sUID ) {
        m_sUID = sUID;
    }
    void    setHandle( QString sHandle ) {
        m_sHandle = sHandle;
    }
    void    setName( QString sName ) {
        m_sName = sName;
    }
    void    setNick( QString sNick ) {
        m_sNick = sNick;
    }
    void    setMobile( QString sMobile ) {
        m_sMobile = sMobile;
    }
    void    setMail ( QString sEmail ) {
        m_sEmail = sEmail;
    }
    void    setBirthday ( QDate dBirthday ) {
        m_Birthday = dBirthday;
    }
    void    setCyworld_CMN( QString sCyworld_CMN ) {
        m_sCyworld_CMN = sCyworld_CMN;
    }
    void    setBirthday_TP_CD( int nBirthday_TP_CD ) {
        m_nBirthday_TP_CD = nBirthday_TP_CD;
    }
    void    setIsAuth( bool bAuth ) {
        m_bAuth = bAuth;
    }
    void    setMIMID( QString sMIMID ) {
        m_sMIMID = sMIMID;
    }
    void    setMusicDate( QDate dMusicDate ) {
        m_MusicDate = dMusicDate;
    }
    void    setIsTong( bool bUseTong ) {
        m_bUseTong = bUseTong;
    }
    void    setIsMobileInvoke( bool bMobileInvoke ) {
        m_bMobileInvoke = bMobileInvoke;
    }
    void    setStatus( QString sStatus ) {
        m_cStatus = sStatus;
    }
    // void    setGID( QString sGID ) { m_sGID = sGID; }
    void    setQuit( bool bQuit ) {
        m_bQuit = bQuit;
    }
    void    setIsOnline( bool bOnline ) {
        m_bOnline_YN = bOnline;
    }
    void setHome2CMN( QString sHome2CMN ) {
        m_sHome2CMN = sHome2CMN;    /*! Home2 */
    }
    // void setHompyType( char cHompyType ) { m_cHompyType = cHompyType; } /*! 주홈피 - C: Cyworld, H: Home2 */
    void setHompyType( HompyType eType ) {
        m_eHompyType = eType;    /*! 주 홈피 타입 */
    }
    void setHompyNew( bool bHompyNew ) {
        m_bHompyNew = bHompyNew;
    }
    void setMajorCMN( QString sCMN ) {
        m_sMajorCMN = sCMN;
    }

    bool useCyworld() {
        return !( (m_sCyworld_CMN == "%00") || (m_sCyworld_CMN == "") );
    }
    bool useHome2() {
        return !( (m_sHome2CMN == "%00") || (m_sHome2CMN == "") );
    }

protected:
    BuddyBase(QString sHandle, QString sNick);
    // QString m_sBuddyFlag; /*! 메신저 등록 상태 정보 */
    QString m_sUID; /*! 접속ID : xxx@nate.com */
    QString m_sHandle; /*! CMN : 내부적으로 사용하는 유일키, 숫자 2312123 */
    QString m_sName;
    QString m_sNick;
    QString m_sMobile;
    QString m_sEmail;
    QDate m_Birthday;
    QString m_sCyworld_CMN;
    int m_nBirthday_TP_CD;    /*! 양력(1), 음력(2) */
    bool m_bAuth;
    QString m_sMIMID;
    QDate m_MusicDate; /*! 음악 최종 갱신일 */
    bool m_bUseTong; /*! 통연동여부 */
    bool m_bMobileInvoke;     /*! 모바일 인보크 여부. 오프라인일때 클라이언트에서 대화가 가능함. */
    // QString m_sGID;     /*! 소속된 그룹ID */
    bool m_bOnline_YN;
    QString m_cStatus; /*! 상태값 */
    bool m_bQuit; /*! 채팅창을 닫았나? */
    QString m_sHome2CMN; /*! Home2 CMN */
    // char m_cHompyType; /*! 주홈피 - C: Cyworld, H: Home2 */
    HompyType m_eHompyType; /*! 주 홈피 타입 */
    bool m_bHompyNew; /*! 홈피 새글 표시 */
    bool bFL; /*! Friend List : 노출 유무 */
    bool bAL; /*! Allow List : Blocked의 반대, 차단안됨 */
    bool bBL; /*! Blocked List : Allow와 반대, 차단됨 */
    bool bRL; /*! Reverse Friend List : 상대 버디리스트에 본인이 노출여부 1: 리스트에 표시됨, 0: 리스트에 표시 안됨 */
    QString m_sMajorCMN; /*! 주계정 CMN */
};
#endif
