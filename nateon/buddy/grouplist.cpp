/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "grouplist.h"

GroupList::GroupList()
        : QPtrList<Group>() {
    clear();
}


GroupList::~GroupList() {
}

Group* GroupList::addGroup(QString sGroupID, QString sGroupName) {
    Group* pGroup;
    QPtrListIterator<Group> it(*this);
    while (it.current() != 0) {
        pGroup = it.current();
        if (pGroup->getGID() == sGroupID) return 0;
        ++it;
    }

    pGroup = new Group( sGroupID, sGroupName );
    append(pGroup);
    return pGroup;
}

Group* GroupList::getGroupByID(QString sGroupID) const {
    Group* pGroup;
    QPtrListIterator<Group> it(*this);
    while (it.current() != 0) {
        pGroup = it.current();
        if (pGroup->getGID() == sGroupID) return pGroup;
        ++it;
    }
    return 0;
}

bool GroupList::removeGroup(Group *pGroup) {
    return remove(pGroup);
}

Group * GroupList::getGroupByName(QString sName) const {
    Group * pGroup;
    QPtrListIterator<Group> it(*this);
    while (it.current() != 0) {
        pGroup = it.current();
        if ( pGroup->getGName() == sName ) return pGroup;
        ++it;
    }
    return 0;
}

Group * GroupList::addGroup(Group * pGroup) {
    if ( find(pGroup) >= 0 ) return 0;

    append(pGroup);

    return pGroup;
}

bool GroupList::isContain(Buddy* pBuddy) {
    Group* pGroup;
    QPtrListIterator<Group> it(*this);
    while (it.current() != 0) {
        pGroup = it.current();
        if ( pGroup->getBuddyList().contains( pBuddy ) > 0 ) {
            return true;
#ifdef NETDEBUG
            kdDebug() << "GROUP : " << pGroup->getGName() << endl;
#endif
        }
        ++it;
    }
    return false;
}

#include "grouplist.moc"

