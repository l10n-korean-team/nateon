/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GROUP_H
#define GROUP_H

#include <qobject.h>
#include <qstringlist.h>
#include <kdebug.h>
#include "mylistitem.h"
#include "buddy.h"

class Buddy;
/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class Group : public MyListItem {
    Q_OBJECT
public:
    Group(QString id, QString name);
    ~Group();
    const QPtrList<Buddy>& getBuddyList() const {
        return m_plBuddyList;
    }

    void addBuddy( const Buddy* sBuddy );
    bool removeBuddy( const Buddy* sBuddy );

private:
    QPtrList<Buddy> m_plBuddyList;
};
#endif
