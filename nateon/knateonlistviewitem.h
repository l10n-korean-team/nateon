/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KNATEONLISTVIEWITEM_H
#define KNATEONLISTVIEWITEM_H

#include <qlistview.h>

class QSimpleRichText;

/**
   @author Jaeha, Lee <dcom@nate.com>
*/
class KNateonListViewItem : public QListViewItem {
public:
    KNateonListViewItem(QListView *parent);

    KNateonListViewItem(QListViewItem *parent);

    ~KNateonListViewItem();
    /*
      enum ListTypes
      {
      LISTTYPE_BASE     = -1,
      LISTTYPE_GROUP    =  0,
      LISTTYPE_CONTACT  =  1
      };

      // Return the type of the item
      virtual int     getType() const;
      // Paint a cell (column 0, always) - this allows us emoticons, through QSimpleRichText
      void            paintCell( QPainter * painter, const QColorGroup & colourGroup, int column, int width, int align );
      // dummy setText method
      void            setHTMLText(const QString &text);

      protected:
      // Re-sort the parent nodes
      void            resortParent();

      private: // Private methods
      // Recreate the rich text
      void            recreateRichText(const QString &text);
      // returns the width of the text in the column
      int              width ( const QFontMetrics & fm, const QListView * lv, int c ) const;

      private: // Private attributes

      // The rich text painter
      QSimpleRichText     *m_pRichText;
      // The rich text painter used to get the real width
      QSimpleRichText     *m_pWidthText;

      // TODO: Remove this, temporary fix
      QListView           *m_pListParent;
    */
};
#endif
