/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "knateonview.h"
#include "util/common.h"

extern nmconfig stConfig;

bool KNateonInterface::initialize() {
    /*! Status Bar */
    m_pStatusLabel = new QLabel(this);
    if (m_pStatusLabel) statusBar()->addWidget( m_pStatusLabel, 2 );
    /*!
      m_pStatusTimer = new QLabel(this);
      if (m_pStatusTimer)
      {
      m_pStatusTimer->setText("00:00");
      statusBar()->addWidget(m_pStatusTimer, 0, true);
      }

      m_pStatusProgress = new KLed(this);
      if (m_pStatusProgress)
      {
      m_pStatusProgress->setMaximumHeight(16);
      statusBar()->addWidget(m_pStatusProgress, 0, true);
      }
    */
    createActions();
    // Create the menus
    createMenus();
    OfflineEnableMenu();
    /// 오른쪽 마우스 메뉴 만들기.
    // initContactPopup();
    // connect( contactListView_, SIGNAL( contextMenuRequested(QListViewItem*,const QPoint&,int) ), this, SLOT( slotContextMenu(QListViewItem*,const QPoint&,int) ) );
    // load window size & position
    readProperties( kapp->config() );

    KStdAction::close( this, SLOT( slotAppClose() ), actionCollection() );
    KStdAction::quit( this, SLOT( slotAppClose() ), actionCollection() );

    return true;
}


/*!
  \fn knateon::createMenus()
*/
void KNateonInterface::createMenus() {
    createFileMenu();
    createActionsMenu();
    createFriendMenu();
    createSetupMenu();
    createHelpMenu();
}

/*
  {

  KAction *close, *quit;
  KPopupMenu *connectMenu;
  QStringList statuses;

  Create the menu
  connectMenu  = new KPopupMenu( this, "connectmenu" );

  Create the "connect" menu items

  m_pConnectActionMenu    = new KActionMenu( UTF8("login"), "connect_creating", this, "connect");
  m_pDisconnect           = new KAction( UTF8("logout"), "connect_no", 0, this, SLOT( disconnectClicked() ), actionCollection(), "disconnect");
  m_pShowProfile          = new KAction( UTF8("View My Profile"), "kmess", 0, this, SLOT( showUserProfile() ), actionCollection(), "showuserprofile");
  m_pStatus               = new KSelectAction( UTF8("My Status"), "misc", 0, this, "status");
  statuses << UTF8("Online") << UTF8("Away") << UTF8("Busy") << UTF8("On the phone") << UTF8("on meeting") << UTF8("Offline");
  m_pStatus->setItems(statuses);

  connect ( m_pStatus, SIGNAL( activated( const QString& ) ), this, SLOT( changeStatus( const QString& ) ) );
  close = KStdAction::close( this, SLOT( menuClose() ), actionCollection() );
  quit = KStdAction::quit( this, SLOT( menuQuit() ), actionCollection() );

  m_pConnectActionMenu->plug( connectMenu );
  m_pDisconnect->plug( connectMenu );
  connectMenu->insertSeparator( );
  m_pStatus->plug( connectMenu );
  m_pShowProfile->plug( connectMenu );
  connectMenu->insertSeparator();
  close->plug( connectMenu );
  quit->plug( connectMenu );

  // Plug the menu into the menubar
  menuBar()->insertItem( UTF8("file(&F)"), connectMenu);

  m_pDisconnect->plug( toolBar("mainToolBar") );
  m_pStatus->plug( toolBar("mainToolBar") );
  close->plug( toolBar("mainToolBar") );
  quit->plug( toolBar("mainToolBar") );
  toolBar("mainToolBar")->show();
  }
*/

/*!
  \fn knateon::createConnectMenu()
*/
void KNateonInterface::createFileMenu() {
    // file menu
    pLogoutAction           = new KAction(UTF8("로그아웃(&N)"), 0/* 이미지 */, 0/* 단축키 */, this, "logout_menu");
    // pLogoutAction->setIcon("/home/x/devel/knateon/src/pics/kmess/offline.png");
    // pOpenchatboxAction      = new KAction(UTF8("대화함 열기(&H)"), 0, 0, this, SLOT( slotOpenChatBox() ), this, "OpenChatBox");
    pOpenchatboxAction      = new KAction(UTF8("대화함 열기(&H)"), 0, 0, this, "OpenChatBox");
    pOpenfileboxAction      = new KAction(UTF8("받은 파일 폴더 열기(&R)"), 0, 0, this, SLOT( slotOpenFileBox() ), this, "OpenFileBox");

    pOpendownboxAction      = new KAction(UTF8("파일전송창 열기(&T)"), 0, 0, this, /* SLOT( slotOpenTransferBox() ), this,*/ "OpenTransferBox");

    pHideAction             = new KAction(UTF8("숨기기(&C)"), 0, KShortcut( Qt::Key_Escape), this, SLOT( slotMenuHide() ), this, "Hide");
    // pHideAction->setShoutcut(UTF8("Esc"));
    // pQuitAction             = KStdAction::quit( this, SLOT( menuQuit() ), actionCollection() );
    pQuitAction = new KAction(UTF8("종료(&X)"), 0, KShortcut( "Ctrl+Q" ), this, /* SLOT( menuQuit() ), this, */ "Quit");

    KPopupMenu *pFile       = new KPopupMenu(this, "filemenu");
    pLogoutAction->plug(pFile);
    pFile->insertSeparator();
    pOpenchatboxAction->plug(pFile);
    pOpenfileboxAction->plug(pFile);
    pOpendownboxAction->plug(pFile);
    pFile->insertSeparator();
    pHideAction->plug(pFile);
    pQuitAction->plug(pFile);
    // menuBar()->insertItem( UTF8("&FILE"), pFile);
    menuBar()->insertItem( UTF8("파일(&F)"), pFile);
}
/*!

 */
void KNateonInterface::createActionsMenu() {
    // action menu
    pChatAction             = new KAction(UTF8("대화하기(&G)"), 0, KShortcut( Qt::CTRL+Qt::Key_G), this, "GoChat");
    // pChatAction->setShoutcut(UTF8("Ctrl+G"));
    pSendmemoAction         = new KAction(UTF8("쪽지 보내기(&N)"), 0, KShortcut( Qt::CTRL+Qt::Key_B), this, "SendMemo");
    // pSendmemoAction->setShoutcut(UTF8("Ctrl+B"));
    pSendfileAction         = new KAction(UTF8("파일 보내기(&F)"), 0, KShortcut( Qt::CTRL+Qt::Key_T), this, "SendFile");
    // pSendfileAction->setShoutcut(UTF8("Ctrl+T"));

    KPopupMenu *pAction     = new KPopupMenu(this, "actionmenu");
    pChatAction->plug(pAction);
    pAction->insertSeparator();
    pSendmemoAction->plug(pAction);
    pSendfileAction->plug(pAction);
    // menuBar()->insertItem( UTF8("&MOVEMENT"), pAction);
    menuBar()->insertItem( UTF8("동작(&M)"), pAction);
}

/*!

 */
void KNateonInterface::createFriendMenu() {
    /*
    // friend menu
    pAddfriendAction        = new KAction(UTF8("Add a buddy"), "1", 0, this, "add_friend_menu");
    pCopyfriendSelectAction = new KSelectAction( UTF8("Copy a buddy"), "/home/x/devel/knateon/src/pics/kmess/offline.png", 0, this, "copy");
    pMovefriendSelectAction = new KSelectAction( UTF8("Move a buddy"), "misc", 0, this, "move");
    // pMovefriendSelectAction ->setItems(m_Group);
    // pMovefriendSelectAction->setEditable(false);
    pDeletefriendAction     = new KAction(UTF8("Delete a buddy"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");

    pBuddynamingSelectAction= new KSelectAction( UTF8("Buddy's screen name setting"), "friend", 0, this, "naming");
    QStringList m_naming;
    m_naming << UTF8("View buddies by name") << UTF8("View buddies by screen name") << UTF8("View buddies by name and ID") << UTF8("View buddies by name and screen name");
    pBuddynamingSelectAction->setItems(m_naming);
    pBuddynamingSelectAction->setCurrentItem(0);

    pSortlistSelectAction   = new KSelectAction( UTF8("Buddy viewing option"), "friend", 0, this, "sorting");
    QStringList m_sorting;
    m_sorting << UTF8("View &All") << UTF8("View &Connected Only") << UTF8("View On/Off");
    pSortlistSelectAction->setItems(m_sorting);
    pSortlistSelectAction->setCurrentItem(0);

    pAddgroupAction         = new KAction(UTF8("Add Group"), "1", 0, this, "add_group_menu");
    pRenamegroupAction      = new KAction(UTF8("Rename Group"), "1", 0, this, "rename_group_menu");
    pDeletegroupAction      = new KAction(UTF8("Delete Group"), "1", 0, this, "delete_group_menu");
    */
    // friend menu
    pAddfriendAction        = new KAction(UTF8("친구 추가(&A)"), 0, 0, this, "Menu_Buddy_Add");
    pCopyfriendSelectAction = new KSelectAction( UTF8("친구 복사(&C)"), 0, 0, this, "Menu_Buddy_Copy");
    // pCopyfriendSelectAction ->setItems(m_Group);
    // pCopyfriendSelectAction->setEditable(false);

    pMovefriendSelectAction = new KSelectAction( UTF8("친구 이동(&O)"), 0, 0, this, "Menu_Buddy_Move");
    // pMovefriendSelectAction ->setItems(m_Group);
    // pMovefriendSelectAction->setEditable(false);

    pDeletefriendAction     = new KAction(UTF8("친구 삭제(&D)"), 0, KShortcut( Qt::Key_Delete), this, "Menu_Buddy_Delete");

    pLoadBuddyListAction = new KAction(UTF8("친구 목록 불러오기(&L)"), 0, 0, this, "Menu_Load_BuddyList");
    pSaveBuddyListAction = new KAction(UTF8("친구 목록 저장하기(&P)"), 0, 0, this, "Menu_Save_BuddyList");

    pBuddynamingSelectAction= new KSelectAction( UTF8("친구 보기 방식(&U)"), 0, 0, this, "Menu_Naming");
    QStringList m_naming;
    m_naming << UTF8("친구 이름으로 보기(&R)") << UTF8("친구 대화명으로 보가(&N)") << UTF8("친구 이름+아이디로 보기(&M)") << UTF8("친구 이름+대화명으로 보기(&E)");
    pBuddynamingSelectAction->setItems(m_naming);
    pBuddynamingSelectAction->setCurrentItem(3);

    pSortlistSelectAction   = new KSelectAction( UTF8("친구 정렬 방식(&Y)"), 0, 0, this, "Menu_Sorting");
    QStringList m_sorting;
    m_sorting << UTF8("친구 전체 보기(&A)") << UTF8("접속한 친구만 보기(&O)") << UTF8("친구 온라인/오프라인으로 보기(&D)");
    pSortlistSelectAction->setItems(m_sorting);
    pSortlistSelectAction->setCurrentItem(0);

    pAddgroupAction         = new KAction(UTF8("그룹 추가(&N)"), 0, 0, this, "add_group_menu");
    pRenamegroupAction      = new KAction(UTF8("그룹 이름 변경(&N)"), 0, 0, this, "rename_group_menu");
    pDeletegroupAction      = new KAction(UTF8("그룹 삭제(&D)"), 0, 0, this, "delete_group_menu");

    KPopupMenu *pFriend     = new KPopupMenu(this, "friendmenu");
    pAddfriendAction->plug(pFriend);
    pCopyfriendSelectAction->plug(pFriend);
    pMovefriendSelectAction->plug(pFriend);
    pDeletefriendAction->plug(pFriend);
    pFriend->insertSeparator();
    pLoadBuddyListAction->plug(pFriend);
    pSaveBuddyListAction->plug(pFriend);
    pFriend->insertSeparator();
    pBuddynamingSelectAction->plug(pFriend);
    pSortlistSelectAction->plug(pFriend);
    pFriend->insertSeparator();
    pAddgroupAction->plug(pFriend);
    pRenamegroupAction->plug(pFriend);
    pDeletegroupAction->plug(pFriend);
    // menuBar()->insertItem( UTF8("BUDDY"), pFriend);
    menuBar()->insertItem( UTF8("친구(&R)"), pFriend);
}



void KNateonInterface::createSetupMenu() {
    QStringList statuses;
    // setup menu
    pChangestatusSelectAction = new KSelectAction( UTF8("내 상태 설정(&U)"), 0, 0, this, "status");
    statuses << UTF8("온라인") << UTF8("자리비움") << UTF8("다른 용무중") << UTF8("통화 중") << UTF8("회의 중") << UTF8("오프라인 표시");
    pChangestatusSelectAction->setItems(statuses);
    pChangestatusSelectAction->setCurrentItem(0);
    connect ( pChangestatusSelectAction, SIGNAL( activated( int ) ), this, SLOT( slotChangeStatusNumber( int ) ) );

    pChangenickAction       = new KAction(UTF8("내 대화명 설정(&P)"), 0, 0, this, "changeNick");
    pAlwaystopAction        = new   KToggleAction(UTF8("항상 위(&N)"), 0, 0, this, "alwaysTop");
    pSetupAction            = new KAction(UTF8("환경 설정(&S)"), 0, 0, this, "setup_menu");
    pNetLogAction            = new KAction(UTF8("프로토콜 로그 보기(&L)"), 0, 0, this, "netlog_menu");

    KPopupMenu *pSetup      = new KPopupMenu(this, "setupmenu");
    pChangestatusSelectAction-> plug(pSetup);
    pChangenickAction->plug(pSetup);
    pSetup->insertSeparator();
    pAlwaystopAction->plug(pSetup);
    pSetupAction->plug(pSetup);
    pSetup->insertSeparator();
    pNetLogAction->plug( pSetup );
    menuBar()->insertItem( UTF8("설정(&S)"), pSetup);

    bool bNetLog = false;
#ifdef DEBUG
    bNetLog = true;
#endif
    pNetLogAction->setEnabled(bNetLog);
}

/*!

 */
void KNateonInterface::createHelpMenu() {
    /*
    // help menu
    pGonateonhomeAction     = new KAction(UTF8("NateOn homepage"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGocyworldhomeAction    = new KAction(UTF8("Cyworld homepage"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGonatedotcomAction     = new KAction(UTF8("Nate.com"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGonateonhelpAction     = new KAction(UTF8("NateOn user guide"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGonateonminihompyAction= new KAction(UTF8("NateOn mini homepage"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGonateonhottipAction   = new KAction(UTF8("NateOn Hot Tip"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGonateonfaqAction      = new KAction(UTF8("NateOn FAQ"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    pGonateoninfo           = new KAction(UTF8("NateOn infomation"), "1", 0, this, SLOT( empty() ), actionCollection(), "11");
    */
    // help menu
    pGonateonhomeAction     = new KAction(UTF8("네이트온 홈(&O)"), 0, 0, this, SLOT( slotGoNateHome() ), this, "GoNateHome");
    pGocyworldhomeAction    = new KAction(UTF8("싸이월드 홈(&C)"), 0, 0, this, SLOT( slotGoCyHome() ), this, "CyworldHome");
    pGonatedotcomAction     = new KAction(UTF8("네이트 홈(&N)"), 0, 0, this, SLOT( slotGoNateDotCom() ), this, "NateDotCom");
    pGonateonhelpAction     = new KAction(UTF8("네이트온 이용 가이드(&G)"), 0, KShortcut( Qt::Key_F1), this, SLOT( slotNateonUsage() ), this, "NateonUsage");
    // pGonateonhelpAction->setShoutcut(UTF8("F1"));
    pGonateonminihompyAction= new KAction(UTF8("네이트온 미니 홈피(&M)"), 0, 0, this, SLOT( slotGoNateonMiniHompy() ), this, "NateonMiniHompy");
    pGonateonhottipAction   = new KAction(UTF8("네이트온 HotTip(&I)"), 0, 0, this, SLOT( slotGoHotTip() ), this, "HotTip");
    pGonateonfaqAction      = new KAction(UTF8("네이트온 FAQ(&F)"), 0, 0, this, SLOT( slotFAQ() ), this, "FAQ");
    // pGonateoninfo           = new KAction(UTF8("네이트온 정보(&A)"), 0, 0, this, SLOT( slotInfo() ), this, "Info");
    // if ( !helpMenu_ )
    helpMenu_ = new KHelpMenu(this, KGlobal::instance()->aboutData(), false, actionCollection());
    // helpMenu_= new KHelpMenu(this, KGlobal::instance()->aboutData());
    pGonateoninfo = KStdAction::aboutApp(helpMenu_, SLOT( aboutApplication()/*slotInfo()*/ ), actionCollection() );

    KPopupMenu *pHelp       = new KPopupMenu(this, "helpmenu");
    pGonateonhomeAction->plug(pHelp);
    pGocyworldhomeAction->plug(pHelp);
    pGonatedotcomAction->plug(pHelp);
    pHelp->insertSeparator();
    pGonateonhelpAction->plug(pHelp);
    pGonateonminihompyAction->plug(pHelp);
    pGonateonhottipAction->plug(pHelp);
    pGonateonfaqAction->plug(pHelp);
    pHelp->insertSeparator();
    pGonateoninfo->plug(pHelp);
    // menuBar()->insertItem( UTF8("HELP"), pHelp);
    menuBar()->insertItem( UTF8("도움말(&H)"), pHelp);
}


/*!
  \fn knateon::readProperty(KConfig *config)
*/
void KNateonInterface::readProperties(KConfig *config) {
    // Pull in the window size and position
    config->setGroup("General");
    QSize windowsize = config->readSizeEntry("Size");
    if (!windowsize.isEmpty()) {
        resize(windowsize);
    }
    QPoint position = config->readPointEntry("Position");
    if (!position.isNull()) {
        move(position);
    }

    /*
    // bar status settings
    bool bViewToolbar = config->readBoolEntry("Show Toolbar", true);
    showToolBar_->setChecked(bViewToolbar);
    showToolBar();

    bool bViewStatusbar = config->readBoolEntry("Show Statusbar", true);
    showStatusBar_->setChecked(bViewStatusbar);
    showStatusBar();
    */
    // bar position settings
    /// XXXXXXXXXXXXXXXXXXXXx

    /*
      KToolBar::BarPosition toolBarPos;
      toolBarPos=(KToolBar::BarPosition) config->readNumEntry("ToolBarPos", KToolBar::Top);
      toolBar("mainToolBar")->setBarPos(toolBarPos);
    */
}


/*!
  \fn knateon::saveProperty(KConfig *config)
*/
void KNateonInterface::saveProperties(KConfig *config) {
    config->setGroup("General");
    config->writeEntry("Size", size() );
    config->writeEntry("Position", pos() );
    //	config->writeEntry("Show Toolbar", showToolBar_->isChecked() );
    //	config->writeEntry("Show Statusbar", showStatusBar_->isChecked() );
    //  config->writeEntry("ToolBarPos", (int) toolBar("mainToolBar")->barPos() );
}


/*!
  \fn knateon::menuQuit()
*/
void KNateonInterface::menuQuit() {
    //	KMessageBox::information( this, "oops, KNateonInterface initialize fail!", "caption", 0, 0);
    KApplication *app = static_cast<KApplication *>(kapp);
    app->quit();
    //app->setQuitSelected(true);

    close();                     // Close this window, initiates quit
}


#include "knateonview.moc"

/*!
  \fn KNateonInterface::initContactPopup()
*/
bool KNateonInterface::initContactPopup() {
    /*
      chatWithContact_   = new KAction(UTF8("Cha&t"),             "launch",       0, this, "chat");

      connect( chatWithContact_,   SIGNAL(activated()),   this,  SLOT(slotForwardStartChat())      );

      moveContactToGroup_ = new KActionMenu(UTF8("&Move to Group"), 0, 0, "moveToGroup");

      // Initialize the popup menu
      contactActionPopup_ = new KPopupMenu(this);
      contactActionPopup_->insertTitle("KNateon", 0);

      chatWithContact_   ->plug(contactActionPopup_);
    */
    return true;
}



/*!
  \fn KNateonInterface::slotRightClickMenu()
*/
void KNateonInterface::slotRightClickMenu(QListViewItem *item, const QPoint &point, int /*col*/) {
    // Not name, because most msn names are too weird.
    /*
      contactActionPopup_->changeTitle(0, "ring0320@nate.com");
      contactActionPopup_->popup(point);
    */
    Q_UNUSED( item );
    Q_UNUSED( point );
}

/*!
  액션 설정
*/
void KNateonInterface::createActions() {
}

void KNateonInterface::slotSetGroupList(QStringList & Group) {
    // m_Group = Group;
    pCopyfriendSelectAction ->setItems(Group);
    pMovefriendSelectAction ->setItems(Group);
}

// #include "dialog/emoticonselector.h"
#include "dialog/filetransfer.h"
FileTransfer* MyDialog;

void KNateonInterface::slotOpenChatBox() {
    /*
      EmoticonSelector *MyDialog = new EmoticonSelector(this, "xx");
      MyDialog->setModal(true);
      MyDialog->exec();
    */
    if (!MyDialog) {
        MyDialog = new FileTransfer(this, "xx");
        MyDialog->setModal(true);
        MyDialog->exec();
    }
    MyDialog->hidelist();
    MyDialog->show();
}

// #include "dialog/webviewer.h"
// #include <kparts/browserextension.h>
void KNateonInterface::slotOpenFileBox() {
    /*
      if (!MyDialog)
      {
      MyDialog = new FileTransfer(this, "xx");
      MyDialog->setModal(true);
      MyDialog->exec();
      }
      MyDialog->showlist();
      MyDialog->show();
    */

    if ( stConfig.filedownloadpath != QString::null )
        new KRun( stConfig.filedownloadpath );
    else
        new KRun( QDir::homeDirPath() );
    /*
      WebViewer *MyDialog = new WebViewer (this, "xx");
      MyDialog->setModal(true);
      MyDialog->exec();
      MyDialog->openURL("http://minihp.cyworld.nate.com/pims/main/pims_main_nateon.asp?tid=20396087");
    */

}



void KNateonInterface::slotSendMemo() {
    emit viewMemoWindow();
}

void KNateonInterface::slotGoNateHome() {
    LNMUtils::openURL("http://nateonweb.nate.com");
}

void KNateonInterface::slotGoCyHome() {
    LNMUtils::openURL( "http://cyworld.nate.com" );
}

void KNateonInterface::slotGoNateDotCom() {
    LNMUtils::openURL( "http://www.nate.com" );
}

void KNateonInterface::slotNateonUsage() {
    QString sURL("http://br.nate.com/index.php");
    sURL += "?code=E069";
    LNMUtils::openURL( sURL );
}

void KNateonInterface::slotGoNateonMiniHompy() {
    LNMUtils::openURL( "http://cyworld.nate.com/nateoncyevent" );
}

void KNateonInterface::slotGoHotTip() {
    LNMUtils::openURL( "http://nateonweb.nate.com/help/guide_hottip_v3_main.html" );
}

void KNateonInterface::slotFAQ() {
    LNMUtils::openURL( "http://nateonweb.nate.com/help/guide_faqsearch_list.html" );
}

// extern KAboutData aboutData;

void KNateonInterface::slotInfo() {
    if ( !helpMenu_ )
        helpMenu_= new KHelpMenu(this, KGlobal::instance()->aboutData());
    helpMenu_->aboutApplication();
}

void KNateonInterface::OfflineEnableMenu() {
    pLogoutAction->setEnabled( false );
    pOpenchatboxAction->setEnabled( false );
    pChatAction->setEnabled( false );
    pSendmemoAction->setEnabled( false );
    pSendfileAction->setEnabled( false );
    pAddfriendAction->setEnabled( false );
    pCopyfriendSelectAction->setEnabled( false );
    pMovefriendSelectAction->setEnabled( false );
    pDeletefriendAction->setEnabled( false );
    pBuddynamingSelectAction->setEnabled( false );
    pSortlistSelectAction->setEnabled( false );
    pAddgroupAction->setEnabled( false );
    pRenamegroupAction->setEnabled( false );
    pDeletegroupAction->setEnabled( false );
    pChangestatusSelectAction->setEnabled( false );
    pChangenickAction->setEnabled( false );

    pLoadBuddyListAction->setEnabled( true );
    pSaveBuddyListAction->setEnabled( true );

    /*! TODO : Disable 다시 수정 필요 */
    pSetupAction->setEnabled( true );
#ifdef DEBUG
    pNetLogAction->setEnabled( true );
#endif
}

void KNateonInterface::OnlineEnableMenu() {
    pLogoutAction->setEnabled( true );
    pOpenchatboxAction->setEnabled( true );
    pChatAction->setEnabled( true );
    pSendmemoAction->setEnabled( true );
    pSendfileAction->setEnabled( true );
    pAddfriendAction->setEnabled( true );
    pCopyfriendSelectAction->setEnabled( true );
    pMovefriendSelectAction->setEnabled( true );
    pDeletefriendAction->setEnabled( true );
    pBuddynamingSelectAction->setEnabled( true );
    pSortlistSelectAction->setEnabled( true );
    pAddgroupAction->setEnabled( true );
    pRenamegroupAction->setEnabled( true );
    pDeletegroupAction->setEnabled( true );
    pChangestatusSelectAction->setEnabled( true );
    pChangenickAction->setEnabled( true );
    pSetupAction->setEnabled( true );
#ifdef DEBBUG
    pNetLogAction->setEnabled( true );
#endif

    pLoadBuddyListAction->setEnabled( true );
    pSaveBuddyListAction->setEnabled( true );
}

void KNateonInterface::slotAppClose() {
    hide();
}

void KNateonInterface::slotMenuHide() {
    hide();
}

void KNateonInterface::slotChangeStatusNumber(int nID) {
    Q_UNUSED( nID );
}
