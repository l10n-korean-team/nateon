/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef HOMPYBUTTON_H
#define HOMPYBUTTON_H

#include "shapewidget.h"
#include <qlistview.h>

class HompyButton : public ShapeButton {
    Q_OBJECT
private:
    QListViewItem* listViewItem_;

public:
    HompyButton(QWidget* parent,const QString& iname, long tcol=-1);
    QListViewItem* listViewItem() {
        return listViewItem_;
    }
    void setListViewItem(QListViewItem* item) {
        listViewItem_ = item;
    }

signals:
    void gotoHompy(QListViewItem*);

protected slots:
    void clickedThis();
};

#endif
