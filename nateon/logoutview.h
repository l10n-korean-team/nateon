/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LOGOUTVIEW_H
#define LOGOUTVIEW_H

#include <qlayout.h>

#include <loginviewinterface.h>

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class LogoutView : public loginviewinterface {
    Q_OBJECT
public:
    LogoutView(QWidget *parent = 0, const char *name = 0);
    ~LogoutView();

    void setID( QString sID ) {
        textLabel1->setText( sID );
        textLabel1->update();
    }

    QLabel* textLabel1;
    ShapeButton* pushButton1;
    LinkLabel* textLabel2;

protected:
    QVBoxLayout* layout6;
    QSpacerItem* spacer10;
    QSpacerItem* spacer1;
    QSpacerItem* spacer3;
    QSpacerItem* spacer11;
    QHBoxLayout* layout2;
    QSpacerItem* spacer4;
    QSpacerItem* spacer5;
    QHBoxLayout* layout3;
    QSpacerItem* spacer6;
    QSpacerItem* spacer7;
    QHBoxLayout* layout4;
    QSpacerItem* spacer8;
    QSpacerItem* spacer9;

protected slots:
    void slotChangeLoginID(const QString & sID) {
        textLabel1->setText( sID );
        textLabel1->update();
    }
};

#endif
