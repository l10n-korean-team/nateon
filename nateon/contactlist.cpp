/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "contactlist.h"
#include <kglobal.h>
#include <kstandarddirs.h>

/*!
  \fn ContactList::setPixmap(QPixmap *p)
*/
ContactList::ContactList(QListView *parent, const QString &s1 )
        : ContactBase( parent, 0 ),
        richText_(0),
        listParent_( parent ),
        hasHompy_(false),
        pContactRoot( 0 ) {
    setType( ContactBase::Buddy );
    setDropEnabled( FALSE );
    setDragEnabled( TRUE );
    Q_UNUSED( s1 );
}

ContactList::ContactList(QListViewItem *parent, const QString &s1 )
        : ContactBase( parent, 0 ),
        richText_(0),
        listParent_( 0 ),
        hasHompy_(false),
        pContactRoot( parent ) {
    setType( ContactBase::Buddy );
    setDropEnabled( FALSE );
    setDragEnabled( TRUE );
    Q_UNUSED( s1 );
}

ContactList::~ ContactList() {
    if (  richText_ != 0 ) delete  richText_;
}

void ContactList::paintCell(QPainter * painter, const QColorGroup & colourGroup, int column, int width, int align) {
    if ( column == 0) {
        QBrush *brush;
        QPalette palette;
        QRect itemRectangle;
        QColorGroup customColourGroup = colourGroup;

        palette = listView()->viewport()->palette();
        brush   = 0;

        itemRectangle = listView()->itemRect( this );
        itemRectangle.setX( listView()->treeStepSize() * depth() );

        listView()->viewport()->erase( itemRectangle );
        // customColourGroup.setColor ( QColorGroup::Highlight, QColor("#c8daea") );
        if ( isSelected() ) {
            palette.setColor( QPalette::Active, QColorGroup::Highlight, QColor("#c8daea") );
            brush = new QBrush( palette.color(QPalette::Active, QColorGroup::Highlight) );
        }
        customColourGroup.setColor( QColorGroup::Text, Qt::black);

        // TODO: Change the font for highlighted text
        richText_->draw( painter, listView()->itemMargin(), 0, QRect( 0, 0, /* richText_->width() */ width, height() ), customColourGroup, brush );
        setHeight( richText_->height() );
        // setHeight( 18 );

        widthChanged( 0 );

        delete brush;
    } else {
        QListViewItem::paintCell( painter, colourGroup, column, width, align );
    }
}

void ContactList::setHTMLText(const QString & text) {
    recreateRichText(text);
    repaint();
}

void ContactList::resortParent() {
    // Ask parent to re-run the sorting algorithm
    if (QListViewItem::parent() != 0) {
        QListViewItem::parent()->sort();
    } else if (listParent_ != 0) {
        // FIXME: remove this if the QListViewItem::parent() also works for listviews
        listParent_->sort();
    }
#ifdef NETDEBUG
    else {
        kdDebug() << "KMessListViewItem::resortParent: No parent found!" << endl;
    }
#endif
}

void ContactList::setHasHompy(bool f) {
    hasHompy_ = f;
}

void ContactList::recreateRichText(const QString & text) {
    // Delete the existing rich text
    if ( richText_ != 0 ) {
        delete richText_;
        richText_ = 0;
    }

    // QStyleSheet style;

    // Create a new rich text object
    QStyleSheetItem* a = new QStyleSheetItem(QStyleSheet::defaultSheet(), "img");
    // a->setAlignment ( Qt::AlignVCenter );
    // a->setVerticalAlignment ( QStyleSheetItem::VAlignBaseline );
    // a->setVerticalAlignment ( QStyleSheetItem::VAlignSub);
    a->setVerticalAlignment ( QStyleSheetItem::VAlignSuper);
    // a->setColor( Qt::red );
    richText_ = new QSimpleRichText( "<qt><nobr>" + text + "</nobr></qt>" , listView()->viewport()->font(), QString::null, a->styleSheet() /* 0, mimeFactory_*/ );
    // TODO: use the QMimeSourceFactory parameter to load all graphics only once.
    // richText_->setPaletteForegroundColor( QColor("#424242") );
    QFont f;
    f.setBold( FALSE );
    richText_->setDefaultFont ( f );
    /*
      listView3->setPaletteForegroundColor( QColor("#226699") );
      QFont listView3_font(  listView3->font() );
      listView3_font.setBold( TRUE );
      listView3->setFont( listView3_font );
    */


    richText_->adjustSize();
    richText_->setWidth( 0 );
    widthChanged(0);
}

int ContactList::width(const QFontMetrics & fm, const QListView * lv, int c) const {
    if (c == 0) {
        return richText_->width();
    }

    return QListViewItem::width( fm, lv, c );
}



