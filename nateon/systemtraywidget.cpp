/***************************************************************************
                          systemtraywidget.cpp  -  description
                             -------------------
    begin                : Sun Dec 29 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "systemtraywidget.h"

#include <kdebug.h>
#include <klocale.h>
#include <kpopupmenu.h>
#include <kiconloader.h>

#include "currentaccount.h"

// The constructor
SystemTrayWidget::SystemTrayWidget(QWidget *parent, const char *name )
        : KSystemTray(parent,name) {
    // slotStatusChanged();
    KIconLoader *loader = KGlobal::iconLoader();
    setPixmap( loader->loadIcon( "main_list_state_offline", KIcon::User ) );
}


// The destructor
SystemTrayWidget::~SystemTrayWidget() {
#ifdef NETDEBUG
    kdDebug() << "DESTROYED SystemTrayWidget" << endl;
#endif
}

// Initialize the class
bool SystemTrayWidget::initialize() {
    connect( CurrentAccount::instance(), SIGNAL( changedStatus() ), SLOT( slotStatusChanged() ) );
    return true;
}


// Return the context menu
KPopupMenu* SystemTrayWidget::menu() const {
    return contextMenu();
}


// Change the icon when the user's status changes
void SystemTrayWidget::slotStatusChanged() {
    QString     iconName;
    switch ( CurrentAccount::instance()->getStatus() ) {
    case 'O' :
        iconName = "main_list_state_online";
        break;
    case 'A' :
        iconName = "main_list_state_vacant";
        break;
    case 'B' :
        iconName = "main_list_state_otherbusiness";
        break;
    case 'P' :
        iconName = "main_list_state_onphone";
        break;
    case 'M' :
        iconName = "main_list_state_meeting";
        break;
    case 'F' :
        iconName = "main_list_state_offline";
        break;
    }

    KIconLoader *loader = KGlobal::iconLoader();
    setPixmap( loader->loadIcon( iconName, KIcon::User ) );
}

void SystemTrayWidget::addToastWindow() {
    QPoint point = mapToGlobal( pos() );
}


#include "systemtraywidget.moc"
