/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CHATQW_H
#define CHATQW_H

#include <qvariant.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qpushbutton.h>
#include <qframe.h>
#include <qlabel.h>
#include <qtextedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qevent.h>
#include <qdragobject.h>
#include <qdir.h>
#include <qdragobject.h>
#include <qmenubar.h>
#include <klocale.h>
#include <khelpmenu.h>

#include <kstandarddirs.h>
#include <kdebug.h>
#include <qsplitter.h>
#include <qaction.h>
#include <qapplication.h>
#include <kaboutdata.h>

#include "shapewidget.h"
#include "textminibrowser.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QFrame;
class QPushButton;
class QLabel;
class QTextEdit;
class ShapeButton;
class ShapeWidget;
class ShapeForm;

class MyEdit : public QTextEdit {
    Q_OBJECT
public:
    MyEdit( QWidget * parent = 0, const char * name = 0 );
    ~MyEdit();

protected:
    void keyPressEvent ( QKeyEvent * e );
    void contentsDropEvent ( QDropEvent * e);
    void contentsDragEnterEvent (QDragEnterEvent* e);
    void contentsDragMoveEvent( QDragMoveEvent *e );

protected slots:
    void slotCheckLength();

signals:
    void onlyReturn();
    void sendFiles( const QStringList & );
};

class ChatQW : public QWidget {
    Q_OBJECT

public:
    ChatQW ( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~ChatQW();

    QFrame* frame4;
    ShapeButton* fileSendButton;
    ShapeButton* inviteButton;
    QLabel* nicknameLabel;
    QLabel* statusLabel;
    TextMiniBrowser* ChatViewQTE;
    QFrame* frame5;
    ShapeButton* emoticonButton;
    ShapeButton* fontButton;
    ShapeButton* fontColorButton;
    QFrame* frame3;
    ShapeButton* SendQPB;
    MyEdit* ChatEditQTE;
    QLabel* textLabel1;
    QFrame* frame10;
    ShapeWidget* statusWidget;
    ShapeWidget* hompyWidget;
    QMenuBar *menu;
    QPopupMenu *file;
    QPopupMenu *action;
    QPopupMenu *buddy;
    QPopupMenu *setup;
    QPopupMenu *help;
    QPopupMenu *status;
    QActionGroup *ag;
    QAction *online;
    QAction *away;
    QAction *busy;
    QAction *phone;
    QAction *meeting;
    QAction *offline;
    KHelpMenu *helpMenu_;

protected:
    QVBoxLayout* ChatQWLayout;
    QVBoxLayout* layout16;
    QVBoxLayout* layout117;
    QGridLayout* frame3Layout;
    QVBoxLayout* layout7;
    QVBoxLayout* frame9Layout;
    QSpacerItem* spacer2_2;
    QSpacerItem* spacer2;
    QHBoxLayout* frame4Layout;
    QHBoxLayout* frame5Layout;
    QHBoxLayout* frame10Layout;
    QSpacerItem* spacer4;
    QSpacerItem* spacer5;
    void hideEvent ( QHideEvent *e );
    QSplitter *s1;

    int lockID;

    int nAlwaysTopID;

    int m_nInvite, m_nSendMemo, m_nSendFile, m_nAddBuddy;

    virtual void resizeEvent ( QResizeEvent * e );

private:
    QString         sPicsPath;

protected slots:
    virtual void languageChange();
    virtual void slotChangeEdit();
    /*! 메뉴 */
    virtual void slotSave(); /*! 대화 저장 */
    virtual void slotFileFolder(); /*! 받은 파일 폴더 열기 */
    virtual void slotShowChatLog(); /*! 지난 대화 보기 */
    virtual void slotClose(); /*! 닫기 */
    virtual void slotInvite(); /*! 친구 초대 */
    virtual void slotSendMemo();
    virtual void slotSendFile();
    virtual void slotShowProfile();
    virtual void slotAddBuddy();
    virtual void slotLock();
    virtual void slotChangeNick();
    virtual void slotEditMyProfile();
    virtual void slotAlwaysTop();
    virtual void slotSetup();
    virtual void slotGoNateonHome();
    virtual void slotGoCyworldHome();
    virtual void slotGoNateDotComHome();
    virtual void slotGoHelp();
    virtual void slotGoNateonMiniHompy();
    virtual void slotGoHotTip();
    virtual void slotGoFaq();
    virtual void slotInfo();
    virtual void slotStatus( QAction * a );
    void slotViewUpdate();
    void slotShowTransfer();

signals:
    void hideChat();
    void changeStatus( int );
    void showTransfer();
};

#endif // CHATQW_H
