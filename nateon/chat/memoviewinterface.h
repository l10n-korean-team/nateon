/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MEMOQW_H
#define MEMOQW_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>
#include <qlabel.h>

#include <kstandarddirs.h>
#include <kmessagebox.h>
#include <kaboutdata.h>

#include "shapewidget.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QPushButton;
class QLineEdit;
class QToolButton;
class QTextEdit;
class QLabel;
// class QCheckBox;
class ShapeButton;
class ShapeWidget;
class ShapeForm;

class MyLabel : public QLabel {
    Q_OBJECT
public:
    MyLabel( QWidget* parent = 0, const char* name = 0);

protected:
    void mousePressEvent ( QMouseEvent * e );
signals:
    void clicked();
};

class MemoQW : public QWidget {
    Q_OBJECT

public:
    MemoQW( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~MemoQW();

    ShapeButton* pushButton3;
    QLineEdit* lineEdit1;
    ShapeButton* emoticonTB;
    ShapeButton* fontButton;
    QTextEdit* textEdit2;
    QLabel* textLabel2;
    // QCheckBox* checkBox1;
    MyLabel* textLabel1;
    ShapeButton* sendPB;
    ShapeButton* clearPB;

protected:
    QVBoxLayout* MemoQWLayout;
    QVBoxLayout* layout19;
    QHBoxLayout* layout16;
    QHBoxLayout* layout18;
    QSpacerItem* spacer8;

protected slots:
    virtual void languageChange();
    virtual void slotClear();

private:
    QPixmap image0;
    QPixmap image1;

};

#endif // MEMOQW_H
