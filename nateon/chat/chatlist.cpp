/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "chatlist.h"

ChatList::ChatList()
        : QObject() {
    m_ChatObjs.clear();
}


ChatList::~ChatList() {
}

void ChatList::addChatObj(ChatView * m_ChatView) {
    /// 채팅객채를 채팅객체리스트에 추가 함.
    m_ChatObjs.append(m_ChatView);

    // 채팅창이 추가 될때 signal이 필요 할까??
    // emit ChatObjAdded(m_ChatView);
}

ChatView * ChatList::getChatViewByTID(int nTID) const {
    QPtrListIterator<ChatView> iterator(m_ChatObjs);
    ChatView* pChatView;

    while (iterator.current() != 0) {
        pChatView = iterator.current();
        if ( pChatView->getTID() == nTID ) {
            return pChatView;
        }
        ++iterator;
    }
    return 0;
}




ChatView * ChatList::creatChatObj(int TID) {
    ChatView* pChatView = 0;

    if ( !getChatViewByTID( TID ) ) {
        pChatView = new ChatView();
        pChatView->setTID(TID);
        addChatObj(pChatView);
    } else {
        /*!
         * 새로 생성한게 아니고 기존에 창을 리턴해 줌.
         */
        pChatView = getChatViewByTID( TID );
    }

    return pChatView;
}

ChatView * ChatList::getChatViewByUID(QString sUID) const {
    QPtrListIterator<ChatView> iterator(m_ChatObjs);

    while (iterator.current() != 0) {
        ChatView* pChatView = iterator.current();
        /*! 그룹 채팅 이면 Skip */
        if ( !pChatView->isGroupChat() ) {
            if ( pChatView->getBuddyByID( sUID ) ) {
                return pChatView;
            }
        }
        ++iterator;
    }
    return 0;
}

void ChatList::removeChatObj(ChatView * pChatView) {
    m_ChatObjs.remove(pChatView);
}

ChatView * ChatList::getChatViewByAllUIDs(QStringList slAllUIDs) const {
    QPtrListIterator<ChatView> iterator(m_ChatObjs);
    while (iterator.current() != 0) {
        ChatView* pChatView = iterator.current();
        if ( pChatView->isEqualBuddyList( slAllUIDs ) ) {
            return pChatView;
        }
        ++iterator;
    }
    return 0;
}
