/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MEMOVIEW_H
#define MEMOVIEW_H

#include <kglobal.h>
#include <kapp.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qtoolbutton.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <qfontdialog.h>
#include <kconfig.h>

#include "memoviewinterface.h"
#include "../dialog/emoticonselector.h"
#include "../dialog/addbuddyselector.h"
#include "define.h"

class QLineEdit;
class QTextEdit;
class QPushButton;
class EmoticonSelector;
class AddBuddySelector;


class MemoView: public MemoQW {
    Q_OBJECT
public:
    MemoView(QWidget *parent = 0, const char *name = 0);
    ~MemoView();

    bool initialize();
    QString getReceiver() {
        return lineEdit1->text();
    };
    void setReceiver(const QString &sReceiver);
    void setBody(QString sBody);
    void moveCursorHome();
    QString getMemo() {
        return textEdit2->text();
    }; /// 쪽지 메세지를 얻는다.
    void show();
    void setMemoHeader( NMStringDict *dict ) {
        m_dMemoHeader = dict;
    }

    NMStringDict *memoHeader() {
        return m_dMemoHeader;
    }

    MEMO_EVENT getEventType() {
        return event_type;
    }

    void setEventType( MEMO_EVENT event ) {
        event_type = event;
    }

protected:
    void hideEvent ( QHideEvent *e );
    void moveEvent ( QMoveEvent *e );
    virtual void closeEvent ( QCloseEvent * e );

private:
    MEMO_EVENT event_type;
    EmoticonSelector* pEmoticonSelector;
    AddBuddySelector* pAddBuddy;
    KConfig *config;
    bool bCheck;
    //! memo header
    NMStringDict *m_dMemoHeader;

private slots:
    void slotViewEmoticonSelector();
    void slotViewAddBuddySelector();
    void slotSendMemo();
    void slotAddBuddies(const QString& sBuddies);
    void slotCharCount();
    void slotPutEmoticon( const QString &sText );
    void slotFontDialog();

signals:
    void sendMemo( MemoView* );
    void addBuddyList(AddBuddySelector*);
    void closeMemoView( MemoView* );
};

#endif
