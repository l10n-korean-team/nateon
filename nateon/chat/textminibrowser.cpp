/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "textminibrowser.h"
#include <kdebug.h>

TextMiniBrowser::TextMiniBrowser( QWidget * parent, const char * name )
        : QTextEdit( parent, name ) {
    setReadOnly( TRUE );
    viewport()->setMouseTracking( TRUE );
}


TextMiniBrowser::~TextMiniBrowser() {
}

void TextMiniBrowser::emitLinkClicked( const QString &s ) {
#ifdef NETDEBUG
    kdDebug() << "Clicked : [" << s << "]" << endl;
#endif

    QString sR(s);
    if ( sR.find( "mailto:" ) >= 0 ) { // mail
        sR.remove( "mailto:" );
        LNMUtils::sendMail( sR );
    } else { // www
        LNMUtils::openURL( sR );
    }
}

void TextMiniBrowser::append( const QString& text ) {
    QString sNewText = text;

#ifdef NETDEBUG
    kdDebug() << "Orig : [" << sNewText << "]" << endl;
#endif
    /*! http://, https://, ftp://, mms:// 로 시작하는 URL 처리 */
    sNewText.replace( QRegExp("\\b((http://|https://|ftp://|mms://)[\\w\\#$%&~/.\\-;:=,?@\\[\\]+]*)\\b"), "<a href=\"\\1\">\\1</a>");

    sNewText.replace( "http://www", "HTTP://WWW" );
    sNewText.replace( "https://www", "HTTPS://WWW" );
    sNewText.replace( "ftp://www", "FTP://WWW" );
    sNewText.replace( "mms://www", "MMS://WWW" );
    /*!
     * www 로 시작하는 URL 처리
     * antz TODO: http://www
     * 대문자로 HTTP://WWW 로 했으면 소문자로 변경됨. 로직 변경 필요.
     */
    sNewText.replace( QRegExp("\\b(www.[a-zA-Z0-9.-]+)\\b"), "<a href=\"http://\\1\">\\1</a>");
    sNewText.replace( "HTTP://WWW", "http://www" );
    sNewText.replace( "HTTPS://WWW", "https://www" );
    sNewText.replace( "FTP://WWW", "ftp://www" );
    sNewText.replace( "MMS://WWW", "mms://www" );

    /*! 메일주소 처리 */
    sNewText.replace( QRegExp("\\b([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4})\\b"), "<a href=\"mailto:\\1\">\\1</a>");

    // sNewText.replace( QRegExp("\\b^(www.[a-zA-Z0-9.-]+)\\b"), "<a href=\"http://\\1\">\\1</a>");
    // sNewText.replace( QRegExp("\\b([^<(]+@[^>)]+)\\b"), "<a href=\"mailto:\\1\">\\1</a>");
    // sNewText.replace( QRegExp("\\b([^;&<( ]+@[^;&>) ]+)\\b"), "<a href=\"mailto:\\1\">\\1</a>");
    QTextEdit::append(sNewText);
#ifdef NETDEBUG
    kdDebug() << "New  : [" << sNewText << "]" << endl;
#endif
}

#include "textminibrowser.moc"
