/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MEMOPOPUP_H
#define MEMOPOPUP_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>
#include <kstandarddirs.h>
#include <qtextedit.h>
#include <kaboutdata.h>

#include "shapewidget.h"

class QTextEdit;
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QPushButton;
class QToolButton;
class ShapeButton;
class ShapeWidget;
class ShapeForm;

class MemoPopup : public QWidget {
    Q_OBJECT

public:
    MemoPopup( QWidget* parent = 0, const char* name = 0 );
    ~MemoPopup();

    QLabel* textLabel1;
    QLabel* labelSender;
    QLabel* textLabel3;
    QLabel* labelReceiver;
    QTextEdit* labelMemo;
    ShapeButton* buttonReply;
    ShapeButton* buttonReplyAll;
    ShapeButton* buttonForward;
    ShapeButton* buttonDelete;

protected:
    QVBoxLayout* MemoPopupLayout;
    QVBoxLayout* layout6;
    QHBoxLayout* layout4;
    QHBoxLayout* layout5;
    QHBoxLayout* layout2;
    QSpacerItem* Horizontal_Spacing2;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;
    QPixmap image1;
    QString sPicsPath;
};

#endif // MEMOPOPUP_H
