/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "memolist.h"

MemoList::MemoList()
        : QObject() {
    m_MemoObjs.clear();
}


MemoList::~MemoList() {
}


void MemoList::addMemoObj(MemoView * m_MemoView) {
    /// 쪽지객채를 쪽지객체리스트에 추가 함.
    m_MemoObjs.append(m_MemoView);
}


MemoView * MemoList::creatMemoObj() {
    MemoView* pMemoView = 0;

    pMemoView = new MemoView();
    pMemoView->initialize();
    addMemoObj(pMemoView);

    return pMemoView;
}


MemoView * MemoList::getMemoViewByTID(int TID) const {
    Q_UNUSED( TID );
    /*
      QPtrListIterator<ChatView> iterator(m_ChatObjs);
      ChatView* pChatView;
      pMemoView = new MemoView();
      while(iterator.current() != 0)
      {
      pChatView = iterator.current();
      if ( pChatView->getTID() == nTID )
      {
      return pChatView;
      }
      ++iterator;
      }
      return 0;
    */
    return 0;
}






