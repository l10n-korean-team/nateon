/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TEXTMINIBROWSER_H
#define TEXTMINIBROWSER_H

#include <qtextedit.h>
#include <qregexp.h>
#include <krun.h>

#include "utils.h"

/**
	@author kakapapa <kakapapa@nate.com>
*/
class TextMiniBrowser : public QTextEdit {
    Q_OBJECT
public:
    TextMiniBrowser( QWidget * parent = 0, const char * name = 0 );
    ~TextMiniBrowser();

private:
    bool linksEnabled() const {
        return TRUE;
    }
    void emitLinkClicked( const QString &s );

public slots:
    virtual void append( const QString& text );
};

#endif
