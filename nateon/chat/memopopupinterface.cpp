/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "memopopupinterface.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qtoolbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

static const unsigned char image0_data[] = {
    0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
    0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x10,
    0x08, 0x06, 0x00, 0x00, 0x00, 0x1f, 0xf3, 0xff, 0x61, 0x00, 0x00, 0x03,
    0x46, 0x49, 0x44, 0x41, 0x54, 0x38, 0x8d, 0x85, 0x93, 0xff, 0x6b, 0x94,
    0x75, 0x00, 0xc7, 0x5f, 0xf7, 0x3c, 0xcf, 0xdd, 0xcd, 0x7b, 0x6e, 0x9d,
    0xe7, 0x7d, 0xdb, 0x8d, 0xcd, 0x6d, 0xe7, 0xdc, 0xdc, 0xe6, 0x35, 0xd7,
    0x34, 0x0a, 0xc3, 0x4d, 0xa4, 0x44, 0x8d, 0x48, 0xc2, 0x88, 0x92, 0xa0,
    0x41, 0x7e, 0x01, 0x09, 0x6c, 0x0a, 0x92, 0x62, 0x6b, 0x14, 0x8b, 0xa6,
    0x46, 0xf4, 0x65, 0x54, 0x3f, 0xc4, 0x4a, 0x4a, 0x42, 0xc2, 0xe9, 0x74,
    0x4d, 0x9a, 0xc6, 0xd2, 0xdc, 0x5a, 0xad, 0x95, 0x73, 0x67, 0x18, 0xd5,
    0x6e, 0x6d, 0xed, 0x5b, 0x76, 0x9e, 0xdb, 0xdd, 0xed, 0xf9, 0xf2, 0x79,
    0xfa, 0xe1, 0xea, 0x97, 0x0a, 0x7a, 0xfd, 0x01, 0x2f, 0x78, 0xc3, 0xfb,
    0x65, 0x7b, 0xaa, 0xe1, 0x0d, 0x00, 0x74, 0x3d, 0x63, 0x25, 0x13, 0x71,
    0x52, 0xe9, 0x04, 0xb9, 0xae, 0x25, 0xe4, 0x7a, 0x97, 0x22, 0xcb, 0x0e,
    0x1b, 0xff, 0x83, 0x02, 0xb0, 0x73, 0xc7, 0x6a, 0xeb, 0x70, 0x73, 0x0b,
    0x25, 0xeb, 0x37, 0x61, 0xf9, 0xf2, 0x39, 0xd3, 0x37, 0x48, 0xf9, 0x54,
    0x3f, 0xfb, 0x0e, 0x34, 0x59, 0x77, 0xe4, 0x47, 0x98, 0xd3, 0x4c, 0xe6,
    0x17, 0x04, 0x29, 0x4d, 0x90, 0xd6, 0x4d, 0x16, 0x74, 0x93, 0x05, 0x43,
    0x10, 0x7b, 0xff, 0x63, 0x9b, 0x92, 0xc9, 0x24, 0xad, 0xc3, 0xcd, 0x2d,
    0x34, 0xbd, 0xfa, 0x1a, 0xa5, 0xc5, 0x85, 0x74, 0x8f, 0x4c, 0x90, 0x8e,
    0xde, 0xcf, 0xe9, 0x4b, 0x97, 0x69, 0x7e, 0x6e, 0x2f, 0x45, 0xe1, 0x30,
    0x86, 0x61, 0xa2, 0x4b, 0x4e, 0xf2, 0xcb, 0x6b, 0xf0, 0x47, 0x2a, 0xf1,
    0x17, 0x57, 0x61, 0x98, 0x02, 0x00, 0xb9, 0x30, 0x50, 0xd0, 0x54, 0xb3,
    0xb9, 0x9e, 0x2b, 0x1d, 0x27, 0xf1, 0xa8, 0x2a, 0xdd, 0xed, 0x6d, 0xec,
    0x7a, 0x70, 0x23, 0xd7, 0x9c, 0x79, 0x7c, 0x3b, 0x34, 0xc8, 0x93, 0xae,
    0x61, 0xd6, 0x56, 0x04, 0xf1, 0xa5, 0x6e, 0xe0, 0x9a, 0xf9, 0x9e, 0x2b,
    0x5d, 0xa7, 0xf8, 0x69, 0x7c, 0x9a, 0x50, 0xd9, 0x9d, 0x24, 0x63, 0x3f,
    0xbe, 0xa0, 0xa8, 0x8b, 0x2c, 0x2c, 0xc9, 0x01, 0xf7, 0x6e, 0xa1, 0xb6,
    0xae, 0x9e, 0xda, 0xba, 0x7a, 0x62, 0xe3, 0x73, 0x08, 0x4b, 0x60, 0xc9,
    0x76, 0x1e, 0xde, 0x7b, 0x8c, 0xc2, 0xf2, 0x3c, 0xec, 0xe9, 0x6b, 0x30,
    0x71, 0x99, 0x1d, 0x83, 0x57, 0x79, 0xf1, 0xf8, 0x59, 0xce, 0xb7, 0x4d,
    0xb2, 0x22, 0x50, 0x6b, 0x29, 0x2e, 0x5f, 0x25, 0x7d, 0x5d, 0x9d, 0x14,
    0xef, 0x3b, 0x4a, 0xeb, 0x0f, 0x1a, 0xc5, 0x0e, 0xb8, 0x9e, 0x56, 0x18,
    0x18, 0xb8, 0xc4, 0x3a, 0x69, 0x86, 0xfc, 0x22, 0x0f, 0x76, 0x69, 0x0e,
    0xdd, 0x90, 0x30, 0x17, 0x97, 0x12, 0x58, 0x67, 0xe7, 0xa8, 0x27, 0x87,
    0x86, 0xd6, 0x7e, 0x46, 0xd3, 0x32, 0x92, 0xac, 0xb8, 0x6d, 0xd1, 0x68,
    0x35, 0xbf, 0x1c, 0x69, 0x64, 0xa8, 0xfb, 0x13, 0xbe, 0xfa, 0x75, 0x8a,
    0x9e, 0x8e, 0x93, 0xdc, 0xd3, 0xdf, 0xce, 0x5b, 0xcf, 0xef, 0x42, 0xb2,
    0x3b, 0x39, 0x78, 0xa8, 0x8d, 0xd1, 0x49, 0x1b, 0x76, 0x35, 0xc2, 0x58,
    0x62, 0x31, 0x46, 0x61, 0x98, 0x3d, 0x1b, 0xf3, 0xb8, 0x39, 0x39, 0x82,
    0x04, 0xf0, 0xf8, 0xee, 0x46, 0xa2, 0xd1, 0x6a, 0xe6, 0x3f, 0x3c, 0xc2,
    0x58, 0xeb, 0x6e, 0x32, 0x1f, 0xbc, 0xcc, 0xbb, 0xcd, 0xfb, 0x89, 0x54,
    0xad, 0x61, 0x6c, 0x6c, 0x96, 0x73, 0x9f, 0x7e, 0x89, 0xa5, 0x2c, 0x41,
    0x56, 0x43, 0x34, 0x1f, 0xeb, 0xe5, 0xf3, 0xe1, 0x04, 0x15, 0x15, 0x7e,
    0x54, 0x29, 0x9d, 0x15, 0x68, 0x36, 0x07, 0x0f, 0x3d, 0xbd, 0x1f, 0xaf,
    0xcf, 0xc7, 0x7b, 0x8f, 0x04, 0x78, 0xe7, 0x60, 0x2d, 0x9e, 0xa0, 0x17,
    0xcb, 0x4c, 0xa3, 0x1b, 0x06, 0xdb, 0xb7, 0x6f, 0x23, 0xb2, 0xac, 0x84,
    0x89, 0xf8, 0x34, 0x23, 0xb1, 0x38, 0x21, 0x7f, 0x2e, 0x58, 0x20, 0x61,
    0x64, 0x05, 0x73, 0x0b, 0x26, 0x29, 0x5d, 0x60, 0xb9, 0x03, 0xdc, 0x1e,
    0x1f, 0x65, 0x43, 0x75, 0x0e, 0x5e, 0x25, 0x86, 0xd0, 0x33, 0x94, 0x16,
    0x87, 0x78, 0xf6, 0x99, 0x27, 0x10, 0xe9, 0x29, 0x64, 0x91, 0xe4, 0x50,
    0xe3, 0x06, 0x56, 0x2e, 0x5d, 0xc4, 0xec, 0x58, 0x02, 0x0d, 0x57, 0xf6,
    0x48, 0x73, 0x9a, 0xc0, 0x14, 0x16, 0xc1, 0xb2, 0x55, 0x74, 0x5f, 0x1d,
    0xa6, 0xaa, 0xec, 0x37, 0x34, 0xb9, 0x0b, 0x29, 0xf8, 0x3b, 0xd8, 0x83,
    0x08, 0x21, 0x30, 0x8d, 0x79, 0xbc, 0x6a, 0x82, 0x4d, 0xf7, 0x79, 0x60,
    0x64, 0x98, 0xf3, 0x5f, 0xcf, 0xe0, 0x0d, 0x47, 0xb3, 0x82, 0x94, 0x66,
    0x60, 0x9a, 0x16, 0xd5, 0x0f, 0x3c, 0x4a, 0xc7, 0x77, 0xfd, 0x94, 0x7c,
    0x31, 0xcd, 0x56, 0xf7, 0x1f, 0x68, 0x99, 0x5e, 0x0c, 0xd5, 0x83, 0xe4,
    0x70, 0x81, 0x30, 0x20, 0x93, 0x84, 0xc9, 0x59, 0x7a, 0x3f, 0x8b, 0xf3,
    0xf6, 0x90, 0xce, 0xca, 0xd5, 0x77, 0x21, 0xd7, 0xd4, 0x6c, 0x26, 0xb8,
    0xaa, 0xaa, 0x49, 0x33, 0x05, 0x42, 0xb2, 0x93, 0x57, 0x79, 0x37, 0x1f,
    0xf5, 0x7c, 0xc3, 0xec, 0xcf, 0x93, 0x94, 0xb9, 0x3c, 0xa8, 0xba, 0x85,
    0x2d, 0x71, 0x9b, 0x9c, 0x44, 0x8a, 0x5b, 0x37, 0x6e, 0xd1, 0xde, 0x19,
    0xe7, 0xa5, 0xde, 0x9b, 0x04, 0xd7, 0x6e, 0xc3, 0x2d, 0xfe, 0x9a, 0x90,
    0xd1, 0x05, 0xa6, 0x29, 0x30, 0x84, 0x40, 0x71, 0x7b, 0xa9, 0xdb, 0xf3,
    0x0a, 0x3d, 0xa7, 0xdb, 0x39, 0xf7, 0xfa, 0x09, 0x76, 0x6e, 0x5d, 0x0f,
    0x92, 0xc6, 0xd9, 0x81, 0x38, 0xd7, 0xc7, 0x13, 0xe4, 0x04, 0x0a, 0x58,
    0xd1, 0x70, 0x00, 0x57, 0xa8, 0x04, 0x71, 0xf1, 0xa2, 0x4d, 0xf9, 0xaf,
    0xc2, 0x24, 0xc5, 0x4e, 0xa8, 0xa8, 0x94, 0x0b, 0x9d, 0xf3, 0xb4, 0x9c,
    0xb8, 0x80, 0x2b, 0x90, 0x8f, 0x2b, 0x6f, 0x19, 0xcb, 0x1f, 0xdb, 0x42,
    0x8e, 0xbf, 0x00, 0xd9, 0xa9, 0x82, 0x2d, 0x1b, 0xaa, 0xed, 0xef, 0x9c,
    0xff, 0xc9, 0x50, 0xdf, 0x9b, 0x16, 0x80, 0x3f, 0xbc, 0x06, 0x5f, 0x60,
    0x39, 0x0e, 0xa7, 0x1b, 0x59, 0xb6, 0xff, 0x2b, 0xef, 0x3f, 0x01, 0x4b,
    0xd0, 0x53, 0x12, 0x37, 0x98, 0xa3, 0x76, 0x00, 0x00, 0x00, 0x00, 0x49,
    0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};

static const char* const image1_data[] = {
    "28 28 122 2",
    ".D c #1a5a98",
    "#Z c #27649d",
    ".K c #28639d",
    ".Q c #2867a2",
    ".G c #2c6ca6",
    ".I c #316da6",
    ".E c #35659a",
    ".6 c #356fa5",
    ".R c #3572ab",
    ".l c #366ba1",
    "#g c #3772a9",
    ".S c #3974ab",
    ".x c #3b689a",
    "#1 c #3b71a5",
    ".L c #3c74a8",
    "#T c #3c78af",
    "#R c #3f75aa",
    ".J c #4178ad",
    "#V c #4377ab",
    ".B c #4483b9",
    "#2 c #4579ab",
    ".d c #4685b8",
    ".y c #4775a6",
    ".1 c #477bae",
    ".P c #4789bd",
    ".0 c #4b7eb0",
    ".f c #4b8fc1",
    ".k c #4c7cac",
    "#f c #4c83b3",
    ".e c #4c8ebf",
    "#p c #4d85b5",
    ".v c #4d8abc",
    ".Z c #4e80b1",
    ".c c #4e85b6",
    ".5 c #508abd",
    ".m c #5183b2",
    ".Y c #5282b1",
    ".X c #5383b2",
    ".M c #538bba",
    ".w c #5784b1",
    ".n c #5b91be",
    ".W c #628eb8",
    "#W c #638eb5",
    ".b c #6491bc",
    ".o c #66a0cb",
    ".O c #6aaed9",
    ".2 c #6b98c3",
    "#Y c #6b9ac7",
    ".u c #6eb1dc",
    "#C c #6fa1c6",
    ".g c #6fa1c9",
    ".p c #71b1d9",
    ".F c #76b8e3",
    ".h c #779fc4",
    ".3 c #7ca8d1",
    ".A c #7cbee8",
    ".q c #7ec2e7",
    ".a c #7fa2c5",
    ".z c #81c4eb",
    ".N c #81c9ee",
    "#o c #84adc9",
    ".t c #84caef",
    "#I c #85aecb",
    "#U c #85b0cf",
    ".s c #88d1f4",
    ".r c #89d4f5",
    "#J c #8cb9d5",
    ".V c #8dadcc",
    "#B c #92bfe1",
    "#e c #93bfe2",
    ".4 c #95c2e6",
    "#n c #96b8d3",
    "#3 c #9ab7d2",
    "#K c #9ac2da",
    "#0 c #9bb8d3",
    "#h c #9fbcd1",
    "#A c #a1c4e1",
    "#D c #a2c6dd",
    "#N c #a6bfd2",
    "#S c #a6c4e0",
    "#z c #a7c4db",
    "#X c #a7c6e4",
    ".i c #a9c1d8",
    "#y c #a9c2d5",
    "#P c #a9cbe0",
    "#d c #a9cceb",
    "#m c #afcae2",
    "#x c #b1c6d5",
    ".# c #b7cbdf",
    "#O c #b7d3e5",
    ".7 c #b8d1e6",
    "#w c #b9cbd7",
    "#c c #bbd6ef",
    "#M c #beced9",
    "#q c #bed1e3",
    "#v c #c1d1da",
    "#i c #c1d1db",
    "#H c #c2d7ea",
    ".T c #c2daf0",
    "#u c #c5d4da",
    "#F c #c6d5de",
    ".H c #c6d6e5",
    "#l c #c6daed",
    ".U c #c7d7e9",
    "#b c #ccdff2",
    "#t c #cfdbe0",
    ".j c #d4e0ec",
    "#E c #d4e5ef",
    "#L c #dbe9f2",
    "#a c #dce9f6",
    "#s c #dde6ea",
    "#j c #e2e9ed",
    ".C c #e2eaf2",
    "#Q c #e2edf4",
    "#r c #e3ebf3",
    "## c #ebf1fa",
    ".8 c #f0f5fc",
    "#k c #f1f4f6",
    "#G c #f5f8f9",
    "#. c #f8fbfe",
    "Qt c #ff00ff",
    ".9 c #ffffff",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQt.#.a.b.c.d.e.f.g.h.iQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.j.k.l.m.n.o.p.q.r.s.t.u.v.iQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.w.x.y.m.n.o.p.q.r.s.t.z.A.B.CQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.D.E.y.m.n.o.p.q.r.s.t.z.F.G.HQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.I.J.K.L.M.o.p.q.r.N.O.P.Q.R.HQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.S.T.U.V.W.X.Y.Z.0.1.2.3.4.5.HQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.6.7.8.9.9.9#.###a#b#c#d#e#f.HQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt#g#h#i#j#k.9#.###a#l#m#n#o#p#qQtQtQtQtQtQtQtQt",
    "QtQtQtQtQt.S.T#r#s#t#u#v#w#x#y#z#A#B.5#C#D#EQtQtQtQtQtQt",
    "QtQtQtQtQt.S.T#F.9.9#G#.###a#H#c#d#I.5#C#J#J#K#LQtQtQtQt",
    "QtQtQtQtQt.S.T#M.9.9#u#.###a#N#c#d#I.5#C#J#J#J#J#OQtQtQt",
    "QtQtQtQtQt.S.T#M.9.9#u#.###a#N#c#d#I.5#C#J#J#J#J#JQtQtQt",
    "QtQtQtQtQt.S.T#M.9.9#u#.###a#N#c#d#I.5#C#J#J#J#J#JQtQtQt",
    "QtQtQtQtQt.S.T#M.9.9#u#.###a#N#c#d#I.5#C#J#J#J#PQtQtQtQt",
    "QtQtQtQtQt.S.T#M.9.9#u#.###a#N#c#d#I.5#C#J#P#QQtQtQtQtQt",
    "QtQtQtQtQt#R#S#M.9.9#u#.###a#N#c#d#I#T#U#EQtQtQtQtQtQtQt",
    "QtQtQtQtQt.j#V#W.H.9#u#.###a#N#X#Y#Z#0QtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQt.#.a.X#1.Y.Z#R#2.W#3QtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
    "QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt"
};


/*
 *  Constructs a MemoPopup as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
MemoPopup::MemoPopup( QWidget* parent, const char* name )
        : QWidget( parent, name ),
        image1( (const char **) image1_data ) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString         sPicsPath;
    sPicsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" );

    QImage img;
    img.loadFromData( image0_data, sizeof( image0_data ), "PNG" );
    image0 = img;
    if ( !name )
        setName( "MemoPopup" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setMinimumSize( QSize( 400, 250 ) );
    setIcon( image0 );
//   setSizeGripEnabled( TRUE );
    MemoPopupLayout = new QVBoxLayout( this, 11, 6, "MemoPopupLayout");

    layout6 = new QVBoxLayout( 0, 0, 6, "layout6");

    layout4 = new QHBoxLayout( 0, 0, 6, "layout4");
    /*
      textLabel1 = new QLabel( this, "textLabel1" );
      textLabel1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1->sizePolicy().hasHeightForWidth() ) );
      layout4->addWidget( textLabel1 );
    */
    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setMinimumSize( QSize( 65, 24 ) );
    textLabel1->setMaximumSize( QSize( 65, 24 ) );
    layout4->addWidget( textLabel1 );
    /*
      labelSender = new QLabel( this, "labelSender" );
      layout4->addWidget( labelSender );
      layout6->addLayout( layout4 );
    */
    labelSender = new QLabel( this, "labelSender" );
    labelSender->setMinimumSize( QSize( 0, 24 ) );
    labelSender->setMaximumSize( QSize( 32767, 24 ) );
    layout4->addWidget( labelSender );
    layout6->addLayout( layout4 );

    layout5 = new QHBoxLayout( 0, 0, 6, "layout5");
    /*
      textLabel3 = new QLabel( this, "textLabel3" );
      textLabel3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel3->sizePolicy().hasHeightForWidth() ) );
      layout5->addWidget( textLabel3 );
    */
    textLabel3 = new QLabel( this, "textLabel3" );
    textLabel3->setMinimumSize( QSize( 65, 24 ) );
    textLabel3->setMaximumSize( QSize( 65, 24 ) );
    layout5->addWidget( textLabel3 );
    /*
      labelReceiver = new QLabel( this, "labelReceiver" );
      layout5->addWidget( labelReceiver );
      layout6->addLayout( layout5 );
    */
    labelReceiver = new QLabel( this, "labelReceiver" );
    labelReceiver->setMinimumSize( QSize( 0, 24 ) );
    labelReceiver->setMaximumSize( QSize( 32767, 24 ) );
    layout5->addWidget( labelReceiver );
    layout6->addLayout( layout5 );

    labelMemo = new QTextEdit( this, "labelMemo" );
    labelMemo->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    labelMemo->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    labelMemo->setAlignment( int( QLabel::AlignTop ) );
    labelMemo->setTextFormat( QTextEdit::RichText );
    labelMemo->setWordWrap( QTextEdit::WidgetWidth );
    labelMemo->setReadOnly( true );
    layout6->addWidget( labelMemo );

    layout2 = new QHBoxLayout( 0, 0, 6, "layout2");
    Horizontal_Spacing2 = new QSpacerItem( 180, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout2->addItem( Horizontal_Spacing2 );

    buttonReply = new ShapeButton( this, sPicsPath + "bt_img_repl_nor.bmp" );
    buttonReply->setPressedShape( sPicsPath + "bt_img_repl_down.bmp" );
    buttonReply->setMouseOverShape( sPicsPath + "bt_img_repl_ov.bmp" );
    buttonReply->setMinimumSize( QSize( 80, 24 ) );
    buttonReply->setMaximumSize( QSize( 80, 24 ) );

    // buttonReply->setAutoDefault( TRUE );
    layout2->addWidget( buttonReply );

    buttonReplyAll = new ShapeButton( this, sPicsPath + "bt_img_allrepl_nor.bmp" );
    buttonReplyAll->setPressedShape( sPicsPath + "bt_img_allrepl_down.bmp" );
    buttonReplyAll->setMouseOverShape( sPicsPath + "bt_img_allrepl_ov.bmp" );
    buttonReplyAll->setMinimumSize( QSize( 80, 24 ) );
    buttonReplyAll->setMaximumSize( QSize( 80, 24 ) );

    // buttonReplyAll->setAutoDefault( TRUE );
    // buttonReplyAll->setDefault( TRUE );
    layout2->addWidget( buttonReplyAll );

    buttonForward = new ShapeButton( this, sPicsPath + "bt_img_forw_nor.bmp" );
    buttonForward->setPressedShape( sPicsPath + "bt_img_forw_down.bmp" );
    buttonForward->setMouseOverShape( sPicsPath + "bt_img_forw_ov.bmp" );
    buttonForward->setMinimumSize( QSize( 80, 24 ) );
    buttonForward->setMaximumSize( QSize( 80, 24 ) );

    // buttonForward->setAutoDefault( TRUE );
    layout2->addWidget( buttonForward );

    buttonDelete = new ShapeButton( this, sPicsPath + "bt_img_del_nor.bmp" );
    buttonDelete->setPressedShape( sPicsPath + "bt_img_del_down.bmp" );
    buttonDelete->setMouseOverShape( sPicsPath + "bt_img_del_ov.bmp" );
    buttonDelete->setMinimumSize( QSize( 60, 24 ) );
    buttonDelete->setMaximumSize( QSize( 60, 24 ) );

    // buttonDelete->setIconSet( QIconSet( image1 ) );
    layout2->addWidget( buttonDelete );
    layout6->addLayout( layout2 );
    MemoPopupLayout->addLayout( layout6 );
    languageChange();
    resize( QSize(400, 250).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
MemoPopup::~MemoPopup() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void MemoPopup::languageChange() {
    setCaption( trUtf8( "\xeb\xb0\x9b\xec\x9d\x80\xec\xaa\xbd\xec\xa7\x80" ) );
    textLabel1->setText( trUtf8( "\x3c\x62\x3e\xeb\xb3\xb4\xeb\x82\xb8\xec\x82\xac\xeb\x9e\x8c\x3c\x2f\x62\x3e\x20\x3a"
                                 "" ) );
    labelSender->setText( tr( "textLabel2" ) );
    textLabel3->setText( trUtf8( "\x3c\x62\x3e\xeb\xb0\x9b\xeb\x8a\x94\xec\x82\xac\xeb\x9e\x8c\x3c\x2f\x62\x3e\x20\x3a"
                                 "" ) );
    labelReceiver->setText( tr( "textLabel4" ) );
    labelMemo->setText( tr( "textLabel5" ) );
    // buttonReply->setText( tr( "Repl&y" ) );
    // buttonReply->setAccel( QKeySequence( tr( "Alt+Y" ) ) );
    // buttonReplyAll->setText( tr( "ReplyAll" ) );
    // buttonReplyAll->setAccel( QKeySequence( QString::null ) );
    // buttonForward->setText( tr( "Forward" ) );
    // buttonForward->setAccel( QKeySequence( QString::null ) );
    // buttonDelete->setText( QString::null );
}

#include "memopopupinterface.moc"
