/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kaction.h>
#include <kactioncollection.h>

#include "memopopupview.h"
#include "../util/emoticon.h"
#include "../util/common.h"

MemoPopupView::MemoPopupView(QWidget *parent, const char *name)
        : MemoPopup(parent, name),
        m_pEmoticon(0),
        m_sOrigText( QString::null ),
        m_sUUID( QString::null ),
        m_sTitle( QString::null ),
        m_sDate( QString::null ),
        m_dMemoHeader(0)	 {
    connect(buttonReply, SIGNAL( clicked() ), SLOT( slotReply() ) );
    connect(buttonReplyAll, SIGNAL( clicked() ), SLOT( slotReplyAll() ) );
    connect(buttonForward, SIGNAL( clicked() ), SLOT( slotForward() ) );
    connect(buttonDelete, SIGNAL( clicked() ), SLOT( slotDelete() ) );

    if (!m_pEmoticon)
        m_pEmoticon = Emoticon::instance();
    config = kapp->config();
    config->setGroup( "MemoPopup" );
    resize( config->readSizeEntry("Size", &QSize( 480,320 ) ) );

    /// 쪽지 보내기 단축
    KActionCollection *ac =  new KActionCollection ( this );
    new KAction( i18n("&Send1"), "memo_send1", KShortcut( Qt::CTRL + Qt::Key_R ) , this, SLOT( slotReply() ), ac, "memo_send1" );
    new KAction( i18n("&Send2"), "memo_send2", KShortcut( Qt::CTRL + Qt::Key_F ) , this, SLOT( slotForward() ), ac, "memo_send2" );
    new KAction( i18n("&Send3"), "memo_send3", KShortcut( Qt::CTRL + Qt::SHIFT + Qt::Key_R ) , this, SLOT( slotReplyAll() ), ac, "memo_send3" );
    new KAction( i18n("&Send4"), "memo_send4", KShortcut( Qt::Key_Escape ) , this, SLOT( close() ), ac, "memo_send4" );
}


MemoPopupView::~MemoPopupView() {
    config->writeEntry("Size", size() );
    config->sync();
}

void MemoPopupView::setSender(const QString &sSender) {
    labelSender->setText(sSender);
}

void MemoPopupView::setReceiver(const QString &sReceiver) {
    labelReceiver->setText(sReceiver);
}

void MemoPopupView::setBody(const QString &sBody) {
    QString sTemp( sBody );
    // sTemp.replace("&", "&amp;" );
    // sTemp.replace("<", "&lt;");
    // sTemp.replace(">", "&gt;");
    /*! 원문 저장 */
    Common::fixOutString( sTemp );
    sTemp.replace("\n", "<br>");
    labelMemo->setText( sTemp );

    m_sOrigText = "From: ";
    m_sOrigText += labelSender->text();
    m_sOrigText += "<br>";
    m_sOrigText += "To: ";
    m_sOrigText += labelReceiver->text();
    m_sOrigText += "<br>";
    m_sOrigText += "Sent: ";
    m_sOrigText += m_sDate;
    m_sOrigText += "<br>";
    m_sOrigText += "Subject: ";
    m_sOrigText += m_sTitle;
    m_sOrigText += "<br>";
    m_sOrigText += sBody;
}

void MemoPopupView::slotReply() {
    event_type = RE;
    emit replyMemo( RE, labelSender->text(), m_sOrigText, m_dMemoHeader );
    close();
}

void MemoPopupView::slotReplyAll() {
    event_type = AL;
    emit replyMemo( AL, labelSender->text() + ";" + labelReceiver->text(), m_sOrigText, m_dMemoHeader );
    close();
}

void MemoPopupView::slotForward() {
    event_type =FW;
    emit replyMemo( FW, "", m_sOrigText, m_dMemoHeader );
    close();
}

void MemoPopupView::slotDelete() {
    int result = KMessageBox::  questionYesNo(this, QString::fromUtf8("쪽지를 삭제 하시겠습니까?"), i18n("쪽지 삭제") );

    if ( result == KMessageBox::No ) return;

    emit deleteMemo( m_sUUID );
    close();
}

void MemoPopupView::closeEvent(QCloseEvent * e) {
    emit closeMemoPopup( this );
    MemoPopup::closeEvent( e );
}

void MemoPopupView::setTitle(const QString & sTitle) {
    m_sTitle = sTitle;
    Common::fixOutString( m_sTitle );
#if 0
    kdDebug() << "XXXXXXXXXXXXx [" << m_sTitle << "]" << endl;
#endif
}


#include "memopopupview.moc"
