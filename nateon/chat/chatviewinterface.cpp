/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qevent.h>
#include <qpixmap.h>
#include <qdragobject.h>
#include <qimage.h>
#include <qdir.h>
#include <qapplication.h>

#include "chatviewinterface.h"
#include "knateoncommon.h"

MyEdit::MyEdit(QWidget * parent, const char * name)
        : QTextEdit(parent, name) {
    setAcceptDrops( TRUE );
    connect( this, SIGNAL( textChanged () ) , SLOT( slotCheckLength() ) );
}

MyEdit::~ MyEdit() {
}

void MyEdit::slotCheckLength() {
    if ( length() >= 4096 ) {
        // QClipboard *cb = QApplication::clipboard();
        setText( text().left( 4095 ) );
        moveCursor ( MoveEnd, FALSE );
    }
}

void MyEdit::keyPressEvent(QKeyEvent * e) {
    /*!
      키보드에서 순수 Return키만 보내기를 하도록 함.
      Retrun 조합키는 개행으로 바꿈.
    */
    if ( e->key() == Key_Return && (e->state() & ControlButton) ) { /*! Ctrl + Return */
        insert("\n");
    } else if ( e->key() == Key_Return && (e->state() & ShiftButton) ) { /*! Shift + Return */
        insert("\n");
    } else if ( e->key() == Key_Return && (e->state() & AltButton) ) { /*! Alt + Return */
        insert("\n");
    } else {
        if ( e->key() == Key_Return ) { /*! Only Return */
            if ( text().length() > 0 ) {
                e->ignore();
                emit onlyReturn();
            }
        } else {
            QTextEdit::keyPressEvent(e);
        }
    }
}

void MyEdit::contentsDragMoveEvent( QDragMoveEvent *e ) {
    e->acceptAction( e->action() == QDropEvent::Copy );
}

void MyEdit::contentsDragEnterEvent(QDragEnterEvent * e) {
    if ( QTextDrag::canDecode( e )
            || QImageDrag::canDecode( e )
            || QUriDrag::canDecode( e ) ) {
        e->accept();
    }

    // Give the user some feedback...
    QString t;
    const char *f;
    for ( int i=0; (f=e->format( i )); i++ ) {
        if ( *(f) ) {
            if ( !t.isEmpty() )
                t += "\n";
            t += f;
        }
    }
    // emit message( t );
    // setBackgroundColor(white);
}

void MyEdit::contentsDropEvent(QDropEvent * e) {
#ifdef NETDEBUG
    kdDebug() << "XXXXXXXXXXXXXXXXXXXXX" <<endl;
#endif

    QStrList strings;
    QString sText;
    QImage img;
    if ( QUriDrag::decode( e, strings ) ) {
        QString m("Full URLs:\n");
        for (const char* u=strings.first(); u; u=strings.next())
            m = m + "   " + u + '\n';

        QStringList files;
        if ( QUriDrag::decodeLocalFiles( e, files ) ) {
            m += "Files:\n";
            for (QStringList::Iterator i=files.begin(); i!=files.end(); ++i)
                m = m + "   " + QDir::convertSeparators(*i) + '\n';
        }
        /*! 파일을 보낸다. */
        emit sendFiles( files );
    } else if ( QTextDrag::decode( e, sText ) ) {
        append( sText );
    }
    /* else if ( QImageDrag::decode( e, img ) ) {
    	append( img );
    } */
}


/*
 *  Constructs a ChatQW as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
/**
 *
 * @param parent
 * @param name
 * @param fl
 */
ChatQW::ChatQW( QWidget* parent, const char* name, WFlags fl )
        : QWidget( parent, name, fl ),
        lockID(0) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    sPicsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" ) ;

    if ( !name )
        setName( "ChatQW" );
    setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    setIcon( QPixmap(sPicsPath + "hi16-app-knateon.png") );
    setFocusPolicy( QWidget::WheelFocus );
    ChatQWLayout = new QVBoxLayout( this, 0, 0, "ChatQWLayout");

    layout16 = new QVBoxLayout( 0, 0, 0, "layout16");

    file = new QPopupMenu( this );

    file->insertItem( /*p1,*/ UTF8("대화 내용 저장 하기(&S)"),  this, SLOT( slotSave() ), 0);
    file->insertSeparator();
    file->insertItem( /*p2,*/ UTF8("받은 파일 폴더 열기(&R)"), this, SLOT( slotFileFolder() ), 0 );
    file->insertItem( /*p2,*/ UTF8("지난 대화 보기(&L)"), this, SLOT( slotShowChatLog() ), 0 );
    file->insertItem( /*p2,*/ UTF8("파일전송창 열기(&T)"), this, SLOT( slotShowTransfer() ), 0 );
    file->insertSeparator();
    file->insertItem( /*p2,*/ UTF8("창 닫기(&X)"), this, SLOT( slotClose() ), Key_Escape );

    action = new QPopupMenu( this );
    m_nInvite = action->insertItem( /*p1,*/ UTF8("초대하기(&I)"),  this, SLOT( slotInvite() ), 0);
    action->insertSeparator();
    m_nSendMemo = action->insertItem( /*p1,*/ UTF8("쪽지 보내기(&N)"),  this, SLOT( slotSendMemo() ), CTRL+Key_J);
    m_nSendFile = action->insertItem( /*p1,*/ UTF8("파일 보내기(&F)"),  this, SLOT( slotSendFile() ), CTRL+Key_F);

    buddy = new QPopupMenu( this );
    buddy->insertItem( /*p1,*/ UTF8("프로필 보기"),  this, SLOT( slotShowProfile() ), 0);
    buddy->insertSeparator();
    m_nAddBuddy = buddy->insertItem( /*p1,*/ UTF8("친구 추가(&A)"),  this, SLOT( slotAddBuddy() ), 0);
    lockID = buddy->insertItem( /*p1,*/ UTF8("친구 차단(&X)"),  this, SLOT( slotLock() ), 0);

    status = new QPopupMenu( this );
    status->setCheckable ( TRUE );

    ag = new QActionGroup( this, 0 );
    ag->setExclusive( TRUE );

    online = new QAction( UTF8("온라인"), QPixmap( sPicsPath + "main_list_state_online.png" ), 0, 0, ag, 0, ag->isExclusive() );
    away = new QAction( UTF8("자리 비움"), QPixmap( sPicsPath + "main_list_state_vacant.png" ), 0, 0, ag, 0, ag->isExclusive() );
    busy = new QAction( UTF8("다른 용무 중"), QPixmap( sPicsPath + "main_list_state_otherbusiness.png" ), 0, 0, ag, 0, ag->isExclusive() );
    phone = new QAction( UTF8("통화 중"), QPixmap( sPicsPath + "main_list_state_onphone.png" ), 0, 0, ag, 0, ag->isExclusive() );
    meeting = new QAction( UTF8("회의 중"), QPixmap( sPicsPath + "main_list_state_meeting.png" ), 0, 0, ag, 0, ag->isExclusive() );
    offline = new QAction( UTF8("오프라인 표시"), QPixmap( sPicsPath + "main_list_state_offline.png" ), 0, 0, ag, 0, ag->isExclusive() );

#if 0
    status->insertItem( QPixmap( sPicsPath + "main_list_state_online.png" ), UTF8("온라인"),  this, SLOT( slotOnline() ), 0);
    status->insertItem( QPixmap( sPicsPath + "main_list_state_vacant.png" ), UTF8("자리 비움"),  this, SLOT( slotAway() ), 0);
    status->insertItem( QPixmap( sPicsPath + "main_list_state_otherbusiness.png" ), UTF8("다른 용무 중"),  this, SLOT( slotBusy() ), 0);
    status->insertItem( QPixmap( sPicsPath + "main_list_state_onphone.png" ), UTF8("통화 중"),  this, SLOT( slotPhone() ), 0);
    status->insertItem( QPixmap( sPicsPath + "main_list_state_meeting.png" ), UTF8("회의 중"),  this, SLOT( slotMeeting() ), 0);
    status->insertItem( QPixmap( sPicsPath + "main_list_state_offline.png" ), UTF8("오프라인 표시"),  this, SLOT( slotOffline() ), 0);
#endif
    ag->addTo( status );

    connect( ag, SIGNAL( selected ( QAction * ) ), SLOT( slotStatus( QAction * ) ) );

    setup = new QPopupMenu( this );
    setup->insertItem( /*p1,*/ UTF8("내 상태 설정(&C)"), status, 0);
    setup->insertItem( /*p1,*/ UTF8("내 대화명 설정(&N)"),  this, SLOT( slotChangeNick() ), 0);
    setup->insertItem( /*p1,*/ UTF8("내 프로필 설정(&P)"),  this, SLOT( slotEditMyProfile() ), 0);
    setup->insertSeparator();
    nAlwaysTopID = setup->insertItem( /*p1,*/ UTF8("항상 위(&T)"),  this, SLOT( slotAlwaysTop() ), 0);
    setup->insertItem( /*p1,*/ UTF8("환경 설정(&S)"),  this, SLOT( slotSetup() ), Key_F11);

    help = new QPopupMenu( this );
    help->insertItem( /*p1,*/ UTF8("네이트온 홈(&O)"),  this, SLOT( slotGoNateonHome() ), 0);
    help->insertItem( /*p1,*/ UTF8("싸이월드 홈(&C)"),  this, SLOT( slotGoCyworldHome() ), 0);
    help->insertItem( /*p1,*/ UTF8("네이트 홈(&N)"),  this, SLOT( slotGoNateDotComHome() ), 0);
    help->insertSeparator();
    help->insertItem( /*p1,*/ UTF8("네이트온 이용 가이드(&H)"),  this, SLOT( slotGoHelp() ), Key_F1);
    help->insertItem( /*p1,*/ UTF8("네이트온 미니홈피(&M)"),  this, SLOT( slotGoNateonMiniHompy() ), 0);
    help->insertItem( /*p1,*/ UTF8("네이트온 HotTip(&I)"),  this, SLOT( slotGoHotTip() ), 0);
    help->insertItem( /*p1,*/ UTF8("네이트온 FAQ(&F)"),  this, SLOT( slotGoFaq() ), 0);

    helpMenu_ = new KHelpMenu( this, KGlobal::instance()->aboutData(), false);

    help->insertItem( /*p1,*/ UTF8("네이트온 정보(&V)"),  this, SLOT( slotInfo() ), 0);

    menu = new QMenuBar( this );
    menu->setFixedHeight ( fontMetrics().height() + 10 );

    menu->insertItem( UTF8("파일(&F)"), file );
    menu->insertItem( UTF8("동작(&A)"), action );
    menu->insertItem( UTF8("친구(&B)"), buddy );
    menu->insertItem( UTF8("설정(&T)"), setup );
    menu->insertItem( UTF8("도움말(&H)"), help );

    layout16->addWidget( menu );

    frame4 = new QFrame( this, "frame4" );
#if 0
    frame4->setMinimumSize( QSize( 0, 50 ) );
    frame4->setMaximumSize( QSize( 32767, 50 ) );
#endif
    frame4->setPaletteBackgroundPixmap( QPixmap(sPicsPath + "list_title_bluebg.bmp") );
    frame4->setFrameShape( QFrame::NoFrame );
    frame4->setFrameShadow( QFrame::Plain );
    frame4->setFixedHeight ( 50 );

    frame4Layout = new QHBoxLayout( frame4, 0, -1, "frame4Layout");

    spacer4 = new QSpacerItem( 5, 10, QSizePolicy::Fixed, QSizePolicy::Minimum );
    frame4Layout->addItem( spacer4 );
    // frame4Layout->setResizeMode( QLayout::Fixed );

    inviteButton = new ShapeButton( frame4, sPicsPath + "chw_ico_invite.bmp" );
    inviteButton->setMinimumSize( QSize( 35, 35 ) );
    inviteButton->setMaximumSize( QSize( 35, 35 ) );
    inviteButton->setCursor(Qt::PointingHandCursor);
    inviteButton->setPressedShape( sPicsPath + "chw_ico_invite_down.bmp" );
    inviteButton->setMouseOverShape( sPicsPath + "chw_ico_invite_ov.bmp" );
    QToolTip::add( inviteButton, UTF8("대화 상대를 초대합니다.") );
    frame4Layout->addWidget( inviteButton );

    fileSendButton = new ShapeButton( frame4, sPicsPath + "chw_ico_sendfile.bmp" );
    fileSendButton->setMinimumSize( QSize( 35, 35 ) );
    fileSendButton->setMaximumSize( QSize( 35, 35 ) );
    fileSendButton->setCursor(Qt::PointingHandCursor);
    fileSendButton->setPressedShape( sPicsPath + "chw_ico_sendfile_down.bmp" );
    fileSendButton->setMouseOverShape( sPicsPath + "chw_ico_sendfile_ov.bmp" );
    QToolTip::add( fileSendButton, UTF8("대화 상대에게 파일을 보냅니다.") );
    frame4Layout->addWidget( fileSendButton );

    spacer5 = new QSpacerItem( 5, 10, QSizePolicy::Expanding, QSizePolicy::Minimum );
    frame4Layout->addItem( spacer5 );

    layout16->addWidget( frame4 );

///
    frame10 = new QFrame( this, "frame10" );
    frame10->setMinimumSize( QSize( 0, 20 ) );
    frame10->setMaximumSize( QSize( 32767, 20 ) );
    frame10->setPaletteBackgroundPixmap( QPixmap( sPicsPath + "chw_nameid_bg.bmp" ) );
    frame10->setFrameShape( QFrame::NoFrame );
    frame10->setFrameShadow( QFrame::Plain );

    frame10Layout = new QHBoxLayout( frame10, 0, 0, "frame4Layout");

    frame10Layout->addItem( spacer4 );

    statusWidget = new ShapeWidget( frame10, sPicsPath + "main_list_state_online.bmp");
    statusWidget->setMinimumSize( QSize( 18, 18 ) );
    statusWidget->setMaximumSize( QSize( 18, 18) );
    frame10Layout->addWidget( statusWidget );

    nicknameLabel = new QLabel( frame10, "nicknameLabel" );
    nicknameLabel->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    nicknameLabel->setMinimumSize( QSize( 300, 20 ) );
    nicknameLabel->setMaximumSize( QSize( 32767, 20 ) );
    nicknameLabel->setAlignment( Qt::SingleLine | Qt::BreakAnywhere );
    nicknameLabel->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Preferred );
    frame10Layout->addWidget( nicknameLabel, 1, 0 );

    frame10Layout->addItem( spacer4 );
#if 0
    /*!
     * 싸이홈피 보이는것 뺌.
     */
    hompyWidget = new ShapeWidget( frame10, sPicsPath + "main_list_hompy.bmp");
    hompyWidget->setMinimumSize( QSize( 18, 18 ) );
    hompyWidget->setMaximumSize( QSize( 18, 18) );
    frame10Layout->addWidget( hompyWidget );
#endif
    frame10Layout->addItem( spacer5 );
    layout16->addWidget( frame10 );
///

    statusLabel = new QLabel( this, "statusLabel" );
    statusLabel->setPaletteBackgroundColor( QColor( 255, 255, 225 ) );
    statusLabel->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    statusLabel->setFrameShape( QLabel::Box );
    statusLabel->setLineWidth( 1 );
    /*! <<< */
    statusLabel->setScaledContents( false );
    statusLabel->setAutoResize( false );
    statusLabel->hide();
    layout16->addWidget( statusLabel );

    s1 = new QSplitter( QSplitter::Vertical, this , "main" );
    s1->setOpaqueResize( TRUE );

    ChatViewQTE = new TextMiniBrowser( s1, "ChatViewQTE" );
    ChatViewQTE->setPaletteBackgroundColor( QColor( 249, 253, 255 ) );
    ChatViewQTE->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    ChatViewQTE->setFrameShape( QTextEdit::NoFrame );
    ChatViewQTE->setFrameShadow( QTextEdit::Plain );
    ChatViewQTE->setWordWrap( QTextEdit::WidgetWidth );
    /*!
     * Anywhere 는 <br> 태그가 먹지 않아서 바꿈.
     */
    ChatViewQTE->setWrapPolicy( QTextEdit::AtWordOrDocumentBoundary /* Anywhere */ );
    ChatViewQTE->resize(0, 240);
    // connect( ChatViewQTE, SIGNAL( repaintChanged () ), SLOT( slotViewUpdate() ) );

    QWidget* privateLayoutWidget = new QWidget( s1, "layout2" );
    layout117 = new QVBoxLayout( privateLayoutWidget, 0, 0, "layout2");
    s1->setResizeMode ( privateLayoutWidget, QSplitter::KeepSize );
    frame5 = new QFrame( privateLayoutWidget, "frame5" );
    frame5->setMinimumSize( QSize( 0, 25 ) );
    frame5->setMaximumSize( QSize( 32767, 25 ) );
    frame5->setPaletteBackgroundPixmap( QPixmap(sPicsPath + "chw_bg_bar_icon.bmp")  );

    frame5->setFrameShape( QFrame::NoFrame );
    frame5->setFrameShadow( QFrame::Plain );

    frame5Layout = new QHBoxLayout( frame5, 0, 0, "frame5Layout");
    frame5Layout->addItem( spacer4 );

    emoticonButton = new ShapeButton( frame5, sPicsPath + "chw_emoticon.bmp" );
    // emoticonButton->move(0,3);
    emoticonButton->setMinimumSize( QSize( 30, 20 ) );
    emoticonButton->setMaximumSize( QSize( 30, 20 ) );
    emoticonButton->setCursor(Qt::PointingHandCursor);
    emoticonButton->setPressedShape( sPicsPath + "chw_emoticon_down.bmp" );
    emoticonButton->setMouseOverShape( sPicsPath + "chw_emoticon_ov.bmp" );
    QToolTip::add( emoticonButton, UTF8("이모티콘") );
    frame5Layout->addWidget( emoticonButton );

    frame5Layout->addItem( spacer4 );

    fontButton = new ShapeButton( frame5, sPicsPath + "chw_font.bmp" );
    // fontButton->move(0,3);
    fontButton->setMinimumSize( QSize( 30, 20 ) );
    fontButton->setMaximumSize( QSize( 30, 20 ) );
    fontButton->setCursor(Qt::PointingHandCursor);
    fontButton->setPressedShape( sPicsPath + "chw_font_down.bmp" );
    fontButton->setMouseOverShape( sPicsPath + "chw_font_ov.bmp" );
    QToolTip::add( fontButton, UTF8("글꼴") );
    frame5Layout->addWidget( fontButton );

    frame5Layout->addItem( spacer4 );

    fontColorButton = new ShapeButton( frame5, sPicsPath + "chw_fontcolor.bmp" );
    // fontColorButton->move(0,3);
    fontColorButton->setMinimumSize( QSize( 30, 20 ) );
    fontColorButton->setMaximumSize( QSize( 30, 20 ) );
    fontColorButton->setCursor(Qt::PointingHandCursor);
    fontColorButton->setPressedShape( sPicsPath + "chw_fontcolor_down.bmp" );
    fontColorButton->setMouseOverShape( sPicsPath + "chw_fontcolor_ov.bmp" );
    QToolTip::add( fontColorButton, UTF8("글꼴 색상") );
    frame5Layout->addWidget( fontColorButton );

    frame5Layout->addItem( spacer5 );
    layout117->addWidget( frame5 );



    frame3 = new QFrame( privateLayoutWidget, "frame3" );
    frame3->setPaletteBackgroundColor( QColor( 249, 253, 255 ) );
    frame3->setFrameShape( QFrame::NoFrame );
    frame3->setFrameShadow( QFrame::Plain );
    frame3Layout = new QGridLayout( frame3, 1, 1, 5, 6, "frame3Layout");

    layout7 = new QVBoxLayout( 0/*frame3*/, 0, 0, "layout7");
    spacer2_2 = new QSpacerItem( 20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout7->addItem( spacer2_2 );

    SendQPB = new ShapeButton( frame3, sPicsPath + "chw_bt_send_nor.bmp" );
    SendQPB->setMinimumSize( QSize( 60, 33 ) );
    SendQPB->setMaximumSize( QSize( 60, 33 ) );
    SendQPB->setPressedShape( sPicsPath + "chw_bt_send_down.bmp" );
    SendQPB->setMouseOverShape( sPicsPath + "chw_bt_send_ov.bmp" );
    /*! 초기 보내기 버튼 Disable */
    SendQPB->setDisabled();
    layout7->addWidget( SendQPB );
    spacer2 = new QSpacerItem( 20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout7->addItem( spacer2 );

    frame3Layout->addLayout( layout7, 0, 1 );

    ChatEditQTE = new MyEdit( frame3, "ChatEditQTE" );
    ChatEditQTE->setPaletteBackgroundColor( QColor( 249, 253, 255 ) );
    ChatEditQTE->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    ChatEditQTE->setBackgroundOrigin( QTextEdit::ParentOrigin );
    ChatEditQTE->setFrameShape( QTextEdit::NoFrame );
    ChatEditQTE->setFrameShadow( QTextEdit::Plain );
    ChatEditQTE->setWordWrap( QTextEdit::WidgetWidth );
    ChatEditQTE->setWrapPolicy( QTextEdit::AtWordOrDocumentBoundary /* Anywhere */ );

    frame3Layout->addWidget( ChatEditQTE, 0, 0 );
    layout117->addWidget( frame3 );

    connect( ChatEditQTE, SIGNAL( textChanged () ), SLOT( slotChangeEdit() ) );

    layout16->addWidget( s1 );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setPaletteForegroundColor( QColor( 0x53, 0x53, 0x53 ) );
    textLabel1->setFrameShape( QLabel::StyledPanel );
    textLabel1->setFrameShadow( QLabel::Sunken );
    layout16->addWidget( textLabel1 );
    ChatQWLayout->addLayout( layout16 );
    languageChange();
    resize( QSize(340, 394).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
ChatQW::~ChatQW() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void ChatQW::languageChange() {
    setCaption( tr( "Chat Window" ) );
    QToolTip::add( this, QString::null );
    nicknameLabel->setText( tr( "Doo-Hyun Jang (ZZANG - GA)" ) );
    statusLabel->setText( tr( "user is away." ) );
    ChatEditQTE->setText( tr( "dsfsdfdsfdsd" ) );
    textLabel1->setText( tr( "textLabel1" ) );
}


void ChatQW::hideEvent(QHideEvent * e) {
    QWidget::hideEvent(e);
}

void ChatQW::slotSave() {
}

void ChatQW::slotFileFolder() {
}

void ChatQW::slotShowChatLog() {
}

void ChatQW::slotClose() {
}

void ChatQW::slotInvite() {
}

void ChatQW::slotSendMemo() {
}

void ChatQW::slotSendFile() {
}

void ChatQW::slotShowProfile() {
}

void ChatQW::slotAddBuddy() {
}

void ChatQW::slotLock() {
}

void ChatQW::slotChangeNick() {
}

void ChatQW::slotEditMyProfile() {
}

void ChatQW::slotAlwaysTop() {
}

void ChatQW::slotSetup() {
}

void ChatQW::slotGoNateonHome() {
}

void ChatQW::slotGoCyworldHome() {
}

void ChatQW::slotGoNateDotComHome() {
}

void ChatQW::slotGoHelp() {
}

void ChatQW::slotGoNateonMiniHompy() {
}

void ChatQW::slotGoHotTip() {
}

void ChatQW::slotGoFaq() {
}

void ChatQW::slotInfo() {
}

void ChatQW::slotStatus(QAction * a) {
    if ( a == online ) {
#ifdef NETDEBUG
        kdDebug() << "is online" << endl;
#endif
        emit changeStatus( 0 );
    } else if ( a == away ) {
#ifdef NETDEBUG
        kdDebug() << "is Away" << endl;
#endif
        emit changeStatus( 1 );
    } else if ( a == busy ) {
#ifdef NETDEBUG
        kdDebug() << "is Busy" << endl;
#endif
        emit changeStatus( 2 );
    } else if ( a == phone ) {
#ifdef NETDEBUG
        kdDebug() << "is Phone" << endl;
#endif
        emit changeStatus( 3 );
    } else if ( a == meeting ) {
#ifdef NETDEBUG
        kdDebug() << "is Meeting" << endl;
#endif
        emit changeStatus( 4 );
    } else if ( a == offline ) {
#ifdef NETDEBUG
        kdDebug() << "is Offline" << endl;
#endif
        emit changeStatus( 5 );
    }
}

void ChatQW::slotChangeEdit() {
    if ( ChatEditQTE->length() > 0 ) {
        SendQPB->setEnabled();
    } else {
        SendQPB->setDisabled();
    }
}

void ChatQW::resizeEvent(QResizeEvent * e) {
#ifdef NETDEBUG
    kdDebug() << "Resize" << endl;
#endif
    QWidget::resizeEvent( e );
    ChatViewQTE->moveCursor ( QTextEdit::MoveEnd, FALSE );
}

void ChatQW::slotViewUpdate() {
#ifdef NETDEBUG
    kdDebug() << "View Update" << endl;
#endif
    ChatViewQTE->moveCursor ( QTextEdit::MoveEnd, FALSE );
}

void ChatQW::slotShowTransfer() {
    emit showTransfer();
}


#include "chatviewinterface.moc"
