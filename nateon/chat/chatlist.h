/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CHATLIST_H
#define CHATLIST_H

#include <qobject.h>

#include "chatview.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class ChatList : public QObject {
public:
    ChatList();
    ~ChatList();
    void addChatObj(ChatView* m_ChatView);
    void removeChatObj(ChatView* pChatView);
    ChatView* creatChatObj(int TID);
    ChatView* getChatViewByTID(int TID) const;
    ChatView* getChatViewByUID( QString sUID ) const;
    ChatView* getChatViewByAllUIDs( QStringList slAllUIDs ) const;
    QPtrList<ChatView> getChatList() const {
        return m_ChatObjs;
    }
    unsigned int count() const {
        return m_ChatObjs.count();
    }
    void clear() {
        m_ChatObjs.clear();
    }

private:
    QPtrList<ChatView> m_ChatObjs;
};

#endif
