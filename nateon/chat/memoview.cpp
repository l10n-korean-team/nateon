/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kaction.h>
#include <kactioncollection.h>
#include <qtooltip.h>

#include "knateoncommon.h"
#include "memoview.h"

MemoView::MemoView(QWidget *parent, const char *name)
        :MemoQW(parent, name),
        pEmoticonSelector(0),
        pAddBuddy(0),
        bCheck( TRUE ),
        event_type(NW),
        m_dMemoHeader(0) {
    config = kapp->config();
    textEdit2->setFocus();
    config->setGroup( "Memo" );
    resize( config->readSizeEntry("Size", &QSize( 480,320 ) ) );

}

MemoView::~ MemoView() {
    if (pEmoticonSelector) delete pEmoticonSelector;
}

void MemoView::slotViewEmoticonSelector() {
    if (!pEmoticonSelector) {
        pEmoticonSelector = new EmoticonSelector( this, "EmoticonSelector" );
        connect(pEmoticonSelector->toolButton1, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton2, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton3, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton4, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton5, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton6, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton7, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton8, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton9, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton10, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton11, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton12, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton13, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton14, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton15, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton16, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton17, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton18, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton19, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton20, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton21, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton22, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton23, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton24, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton25, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton26, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton27, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton28, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton29, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton30, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton31, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton32, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton33, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton34, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton35, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton36, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton37, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton38, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton39, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton40, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton41, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton42, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton43, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton44, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton45, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton46, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton47, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton48, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton49, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton50, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton51, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton52, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton53, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton54, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton55, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton56, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton57, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton58, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton59, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton60, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton61, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton62, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton63, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton64, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton65, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton66, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton67, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton68, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton69, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton70, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton71, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton72, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton73, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton74, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton75, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton76, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton77, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton78, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton79, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton80, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton81, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton82, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton83, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton84, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton85, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton86, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton87, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton88, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton89, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton90, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton91, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton92, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton93, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton94, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton95, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton96, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton97, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton98, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton99, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton100, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton101, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton102, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton103, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton104, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton105, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton106, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton107, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton108, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton109, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton110, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton111, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton112, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton113, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton114, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton115, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton116, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton117, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton118, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton119, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton120, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton121, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton122, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton123, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton124, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton125, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton126, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton127, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton128, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton129, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton130, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton131, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton132, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton133, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton134, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton135, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton136, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton137, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton138, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton139, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton140, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton141, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton142, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton143, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton144, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton145, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton146, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton147, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton148, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton149, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton150, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton151, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton152, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton153, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton154, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton155, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton156, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton157, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton158, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton159, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton160, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton161, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton162, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton163, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton164, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton165, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton166, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton167, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton168, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton169, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton170, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton171, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton172, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton173, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton174, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton175, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton176, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton177, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton178, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton179, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton180, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton181, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton182, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton183, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton184, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton185, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton186, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton187, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton188, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton189, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton190, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton191, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton192, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton193, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton194, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton195, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton196, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton197, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton198, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton199, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton200, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton201, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton202, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton203, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton204, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton205, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
    }

    int screen = kapp->desktop()->screenNumber();
    QRect screenSize = kapp->desktop()->screenGeometry(screen);

    if ( ( x() + ( width() / 2) ) > ( screenSize.width() / 2 ) )
        pEmoticonSelector->move( x() - pEmoticonSelector->width(), y() + ( height() - pEmoticonSelector->height() ) );
    else
        pEmoticonSelector->move( x() + width(), y() + ( height() - pEmoticonSelector->height() ) );
    if ( pEmoticonSelector->isShown() )
        pEmoticonSelector->hide();
    else
        pEmoticonSelector->show();
}

void MemoView::setReceiver(const QString &sReceiver) {
    lineEdit1->setText( sReceiver );

    QString sTooltip = sReceiver;
    int pos, cnt;

    for ( pos = 0, cnt = 1;; pos++, cnt++ ) {
        if ( ( pos = sTooltip.find( ';', pos, FALSE) ) < 0 ) break;
        if ( cnt % 3 == 0 ) {
            sTooltip.replace( pos, 1, "\n" );
        }
    }
    if ( cnt > 1 ) {
        QToolTip::add( lineEdit1, UTF8("받는 사람\n") + sTooltip );
    } else {
        QToolTip::remove( lineEdit1 );
    }
}

bool MemoView::initialize() {
    /// 이모티콘 선택 버튼 클릭시
    connect( emoticonTB, SIGNAL( clicked() ), this, SLOT( slotViewEmoticonSelector() ) );
    /// 버디 추가 버튼 클릭시.
    connect( pushButton3, SIGNAL( clicked() ), this, SLOT( slotViewAddBuddySelector() ) );
    /// 쪽지 보내기 클릭시
    connect( sendPB, SIGNAL( clicked() ), this, SLOT( slotSendMemo() ) );
    /// 쪽지 보내기 단축
    KActionCollection *ac =  new KActionCollection ( this );
    new KAction( i18n("&Send1"), "memo_send1", KShortcut( Qt::CTRL+Qt::Key_S) , this, SLOT( slotSendMemo() ), ac, "memo_send1" );
    new KAction( i18n("&Send2"), "memo_send2", KShortcut( Qt::ALT+Qt::Key_S) , this, SLOT( slotSendMemo() ), ac, "memo_send2" );
    new KAction( i18n("&Send3"), "memo_send3", KShortcut( Qt::CTRL+Qt::Key_Return) , this, SLOT( slotSendMemo() ), ac, "memo_send3" );
    new KAction( i18n("&Send3"), "memo_send4", KShortcut( Qt::Key_Escape ) , this, SLOT( close() ), ac, "memo_send4" );

    /*! 쪽지 개수 */
    connect( textEdit2, SIGNAL( textChanged () ), SLOT( slotCharCount() ) );
    /*! 폰트 선택 */
    connect( fontButton, SIGNAL( clicked() ), this, SLOT( slotFontDialog() ) );

    return true;
}

void MemoView::slotSendMemo() {
    if ( textEdit2->length() > 0 )
        emit sendMemo(this);
    else {
        KMessageBox::information( this, UTF8("쪽지 내용이 없습니다."), UTF8("쪽지 보내기") );
    }
}

void MemoView::slotViewAddBuddySelector() {
    if (!pAddBuddy) {
        pAddBuddy = new AddBuddySelector(this, "XXX");
        connect(pAddBuddy, SIGNAL( selectedBuddies(const QString&) ), this, SLOT( slotAddBuddies(const QString& ) ) );
    }

    emit addBuddyList(pAddBuddy);

    if (lineEdit1->text().length() > 0) {
        /// 선택된 QListView 채움.
        pAddBuddy->setSelectedBuddies( lineEdit1->text() );
        /// 석택되지 못한 QListView 목록 채움.
        pAddBuddy->setAllList();
    }
}

void MemoView::slotAddBuddies(const QString& sBuddies) {
    /// 처음에는 쪽지추가가 되는걸로 생각했는데..
    /// 나중에는 완전 갱신으로 바꿈.
    setReceiver( sBuddies );
}

void MemoView::setBody(QString sBody) {
    textEdit2->setText(sBody);
}

void MemoView::moveCursorHome() {
    textEdit2->moveCursor(QTextEdit::MoveHome, false);
}

void MemoView::slotCharCount() {
    if ( ( textEdit2->length() > 2000 ) ) {
        /* QClipboard *cb = QApplication::clipboard(); */
        textEdit2->setText( ( textEdit2->text() ).left(2000) );
        textEdit2->moveCursor ( QTextEdit::MoveEnd, FALSE );

        QMessageBox::warning(this,UTF8("알림"), UTF8("글자 제한 초과입니다. 메모 내용을 줄이시고, 나눠 보내시기 바랍니다.") );
    }

    QString sCount;
    sCount.sprintf("%04d/2000", ( textEdit2->text() ).length() );
    textLabel2->setText( sCount );
}

void MemoView::slotPutEmoticon(const QString & sText) {
    textEdit2->insert( sText );
    textEdit2->setActiveWindow ();
}

void MemoView::slotFontDialog() {
    bool ok;
    QFont font = QFontDialog::getFont( &ok, QFont( UTF8("굴림"), 10 ), this );

    if ( ok ) {
        textEdit2->setFont( font );
        // config->setGroup( "Memo" );
        config->writeEntry( "Font", font );
        config->sync();
    }
}

void MemoView::show() {
    // config->setGroup( "Memo" );
    QFont font = config->readFontEntry( "Font", &( QFont( UTF8("굴림"), 10 ) ) );
    textEdit2->setFont( font );

    MemoQW::show();
}

void MemoView::moveEvent(QMoveEvent * e) {
    MemoQW::moveEvent ( e );

    if ( pEmoticonSelector ) {
        if ( pEmoticonSelector->isShown() ) {
            int screen = kapp->desktop()->screenNumber();
            QRect screenSize = kapp->desktop()->screenGeometry(screen);

            if ( ( x() + ( width() / 2 ) ) > ( screenSize.width() / 2 ) )
                pEmoticonSelector->move( x() - pEmoticonSelector->width(), y() + ( height() - pEmoticonSelector->height() ) );
            else
                pEmoticonSelector->move( x() + width(), y() + ( height() - pEmoticonSelector->height() ) );
        }
    }
}

void MemoView::hideEvent(QHideEvent * e) {
    MemoQW::hideEvent( e );
    if ( pEmoticonSelector )
        if ( pEmoticonSelector->isShown() )
            pEmoticonSelector->hide();
}

void MemoView::closeEvent(QCloseEvent * e) {
    config->writeEntry("Size", size() );
    config->sync();
    emit closeMemoView( this );
    MemoQW::closeEvent( e );
}


#include "memoview.moc"
