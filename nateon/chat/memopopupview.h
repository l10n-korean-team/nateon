/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MEMOPOPUPVIEW_H
#define MEMOPOPUPVIEW_H

#include <qlabel.h>
#include <qpushbutton.h>
#include <kdebug.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <kapplication.h>
#include <kconfig.h>

#include "memopopupinterface.h"
#include "define.h"

class QLabel;
class QPushButton;
class Emoticon;
class Common;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class MemoPopupView : public MemoPopup {
    Q_OBJECT
public:
    // enum SendType { Reply, ReplyAll, Forward };

    MemoPopupView ( QWidget* parent = 0, const char *name = 0 );
    ~MemoPopupView();

    void setSender ( const QString &sSender );
    void setReceiver ( const QString &sReceiver );
    void setBody ( const QString &sBody );
    void setTitle ( const QString &sTitle );
    void setDate ( QString sDate ) {
        m_sDate = sDate;
    }
    void setUUID ( QString UUID ) {
        m_sUUID = UUID;
    }
    void setMemoHeader( NMStringDict *dict ) {
        m_dMemoHeader = dict;
    }

    NMStringDict *memoHeader() {
        return m_dMemoHeader;
    }

    MEMO_EVENT getEventType() {
        return event_type;
    }


private:
    MEMO_EVENT event_type;
    Emoticon* m_pEmoticon;
    /*!
     * 이모티콘 적용안한 원문
     * Reply에 쓰임.
     */
    QString m_sOrigText;
    QString m_sUUID;
    QString m_sTitle;
    QString m_sDate;
    KConfig* config;
    //! memo header
    NMStringDict *m_dMemoHeader;

protected:
    virtual void closeEvent ( QCloseEvent * e );

private slots:
    void slotReply();
    void slotReplyAll();
    void slotForward();
    void slotDelete();

signals:
    void replyMemo ( MEMO_EVENT, const QString& , const QString&, const NMStringDict * );
    void deleteMemo ( const QString & );
    void closeMemoPopup ( MemoPopupView * );
};

#endif
