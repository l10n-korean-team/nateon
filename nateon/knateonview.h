/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KNATEONVIEW_H
#define KNATEONVIEW_H

#include <kaction.h>
#include <kactionclasses.h>
#include <kpopupmenu.h>
#include <qlistview.h>
#include <kmainwindow.h>
#include <qlabel.h>
#include <kmessagebox.h>
#include <kstatusbar.h>
#include <qlabel.h>
#include <kled.h>
#include <kconfig.h>
#include <kdebug.h>
#include <kglobal.h>
#include <khelpmenu.h>
#include <kled.h>
#include <klocale.h>
#include <kmenubar.h>
#include <kpopupmenu.h>
#include <krun.h>
#include <kstdaction.h>
#include <ktoolbar.h>
#include <kurl.h>
#include <kapp.h>
#include <qdir.h>
#include <kaboutdialog.h>
#include <kaboutdata.h>

#include <dialog/webviewer.h>

#include "utils.h"

//forward declaration
class QHBox;
class QLabel;
class KActionMenu;
class KConfig;
class KHelpMenu;
class KLed;
class KMessTest;
class KPopupMenu;
class KSelectAction;
class KToggleAction;
class KAction;
class KActionMenu;

class KNateonInterface : public KMainWindow {
    Q_OBJECT

public:
    KNateonInterface(QWidget* parent = 0, const char* name = 0)
            : KMainWindow(parent, name) {
        setMinimumSize( QSize( 350, 600 ) );
    };
    ~KNateonInterface() {};
    void OfflineEnableMenu();
    void OnlineEnableMenu();
    void setCurrentStatus( int nID ) {
        pChangestatusSelectAction->setCurrentItem( nID );
    }

public slots:
    void slotSetGroupList(QStringList &Group);
    void slotUpdateStatusText( const QString &sText ) {
        m_pStatusLabel->setText( sText );
        m_pStatusLabel->update();
    }

protected:
    bool initialize();

    void createActions();
    void createMenus();

    void createFileMenu();
    void createActionsMenu();
    void createFriendMenu();
    void createSetupMenu();
    void createHelpMenu();
    bool initContactPopup();

    virtual void readProperties(KConfig *config);
    virtual void saveProperties(KConfig *config);

    KPopupMenu*     fileMenu;
    KPopupMenu*     ActionMenu;
    KPopupMenu*     friendMenu;
    KPopupMenu*     setupMenu;
    KPopupMenu*     helpMenu;

    // file menu
    KAction*        pLogoutAction;     // 로그아웃.
    KAction*        pOpenchatboxAction;    // 대화함 열기.
    // KAction*     pOpenvoiceboxAction;   // 화상/음성대화 내용 보기.
    KAction*        pOpenfileboxAction;    // 받은 파일 폴더 열기.
    KAction*        pOpendownboxAction;    // 받은 파일 폴더 열기.
    // KAction*     pLockAction;           // 잠그기.
    KAction*        pHideAction;           // 숨기기.
    KAction*        pQuitAction;           // 종료.

    // action menu
    KAction*        pChatAction;           // 대화 하기.
    // KAction*     pMinichatAction;       // 미니대화 하기
    // KAction*     pVideochatAction;      // 화상대화 하기.
    // KAction*     pVoicechatAction;      // 음성대화 하기.
    // KAction*     pNatephoneAction;      // 네이트폰 하기.
    // KAction*     pSMSchatAction;        // 문자대화(SMS) 하기.
    KAction*        pSendmemoAction;       // 쪽지 보내.
    // KAction*     pSendactionmemoAction; // 액션쪽지 보내기.
    // KAction*     pSendSMSAction;        // SMS보내기.
    // KAction*     pSendmailAction;       // 메일보내기.
    KAction*        pSendfileAction;       // 파일보내기.
    // KAction*     pViewhotclipAction;    // 핫클립보기.
    // KAction*     pViewalarmhistoryAction;   // 알람히스토리 보기

    // friend menu
    KAction*        pAddfriendAction;          // 친구 추가.
    KSelectAction*  pCopyfriendSelectAction; // 친구 복사.
    KSelectAction*  pMovefriendSelectAction; // 친구 이동.slotSendMemo
    KAction*        pDeletefriendAction;       // 친구 삭제.
    KSelectAction*  pBuddynamingSelectAction;// 친구 보기 방식.
    KAction*	pLoadBuddyListAction;	// 친구목록 불러오기
    KAction*	pSaveBuddyListAction;	// 친구목록 저장하기
    /*
      KAction*    pViewnameAction;       // 친구 이름으로 보기.
      KAction*    pViewnickAction;       // 친구 대화명으로 보기.
      KAction*    pViewnameIDAction;     // 친구 이름+아이디로 보기.
      KAction*    pViewnamenickAction;   // 친구 이름+대회명으로 보기.
    */
    KSelectAction*  pSortlistSelectAction;   // 친구 정렬 방식.
    /*
      KAction*    pViewAllAction;        // 친구 전체 보기.
      KAction*    pViewConnectedAction;  // 접속한 친구만 보기.
      KAction*    pViewonoffAction;      // 친구 온라인/오프라인으로 보기.
    */
    // KAction*     pMakefriendAction;         // 친구 만들기.
    // KAction*     pSearchfriendAction;       // 친구 검색.
    // KAction*     pSavefriendlistAction;     // 친구 목록 저장하기.
    // KAction*     pLoadfriendlistAction;     // 친구 목록 불러오기.
    KAction*        pAddgroupAction;           // 그룹 추가.
    KAction*        pRenamegroupAction;        // 그룹 이름 변경.
    KAction*        pDeletegroupAction;        // 그룹 삭제.

    // setup menu
    KSelectAction*  pChangestatusSelectAction;       // 내 상태 설정
    KAction*        pChangenickAction;         // 내 대화명 설정
    KAction*        pChangeprofileAction;      // 내 프로필 설정
    // KAction*     pSetawaymessageAction;     // 부제중 메시지 설정
    // KAction*     pWebcamsetupAction;        // 웹카메라 설정
    // KAction*     pVideovoicesetupAction;    // 화상/음성대화 설정 마법사
    KToggleAction*  pAlwaystopAction;          // 항상 위
    KAction*        pSetupAction;              // 환경 설정
    KAction*        pNetLogAction;              // 프로토콜 로그 보기.

    // help menu
    KAction*        pGonateonhomeAction;       // 네이트온 홈
    KAction*        pGocyworldhomeAction;      // 싸이월드 홈
    KAction*        pGonatedotcomAction;       // 네이트 홈
    KAction*        pGonateonhelpAction;       // 네이트온 이용 가이드
    KAction*        pGonateonminihompyAction;  // 네이트온 미니홈피
    KAction*        pGonateonhottipAction;     // 네이트온 HotTip
    KAction*        pGonateonfaqAction;        // 네이트온 FAQ
    KAction*        pGonateoninfo;             // 네이트온 정보

    KAction*        m_pShowProfile;
    KAction*        m_pConfigureNotifications;
    KActionMenu*    m_pConnectActionMenu;
    KActionMenu*    m_pSettingsActionMenu;


    KAction*        chatWithContact_;
    KActionMenu*    moveContactToGroup_;

    // Show the network window
    KAction*        m_pShowNetworkAction;
    KSelectAction*  m_pStatus;
    KSelectAction*  m_pViewMode;

protected slots:
    virtual void slotChangeStatusNumber( int nID );

private:
    QTimer*         m_pOnlineTimer;
    QLabel*         m_pStatusLabel;
    KLed*           m_pStatusProgress;
    QLabel*         m_pStatusTimer;
    KHelpMenu*			helpMenu_;

private slots:
    void menuQuit();
    void slotRightClickMenu(QListViewItem *item, const QPoint &point, int col);
    // void slotLogout();
    void slotOpenChatBox();
    void slotOpenFileBox();
    void slotSendMemo();
    /*
      void slotAddGroup();
      void slotRenameGroup();
    */
    void slotGoNateHome();
    void slotGoCyHome();
    virtual void slotGoNateDotCom();
    void slotNateonUsage();
    void slotGoNateonMiniHompy();
    void slotGoHotTip();
    void slotFAQ();
    void slotInfo();
    void slotAppClose();
    void slotMenuHide();

signals:
    void StartChat(QListViewItem& itemQLVI);
    // void slotForwardStartChat();
    void viewMemoWindow();
    /*
      void addGroup();
      void renameGroup();
    */
    /*
      void statusOnline();
      void statusAway();
      void statusBusy();
      void statusPhone();
      void statusMeeting();
      void statusOffline();
    */
};
#endif
