/***************************************************************************
                          idletimer.h  -  description
                             -------------------
    begin                : Tue Oct 22 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IDLETIMER_H
#define IDLETIMER_H

#include <qobject.h>

#include "xautolock.h"

// Forward declarations
class CurrentAccount;
class XAutoLock;

/**
 * @brief Wrapper for XAutoLock to fire idle-events based on account settings.
 *
 * The XAutoLock class is used to detect whether the user is currently idle.
 * The updateWatcher() method reads the account settings to determine
 * whether the idle detection should be active.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class IdleTimer : public QObject  {
    Q_OBJECT

public:
    // The constructor
    IdleTimer();
    // The destructor
    ~IdleTimer();

    void stopTimer(); // { watcher_->stopTimer(); }
    void startTimer(); // { watcher_->startTimer(); }
	void restartTimer();
	void setFakeIdle();
    bool isActive() {
        return watcher_->isActive();
    }

private: // Private attributes
    // The idle detector
    XAutoLock       *watcher_;

private slots: // Private slots
    // Echo the watcher's activity signal.
    void             slotActivity();
    // Echo the watcher's timeout.
    void             slotTimeout();
    // Reset the watcher based on the account's settings.
    void             updateWatcher();

signals: // Signals
    // Signal user activity when the watcher detects it.
    void             activity();
    // Signal user inactivity when the watcher detects it.
    void             timeout();
};

#endif
