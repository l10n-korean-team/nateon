/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CONTENTBASE_H
#define CONTENTBASE_H

#include <qlistview.h>

/**
   메신저 buddy리스트를 만들때 필요한 요소의 근본 클래스.
   ContactRoot(그룹)과 ContactBuddy(버디)를 파생클래스로 생성한다.

   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class ContactBase : public QListViewItem {
public:
    enum ItemType { Group, Buddy };
    ContactBase( QListView *parent = 0 );
    ContactBase( QListView *parent = 0, const char *name = 0 );
    ContactBase( QListView *parent = 0, const QString &s1 = 0 );
    ContactBase( QListView *parent = 0, const QString &s1 = 0, const QString &s2 = 0 );
    ContactBase( QListViewItem *parent = 0 );
    ContactBase( QListViewItem *parent = 0, const char *name = 0 );
    ContactBase( QListViewItem *parent = 0, const QString &s1 = 0 );

    ItemType getType() {
        return m_Type;
    }
    void setType( ItemType eType ) {
        m_Type = eType;
    }

private:
    ItemType m_Type;
};
#endif
