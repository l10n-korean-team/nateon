/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "contactbase.h"

ContactBase::ContactBase(QListView * parent) :
        QListViewItem( parent ) {

}
ContactBase::ContactBase(QListView * parent, const char * name) :
        QListViewItem( parent, name ) {
}

ContactBase::ContactBase(QListViewItem * parent) :
        QListViewItem( parent ) {
}

ContactBase::ContactBase(QListViewItem * parent, const char * name) :
        QListViewItem( parent, name ) {

}

ContactBase::ContactBase( QListView * parent, const QString &s1, const QString &s2 )
        : QListViewItem( parent, s1, s2 ) {
}

ContactBase::ContactBase(QListView * parent, const QString &s1)
        : QListViewItem( parent, s1 ) {
}


ContactBase::ContactBase(QListViewItem * parent, const QString &s1)
        : QListViewItem( parent, s1 ) {
}

