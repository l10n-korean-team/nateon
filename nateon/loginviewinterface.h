/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LOGINVIEWINTERFACE_H
#define LOGINVIEWINTERFACE_H
#include <qvariant.h>
#include <qpushbutton.h>
#include <qframe.h>
#include <qlabel.h>
#include <klineedit.h>
#include <kcombobox.h>
#include <kpassdlg.h>
#include <kpushbutton.h>
#include <qcheckbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include <qvariant.h>
#include <qpixmap.h>
#include <qwidget.h>
#include <kstandarddirs.h>
#include <kaboutdata.h>

#include "shapewidget.h"
#include "linklabel.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QFrame;
class QLabel;
class KLineEdit;
class KHistoryCombo;
class KPasswordEdit;
class KPushButton;
class QCheckBox;
class ShapeButton;
class LinkLabel;

class MyPasswordEdit : public /* KPasswordEdit */ QLineEdit {
    Q_OBJECT
public:
    MyPasswordEdit( QWidget* parent = 0, const char* name = 0 ):
            /* KPasswordEdit */QLineEdit( parent, name ) {}
protected:
    void keyPressEvent (QKeyEvent *e);
signals:
    void pressReturn();
};

class loginviewinterface : public QWidget {
    Q_OBJECT

public:
    loginviewinterface( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~loginviewinterface();

    QFrame* titleFrame;
    QLabel* titleLabel;

public slots:
    virtual void connectToDPLserver();

protected:
    QVBoxLayout* loginviewinterfaceLayout;
    QVBoxLayout* titleFrameLayout;
    QHBoxLayout* layout8;
    QSpacerItem* spacer12;
    QSpacerItem* spacer11;
    QPixmap image3;

private:
    QPixmap image0;
    QPixmap image1;
    QPixmap image2;

};

#endif // LOGINVIEWINTERFACE_H
