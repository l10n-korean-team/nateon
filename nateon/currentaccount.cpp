/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "currentaccount.h"
#include "buddy/buddylist.h"

CurrentAccount* CurrentAccount::pInstance(0);

CurrentAccount::CurrentAccount():
        m_sMyCyworldCMN("%00"),
        m_sMyNateCMN("%00"),
        m_sMyNateID("%00"),
        m_sMyCyworldID("%00"),
        m_sMyHome2ID("%00"),
        m_unHompyType(Cyworld) {
}


CurrentAccount::~CurrentAccount() {
}


/*!
  \fn CurrentAccount::copyAccount(const Account *pAccount)
*/
void CurrentAccount::copyAccount(const Account *pAccount) {
    Account::copyAccount( pAccount );
}


CurrentAccount* CurrentAccount::instance() {
    // If the instance is null, create a new current account and return that.
    if ( pInstance == 0 ) {
        pInstance = new CurrentAccount();
    }
    return pInstance;
}



// Store the contact list, maintained by the MsnNotificationConnection class.
// Allows other classes to access the contact list from this central class.
/*
  void CurrentAccount::setBuddyList(const BuddyList *pBuddyList)
  {
  m_pBuddyList = pBuddyList;
  }
*/

const QString CurrentAccount::getMyID() const {
    if ( getMyLoginType() == 'C' )
        return m_sMyCyworldID;
    else
        return m_sMyNateID;
}

#include "currentaccount.moc"
