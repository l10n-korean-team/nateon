/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TOASTWINDOW_H
#define TOASTWINDOW_H

#include <kstandarddirs.h>
#include <kapplication.h>
#include <kaboutdata.h>

#include <qtimer.h>
#include <qlabel.h>
#include <qpointarray.h>
#include <qcursor.h>

#include "shapewidget.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class ClickLabel : public QLabel {
    Q_OBJECT
public:
    ClickLabel(QWidget* parent, const char *name) : QLabel( parent, name ) {};
    ClickLabel(QWidget* parent, const char *name, WFlags f = 0) : QLabel( parent, name, f ) {};

protected:
    virtual void mouseMoveEvent( QMouseEvent *);
    virtual void mousePressEvent( QMouseEvent *);
    virtual void mouseReleaseEvent( QMouseEvent *);

signals:
    void clicked();
};


class ToastWindow : public ShapeWidget {
    Q_OBJECT
public:
    ToastWindow(QWidget *parent = 0, const char *name = 0);

    ~ToastWindow();
    void updateMask();
    void setAnchor(const QPoint& anchor);
    void setText( QString sMsg );
    void setRUrl( QString sUrl ) {
        m_sUrl = sUrl;
    }
    void show();

private:
    QPoint        m_anchor;
    ShapeWidget*  pLogo;
    ShapeButton*  pClose;
    ShapeWidget*  pCyLogo;
    ClickLabel*   pText;
    QTimer*       pTimer;
    QString m_sUrl;
    QString sPicsPath;
private slots:
    void slotClicked();
    void slotHide();

signals:
    void textClicked( const QString & );
};

class PopupWindow : public ShapeWidget {
    Q_OBJECT
public:
    PopupWindow(bool bMulti = 0, QWidget *parent = 0, const char *name = 0);

    ~PopupWindow();
    void updateMask();
    void setAnchor(const QPoint& anchor);
    void show();
    void setText(QString sText);
    void setLogo( const QImage  &, long =-1 );
    void setLogo( const QString &, long =-1 );
	void setTimeout( int timeout );
    void setID( QString ID ) {
        sID = ID;
    }
    /*! 0: Online, 1: incoming chat, 2: incoming memo, 3: incoming mail */
    void setType( int Type ) {
        nType = Type;
    }

private:
    QPoint        m_anchor;
    ShapeWidget*  pLogo;
    ShapeButton*  pClose;
    ShapeWidget*  pCyLogo;
    ClickLabel*   pText;
    QTimer*       pTimer;
    QString       sID;
    int           nType;
    QString       sPicsPath;
	int			  nTimeout;

private slots:
    void slotTextClick();
    void slotHide();

signals:
    void clickText( int,  QString );
    void hidePopup();
};


#endif
