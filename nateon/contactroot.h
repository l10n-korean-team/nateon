/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CONTENTROOT_H
#define CONTENTROOT_H

// #include <qlistview.h>
#include <kdebug.h>
#include <klistview.h>
#include <qlistview.h>

#include "contactbase.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class ContactRoot : public ContactBase {
public:
    ContactRoot ( QListView *parent, const QString &s1, const QString &s2);

    const QString& getName() {
        return m_sName;
    }
    const QString& getID() {
        return m_sID;
    }

    void setPixmap(QPixmap *p);
    const QPixmap* pixmap( int i ) const;
    void setTotal( int nTotal ) {
        m_nTotal = nTotal;
    }
    void setOnlineCount( int nCount );
    void setTotalCount( int nTotal );

    void incOnline();
    void decOnline();
    // const char getType() const { return m_cType; }


protected:
    void paintCell( QPainter * painter, const QColorGroup & colourGroup, int column, int width, int align );

private:
    QString m_sName;
    QString m_sID;
    QPixmap* pix;
    int m_nTotal;
    int m_nNotOffline;
    // const char m_cType;
};
#endif
