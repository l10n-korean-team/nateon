/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qdragobject.h>
#include "contactroot.h"


ContactRoot::ContactRoot ( QListView *parent, const QString &s1, const QString &s2)
        : ContactBase( parent, s1, s2 ),
        m_sName( s1 ),
        m_sID( s2 ),
        pix( 0 ),
        m_nTotal(0),
        m_nNotOffline(0) {
    /*
      QFont f;
      f.setBold ( true );
      setFont( f );
      setPaletteForegroundColor( QColor("#226699") );
    */
    setDropEnabled( TRUE );
    setDragEnabled( FALSE );
    setType( ContactBase::Group );
}

/*!
  \fn ContactList::setPixmap(QPixmap *p)
*/
void ContactRoot::setPixmap(QPixmap *p) {
    pix = p;
    setup();
    widthChanged( 0 );
    invalidateHeight();
    repaint();
}


/*!
  \fn ContactRoot::pixmap( int i ) const
*/
const QPixmap* ContactRoot::pixmap( int i ) const {
    if ( i )
        return 0;
    return pix;
}

void ContactRoot::paintCell(QPainter * painter, const QColorGroup & colourGroup, int column, int width, int align) {
#if 0
    QColorGroup customColourGroup = colourGroup;
    QBrush *brush;
    QPalette palette;

    if ( isSelected() ) {
        palette.setColor( QPalette::Active, QColorGroup::Highlight, QColor("#c8daea") );
        brush = new QBrush( palette.color(QPalette::Active, QColorGroup::Highlight) );
    }
    customColourGroup.setColor( QColorGroup::Text, Qt::black);
#endif
    const_cast<QColorGroup*>(&colourGroup)->setColor ( QColorGroup::Highlight, QColor("#c8daea") );
    QListViewItem::paintCell( painter, colourGroup, column, width, align );
}

void ContactRoot::setOnlineCount(int nCount) {
    m_nNotOffline = nCount;
    // m_sName = text(0);
    QString sRenaming;
    sRenaming = m_sName;
    sRenaming += "(";
    sRenaming += QString::number( m_nNotOffline );
    sRenaming += "/";
    sRenaming += QString::number( m_nTotal );
    sRenaming += ")";
    setText(0, sRenaming );
}

void ContactRoot::incOnline() {
    m_nNotOffline++;

    QString sRenaming;

    sRenaming = m_sName;
    sRenaming += "(";
    sRenaming += QString::number( m_nNotOffline );
    sRenaming += "/";
    sRenaming += QString::number( m_nTotal );
    sRenaming += ")";
    setText(0, sRenaming );
}

void ContactRoot::decOnline() {
    m_nNotOffline--;

    QString sRenaming;
    sRenaming = m_sName;
    sRenaming += "(";
    sRenaming += QString::number( m_nNotOffline );
    sRenaming += "/";
    sRenaming += QString::number( m_nTotal );
    sRenaming += ")";
    setText(0, sRenaming );
}

void ContactRoot::setTotalCount(int nTotal) {
    QString sRenaming;
    sRenaming = m_sName;
    sRenaming += "(";
    sRenaming += QString::number( nTotal );
    sRenaming += ")";
    setText(0, sRenaming );
}
