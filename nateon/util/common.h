/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COMMON_H
#define COMMON_H

#include <qhttp.h>
#include "../network/knateonbuffer.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/

class Common : public QObject {
    Q_OBJECT
public:
    Common(QObject *parent = 0, const char *name = 0);

    ~Common();
    QStringList getLocalIP();
    void removePercents(QString &sWord);
    void percent2HTML(QString &sWord);
    static QString qEllipsisText( const QString &org, const QFontMetrics &fm, int width, int align );
    static void fixOutString( QString &sString );
    static void nateLog( const QString &sLog );
    void openWeb( const QString &sWeb );
    // void playsound(QString sFile);
    // static void loadPercnet();
private:
    // Emoticon *stEmoticon;
};

class InviteWeb : public QObject {
    Q_OBJECT
public:
    InviteWeb(QObject *parent = 0, const char *name = 0);

    ~InviteWeb();
    int doResultFromCMN(QString sCMN, QString sTicket);

private:
    int httpId;
    QByteArray buffer;
    QHttp* http;

private slots:
    void httpRequestFinished(int id, bool error);
    // void slotReceivedData( int, int );
    void slotReceivedData(const QHttpResponseHeader &qResponse);

signals:
    void gotDataFromURL(QStringList&);
};

class PostCGI : public QObject {
    Q_OBJECT
public:
    PostCGI(QObject *parent = 0, const char *name = 0);
    ~PostCGI();

    int start( QString sURL, QString sParam );
    int start2( QString sURL, QString sParam );

private:
    int httpId;
    QHttp* http;

private slots:
    void slotReadAll( const QHttpResponseHeader& resp );

signals:
    void gotResult( const QString& );
};

class KNateonBuffer;

class WebCGI : public QObject {
    Q_OBJECT
public:
    WebCGI(QObject *parent = 0, const char *name = 0);
    ~WebCGI();

    int start(QString sURL);

private:
    int httpId;
    // QByteArray buffer;
    QHttp* http;
    KNateonBuffer sFullData;


private slots:
    void httpRequestFinished(int id, bool error);
    void slotReceivedData(const QHttpResponseHeader &qResponse);
    void slotClose( bool bErr );

signals:
    void gotResult( QStringList& );
};
#endif

#ifndef NMCONFIG_H
#define NMCONFIG_H
/*! 멀티세션 정보 저장 structure */
struct sessioninfo {
    QString dpkey;
	unsigned short int mediatype;
	unsigned short int equiptype;
	QString langos;
	QString displayname;
};

/*! 환경설정 항목 저장 stucture */
struct nmconfig {
    /*! PC Unique ID */
    QString pcid;
    bool hiddenretry;
    QString lockey;
    QString pcname;
    QString dpkey;

    /*! 환경설정 이외의 기타 설정 사항 */
    bool autologin;
    /*! 버디 리스트 보기 방법 */
    unsigned short int typeofbuddylist; /*! 0: 이름, 1: 별명, 2: 이름(ID), 3: 이름(별명) */
    unsigned short int typeofbuddysort; /*! 0: 전체보기, 1: 온라인친구만, 2: 온/오프라인으로표시 */

    /*! 기본 */
    bool autorun;
    bool hidestart;
    bool alwaystop;
    bool viewemoticonlist;
    short int buddydoubleclickaction; /*! 0: 대화 하기, 1: 쪽지 보내기 */
    bool usedefaultwebbrowser;
    QString defaultwebbrowser;
    /*! 개인 */
    // QString nickname; /*! 개인설정으로 빠짐 */
    bool checkawaytime;
    unsigned short int awaytime;
    /*! 알람/소리 */
    bool alarmbuddyconnect;
    bool alarmrequirechat;
    bool usealarmreceivenewmemo;
    short int receivenewmemo; /*! 0: 쪽지창 바로 보여주기, 1: 알려주기 */
    bool usealarmwithsound;
    /*! 알람/소리 - 고급설정 */
    bool useallsound;
    bool usesound;
    bool usebuddyconnectsound;
    QString buddyconnectsoundpath;
    bool usememorecievesound;
    QString memorecievesoundpath;
    bool usefilesenddonesound;
    QString filesenddonesoundpath;
    bool usemyloginsound;
    QString myloginsoundpath;
    bool usestartchatsound;
    QString startchatsoundpath;
    bool useminihompynewsound;
    QString minihompynewsoundpath;
    /*! 메시지 */
    bool showawaymemo;
    bool showawaymemo_busy;
    QString awaymemo;
    short int savechatlog; /*! 0: 저장 여부 확인하기, 1: 자동저장, 2: 자동저장않기 */
    bool useemoticon;
    bool replymemoattach; /*! 답장 */
    bool allreplymemoattach; /*! 전체답장 */
    bool forwardmemoattach; /*! 전달 */
    /*! 프라이버시 */
    bool allowonlypermitchat;
    bool allowonlyfriendmemo;
    bool allowother;
    /*! 연결(방화벽) */
    bool useproxy;
    unsigned short int proxytype; /*! 0: sock4, 1: sock5, 2: http */
    QString proxyserver;
    unsigned short int proxyport; /*! default - sock4, sock5 : 1080 , http : 6588 */
    QString proxyid;
    QString proxypassword;
    /*! P2P */
    QString p2pserverip; /*! 자신의 IP를 등록 한다. */
    unsigned short int p2pport;
    QString filedownloadpath;
    QString p2pdbfilepath; /*! P2P 정보 DB 파일 */
    /*! 플러그인 */
    // bool pluginautorun; /*! 각 플러그인 마다 autorun을 할것인지 따로 저장해야 함. */
    char logintype; /*! N: Nate ID, C: Cyworld ID */
    /*! 싸이월드 */
    bool usecyworld;
    QString cyid;
    QString cycmn;
    QString cypw;
    int minihompypublic; /*! 0 : "B" 친구에게만 공개, 1 : "O" 모두에게 공개, 2 : "C" 모두에게 비공개 */
    short int alarmminihompy; /*! 0: 내 미니미로 알려주기, 1: 알려주기 */
    bool alarmminihompynew;
    bool alarmetc;
    /*! 네이트 */
    bool usenatedotcom;
    QString nateid;
    QString natecmn;
    QString natepw;
    /*! 엠파스정보 */
    QString empasid;
    QString empascmn;
    /*! 기타정보 */
    QString area;
    /*! PRS 서버 */
    QString prsserver; /*! prs.nate.com */
    int prsport; /*! 80 or 1863 */
    bool dplconnectionfail; /*! dpl connection fail => prs connecting */
	/*! 멀티세션 정보 */
	int sessioncount;
	QPtrList<sessioninfo> sessionlist;
};
#endif
