/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "urlencode.h"

char hexVals[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

QString URLEncode::csUnsafeString= "\"<>%\\^[]`+$,@:;/!#?=&";

bool URLEncode::isUnsafe(char compareChar) {
    bool bcharfound = false;
    char tmpsafeChar;
    int m_strLen = 0;

    m_strLen = csUnsafeString.length();
    for (int ichar_pos = 0; ichar_pos < m_strLen ;ichar_pos++) {
        tmpsafeChar = csUnsafeString[ichar_pos].latin1();
        if (tmpsafeChar == compareChar) {
            bcharfound = true;
            break;
        }
    }
    int char_ascii_value = 0;
    //char_ascii_value = __toascii(compareChar);
    char_ascii_value = (int) compareChar;

    if (bcharfound == false &&  char_ascii_value > 32 &&
            char_ascii_value < 123) {
        return false;
    }
    // found no unsafe chars, return false
    else {
        return true;
    }

    return true;
}

QString URLEncode::decToHex(char num, int radix) {
    int temp=0;
    QString csTmp;
    int num_char;

    num_char = (int) num;
    if (num_char < 0)
        num_char = 256 + num_char;

    while (num_char >= radix) {
        temp = num_char % radix;
        num_char = (int)floor(num_char / radix);
        csTmp = hexVals[temp];
    }

    csTmp += hexVals[num_char];

    if (csTmp.length() < 2) {
        csTmp += '0';
    }

    QString strdecToHex;
    // Reverse the String
    for (unsigned int i=0; i<csTmp.length(); i++) {
        strdecToHex.append(csTmp[(csTmp.length()-1) - i]);
    }

    return strdecToHex;
}

QString URLEncode::convert(char val) {
    QString csRet;
    csRet += "%";
    csRet += decToHex(val, 16);
    return  csRet;
}

QString URLEncode::encode(QString pcsEncode) {
    int ichar_pos;
    QString csEncode;
    QString csEncoded;
    int m_length;
    // int ascii_value;

    csEncode = pcsEncode;
    m_length = csEncode.length();

    for (ichar_pos = 0; ichar_pos < m_length; ichar_pos++) {
        char ch = csEncode.at(ichar_pos).latin1();
        if (ch < ' ') {
            ch = ch;
        }

        if (!isUnsafe(ch)) {
            // Safe Character
            csEncoded += QChar(ch);
        } else {
            // get Hex Value of the Character
            csEncoded += convert(ch);
        }
    }

    return csEncoded;
}

QString URLEncode::encodeName(QString vData) {
    QCString csEncode = vData.utf8();
    QString sEncoded;
    int nLen = csEncode.length();
    unsigned char ch;

    for (int i=0; i<nLen; i++) {
        ch = csEncode.at(i);
        sEncoded += convert(ch);
    }

    return sEncoded;
}
