/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SOUND_H
#define SOUND_H

#include <qthread.h>
#include <ao/ao.h>
#include <fcntl.h>

typedef struct {
    int fd;
    int channels;
    int data_bit;
    int rate;
} fd_sound_format_t;

class MyClass : public QThread {
public:
    virtual void run();

private:
};

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
   @modifier Kyu-Ung Lee <kyu419@revdigit.pe.kr>
*/
class Sound {
public:
    Sound();
    ~Sound();

    static void play(QString sFile);

    static void* soundPlay(void* p);
    static void shutdown(void* p);
    static fd_sound_format_t soundcard_open_file(QString filename);

    static bool soundInitiated;
    static QString fileName;

    static pthread_t soundThread;
};

#endif
