/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef EMOTICON_H
#define EMOTICON_H

#include <qdom.h>
#include <qfile.h>
#include <kdebug.h>
#include <kstandarddirs.h>
#include <qmap.h>
#include <qregexp.h>
#include <kaboutdata.h>

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class Emoticon {
public:
    void replaseEmoticons(QString &sText);
    static void destroy();
    static Emoticon* instance();

protected:
    void createMapList();

private:
    Emoticon();
    ~Emoticon();
    Emoticon&        operator=(const Emoticon&);
    static Emoticon *instance_;
    QMap<QString,QString>      smallEmoticon;
    QString		sEmoticonsPath;
};

#endif
