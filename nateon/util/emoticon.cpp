/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "emoticon.h"

// Initialize the instance to zero
Emoticon* Emoticon::instance_(0);

Emoticon::Emoticon() {
    createMapList();
}


Emoticon::~Emoticon() {
}

void Emoticon::createMapList() {
    QDomDocument domTree;
    KStandardDirs   *dirs   = KGlobal::dirs();
    smallEmoticon.clear();
    QString fileName;
    sEmoticonsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/emoticons/" );
    fileName = sEmoticonsPath;
    fileName += "Emoticons_u.sm";
    QFile opmlFile( fileName );
    if ( !opmlFile.open( IO_ReadOnly ) ) {
        kdDebug() << "File Open Error [" << fileName << "]" << endl;
    }
    if ( !domTree.setContent( &opmlFile ) ) {
        kdDebug() << " XML Error [" << fileName << "]" << endl;
    }
    opmlFile.close();

    // get the header information from the DOM
    QDomElement root = domTree.documentElement();
    QDomNode node;
    node = root.firstChild();
    while ( !node.isNull() ) {
        if  ( node.isElement() && node.nodeName() == "tab" ) {
            QDomNode node1;
            QDomElement root1 = node.toElement();
            node1 = root1.firstChild();
            while ( !node1.isNull() ) {
                if ( node1.isElement() && node1.nodeName() == "item" ) {
                    smallEmoticon.insert(node1.toElement().attribute( "name" ), node1.toElement().attribute( "url" ));
                }
                node1 = node1.nextSibling();
            }
            node = node.nextSibling();
        }
    }
}

// createMapList();

void Emoticon::replaseEmoticons(QString & sText) {
    // KStandardDirs   *dirs   = KGlobal::dirs();

    QRegExp rx("/[^/\r\n]*/");
    QString sHTML;

    typedef QMap<QString, QString> mapString;
    mapString mapReplace;

    int pos = 0;
    while ( pos >= 0 ) {
        pos = rx.search( sText, pos );
        if ( pos > -1 ) {
            if ( smallEmoticon.find( rx.cap(0).replace("/", "") ) != smallEmoticon.end() ) {
                sHTML = "<IMG src=";
                sHTML += sEmoticonsPath;
                sHTML += smallEmoticon[rx.cap(0).replace("/", "")];
                sHTML += " width=16 height=16 /> ";
                mapReplace[rx.cap(0)] = sHTML;
            }

            pos  += rx.matchedLength();
        }
    }

    mapString::Iterator it;
    for ( it = mapReplace.begin(); it != mapReplace.end(); ++it ) {
        sText.replace(it.key(), it.data());
    }
}

void Emoticon::destroy() {
    delete instance_;
    instance_ = 0;
}

Emoticon * Emoticon::instance() {
    if ( instance_ == 0 ) {
        instance_ = new Emoticon();
        // connect( CurrentAccount::instance(), SIGNAL(changedEmoticonSettings()     ),
        //          instance_,                    SLOT(slotChangedEmoticonSettings() ) );
    }

    return instance_;
}

