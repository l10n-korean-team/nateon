/***************************************************************************
                          idletimer.cpp  -  description
                             -------------------
    begin                : Tue Oct 22 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "idletimer.h"

#include <kdebug.h>

#include "currentaccount.h"
// #include "kmessdebug.h"

// The constructor
IdleTimer::IdleTimer()
        : QObject() {
    watcher_ = new XAutoLock();
    // Connect the timer to signal when the user is away.
    connect( watcher_, SIGNAL( timeout() ), SLOT( slotTimeout() ) );
    connect( watcher_, SIGNAL( activity() ), SLOT( slotActivity() ) );
    watcher_->stopTimer();
    // Make the connections
    connect( CurrentAccount::instance(), SIGNAL( changedTimerSettings() ), SLOT  ( updateWatcher() ) );
}



// The destructor
IdleTimer::~IdleTimer() {
    delete watcher_;
#ifdef NETDEBUG
    kdDebug() << "DESTROYED IdleTimer" << endl;
#endif
}



// Echo the watcher's timeout.
void IdleTimer::slotTimeout() {
    emit timeout();
}



// Echo the watcher's activity .
void IdleTimer::slotActivity() {
    emit activity();
}



// Reset the watcher based on the account's settings.
void IdleTimer::updateWatcher() {
#ifdef NETDEBUG
    kdDebug() << "IdleTimer::updateWatcher()" << endl;
#endif
    Account *account = CurrentAccount::instance();
    int nIdleTime = account->getIdleTime() * 60;
#ifdef NETDEBUG
    kdDebug() << "IT: " << nIdleTime << " (sec)" << endl;
#endif
    watcher_->setTimeOut( nIdleTime );
    if ( account->getUseIdleTimer() ) {
#ifdef NETDEBUG
        kdDebug() << "IT:   Start the timer." << endl;
#endif
        watcher_->startTimer();
    } else {
#ifdef NETDEBUG
        kdDebug() << "IT:   Stop the timer." << endl;
#endif
        watcher_->stopTimer();
    }
#ifdef NETDEBUG
    kdDebug() << "IT:   Done updateWatcher()." << endl;
#endif
}

void IdleTimer::stopTimer() {
    if ( watcher_ )
        watcher_->stopTimer();
}

void IdleTimer::startTimer() {
    if ( watcher_ )
        watcher_->startTimer();
}

void IdleTimer::restartTimer() {
	if ( watcher_ )
		watcher_->restartTimer();
}

void IdleTimer::setFakeIdle() {
	if ( watcher_ )
		watcher_->setFakeIdle();
}
#include "idletimer.moc"
