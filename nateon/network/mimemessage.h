/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MIMEMESSAGE_H
#define MIMEMESSAGE_H

#include <qobject.h>
#include <ctype.h>
#include <qobject.h>
#include <qstringlist.h>
#include "define.h"

/**
   @author Jaeha, Lee
*/
class MimeMessage : public QObject {
    Q_OBJECT

public:
    // The constructor
    MimeMessage();
    // The constructor that parses a message
    MimeMessage(QString message);
    // The constructor that parses a binary message
    MimeMessage(QByteArray message);

    // The copy constructor
    MimeMessage(const MimeMessage& other);

    // The destructor
    ~MimeMessage();

    // Add a field to the message
    void                addField(const QString& field, const QString& value);
    /** Sets the field in the message, or adds it if it doesn't exist. */
    void                setField(const QString& field, const QString& value);
    // decodes MIME strings like =?iso...=...?= ...
    QString             decodeRFC2047String(const QCString& aStr) const;
    // Return the body of the message
    void                getBody(QString& body) const;
    // Return the body of the message
    const               QString& getBody() const;
    // Return the P2P data of the message
    const               QByteArray& getBinaryData() const;
    // Return the field and value at the given index
    void                getFieldAndValue(QString& field, QString& value, const uint& index) const;
    // Return the message fields as a big string
    QString             getFields() const;
    // Return the entire message as a big string
    QString             getMessage() const;
    // The total number of fields
    uint                getNoFields() const;
    // Get a sub-value of a value that has multiple parameters
    QString             getSubValue(const QString& field, const QString& subField = QString::null) const;
    // Get a value given a field
    const QString&      getValue(const QString& field) const;
    // Test whether a given field exists in the message header
    bool                hasField(const QString& field) const;
    // Print the contents of the message to kdDebug (for debugging purposes)
    void                print() const;
    // Set the message body
    void                setBody(const QString& body);

    NMStringDict*       getDict();

private:                     // Private methods
    // returns the appropriate QTextCodec for the characterset name
    static QTextCodec*  getCodecByName(const QCString& codecName);
    // Parse the message into type, body, and fields and values
    void                parseMessage(const QString& message);
    // Split the message and store it in the string list
    void                splitHead(QStringList& stringList, const QString& head) const;
    // Split a line between field and value
    void                splitLine(QString& field, QString& value, const QString& line) const;
    // Split a message into head and body
    void                splitMessage(QString& head, QString& body, const QString& message) const;

private:                     // Private attributes
    // The message body
    QString             body_;
    // The message fields
    QStringList         fields_;
    // The message values (corresponding to the fields)
    QStringList         values_;
    // The data when the message is of type P2P, invalid in any other case
    QByteArray          binaryData_;
};
#endif
