/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdlib.h>
#include "p2pconnection.h"
#include "../util/common.h"

extern nmconfig stConfig;

int Client::m_count = 0;

void Client::openSaveFile(QString pPath) {
    QString sFileName( m_pSendFileInfo->getFileName() );
    // sFileName.replace("%20", " ");
    QString pFilePath = pPath + "/" + sFileName;
    if ( QFile::exists( pFilePath ) )
        QFile::remove ( pFilePath );
    qFile.setName(pFilePath);
    qFile.open( IO_Raw | IO_WriteOnly | IO_Append );
}

#if 1
void Client::writeSaveFile(const QByteArray &qArray, unsigned long size) {
    ulWriteSum += size;
    // uiPacketSum -= size;
    qFile.writeBlock(qArray, size);
    qFile.flush();
}
#endif

void Client::closeSaveFile() {
    qFile.close();
}

P2PConnection::P2PConnection(QObject *parent, const char *name)
        : QObject(parent, name), m_nTrid(0) {
    m_clients.clear();
    m_SendFileInfo.clear();
}


P2PConnection::~P2PConnection() {
}

void P2PConnection::addSendFileInfo(SendFileInfo * pSendFileInfo) {
    m_SendFileInfo.append(pSendFileInfo);
}

/*!
  socket 값으로 파일 정보 Class를 구하는 함수.
*/
SendFileInfo * P2PConnection::getSendFileInfoBySocket(int Socket) {
    SendFileInfo* pSendFileInfo;
    for ( pSendFileInfo = m_SendFileInfo.first(); pSendFileInfo; pSendFileInfo = m_SendFileInfo.next() )
        if ( pSendFileInfo ) {
            QSocket* pSock = pSendFileInfo->getSocket();
            if ( pSock ) {
                if ( pSock->socket() == Socket)
                    return pSendFileInfo;
            }
        }
    return 0;
}

/*!
  SS Cookie값으로 파일 정보 Class를 구하는 함수.
*/
SendFileInfo * P2PConnection::getSendFileInfoBySSCookie(QString sCookie) {
    SendFileInfo* pSendFileInfo;
    for ( pSendFileInfo = m_SendFileInfo.first(); pSendFileInfo; pSendFileInfo = m_SendFileInfo.next() )
        if ( pSendFileInfo )
            if ( pSendFileInfo->getSSCookie() == sCookie )
                return pSendFileInfo;
    return 0;
}

/*!
  DP Cookie값으로 파일 정보 Class를 구하는 함수.
*/
SendFileInfo * P2PConnection::getSendFileInfoByDPCookie(QString sCookie) {
    SendFileInfo* pSendFileInfo;
    for ( pSendFileInfo = m_SendFileInfo.first(); pSendFileInfo; pSendFileInfo = m_SendFileInfo.next() )
        if ( pSendFileInfo->getDPCookie() == sCookie )
            return pSendFileInfo;
    return 0;
}

SendFileInfo * P2PConnection::getSendFileInfoByID(QString sID) {
    SendFileInfo* pSendFileInfo;

    for ( pSendFileInfo = m_SendFileInfo.first(); pSendFileInfo; pSendFileInfo = m_SendFileInfo.next() )
        if  ( ( pSendFileInfo->getYourID() == sID ) &&  ( !pSendFileInfo->isConnected() ) )
            return pSendFileInfo;
    return 0;
}

SendFileInfo * P2PConnection::getSendFileInfoByDPTid(int sTid) {
    SendFileInfo* pSendFileInfo;

    for ( pSendFileInfo = m_SendFileInfo.first(); pSendFileInfo; pSendFileInfo = m_SendFileInfo.next() )
        if  ( pSendFileInfo->getDPTid() == sTid )
            return pSendFileInfo;
    return 0;
}


/*!
  ATHC를 받으면...
*/
void P2PConnection::gotATHC(const QStringList & slCommand, SendFileInfo * pSendFileInfo) {
    QString sCommand;
    sCommand = "FILE";
    sCommand += " ";
    sCommand += QString::number(getTrid());
    sCommand += " ";
    sCommand += "ACCEPT";
    sCommand += " ";
    sCommand += pSendFileInfo->getSSCookie();
    sCommand += " ";
    sCommand += "0";
    sCommand += "\r\n";

    sendCommand_noTid(pSendFileInfo->getSocket(), sCommand);

    emit AcceptOk(pSendFileInfo);

    Q_UNUSED( slCommand );
}

int P2PConnection::sendCommand(QSocket * pSocket, const QString & sPrefix, const QString & sText) {
    QTextStream stream( pSocket );
    int nTid = getTrid();

    QString sCommand = sPrefix + " " + QString::number(nTid) + " " + sText;
    stream << sCommand; // << endl;

    pSocket->flush();

    emit OutgoingMessage("[ P2P ] {" + sCommand +"}");

    return nTid;
}

void P2PConnection::sendCommand_noTid(QSocket * pSocket, const QString & sText) {
    QTextStream stream( pSocket );
    stream << sText; // << endl;

    pSocket->flush();

    emit OutgoingMessage("[ P2P ] {" + sText +"}");

    return;
}

/*!
  명령어 TID 값, 명령어 입력때 마다 증가가 된다.
*/
int P2PConnection::getTrid() {
    return m_nTrid++;
}

void P2PConnection::SendFile(QSocket * socket, Client * client, unsigned long ulStart) {
    // QDataStream stream(socket);
    // double ulTotal = 0;
    // double ulSum = 0;
    // int nIdx = 0;
    // int nPercent = 0;


    SendFileInfo* pSendFileInfo;
    pSendFileInfo = client->fileInfo();

    if (!pSendFileInfo) {
        return;
    }

    /*!
    QSocketDevice * pSockDev = socket->socketDevice ();
    pSockDev->setBlocking ( TRUE );
     */

    QFile *pFile = new QFile( pSendFileInfo->getFileName() );
    bool bOpen = pFile->open( IO_Raw | IO_ReadOnly );
    if ( !bOpen ) {
        kdDebug() << "Open_Error !!!" << endl;
        return;
    }

    bool bRet = pFile->at( ulStart );
    if ( !bRet ) {
        kdDebug() << "Open_at_Error !!!" << endl;
        return;
    }

    pSendFileInfo->setFile( pFile );

    QSocketNotifier *sn = new QSocketNotifier( socket->socket(), QSocketNotifier::Write );
    pSendFileInfo->setSocketNotifier( sn );
    connect( sn, SIGNAL( activated( int ) ), SLOT( slotSendFile( int ) ) );
}

void P2PConnection::slotSendFile(int nSocket) {
    SendFileInfo* pSendFileInfo	= getSendFileInfoBySocket( nSocket );

    if ( ! pSendFileInfo )
        return;

    if ( pSendFileInfo->isCanceled() ) {
        pSendFileInfo->getChatView()->sendFILE_CANCEL( pSendFileInfo->getSSCookie() );
        pSendFileInfo->getSocket()->flush();
        pSendFileInfo->getSocketNotifier()->setEnabled( FALSE );
        pSendFileInfo->getFile()->close();
        return;
    }

    if ( pSendFileInfo->getFile()->atEnd() ) {
        pSendFileInfo->getSocketNotifier()->setEnabled( FALSE );
        pSendFileInfo->getFile()->close();
        return;
    }

    char data[8192];
    // data = (char*)malloc(8192);
    memset(data, 0x00, 8192);
    unsigned long ulDataLength = pSendFileInfo->getFile()->readBlock(data, 4096);

    QCString header;
    header = "FILE " + QString::number( ++(pSendFileInfo->m_nCount) ) +" DATA " + QString::number(ulDataLength) + "\r\n";

    int rawSize = ulDataLength + header.length();

    char *rawData = new char[rawSize];
    memcpy( rawData, header, header.length() );
    memcpy( rawData + header.length(), data, ulDataLength);

    QString sTemp;
    sTemp = header.stripWhiteSpace();
    sTemp += " [Size: ";
    // sTemp += QString::number( header.length() + ulDataLength );
    sTemp += QString::number( rawSize );
    sTemp += "bytes]";

    emit OutgoingMessage("[ P2P ] {" + sTemp +"}");

    int nRet = pSendFileInfo->getSocket()->writeBlock( rawData, rawSize );

    if ( nRet == -1 ) {
#ifdef NETDEBUG
        kdDebug() << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << endl;
#endif
        pSendFileInfo->getSocketNotifier()->setEnabled( FALSE );
        pSendFileInfo->getFile()->close();
        return;
    }

    emit updateProgress( pSendFileInfo->getSSCookie(), pSendFileInfo->getSum(), pSendFileInfo->getFileSize(), pSendFileInfo->getPercent( ulDataLength ) );
}


void P2PConnection::RecvFile(QSocket * socket, Client * client) {
}


void P2PConnection::sendAccept(SendFileInfo * pSendFileInfo) {
    Q_UNUSED( pSendFileInfo );
}


void P2PConnection::sendReject(SendFileInfo * pSendFileInfo) {
    QString sCommand;
    sCommand = "FILE ";
    sCommand += QString::number(getTrid()) + " ";
    sCommand += "CANCEL ";
    sCommand += pSendFileInfo->getSSCookie() + " ";
    sCommand += "0\r\n";
    sendCommand_noTid(pSendFileInfo->getSocket(), sCommand);
}


#include "p2pconnection.moc"
