/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SENDFILEINFO_H
#define SENDFILEINFO_H

#include <qstring.h>
#include <qstringlist.h>
#include <qsocket.h>
#include <qtimer.h>
#include "../chat/chatview.h"

class ChatView;
/**
@author Doo-Hyun Jang <ring0320@nate.com>
*/

class SendFileInfo : public QObject {

    Q_OBJECT

public:
    SendFileInfo(const QString MyID, const QString YourID, const QString File, unsigned long Size);
    SendFileInfo(const QString MyID, const QString YourID, const QString sDPCookie, const QString sFRCookie);

    ~SendFileInfo();

    const QString getMyID() const {
        return sMyID;
    }
    const QString getYourID() const {
        return sYourID;
    }
    const QString getFileName() const {
        return sFile;
    }
    const unsigned long getFileSize() const {
        return ulSize;
    }
    QString getSSCookie() const {
        return sSSCookie;
    }
    QString getDPCookie() const {
        return sDPCookie;
    }
    QString getFRCookie() const {
        return sFRCookie;
    }
    QSocket* getSocket() const {
        return pSocket;
    }
    bool isSend() const {
        return (bSend == true);
    }
    bool isReceive() const {
        return (bSend == false);
    }
    bool isUseFR() const {
        return bFR;
    }
    int getDPTid() const {
        return nDPTid;
    }
    QString getFRIP() const {
        return sFRIP;
    }
    int getFRPort() const {
        return nFRPort;
    }
    ChatView* getChatView() {
        return m_pChatView;
    }
    QFile *getFile() {
        return m_pFile;
    }
    QSocketNotifier *getSocketNotifier() {
        return m_pSocketNotifier;
    }
    unsigned long getSum() {
        return m_ulSum;
    }

    /*! 접속 했는지 유무 */
    bool isConnected() {
        return bConnected;
    }

    /*! FILE [TID] ACCEPT ... 유무 */
    bool isAccepted() {
        return bAccept;
    }

    /*! ATHC */
    bool isATHC() {
        return bATHC;
    }

    bool isCanceled() {
        return bCanceled;
    }

    void setSSCookie(QString Cookie) {
        sSSCookie = Cookie;
    }
    void setDPCookie(QString Cookie) {
        sDPCookie = Cookie;
    }
    void setFRCookie(QString Cookie) {
        sFRCookie = Cookie;
    }
    void setSocket(QSocket* Socket) {
        pSocket = Socket;
    }
    void setSend() {
        bSend = true;
    }
    void setReceive() {
        bSend = false;
    }
    void setUseFR() {
        bFR = true;
    }
    void setDPTid(int Tid) {
        nDPTid = Tid;
    }
    void setFRIP(QString FRIP) {
        sFRIP = FRIP;
    }
    void setFRPort(int FRPort) {
        nFRPort = FRPort;
    }
    void setChatView( ChatView *pChatView ) {
        m_pChatView = pChatView;
    }
    void setFile( QFile *pFile ) {
        m_pFile = pFile;
    }
    void setSocketNotifier( QSocketNotifier *pSocketNotifier ) {
        m_pSocketNotifier = pSocketNotifier;
    }

    /*!
      접속했을때 세팅함, 접속받는 서버와
      접속시도하는 클라이언트의 경쟁에서 먼저 된 접속에서 셋팅함.
    */
    void setConnected() {
        bConnected = true;
    }

    /*!
     * FILE [TID] ACCEPT ...
     */
    void sentAccept() {
        bAccept = true;
    }

    /*!
     * ATHC
     */
    void sentATHC() {
        bATHC = true;
    }

    void startTimer();

    void setCanceled( bool Canceled ) {
        bCanceled = Canceled;
    }

    int m_nCount;

    int setStart( unsigned long ulStart ) {
        return ( m_ulStart = ulStart );
    }
    int getPercent( int nAddSize );

private:
    /*! 접속 유무 필드 */
    bool bConnected;
    bool bAccept;
    bool bATHC;
    bool bSend;
    static int nCount;
    const unsigned int nID;
    const QString sMyID;
    // const QStringList slReceiver;
    const QString sYourID;
    const QString sFile;
    const unsigned long ulSize;
    unsigned long m_ulStart;
    unsigned long m_ulSum;
    QString sSSCookie;
    QString sDPCookie;
    QString sFRCookie;
    QSocket* pSocket;
    QTimer* P2PTimer;
    bool bFR;
    int nDPTid;
    QString sFRIP;
    int nFRPort;
    ChatView *m_pChatView;
    bool bCanceled;
    QFile *m_pFile;
    QSocketNotifier *m_pSocketNotifier;

protected slots:
    void slotP2PTimeout();

signals:
    void P2PTimeout(SendFileInfo*);
};

#endif
