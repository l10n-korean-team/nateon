/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateonbuffer.h"
#include <string.h>              // for memcpy
#include <kdebug.h>

// Constructor
KNateonBuffer::KNateonBuffer( unsigned int sz )
        : QByteArray( sz ) {
}


// Destructor
KNateonBuffer::~KNateonBuffer() {
}


#if 1
// Add data
void KNateonBuffer::add( char *data, unsigned int dataSize ) {
    // Get old data
    int         oldSize   = size();
    const char *oldBuffer = this->data();

    // Make room for new data
    int   newSize   = oldSize + dataSize;
    if ( newSize > 0) {
        char *newBuffer = new char[ newSize ];

        // Copy the bytes
        // Copy old
        memcpy(newBuffer,           oldBuffer, oldSize);
        // Copy new
        memcpy(newBuffer + oldSize, data,      dataSize);

        // Assign to this object
        assign( newBuffer, newSize );// does the deletion automatically
    }
}
#endif

// Retrieve data
QByteArray KNateonBuffer::left( unsigned int blockSize ) const {
    if ( size() < blockSize ) {
        // FIXME: en ifdef
#ifdef NETDEBUG
        kdDebug() << "Buffer size " << size() << " < asked size " << blockSize << "!" << endl;
#endif
        return QByteArray();
    }

    char *leftPart = new char[ blockSize ];
    memcpy(leftPart, data(), blockSize);

    QByteArray qbaWrapper;
    // does the deletion automatically
    qbaWrapper.assign(leftPart, blockSize);

    return qbaWrapper;
}

void KNateonBuffer::clear() {
    resize(0);
    return;
}


// Remove read data
void KNateonBuffer::remove( unsigned blockSize ) {
    if ( size() < blockSize ) {
#ifdef NETDEBUG
        kdDebug() << "KNateonBuffer - WARNING - size " << size() << " < asked size " << blockSize << "!" << endl;
#endif
        return;
    }

    uint  newSize   = size() - blockSize;
    char *newBuffer = new char[ newSize ];
    memcpy(newBuffer, data() + blockSize, newSize);

    // Assign new buffer
    assign( newBuffer, newSize );// does the deletion automatically
}


// Find a newline character
int KNateonBuffer::findNewline() const {
    int   index = -1;
    char *data  = this->data();

    // Avoid the "index 0 out of range" errors
    if (size() == 0) {
        return -1;
    }

    // Keep searching until a \r is followed by a \n
    do {
        // Find the next \r
        index = find('\r', index + 1);
        if (index == -1) {
            return -1;
        }
    } while (data[index + 1] != '\n');

    return index;
}




// Return the length of the buffer
int KNateonBuffer::length() const {
    return size();
}

int KNateonBuffer::findString(char * str) {
    int   index = -1;
    char *data  = this->data();

    // Avoid the "index 0 out of range" errors
    if (size() == 0) {
        return -1;
    }

    do {
        // Find the next \r
        index = find(str[0], index + 1);
        if (index == -1) {
            return -1;
        }
    } while (strncmp( (data+index), str, strlen(str) ) != 0 );

    return index;
}

int KNateonBuffer::findString_re( const QString &csStr ) {
    QCString csDummy( this->data() );
    // "(FILE [0-9]+ DATA)"
    return csDummy.find( QRegExp( csStr ) );
    // return find( QRegExp( csStr ) );
}

#if 0
void KNateonBuffer::add(char * str, unsigned int size) {
    // Get old data
    int         oldSize   = size();
    const char *oldBuffer = this->data();

    // Make room for new data
    int   newSize   = oldSize + dataSize;
    if ( newSize > 0) {
        char *newBuffer = new char[ newSize ];
        // Copy the bytes
        // Copy old
        memcpy( newBuffer, oldBuffer, oldSize);
        // Copy new
        memcpy( newBuffer + oldSize, str, size );

        // Assign to this object
        assign( newBuffer, newSize );// does the deletion automatically
    }
}
#endif


