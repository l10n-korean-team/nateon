/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef NATEONDPCONNECTION_H
#define NATEONDPCONNECTION_H

#include <qdict.h>
#include <kmessagebox.h>
#include <kmdcodec.h>
#include <qdatetime.h>
#include <kdebug.h>
#include <qlistview.h>
#include <qstringlist.h>
#include <qdict.h>

#include "../buddy/buddybase.h"
#include "../buddy/buddy.h"
#include "../buddy/buddylist.h"
#include "../buddy/group.h"
#include "../buddy/grouplist.h"
#include "../specialgroups.h"
#include "nateonconnection.h"
#include "ssconnection.h"
#include "../currentaccount.h"
#include "mimemessage.h"
#include "../util/common.h"


class BuddyBase;
class Buddy;
class BuddyList;
class Group;
class GroupList;
class Account;
class SSConnection;
class MimeMessage;
class Common;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class NateonDPConnection : public NateonConnection {
    Q_OBJECT
public:
    NateonDPConnection ( QObject *parent = 0, const char *name = 0 );
    ~NateonDPConnection();
    bool openConnection();
    void closeConnection();
    void initialize();

    bool parseCommand ( const QStringList& slCommand );
    bool parseMessage ( const QString &sCommand, const QStringList& slCommand, const MimeMessage& message );
    bool parseBuffer ( const QStringList& slCommand, QString m_pBuffer );
    void putRCON();
    // void gotRCON();
    void putLSIN();
    void putLSINOTP();
	void putCPRM();
    void gotLSIN ( const QStringList& slCommand );
    void gotLOPT ( const QStringList& slCommand );
    void gotCONF ( const QStringList& slCommand );
    void gotGLST ( const QStringList& slCommand );
    void gotLIST ( const QStringList& slCommand );
    void gotPING ( const QStringList& slCommand );
    void gotPACK ( const QStringList& slCommand, const QString& sTid );
    void gotRESS ( const QStringList& slCommand );
    void gotINFY ( const QStringList& slCommand );
    void gotADDG ( const QStringList& slCommand );
    void gotRMVG ( const QStringList& slCommand );
    void gotRENG ( const QStringList& slCommand );
    void gotADSB ( const QStringList& slCommand );
    void gotADDB ( const QStringList& slCommand );
    void gotMVBG ( const QStringList& slCommand );
    void gotMCNT ( const QStringList& slCommand );
    void gotMLST ();
	void gotONST ();
    void gotREQCCTOC ( QStringList& slCommand );
    // void gotINVT( const QStringList& slCommand );
    int putRESS();
    int getGroupCache() {
        return m_nGLSTCacheNum;
    };
    int putADDG ( QString sCommand );
    int putRENG ( QString sCommand );
    int putRMVG ( QString sCommand );
    int putADSB ( QString sCommand );
    int putCTOC_INVT ( QString sCommand );
    int putCTOC ( QString sCommand );
    int getTID() {
        return m_nTrid;
    };

    void sendMsg( QString sCommand );
    int putLock ( const QString &sLock );
    int putUnlock ( const QString &sUnlock );

    QStringList getFixHandle() {
        return slFixHandle;
    }
    void setGroup0( Buddy* pBuddy );

	void setReceivedONST();
	bool isReceivedONST();

private:
    // Buddy* m_pBuddy;
    BuddyList *m_pBuddyList;
    GroupList *m_pGroupList;
    Account *m_pAccount;
    SSConnection *m_SSConnection;
    // KConfig *config;
    int m_nGLSTCacheNum;
    /*!
     * Buddy가 여러 Group에 복사 될 수 있음.
     */
    // QDict<QString> m_GetGID; // QString keys, QLineEdit* values
    Common* m_pCommon;
    QDict <char> pBList;
    QStringList slFixHandle;

    // key(QString) : GID, values(QStringList) : 그룹명, BuddyHandle
    QDict <QStringList> pGroupList;
    // key(QString) : BuddyHandle(slCommand[6]), values(QString) : slCommand
    QDict <QStringList> pBuddyList;
	bool m_bReceivedONST;

public slots:
    void messageSent ( const QString &m_cMessageQS );
    void messageReceived ( const QString &m_cMessageQS );

    /*! 상태변경 */
    void slotChangeStatusOnline();
    void slotChangeStatusAway();
    void slotChangeStatusAutoAway();
    void slotChangeStatusBusy();
    void slotChangeStatusPhone();
    void slotChangeStatusMeeting();
    void slotChangeStatusOffline();
    void slotPutLockList ( QStringList &slList );
    void slotPutUnlockList ( QStringList &slList );
    void slotRealDelete( const QString &sID );
    void slotSendLock( const QString &sID );
    void slotSendUnlock( const QString &sID );

protected slots:             // Protected slots
    // The socket connected, so send the version command.
    void connectionSuccess();
    void slotAddAccept(QString& sCMN, QString& sUID);
    void slotAddReject(QString& sCMN, QString& sUID);
    void slotChangeNick(QString sNick);

signals:
    void connected();
    // void disconnected();
    void ReceiveRESS ( SSConnection *m_SSConnection );
    void receivedINFY ( const QStringList& slCommand );
    void receivedLSIN();
    // void receivedLSIN();
    void receivedMemoCTOC ( const MimeMessage& message, bool fromServer );
    void receivedCTOCAMSG ( const MimeMessage& message );
    void receivedADDG ( const QStringList& slCommand );
    void receivedRMVG ( const QStringList& slCommand );
    void receivedRENG ( const QStringList& slCommand );
    void receivedINVT ( const QStringList& slCommand );
    void receivedREQST ( const QStringList& slCommand );
    void allowAccept ( QListViewItem* , Buddy* );
	void otherAllowAccept ();
	void otherAllowReject ();
    void receivedREJCT ( const QStringList& slCommand );
	void receivedMVBG ( Buddy *pBuddy, Group *pFromGroup, Group *pToGroup );
	void receivedCPBG ( Buddy *pBuddy, Group *pGroup );
	void receivedRMBG ( Buddy *pBuddy, Group *pGroup );
	void receivedADDB ( const QString & sID );
	void receivedADDBFL ( const QStringList& slCommand );
    void addCommandQueue ( const QStringList& slCommand );
	void showLogManager ();
	void notiMultiSession( const int count, const QString &status );
    void err200();
    void err201();
    void err202();
    void err300();
    /*! 인증 에러, 싸이연동 에러 */
    void err301( const QStringList& slCommand );
    void err302();
    void err304( const QString &sTID );
    void err306();
    void err309();
    void err395();
    /*! 브라우져 띄움 */
    void err998( const QStringList& slCommand );
    // void connectToP2P ( const QStringList& slCommand );
    void gotREQCNEW( const QStringList& slCommand );
    void gotREQCRES( const QStringList& slCommand );
    void showAddConfirm( const QStringList& slCommand );
    void connectP2PFR( const QStringList& slCommand );
    void receivedREFR( const QStringList& slCommand );
    void receivedCPRF( const QStringList& slCommand );
	void receivedCPRFBody( const QStringList& slCommand, const QString &sBody);
    void receivedNNIK( const QStringList& slCommand );
    void receivedNPRF( const QStringList& slCommand );
    void kill();
    void receivedALRM( const QStringList& slCommand );
    void receivedCALM( const QStringList& slCommand );
    /*! cannot connect back-end server error mr, css, cfr, ipml등에 연결할 수 없을 때. */
    void err421();
    void err500( const QStringList & );
    void updateStatusText( const QString & );
    void addBuddyADSB_REQST( const QStringList& slCommand );
    void refreshBuddyList();
	void removeBuddySync( Buddy * );
	void fileCanceled( const QStringList & );
};
#endif
