/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdlib.h>
#include "nateondplconnection.h"
#include <klocale.h>
#include <qtimer.h>
#include <kmessagebox.h>
#include "../currentaccount.h"
#include "util/common.h"
#include "../shared/version.h"
#include "../shared/define.h"

extern nmconfig stConfig;

NateonDPLConnection::NateonDPLConnection()
        : NateonConnection("nateondplconnection") {
}


bool NateonDPLConnection::parseCommand(const QStringList& slCommand) {
#if 0
    if (slCommand[0].toInt() != 0) {
        // I've desided to always print these messages, should make "error-resolving" easier.
        kdDebug() << "WARNING - Received error code " << command[0] << " from server." << endl;
    }
#endif

    if ( slCommand[0] == "PVER" ) {
        gotPVER( slCommand );
    } else if ( slCommand[0] == "AUTH" ) {
        gotAUTH( slCommand );
    } else if ( slCommand[0] == "REQS" ) {
        gotREQS( slCommand );
    } else if ( slCommand[0] == "RCON" ) {
        putVer();
    } else if ( slCommand[0] == "421" ) {
        gotError421();
    }

    return true;
}


bool NateonDPLConnection::parseMessage(const QString& sCommand, const QStringList& slCommand, const MimeMessage& message) {
    Q_UNUSED( sCommand );
    Q_UNUSED( slCommand );
    Q_UNUSED( message );

    return true;
}


/*!
  \fn NateonDPLConnection::initialize()
*/
void NateonDPLConnection::initialize() {
    NateonConnection::initialize();
}


/*!
  \fn NateonDPLConnection::openConnection()
*/
bool NateonDPLConnection::openConnection() {
    /* connect to dpl server */
    /*!
     * dpl connection fail 초기화
     * true가 되면 PRS 서버 경유 접속 시도
     */
    stConfig.dplconnectionfail = FALSE;
    stConfig.prsserver = "";
    stConfig.prsport = 0;
    return connectToServer( "dpl.nate.com", 5004 );
}


/*!
  \fn NateonDPLConnection::closeConnection()
*/
void NateonDPLConnection::closeConnection() {
    //  m_loginTimer.stop();
    setSendPings( FALSE );
    disconnectFromServer();
}


void NateonDPLConnection::connectionSuccess() {
    connectionTimer_.stop();
#ifdef NETDEBUG
    kdDebug() << "Connection Success!!!" << endl;
#endif
    if ( stConfig.dplconnectionfail )
        putRCON();
    else
        putVer();
}


/*!
  \fn NateonDPLConnection::putVer()
*/
void NateonDPLConnection::putVer() {
    using namespace AutoVersion;

    QString cmd;
    cmd.sprintf( "%d.%d.%d.%d %s %s\r\n", MAJOR, MINOR, BUILD, REVISION, PVER_VERSION, PVER_LANG_OS );
    sendCommand( "PVER", cmd );
}


/*!
  \fn NateonDPLConnection::gotAUTH()
*/
void NateonDPLConnection::gotAUTH(const QStringList& slCommand) {
    QString sID = m_pCurrentAccount->getID();
    sendCommand( "REQS", "DES " + sID + "\r\n" );

    Q_UNUSED( slCommand );
}


/*!
  \fn NateonDPLConnection::gotREQS()
*/
void NateonDPLConnection::gotREQS(const QStringList& slCommand) {
    //KMessageBox::information(0, slCommand[0]+slCommand[1]+slCommand[2]);
    Account *pAccount = new Account();
    if (pAccount && slCommand[3] != 0 && slCommand[4] != 0) {
        pAccount->setLoginInformation( m_pCurrentAccount->getID(), m_pCurrentAccount->getPassword());
        // set DP ip , port
        pAccount->setDPInformation( slCommand[3], atoi(slCommand[4]) );
    }

    emit connectDPWithAccount( pAccount );
}


/*!
  \fn NateonDPLConnection::gotPVER()
*/
void NateonDPLConnection::gotPVER(const QStringList& slCommand) {
    sendCommand( "AUTH", "DES\r\n" );

    Q_UNUSED( slCommand );
}

bool NateonDPLConnection::openPRSConnection() {
    /* connect to dpl server */
    /*!
     * PRS 접속 시도 ( 80포트 )
     */
    stConfig.dplconnectionfail = TRUE;
    stConfig.prsserver = "prs.nate.com";
    stConfig.prsport = 80;
    if ( connectToServer( stConfig.prsserver, stConfig.prsport ) ) {
        return TRUE;
    } else {
        /*!
         * 1863포트
         */
        stConfig.prsport = 1863;
        return connectToServer( stConfig.prsserver, stConfig.prsport );
    }
}

void NateonDPLConnection::putRCON() {
    QString sCommand( "dpl.nate.com" );
    sCommand += " ";
    sCommand += "5004";
    sCommand += "\r\n";
    sendCommand( "RCON", sCommand );
}

void NateonDPLConnection::gotError421() {
    emit dplError421();
}

#if 0
void NateonDPLConnection::socketError(int nError) {
    if ( stConfig.dplconnectionfail == FALSE )
        openPRSConnection();
}
#endif

#include "nateondplconnection.moc"
