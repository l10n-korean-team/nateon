/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef NATEONDPLCONNECTION_H
#define NATEONDPLCONNECTION_H

#include <qobject.h>
#include <kdebug.h>
#include "nateonconnection.h"

class NateonConnection;
class    Account;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class NateonDPLConnection : public NateonConnection {
    Q_OBJECT
public:
    NateonDPLConnection();

    void initialize();
    bool openConnection();
    bool openPRSConnection();
    void putRCON();
    void putVer();
    void gotPVER(const QStringList& slCommand);
    void gotAUTH(const QStringList& slCommand);
    void gotREQS(const QStringList& slCommand);
    void gotError421();

private:

    bool parseCommand( const QStringList& slCommand );
    bool parseMessage( const QString& sCommand, const QStringList& slCommand, const MimeMessage& message );
    bool parseBuffer( const QStringList& slCommand, QString m_pBuffer ) {
        m_pBuffer = slCommand[0];
        return true;
    };

    QTimer m_loginTimer;

public slots:
    // Protected slots
    // The socket connected, so send the version command.
    void connectionSuccess();
    void closeConnection();
    // void socketError(int nError);

signals:
    void connectDPWithAccount(Account *pAccount);
    void dplError421(); /*! PRS 경유 접속 에러 */
};
#endif
