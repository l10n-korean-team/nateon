/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef NOMP2PLIST_H
#define NOMP2PLIST_H

#include <qobject.h>
#include <qptrlist.h>

class NOMP2PBase;

class NOMP2PList {
public:
    NOMP2PList();
    ~NOMP2PList();
    NOMP2PBase *add( NOMP2PBase *P2PBase );
    /*
    NOMP2PBase *getNOMP2PBaseByIP( const QString &ip );
    NOMP2PBase *getNOMP2PBaseByID( const QString &id );
    */
    NOMP2PBase *getNOMP2PBaseByP2PCookie( const QString &cookie );
    NOMP2PBase *getNOMP2PBaseByDPCookie( const QString &cookie );
    void removeByP2PCookie( const QString &cookie );
    void removeByDPCookie( const QString &cookie );

protected:

private:
    QPtrList<NOMP2PBase> P2PList;
};

#endif
