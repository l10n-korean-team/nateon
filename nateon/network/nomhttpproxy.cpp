/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "nomhttpproxy.h"

NOMHTTPProxy::NOMHTTPProxy(QString Host, unsigned int Port, unsigned int Ticket)
        : sHost( Host ),
        nPort( Port ),
        nTicket( Ticket ) {
}

NOMHTTPProxy::~ NOMHTTPProxy() {
}

QString NOMHTTPProxy::getPOST(QString Command) {
    QString sPostHTML;

    QString sTarget;
    sTarget = sHost;
    sTarget += ":";
    sTarget += QString::number( nPort );

    sPostHTML = "POST http://pri.nate.com:80/ HTTP/1.1\r\n";
    sPostHTML += "Target: " + sTarget + "\r\n";
    sPostHTML += "Proxy-Connection: Keep-Alive\r\n";
    sPostHTML += "Connection: Keep-Alive\r\n";
    sPostHTML += "Pragma: no-cache\r\n";
    sPostHTML += "Ticket: " + QString::number( nTicket ) +"\r\n";
    sPostHTML += "Host: " + sHost + "\r\n";
    sPostHTML += "Content-Length: " + QString::number( Command.length() ) + "\r\n";
    sPostHTML += "\r\n";
    sPostHTML += Command;

    return sPostHTML;
}

