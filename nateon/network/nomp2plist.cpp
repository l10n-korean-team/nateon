/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qptrlist.h>

#include "nomp2plist.h"
#include "nomp2pbase.h"

NOMP2PList::NOMP2PList() {
    P2PList.setAutoDelete( false );
    P2PList.clear();
}

NOMP2PList::~ NOMP2PList() {
}

/*!
  P2P 리스트는 같은 ID에 대해서 여러개의 Session이 생길 수 있다.
  그래서 ID로 검색을 하고 IP로 검색을 하지만 최종 SS Cookie를 가지고
  파일 전송을 가려야 한다.
*/
NOMP2PBase * NOMP2PList::add( NOMP2PBase *P2PBase ) {
//    NOMP2PBase *pBase = getNOMP2PBaseByID( P2PBase->getYourID() );
    /*
      if ( pBase ) {
      if ( pBase->getYourIP() != P2PBase->getYourIP() )
      pBase->setYourIP( P2PBase->getYourIP() );
      return pBase;
      }
      else {
    */
    P2PList.append( P2PBase );
    return P2PBase;
    /*
      }
    */

}
/*
NOMP2PBase * NOMP2PList::getNOMP2PBaseByIP(const QString &ip)
{
    NOMP2PBase *pBase = 0;
    for ( pBase = list.first(); pBase; pBase = list.next() )
        if ( pBase->getYourIP() == ip )
            return pBase;
}

NOMP2PBase * NOMP2PList::getNOMP2PBaseByID(const QString &id)
{
    NOMP2PBase *pBase = 0;
    for ( pBase = list.first(); pBase; pBase = list.next() )
        if ( pBase->getYourID() == id )
            return pBase;
}
*/

NOMP2PBase * NOMP2PList::getNOMP2PBaseByP2PCookie( const QString & cookie ) {
    NOMP2PBase *pBase = 0;
    for ( pBase = P2PList.first(); pBase; pBase = P2PList.next() )
        if ( pBase->getP2PCookie() == cookie )
            return pBase;
}

NOMP2PBase * NOMP2PList::getNOMP2PBaseByDPCookie( const QString & cookie ) {
    NOMP2PBase *pBase = 0;
    for ( pBase = P2PList.first(); pBase; pBase = P2PList.next() )
        if ( pBase->getP2PCookie() == cookie )
            return pBase;
}

void NOMP2PList::removeByP2PCookie(const QString & cookie) {
    NOMP2PBase *pBase = getNOMP2PBaseByP2PCookie( cookie );
    P2PList.remove( pBase );
}

void NOMP2PList::removeByDPCookie(const QString & cookie) {
    NOMP2PBase *pBase = getNOMP2PBaseByDPCookie( cookie );
    P2PList.remove( pBase );
}

