/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KNATEONBUFFER_H
#define KNATEONBUFFER_H

#include <qcstring.h>
#include <qstring.h>
#include <qregexp.h>

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class KNateonBuffer : public QByteArray {
public:
    // Constructor
    KNateonBuffer( unsigned size = 0 );
    // Destructor
    // virtual ~KNateonBuffer();
    ~KNateonBuffer();

    void clear();

    // Add data
    void add( char *str, unsigned int size );
    // Retrieve data
    QByteArray left( unsigned int size ) const;
    // Remove read data
    void remove( unsigned size );
    // Find a newline character
    int findNewline() const;
    // Return the length of the buffer
    int length() const;
    int findString( char *str );
    int findString_re( const QString &csStr );
};
#endif
