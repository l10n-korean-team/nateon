/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "sendfileinfo.h"

#if 0
SendFileInfo::SendFileInfo(QObject * parent, const char * name)
        : QObject(parent, name) {
}
#endif

SendFileInfo::SendFileInfo(const QString MyID, const QString YourID, const QString File, unsigned long Size)
        :m_nCount(0),
        bConnected(FALSE),
        bAccept(FALSE),
        bATHC(FALSE),
        nID(++nCount),
        sMyID(MyID),
        sYourID(YourID),
        sFile(File),
        ulSize(Size),
        m_ulStart(0),
        m_ulSum(0),
        pSocket(0),
        P2PTimer(0),
        bFR(false),
        nDPTid(0),
        m_pChatView(0),
        bCanceled(FALSE),
        m_pFile(0),
        m_pSocketNotifier(0) {
}

SendFileInfo::SendFileInfo(const QString MyID, const QString YourID, const QString DPCookie, const QString FRCookie)
        : m_nCount(0),
        bConnected(FALSE),
        bAccept(FALSE),
        bATHC(FALSE),
        nID(++nCount),
        sMyID(MyID),
        sYourID(YourID),
        ulSize(0),
        m_ulStart(0),
        m_ulSum(0),
        sDPCookie(DPCookie),
        sFRCookie(FRCookie),
        pSocket(0),
        P2PTimer(0),
        bFR(false),
        nDPTid(0),
        m_pChatView(0),
        bCanceled( FALSE ),
        m_pFile(0),
        m_pSocketNotifier(0) {
}


SendFileInfo::~SendFileInfo() {
}

int SendFileInfo::nCount = 0;

void SendFileInfo::startTimer() {
    if (!P2PTimer) {
        P2PTimer = new QTimer(this, "timer");
        connect( P2PTimer, SIGNAL( timeout() ), this, SLOT( slotP2PTimeout() ) );
    }
    P2PTimer->start(5000, TRUE);
}

void SendFileInfo::slotP2PTimeout() {
    emit P2PTimeout(this);
}

int SendFileInfo::getPercent(int nAddSize) {
    m_ulSum += nAddSize;
    unsigned long ulTotal = ulSize - m_ulStart;
    return ( m_ulSum * 100 ) / ulTotal;
}

#include "sendfileinfo.moc"
