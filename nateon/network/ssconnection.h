/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SSCONNECTION_H
#define SSCONNECTION_H

#include <qobject.h>
#include <qptrlist.h>
// #include "nateonconnection.h"
#include "../buddy/buddy.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class SSConnection : public QObject {
    Q_OBJECT
public:
    SSConnection();
    ~SSConnection();

    QString getServer() {
        return m_sSS_Server;
    };
    int getPort() {
        return m_nSS_Port;
    };
    int getConnectionCount() {
        return m_nConnectionCount;
    };
    QPtrList<Buddy>& getBuddyList() {
        return m_BuddyList;
    };
    int getTID() {
        return m_nTID;
    };
    QString getAuthKey() {
        return m_sAuthKey;
    };

    void setServer(QString m_Server) {
        m_sSS_Server = m_Server;
    };
    void setPort(int m_Port) {
        m_nSS_Port = m_Port;
    };
    void setTID(int m_TID) {
        m_nTID = m_TID;
    };
    void setAuthKey(QString m_AuthKey) {
        m_sAuthKey = m_AuthKey;
    };

    void increaseConnectionCount() {
        m_nConnectionCount++;
    };
    void decreaseConnectionCount() {
        m_nConnectionCount--;
    };

    void AddBuddy(Buddy* pBuddy);

private:
    QString m_sSS_Server;
    int m_nConnectionCount;
    int m_nSS_Port;
    QPtrList<Buddy> m_BuddyList;
    int m_nTID;
    QString m_sAuthKey;

signals:
    void BuddyAdded(Buddy* pBuddy);
};
#endif
