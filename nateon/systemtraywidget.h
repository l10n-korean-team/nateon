/***************************************************************************
                          systemtraywidget.h  -  description
                             -------------------
    begin                : Sun Dec 29 2002
    copyright            : (C) 2002 by Mike K. Bennett
    email                : mkb137b@hotmail.com
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SYSTEMTRAYWIDGET_H
#define SYSTEMTRAYWIDGET_H

#include <qwidget.h>
#include <ksystemtray.h>
#include "currentaccount.h"

class CurrentAccount;

/**
 * @brief System tray event handling.
 *
 * The user's status is displayed in the system tray.
 * It also gives access to some settings.
 * This class handles the events fired by the KDE systemtray.
 *
 * @author Mike K. Bennett
 * @ingroup Root
 */
class SystemTrayWidget : public KSystemTray {
    Q_OBJECT

public:
    // The constructor
    SystemTrayWidget(QWidget *parent=0, const char *name=0);
    // The destructor
    ~SystemTrayWidget();
    // Initialize the class
    bool             initialize();
    // Return the context menu
    KPopupMenu      *menu() const;
    void addToastWindow();

private slots:               // Private slots
    // Change the icon when the user's status changes
    void             slotStatusChanged();
};
#endif
