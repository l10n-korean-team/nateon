/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KNATEONMAINVIEWINTERFACE_H
#define KNATEONMAINVIEWINTERFACE_H

#include <qvariant.h>
#include <qwidget.h>
#include <klocale.h>
#include <qvariant.h>
#include <qpushbutton.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qheader.h>
#include <klistview.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qsplitter.h>

#include <kstandarddirs.h>
#include <kdebug.h>
#include <kaboutdata.h>

#include "shapewidget.h"
#include "buddylistview.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QFrame;
class QPushButton;
class QLabel;
class QLineEdit;
class KListView;
class BuddyListView;
class QListViewItem;
class ShapeWidget;
class ShapeButton;
class ShapeWidget;

class MyLineEdit : public QLineEdit {
    Q_OBJECT
public:
    MyLineEdit( QWidget * parent, const char * name = 0 ) :
            QLineEdit(parent, name) {}

protected:
    // void mousePressEvent ( QMouseEvent * e );
    void focusInEvent ( QFocusEvent * e);
    void focusOutEvent ( QFocusEvent * e);
signals:
    void searchEnd();
};


class knateonmainviewinterface : public QWidget {
    Q_OBJECT

public:
    knateonmainviewinterface( QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~knateonmainviewinterface();

    QFrame* frame4;
    ShapeButton* PBbi;
    QLabel* TL_NickName;
    QLabel* TL_Status;
    ShapeButton* PB_Memo;
    ShapeButton* PB_LoginMgr;
    QLabel* memoCountLabel;
    ShapeButton* PB_Cyworld;
    ShapeButton* PB_Hompy;
    ShapeButton* m_pSearchReset;
    QFrame* frame3;
    QLabel* pixmapLabel1;
    MyLineEdit* lineEdit1;
    // ShapeButton* pushButton5;
    ShapeButton* PB_SortList;
    // ShapeButton* pushButton6;
    ShapeButton* PB_AddBuddy;
    // ShapeButton* pushButton7;
    ShapeButton* PB_AddGroup;
    //KListView* listView3;
    BuddyListView* listView3;
    QSplitter* splitter1;
    QFrame* frame6;
    ShapeButton* PB_FreeSMS;
    /*!
     * added by luciferX2@gmail.com, 20081103 for Search
     */
    //bool bSearchMode;

protected:
    QVBoxLayout* knateonmainviewinterfaceLayout;
    QHBoxLayout* frame4Layout;
    QSpacerItem* spacer3_2;
    QSpacerItem* spacer4;
    QVBoxLayout* layout7;
    QHBoxLayout* layout6;
    QHBoxLayout* layout66;
    QSpacerItem* spacer2;
    QSpacerItem* spacer5;
    QHBoxLayout* layout5;
    QHBoxLayout* frame3Layout;
    QHBoxLayout* layout5_2;
    QSpacerItem* spacer3;
    QSpacerItem* spacer10;
    QSpacerItem* spacer11;
    QSpacerItem* spacer12;
    QSpacerItem* spacer13;
    QHBoxLayout* layout50;
    QSpacerItem* spacer50;
    QSpacerItem* spacer2_9;
protected slots:
    virtual void languageChange();
    //void slotShowSearchReset();			// removed by luciferX2@gamil.com
    /*!
     * added by luciferX2@gmail.com, 20081103 for Search
     */
    void slotClickedSearchReset();
};

#endif // KNATEONMAINVIEWINTERFACE_H
