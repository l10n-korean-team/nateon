/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KNATEONDEBUG_H
#define KNATEONDEBUG_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define KNATEON_DEBUG   1

#if ( KNATEON_DEBUG == 1)

#define KNATEONTEST

#define KNATEON_NETWORK_WINDOW

#define KNATEONDEBUG_KNATEON
#define KNATEONDEBUG_KNATEONINTERFACE
#define KNATEONDEBUG_KNATEONVIEW
#define KNATEONDEBUG_KNATEONVIEWINTERFACE
#define KNATEONDEBUG_KNATEONLISTVIEWITEM
#define KNATEONDEBUG_CONTACTLISTVIEWITEM
#define KNATEONDEBUG_GROUPLISTVIEWITEM
#define KNATEONDEBUG_INITIALVIEW
#define KNATEONDEBUG_CONNECTION

#define KNATEONDEBUG_NOTIFICATION
#define KNATEONDEBUG_SWITCHBOARD
#define KNATEONDEBUG_SERVERMESSAGES
#define KNATEONDEBUG_SSLLOGINHANDLER

#define KNATEONDEBUG_ACCOUNT

#define KNATEONDEBUG_CURRENTACCOUNT
#define KNATEONDEBUG_CONTACT
#define KNATEONDEBUG_CONTACTEXTENSION
#define KNATEONDEBUG_CONTACTLIST
#define KNATEONDEBUG_GROUP
#define KNATEONDEBUG_GROUPLIST
#define KNATEONDEBUG_SETTINGSDIALOG
#define KNATEONDEBUG_CHATMASTER
#define KNATEONDEBUG_CHATWINDOW
#define KNATEONDEBUG_CHATVIEW
#define KNATEONDEBUG_CONTACTSIDEBAR
#define KNATEONDEBUG_CONTACTFRAME
#define KNATEONDEBUG_CONTACTACTION
#define KNATEONDEBUG_IDLETIMER
#define KNATEONDEBUG_POPUPNOTIFICATION
#define KNATEONDEBUG_CONTACTOFFLINENOTIFICATION
#define KNATEONDEBUG_CONTACTONLINENOTIFICATION
#define KNATEONDEBUG_EMAILNOTIFICATION
#define KNATEONDEBUG_CHATNOTIFICATION

#define KNATEONDEBUG_MIMEMESSAGE
#define KNATEONDEBUG_APPLICATION
#define KNATEONDEBUG_MIMEAPPLICATION
#define KNATEONDEBUG_P2PAPPLICATION
#define KNATEONDEBUG_FILETRANSFER_P2P
#define KNATEONDEBUG_PICTURETRANSFER_P2P
#define KNATEONDEBUG_TRANSFERFILE
#define KNATEONDEBUG_RECEIVEFILE
#define KNATEONDEBUG_SENDFILE
#define KNATEONDEBUG_IPDIALOG
#define KNATEONDEBUG_TRANSFERPROGRESS
#define KNATEONDEBUG_KDEREMOTEDESKTOP
#define KNATEONDEBUG_REMOTEDESKTOP

#define KNATEONDEBUG_CONTACTPROPERTIES
#endif
#endif
