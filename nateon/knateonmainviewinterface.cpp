/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "knateonmainviewinterface.h"
/*!
 * added by luciferX2@gmail.com, 20081103 for Search
 */
#include <qevent.h>
#include <qapplication.h>
#include "util/common.h"

extern nmconfig stConfig;

#if 0
void MyLineEdit::mousePressEvent ( QMouseEvent * e ) {
    if ( text() == UTF8( "\xec\xb9\x9c\xea\xb5\xac\x20\xea\xb2\x80\xec\x83\x89\xed\x95\x98\xea\xb8\xb0" ) )
        setText("");
    else
        selectAll();

    QLineEdit::mousePressEvent( e );
}
#endif
void MyLineEdit::focusInEvent ( QFocusEvent * e ) {
    if ( text() == UTF8( "\xec\xb9\x9c\xea\xb5\xac\x20\xea\xb2\x80\xec\x83\x89\xed\x95\x98\xea\xb8\xb0" ) )
        setText("");
    else
        selectAll();

    QLineEdit::focusInEvent ( e );
}

void MyLineEdit::focusOutEvent ( QFocusEvent * e ) {
    /*!
     * modified by luciferX2@gmail.com, 20081103 for Search
     */
    if ( text() == "" )
        emit searchEnd();

    QLineEdit::focusOutEvent ( e );
}


/*
 *  Constructs a knateonmainviewinterface as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
knateonmainviewinterface::knateonmainviewinterface( QWidget* parent, const char* name, WFlags fl )
        : QWidget( parent, name, fl ) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString         sPicsPath( dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" ) );

    if ( !name ) setName( "knateonmainviewinterface" );
    setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    setBackgroundOrigin( QWidget::AncestorOrigin );
    setIcon( QPixmap( sPicsPath + "hi16-app-knateon.png" ) );
    setFocusPolicy( QWidget::WheelFocus );
    knateonmainviewinterfaceLayout = new QVBoxLayout( this, 0, 0, "knateonmainviewinterfaceLayout");

    frame4 = new QFrame( this, "frame4" );
    frame4->setPaletteBackgroundPixmap( QPixmap( sPicsPath + "list_title_bluebg.bmp" ) );
    frame4->setFrameShape( QFrame::NoFrame );
    frame4->setFrameShadow( QFrame::Plain );
    frame4Layout = new QHBoxLayout( frame4, 0, 0, "frame4Layout");
    spacer3_2 = new QSpacerItem( 5, 10, QSizePolicy::Fixed, QSizePolicy::Minimum );
    frame4Layout->addItem( spacer3_2 );

    // PBbi = new ShapeButton( frame4, sPicsPath + "bi.bmp" );
    PBbi = new ShapeButton( frame4, sPicsPath + "bi.bmp" );
    PBbi->setMinimumSize( QSize( 37, 37 ) );
    PBbi->setMaximumSize( QSize( 37, 37 ) );
    PBbi->setCursor(Qt::PointingHandCursor);
    frame4Layout->addWidget( PBbi );
    spacer4 = new QSpacerItem( 5, 10, QSizePolicy::Fixed, QSizePolicy::Minimum );
    frame4Layout->addItem( spacer4 );

    layout7 = new QVBoxLayout( 0, 0, 0, "layout7");

    spacer2_9 = new QSpacerItem( 3, 3, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout7->addItem( spacer2_9 );

    layout66 = new QHBoxLayout( 0, 0, 6, "layout6");

    TL_NickName = new QLabel( frame4, "TL_NickName" );
    TL_NickName->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    TL_NickName->setBackgroundOrigin( QLabel::ParentOrigin );
    TL_NickName->setMinimumSize( QSize( 0, 21 ) );
    TL_NickName->setMaximumSize( QSize( 3000, 21 ) );
    TL_NickName-> setAlignment ( Qt::SingleLine | Qt::BreakAnywhere );
    layout66->addWidget( TL_NickName );

    spacer5 = new QSpacerItem( 156, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout66->addItem( spacer5 );

    PB_LoginMgr = new ShapeButton( frame4, sPicsPath + "main_device01.bmp" );
    PB_LoginMgr->setPressedShape( sPicsPath + "main_device01.bmp" );
    PB_LoginMgr->setMouseOverShape( sPicsPath + "main_device01.bmp" );
    PB_LoginMgr->setMinimumSize( QSize( 14, 14 ) );
    PB_LoginMgr->setMaximumSize( QSize( 14, 14 ) );
    PB_LoginMgr->setCursor(Qt::PointingHandCursor);
	
    layout66->addWidget( PB_LoginMgr );

    layout66->addItem(new QSpacerItem( 16, 16, QSizePolicy::Fixed, QSizePolicy::Minimum ));

    layout7->addLayout( layout66 );
     
    layout6 = new QHBoxLayout( 0, 0, 6, "layout6");

    TL_Status = new QLabel( frame4, "TL_Status" );
    TL_Status->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    TL_Status->setBackgroundOrigin( QLabel::ParentOrigin );
    TL_Status->setMinimumSize( QSize( 0, 18 ) );
    TL_Status->setMaximumSize( QSize( 3000, 18 ) );
    layout6->addWidget( TL_Status );

    spacer2 = new QSpacerItem( 156, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addItem( spacer2 );

    PB_Memo = new ShapeButton( frame4, sPicsPath + "main_memo_count_btn.bmp" );
    PB_Memo->setPressedShape( sPicsPath + "main_memo_count_btn_down.bmp" );
    PB_Memo->setMouseOverShape( sPicsPath + "main_memo_count_btn_ov.bmp" );
    PB_Memo->setMinimumSize( QSize( 18, 18 ) );
    PB_Memo->setMaximumSize( QSize( 18, 18 ) );
    PB_Memo->setCursor(Qt::PointingHandCursor);
    QToolTip::add( PB_Memo, UTF8("쪽지 읽기") );
    layout6->addWidget( PB_Memo );

    memoCountLabel = new QLabel( frame4, "memoCountLabel");
    memoCountLabel->setBackgroundOrigin( QLabel::ParentOrigin );
    layout6->addWidget( memoCountLabel);

    PB_Cyworld = new ShapeButton( frame4, sPicsPath + "main_mycyworld_btn.bmp" );
    PB_Cyworld->setPressedShape( sPicsPath + "main_mycyworld_btn_down.bmp" );
    PB_Cyworld->setMouseOverShape( sPicsPath + "main_mycyworld_btn_ov.bmp" );
    PB_Cyworld->setMinimumSize( QSize( 18, 18 ) );
    PB_Cyworld->setMaximumSize( QSize( 18, 18 ) );
    PB_Cyworld->setCursor(Qt::PointingHandCursor);
    QToolTip::add( PB_Cyworld, UTF8("싸이월드 메인으로 이동") );
    layout6->addWidget( PB_Cyworld );

    PB_Hompy = new ShapeButton( frame4, sPicsPath + "main_hompy_btn.bmp" );
    PB_Hompy->setPressedShape(sPicsPath + "main_hompy_btn_down.bmp");
    PB_Hompy->setMouseOverShape(sPicsPath + "main_hompy_btn_ov.bmp");
    PB_Hompy->setMinimumSize( QSize( 18, 18 ) );
    PB_Hompy->setMaximumSize( QSize( 18, 18 ) );
    PB_Hompy->setCursor(Qt::PointingHandCursor);
    QToolTip::add( PB_Hompy, UTF8("내 미니홈피로 이동") );
    layout6->addWidget( PB_Hompy );

    spacer10 = new QSpacerItem( 16, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout6->addItem( spacer10 );

    layout7->addLayout( layout6 );
    frame4Layout->addLayout( layout7 );
    knateonmainviewinterfaceLayout->addWidget( frame4 );

    layout5 = new QHBoxLayout( 0, 0, 6, "layout5");

    frame3 = new QFrame( this, "frame3" );
    frame3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, frame3->sizePolicy().hasHeightForWidth() ) );
    frame3->setPaletteForegroundColor( QColor( 188, 188, 188 ) );
    frame3->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    frame3->setFrameShape( QFrame::Box );
    frame3->setFrameShadow( QFrame::Plain );
    frame3Layout = new QHBoxLayout( frame3, 0, 0, "frame3Layout");

    spacer11 = new QSpacerItem( 6, 6, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout5->addItem(spacer11);

    layout5_2 = new QHBoxLayout( 0, 0, 6, "layout5_2");

    pixmapLabel1 = new QLabel( frame3, "pixmapLabel1" );
    pixmapLabel1->setPixmap( QPixmap(sPicsPath + "bd_search_left_bg.bmp") );
    pixmapLabel1->setScaledContents( TRUE );
    layout5_2->addWidget( pixmapLabel1 );

    lineEdit1 = new MyLineEdit( frame3, "lineEdit1" );
    lineEdit1->setPaletteForegroundColor( QColor( 180, 180, 180 ) );
    //lineEdit1->setFrameShape( QLineEdit::NoFrame );	// blocked by luciferX2@gmail.com temporarily, 20081103 for Search
    lineEdit1->setFrameShadow( QLineEdit::Plain );
    /*!
     * added by luciferX2@gmail.com, 20081103 for Search
     */
    lineEdit1->setAlignment(Qt::AlignVCenter);
    // lineEdit1->setLineWidth( 2 );
    lineEdit1->setMaximumHeight ( 16 );
    lineEdit1->setMinimumHeight ( 16 );
    // connect ( lineEdit1, SIGNAL( textChanged () ), SLOT( slotShowSearchReset() ) );
    layout5_2->addWidget( lineEdit1 );

    m_pSearchReset = new ShapeButton( frame3, sPicsPath + "search_reset_btn.bmp" );
    m_pSearchReset->setMinimumSize( QSize( 14, 14 ) );
    m_pSearchReset->setMaximumSize( QSize( 14, 14 ) );
    m_pSearchReset->setCursor(Qt::PointingHandCursor);
    /*!
     * added by luciferX2@gmail.com, 20081103 for Search
     */
    //connect(m_pSearchReset, SIGNAL(clicked()), SLOT(slotClickedSearchReset()));
    QToolTip::add( m_pSearchReset, UTF8("검색 내용을 지우려면 클릭하세요") );
    layout5_2->addWidget( m_pSearchReset );
    m_pSearchReset->hide();

    spacer3 = new QSpacerItem( 5, 5, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout5_2->addItem( spacer3 );

    frame3Layout->addLayout( layout5_2 );
    layout5->addWidget( frame3 );

    PB_SortList = new ShapeButton( this, sPicsPath + "bd_menu_btn.bmp" );
    PB_SortList->setPressedShape( sPicsPath + "bd_menu_btn_down.bmp" );
    PB_SortList->setMouseOverShape( sPicsPath + "bd_menu_btn_ov.bmp" );
    PB_SortList->setMinimumSize( QSize( 20, 20 ) );
    PB_SortList->setMaximumSize( QSize( 20, 20 ) );
    PB_SortList->setCursor(Qt::PointingHandCursor);
    QToolTip::add( PB_SortList, UTF8("친구/그룹 보기 방식") );
    layout5->addWidget( PB_SortList );

    PB_AddBuddy = new ShapeButton( this, sPicsPath + "bd_buddy_add_btn.bmp" );
    PB_AddBuddy->setPressedShape( sPicsPath + "bd_buddy_add_btn_down.bmp" );
    PB_AddBuddy->setMouseOverShape( sPicsPath + "bd_buddy_add_btn_ov.bmp" );
    PB_AddBuddy->setMinimumSize( QSize( 20, 20 ) );
    PB_AddBuddy->setMaximumSize( QSize( 20, 20 ) );
    PB_AddBuddy->setCursor(Qt::PointingHandCursor);
    QToolTip::add( PB_AddBuddy, UTF8("친구 추가하기") );
    layout5->addWidget( PB_AddBuddy );

    PB_AddGroup = new ShapeButton( this, sPicsPath + "bd_group_add_btn.bmp" );
    PB_AddGroup->setPressedShape( sPicsPath + "bd_group_add_btn_down.bmp" );
    PB_AddGroup->setMouseOverShape( sPicsPath + "bd_group_add_btn_ov.bmp" );
    PB_AddGroup->setMinimumSize( QSize( 20, 20 ) );
    PB_AddGroup->setMaximumSize( QSize( 20, 20 ) );
    PB_AddGroup->setCursor(Qt::PointingHandCursor);
    QToolTip::add( PB_AddGroup, UTF8("그룹 추가하기") );
    layout5->addWidget( PB_AddGroup );

    spacer12 = new QSpacerItem( 16, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout5->addItem(spacer12);

    knateonmainviewinterfaceLayout->addLayout( layout5 );
    spacer13 = new QSpacerItem( 6, 6, QSizePolicy::Fixed, QSizePolicy::Minimum );
    knateonmainviewinterfaceLayout->addItem( spacer13 );

#if 0
    layout50 = new QHBoxLayout( 0, 0, 6, "layout50");
    spacer50 = new QSpacerItem( 6, 6, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout50->addItem( spacer50 );
    listView3 = new KListView( this, "listView3" );
    listView3->setPaletteForegroundColor( QColor( 0x42, 0x42, 0x42 ) );
    listView3->setFrameShape( KListView::NoFrame );
    listView3->setFrameShadow( KListView::Plain );
    listView3->setLineWidth( 0 );
    listView3->setFullWidth( TRUE );
    listView3->addColumn( QString::null );
    listView3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)2, 0, 0, listView3->sizePolicy().hasHeightForWidth() ) );
    /*! 수평 스크롤바 없애기. */
    listView3->setHScrollBarMode( KListView::AlwaysOff );
    listView3->setProperty( "selectionMode", "Multi" );
    layout50->addWidget( listView3 );

    // knateonmainviewinterfaceLayout->addWidget( listView3 );
    knateonmainviewinterfaceLayout->addLayout( layout50 );
#endif
    splitter1 = new QSplitter( this, "splitter1" );
    splitter1->setOrientation( QSplitter::Vertical );

    //listView3 = new KListView( splitter1, "listView3" );
    listView3 = new BuddyListView( splitter1, "listView3" );
    listView3->setFrameShape( KListView::NoFrame );
    listView3->setFrameShadow( KListView::Plain );
    listView3->setLineWidth( 0 );
    listView3->setHScrollBarMode( KListView::AlwaysOff );
    listView3->setProperty( "selectionMode", "Multi" );

    frame6 = new QFrame( splitter1, "frame6" );
    frame6->setFrameShape( QFrame::StyledPanel );
    frame6->setFrameShadow( QFrame::Raised );
    frame6->setMaximumHeight ( 65 );
    frame6->setMinimumHeight ( 65 );
    // frame6->resize(0, 40);

    PB_FreeSMS = new ShapeButton( frame6, sPicsPath + "hotkey_sms_nor.bmp" );
    PB_FreeSMS->setPressedShape( sPicsPath + "hotkey_sms_down.bmp" );
    PB_FreeSMS->setMouseOverShape( sPicsPath + "hotkey_sms_ov.bmp" );
    // PB_FreeSMS->setMinimumSize( QSize( 31, 30 ) );
    // PB_FreeSMS->setMaximumSize( QSize( 31, 30 ) );
    PB_FreeSMS->setGeometry( QRect( 30, 10, 45, 45 ) );
    PB_FreeSMS->setCursor(Qt::PointingHandCursor);
    knateonmainviewinterfaceLayout->addWidget( splitter1 );

    languageChange();
    resize( QSize(426, 616).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
knateonmainviewinterface::~knateonmainviewinterface() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void knateonmainviewinterface::languageChange() {
    setCaption( UTF8( "네이트온 메신저" ) );
    QToolTip::add( this, QString::null );
    // PBbi->setText( QString::null );
    // PBbi->setAccel( QKeySequence( QString::null ) );
    memoCountLabel->setText( "(0)" );
    /*
      PB_Memo->setText( "(0)" );
      PB_Memo->setAccel( QKeySequence( QString::null ) );
    */
    // PB_Cyworld->setText( QString::null );
    // PB_Cyworld->setAccel( QKeySequence( QString::null ) );
    // PB_Hompy->setText( QString::null );
    // PB_Hompy->setAccel( QKeySequence( QString::null ) );
    lineEdit1->setText( UTF8( "\xec\xb9\x9c\xea\xb5\xac\x20\xea\xb2\x80\xec\x83\x89\xed\x95\x98\xea\xb8\xb0" ) );
    // PB_SortList->setText( QString::null );
    /*
      PB_AddBuddy->setText( QString::null );
      PB_AddGroup->setText( QString::null );
    */
    listView3->header()->setLabel( 0, QString::null );
    /// KListView에서 header를 hide
    listView3->header()->hide();
    listView3->setStaticBackground(true);
    //! 배경을 넣으려면
    /// listView3->setPaletteBackgroundPixmap( backgroundPixmap_ );
}

/*!
 * removed by luciferX2@gmail.com, 20081103 for Search
 */
#if 0
void knateonmainviewinterface::slotShowSearchReset() {
    m_pSearchReset->show();
}
#endif

/*!
 * added by luciferX2@gmail.com, 20081103 for Search
 */
void knateonmainviewinterface::slotClickedSearchReset() {
    //bSearchMode = FALSE;
    QApplication::sendEvent(lineEdit1, new QFocusEvent(QEvent::FocusOut));
}


#include "knateonmainviewinterface.moc"
