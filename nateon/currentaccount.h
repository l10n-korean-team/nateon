/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CURRENTACCOUNT_H
#define CURRENTACCOUNT_H

#include "account.h"

class BuddyList;
class GroupList;
/**
   @author Doo-Hyun Jang
*/
class CurrentAccount : public Account {
    Q_OBJECT

public:
    CurrentAccount();
    ~CurrentAccount();

    enum HompyType { Cyworld, Home2 };

    static CurrentAccount* instance();
    virtual void copyAccount(const Account *pAccount);

    // Getter
    BuddyList* getBuddyList() const             {
        return m_pBuddyList;
    }
    GroupList* getGroupList() const             {
        return m_pGroupList;
    }
    const QString getMyID() const;
    const QString getMyCMN() const              {
        return m_sMyCMN;
    }
    const QString getMyName() const             {
        return m_sMyName;
    }
    const QString getMyNickName() const         {
        return m_sMyNickName;
    }
    const QString getMyPhone() const            {
        return m_sMyPhone;
    }
    const QString getMyEmail() const            {
        return m_sMyEmail;
    }
    const QString getMyTicket() const           {
        return m_sMyTicket;
    }
    const QString getMyCyworldCMN() const       {
        return m_sMyCyworldCMN;
    }
    const int getMyAuthYN() const               {
        return m_nMyAuthYN;
    }
    const QString getMyMIMID() const            {
        return m_sMyMIMID;
    }
    const QString getMyMusicDate() const        {
        return m_sMyMusicDate;
    }
    const char getMyLoginType() const           {
        return m_cMyLoginType;
    }
    const QString getMyNateCMN() const          {
        return m_sMyNateCMN;
    }
    const QString getMyNateID() const           {
        return m_sMyNateID;
    }
    const QString getMyCyworldID() const        {
        return m_sMyCyworldID;
    }
    const bool getMyTongYN() const              {
        return m_bMyTongYN;
    }
    const bool getMyTownYN() const              {
        return m_bMyTownYN;
    }
    const char getStatus() const                {
        return m_cStatus;
    }
    /*! 홈피 새글 등록 */
    const bool isMyHompyNew() const             {
        return m_bHompyNew;
    }
    /*! 홈2 ID */
    const QString getMyHome2ID() const          {
        return m_sMyHome2ID;
    }
    /*! 홈2 CMN */
    const QString getMyHome2CMN() const         {
        return m_sMyHome2CMN;
    }
    /*! 홈피 타입, 싸이월드 미니홈피, Home2 */
    HompyType getHompyType() const              {
        return (HompyType)m_unHompyType;
    }
    /*! 주계정 CMN */
    const QString getMyMajorCMN() const         {
        return m_sMyMajorCMN;
    }
    const QString getMyArea() const             {
        return m_sMyArea;
    }
    const QString getMyEmpasCMN() const         {
        return m_sMyEmpasCMN;
    }
    const QString getMyEmpasID() const          {
        return m_sMyEmpasID;
    }
    /*! Home2를 사용하는가? */
    bool useHome2()                             {
        return !( ( m_sMyHome2ID == "%00" ) || ( m_sMyHome2ID == "" ) );
    }
    /*! Cyworld를 사용하는가? */
    bool useCyworld()                           {
        return !( ( m_sMyCyworldID == "%00" ) || ( m_sMyCyworldID == "" ) );
    }
    bool useOTP() {
      return m_bMyOTP;
    }
    
    const QString getMyDPKey() const            {
      return m_sMyDPKey;
    }
    
    // Setter
    void setBuddyList(BuddyList *pBuddyList)    {
        m_pBuddyList = pBuddyList;
    }
    void setGroupList(GroupList *pGroupList)    {
        m_pGroupList = pGroupList;
    }
    void setMyCMN(QString m_CMN)                {
        m_sMyCMN = m_CMN;
    }
    void setMyName(QString m_Name)              {
        m_sMyName = m_Name;
    }
    void setMyNickName(QString m_NickName)      {
        m_sMyNickName = m_NickName;
    }
    void setMyPhone(QString m_Phone)            {
        m_sMyPhone = m_Phone;
    }
    void setMyEmail(QString m_Email)            {
        m_sMyEmail = m_Email;
    }
    void setMyTicket(QString m_Ticket)          {
        m_sMyTicket = m_Ticket;
    }
    void setMyCyworldCMN(QString m_CyworldCMN)  {
        m_sMyCyworldCMN = m_CyworldCMN;
    }
    void setMyAuthYN(int m_AuthYN)              {
        m_nMyAuthYN = m_AuthYN;
    }
    void setMyMIMID(QString m_MIMID)            {
        m_sMyMIMID = m_MIMID;
    }
    void setMyMusicDate(QString m_MusicDate)    {
        m_sMyMusicDate = m_MusicDate;
    }
    void setMyLoginType(char m_LoginType)       {
        m_cMyLoginType = m_LoginType;
    }
    void setMyNateCMN(QString m_NateCMN)        {
        m_sMyNateCMN = m_NateCMN;
    }
    void setMyNateID(QString m_NateID)          {
        m_sMyNateID = m_NateID;
    }
    void setMyCyworldID(QString m_CyworldID)    {
        m_sMyCyworldID = m_CyworldID;
    }
    void setMyTongYN(bool m_TongYN)             {
        m_bMyTongYN = m_TongYN;
    }
    void setMyTownYN(bool m_TownYN)             {
        m_bMyTownYN = m_TownYN;
    }
    void setStatus(char status)                 {
        m_cStatus = status;
        emit changedStatus();
    }
    /*! 홈피 새글 등록 */
    void setMyHompyNew( bool bHompyNew )        {
        m_bHompyNew = bHompyNew;
    }
    /*! 홈2 ID */
    void setMyHome2ID( QString sID )            {
        m_sMyHome2ID = sID;
    }
    /*! 홈2 CMN */
    void setMyHome2CMN( QString sCMN )          {
        m_sMyHome2CMN = sCMN;
    }
    /*! 한국 : KR */
    void setMyArea( QString sArea )             {
        m_sMyArea = sArea;
    }
    /*! 엠파스 CMN */
    void setMyEmpasCMN( QString sEmpasCMN )     {
        m_sMyEmpasCMN = sEmpasCMN;
    }
    /*! 엠파스 ID */
    void setMyEmpasID( QString sEmpasID )       {
        m_sMyEmpasID = sEmpasID;
    }
    void setHompyType( HompyType unType )       {
        m_unHompyType = unType;
    }
    /*! 주계정 CMN 등록 */
    void setMyMajorCMN( QString sCMN )          {
        m_sMyMajorCMN = sCMN;
    }
    
    void setMyOTP( bool bOTP )                  {
      m_bMyOTP=bOTP;
    }
    
    /*! DP KEY */
    void setMyDPKey( QString sDPKey )           {
      m_sMyDPKey = sDPKey;
    }

private:
    // Return a singleton instance of the current account
    static CurrentAccount* pInstance;
    BuddyList* m_pBuddyList;
    GroupList* m_pGroupList;

    /*! LSIN */
    QString m_sMyCMN;
    QString m_sMyName;
    QString m_sMyNickName;
    QString m_sMyPhone;
    QString m_sMyEmail;
    QString m_sMyTicket;
    QString m_sMyCyworldCMN;
    int m_nMyAuthYN;
    QString m_sMyMIMID;
    QString m_sMyMusicDate;   /*! 20070312 */
    char m_cMyLoginType;      /*! N : NateID로 로그인 / C : 싸이월드ID로 로그인 */
    QString m_sMyNateCMN;     /*! 네이트 CMN */
    QString m_sMyNateID;      /*! 네이트 ID */
    QString m_sMyCyworldID;   /*! 싸이월드 ID */
    QString m_sMyHome2ID;     /*! 싸이홈2 ID */
    QString m_sMyHome2CMN;    /*! 싸이홈2 CMN */
    bool m_bMyTongYN;         /*! Y/N */
    bool m_bMyTownYN;         /*! Y/N */
    char m_cStatus;           /*! O:온라인, B:자리비움, A:자리비움, P:통화중, M:회의중, X:오프라인으로표시. */
    bool m_bHompyNew;         /*! 홈피 새글 등록 */
    HompyType m_unHompyType;  /*! 홈피타입 : 싸이월드미니홈피, Home2 */
    QString m_sMyMajorCMN;    /*! 주계정 CMN */
    QString m_sMyArea;        /*! 한국: KR */
    QString m_sMyEmpasCMN;    /*! 엠파스 CMN */
    QString m_sMyEmpasID;     /*! 엠파스 ID */
    
    bool m_bMyOTP;            /*! OTP */
    QString m_sMyDPKey;       /*! 멀티세션에서 접속한 DP Key */

signals:
    void changedStatus();
};
#endif
