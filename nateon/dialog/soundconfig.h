/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SOUNDCONFIG_H
#define SOUNDCONFIG_H

#include <klistview.h>
#include <qheader.h>
#include <qlistview.h>
#include <klocale.h>
#include <qpushbutton.h>
#include <qfiledialog.h>
#include <kdebug.h>
#include <qlineedit.h>
#include <kconfig.h>
#include <kapplication.h>
#include <kstandarddirs.h>
#include <kglobal.h>
#include <qsound.h>
#include <qmessagebox.h>
#include <kurllabel.h>
#include <kurl.h>
#include <krun.h>
#include <qgroupbox.h>

#include "soundconfigview.h"

class KRun;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class SoundConfig : public SoundSelectDialog {
    Q_OBJECT
public:
    SoundConfig(QWidget *parent = 0, const char *name = 0);
    ~SoundConfig();
    // void setEnabled( bool bUse );
    void init();
    void show();

private:
    QCheckListItem  *pFriendConnect;
    QCheckListItem  *pReceiveMemo;
    QCheckListItem  *pFileTransferDone;
    QCheckListItem  *pLogin;
    QCheckListItem  *pChat;
    QCheckListItem  *pMiniHompyNew;

#if 0
    QString sFriendConnectSoundFile;
    QString sReceiveMemoSoundFile;
    QString sFileTransferDoneSoundFile;
    QString sLoginSoundFile;
    QString sChatSoundFile;
    QString sMiniHompyNewSoundFile;
#endif

    /*! 환경설정 */
    KConfig         *config;

    QString sSoundPath;
    /*
      bool bIsOnFriendConnect;
      bool bIsOnReceiveMemo;
      bool bIsOnFileTransferDone;
      bool bIsOnLogin;
      bool bIsOnChat;
      bool bIsOnMiniHompyNew;
    */
    int nListID;

protected:
    void accept();
    // void reject();

private slots:
    void slotShowFileSelectDialog();
    void slotSelectList( QListViewItem *pItem );
    void slotPlaySound();
    void slotSoundDownURL( const QString& sURL );
    void slotUseSound( bool bUse );
};

#endif
