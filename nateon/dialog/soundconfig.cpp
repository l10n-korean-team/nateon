/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "soundconfig.h"
#include <kfiledialog.h>
#include <kaboutdata.h>
#include "../util/common.h"
#include "../util/sound.h"
#include "utils.h"

extern nmconfig stConfig;

SoundConfig::SoundConfig(QWidget *parent, const char *name)
        : SoundSelectDialog(parent, name),
        nListID(0)
        /*,
          bIsOnFriendConnect(true),
          bIsOnReceiveMemo(true),
          bIsOnFileTransferDone(true),
          bIsOnLogin(true),
          bIsOnChat(true),
          bIsOnMiniHompyNew(true)
        */
{
    KStandardDirs   *dirs   = KGlobal::dirs();
    sSoundPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/sound/" );

    config = kapp->config();

    resize( QSize(394, 260).expandedTo(minimumSizeHint()) );

    soundListView->clear();
    soundListView->header()->hide();
    soundListView->setSortColumn(-1);

    pMiniHompyNew = new QCheckListItem(soundListView, UTF8("미니홈피 새글 알림"), QCheckListItem::CheckBox );
    pChat = new QCheckListItem(soundListView, UTF8("대화 메시지 알림"), QCheckListItem::CheckBox );
    pLogin = new QCheckListItem(soundListView, UTF8("본인 로그인"), QCheckListItem::CheckBox );
    pFileTransferDone = new QCheckListItem(soundListView, UTF8("파일 전송 완료"), QCheckListItem::CheckBox );
    pReceiveMemo= new QCheckListItem(soundListView, UTF8("쪽지 받음"), QCheckListItem::CheckBox );
    pFriendConnect = new QCheckListItem(soundListView, UTF8("친구 접속 알림"), QCheckListItem::CheckBox );

    connect( selectFileButton, SIGNAL( clicked() ), this, SLOT( slotShowFileSelectDialog() ) );
    connect( soundListView, SIGNAL( clicked ( QListViewItem * ) ), this, SLOT( slotSelectList( QListViewItem * ) ) );
    connect( playButton, SIGNAL( clicked() ), SLOT( slotPlaySound() ) );
    connect( soundDownKURLLabel, SIGNAL( leftClickedURL( const QString& ) ), SLOT( slotSoundDownURL( const QString& ) ) );
    connect( soundGroupBox, SIGNAL( toggled( bool ) ), SLOT( slotUseSound( bool ) ) );
}


SoundConfig::~SoundConfig() {
}

void SoundConfig::init() {
    /*! 초기화 */
    config->setGroup("Sound");

#if 1
    stConfig.usesound = config->readBoolEntry("UseSound", false);

    stConfig.buddyconnectsoundpath = config->readEntry("FriendConnect", sSoundPath + "buddy_login.wav");
    stConfig.memorecievesoundpath = config->readEntry("ReceiveMemo", sSoundPath + "recv_memo.wav");
    stConfig.filesenddonesoundpath = config->readEntry("FileTransferDone", sSoundPath + "trans_comp.wav");
    stConfig.myloginsoundpath = config->readEntry("Login", sSoundPath + "login.wav");
    stConfig.startchatsoundpath = config->readEntry( "ChatRequest", sSoundPath + "recv_chat.wav" );
    stConfig.minihompynewsoundpath = config->readEntry( "MiniHompyNew", sSoundPath + "minihompy.wav" );

    if ( stConfig.usesound ) {
        stConfig.usebuddyconnectsound = config->readBoolEntry("IsOnFriendConnect", false );
        stConfig.usememorecievesound = config->readBoolEntry("IsOnReceiveMemo", false );
        stConfig.usefilesenddonesound = config->readBoolEntry("IsOnFileTransferDone", false );
        stConfig.usemyloginsound = config->readBoolEntry("IsOnLogin", false );
        stConfig.usestartchatsound = config->readBoolEntry("IsOnChatRequest", false );
        stConfig.useminihompynewsound = config->readBoolEntry( "IsOnMiniHompyNew", false );
    } else {
        stConfig.usebuddyconnectsound = FALSE;
        stConfig.usememorecievesound = FALSE;
        stConfig.usefilesenddonesound = FALSE;
        stConfig.usemyloginsound = FALSE;
        stConfig.usestartchatsound = FALSE;
        stConfig.useminihompynewsound = FALSE;
    }

    soundGroupBox->setChecked ( stConfig.usesound );
    pFriendConnect->setOn( stConfig.usebuddyconnectsound );
    pReceiveMemo->setOn( stConfig.usememorecievesound );
    pFileTransferDone->setOn( stConfig.usefilesenddonesound );
    pLogin->setOn( stConfig.usemyloginsound );
    pChat->setOn( stConfig.usestartchatsound );
    pMiniHompyNew->setOn( stConfig.useminihompynewsound );
#endif
#if 0
    soundGroupBox->setCheckable ( config->readBoolEntry("UseSound", true ) );

    pFriendConnect->setOn( config->readBoolEntry("IsOnFriendConnect", true ) );
    sFriendConnectSoundFile = config->readEntry("FriendConnect", sSoundPath + "buddy_login.wav");

    pReceiveMemo->setOn( config->readBoolEntry("IsOnReceiveMemo", true ) );
    sReceiveMemoSoundFile = config->readEntry("ReceiveMemo", sSoundPath + "recv_memo.wav");

    pFileTransferDone->setOn( config->readBoolEntry("IsOnFileTransferDone", true ) );
    sFileTransferDoneSoundFile = config->readEntry("FileTransferDone", sSoundPath + "trans_comp.wav");

    pLogin->setOn( config->readBoolEntry("IsOnLogin", true ) );
    sLoginSoundFile = config->readEntry("Login", sSoundPath + "login.wav");

    pChat->setOn( config->readBoolEntry("IsOnChat", true ) );
    sChatSoundFile = config->readEntry( "ChatRequest", sSoundPath + "recv_chat.wav" );

    pMiniHompyNew->setOn( config->readBoolEntry( "IsOnMiniHompyNew", true ) );
    sMiniHompyNewSoundFile = config->readEntry( "MiniHompyNew", sSoundPath + "minihompy.wav" );
#endif
}

void SoundConfig::slotShowFileSelectDialog() {
    QString sFile( filePathLineEdit->text() );

    if ( sFile.isEmpty() ) {
        // sFile = QDir::homeDirPath();
        sFile = "/usr/share/apps/knateon/sound/";
    }
    // sFile += "/xxx.txt";
    QString sFilePath = KFileDialog::getOpenFileName(
                            sFile,
                            // UTF8("소리파일 (*.wav *.mp3)"),
                            UTF8("*.wav|소리 파일 (*.wav)"),
                            this,
                            UTF8("소리 파일 선택")
                        );

    QString sTemp = sFilePath.stripWhiteSpace();
    if ( sTemp.length() > 0 ) {
        filePathLineEdit->setText( sFilePath );
        switch ( nListID ) {
        case 1:
            stConfig.buddyconnectsoundpath = sFilePath;
            break;
        case 2:
            stConfig.memorecievesoundpath = sFilePath;
            break;
        case 3:
            stConfig.filesenddonesoundpath = sFilePath;
            break;
        case 4:
            stConfig.myloginsoundpath = sFilePath;
            break;
        case 5:
            stConfig.startchatsoundpath = sFilePath;
            break;
        case 6:
            stConfig.minihompynewsoundpath = sFilePath;
            break;
        }
    }
}

void SoundConfig::slotSelectList(QListViewItem * pItem) {
#ifdef NETDEBUG
    kdDebug() << "Entered!!!" << endl;
#endif

    if ( static_cast <QCheckListItem*> ( pItem ) == pFriendConnect ) {
        filePathLineEdit->setText( stConfig.buddyconnectsoundpath );
        nListID = 1;
    } else if ( static_cast <QCheckListItem*> ( pItem ) == pReceiveMemo ) {
        filePathLineEdit->setText( stConfig.memorecievesoundpath );
        nListID = 2;
    } else if ( static_cast <QCheckListItem*> ( pItem ) == pFileTransferDone ) {
        // filePathLineEdit->setText( sFileTransferDoneSoundFile );
        filePathLineEdit->setText( stConfig.filesenddonesoundpath );
        nListID = 3;
    } else if ( static_cast <QCheckListItem*> ( pItem ) == pLogin ) {
        // filePathLineEdit->setText( sLoginSoundFile );
        filePathLineEdit->setText( stConfig.myloginsoundpath );
        nListID = 4;
    } else if ( static_cast <QCheckListItem*> ( pItem ) == pChat ) {
        // filePathLineEdit->setText( sChatSoundFile );
        filePathLineEdit->setText( stConfig.startchatsoundpath );
        nListID = 5;
    } else if ( static_cast <QCheckListItem*> ( pItem ) == pMiniHompyNew ) {
        // filePathLineEdit->setText( sMiniHompyNewSoundFile );
        filePathLineEdit->setText( stConfig.minihompynewsoundpath );
        nListID = 6;
    }
}

void SoundConfig::accept() {
    config->setGroup("Sound");
    stConfig.usesound = soundGroupBox->isChecked();
    stConfig.usebuddyconnectsound = pFriendConnect->isOn();
    stConfig.usememorecievesound = pReceiveMemo->isOn();
    stConfig.usefilesenddonesound = pFileTransferDone->isOn();
    stConfig.usemyloginsound = pLogin->isOn();
    stConfig.usestartchatsound = pChat->isOn();
    stConfig.useminihompynewsound = pMiniHompyNew->isOn();

    config->writeEntry( "UseSound", stConfig.usesound );
    config->writeEntry( "IsOnFriendConnect", stConfig.usebuddyconnectsound );
    config->writeEntry( "IsOnReceiveMemo", stConfig.usememorecievesound );
    config->writeEntry( "IsOnFileTransferDone", stConfig.usefilesenddonesound );
    config->writeEntry( "IsOnLogin", stConfig.usemyloginsound );
    config->writeEntry( "IsOnChatRequest", stConfig.usestartchatsound );
    config->writeEntry( "IsOnMiniHompyNew", stConfig.useminihompynewsound );

    config->writeEntry( "FriendConnect", stConfig.buddyconnectsoundpath );
    config->writeEntry( "ReceiveMemo", stConfig.memorecievesoundpath );
    config->writeEntry( "FileTransferDone", stConfig.filesenddonesoundpath );
    config->writeEntry( "Login", stConfig.myloginsoundpath );
    config->writeEntry( "ChatRequest", stConfig.startchatsoundpath );
    config->writeEntry( "MiniHompyNew", stConfig.minihompynewsoundpath );
    config->sync();
    SoundSelectDialog::accept();
}

void SoundConfig::slotPlaySound() {
#ifdef NETDEBUG
    kdDebug() << "PLAY FILE : [" << filePathLineEdit->text() << "]" << endl;
#endif
    if (QFile( filePathLineEdit->text() ).exists() ) {
        Sound::play( filePathLineEdit->text() );
    }
#ifdef NETDEBUG
    else {
        kdDebug() << "Not Exist!!! [" << filePathLineEdit->text() << "]" << endl;
    }
#endif
}

void SoundConfig::show() {
    init();
    SoundSelectDialog::show();
#if 0
    if (!QSound::isAvailable()) {
        // Bail out.  Programs in which sound is not critical
        // could just silently (hehe) ignore the lack of a server.
        //
        /*
          QMessageBox::warning(this,"No Sound",
          "<p><b>Sorry, you are not running the Network Audio System.</b>"
          "<p>If you have the `au' command, run it in the background before this program. "
          "The latest release of the Network Audio System can be obtained from:"
          "<pre>\n"
          " &nbsp;\n"
          "   ftp.ncd.com:/pub/ncd/technology/src/nas\n"
          "   ftp.x.org:/contrib/audio/nas\n"
          "</pre>"
          "<p>Release 1.2 of NAS is also included with the X11R6"
          "contrib distribution."
          "<p>After installing NAS, you will then need to reconfigure Qt with NAS sound support");
        */
        QMessageBox::warning(this,"No Sound",
                             UTF8("<p><b>죄송합니다. 소리를 들으려면 Network Audio System 이 필요합니다..</b>"
                                  "<p>If you have the `au' command, run it in the background before this program. "
                                  "The latest release of the Network Audio System can be obtained from:"
                                  "<pre>\n"
                                  " &nbsp;\n"
                                  "   ftp.ncd.com:/pub/ncd/technology/src/nas\n"
                                  "   ftp.x.org:/contrib/audio/nas\n"
                                  "</pre>"
                                  "<p>Release 1.2 of NAS is also included with the X11R6"
                                  "contrib distribution."
                                  "<p>After installing NAS, you will then need to reconfigure Qt with NAS sound support") );
    }
#endif
}

void SoundConfig::slotSoundDownURL(const QString & sURL) {
    LNMUtils::openURL( sURL );
}


#if 0
void SoundConfig::setEnabled(bool bUse) {
    soundGroupBox-> setChecked ( stConfig.usesound );
}
#endif

void SoundConfig::slotUseSound( bool bUse ) {
    filePathLineEdit->setEnabled( bUse );
    playButton->setEnabled( bUse );
    selectFileButton->setEnabled( bUse );
    pFriendConnect->setOn( bUse );
    pReceiveMemo->setOn( bUse );
    pFileTransferDone->setOn( bUse );
    pLogin->setOn( bUse );
    pChat->setOn( bUse );
    pMiniHompyNew->setOn( bUse );
}

#include "soundconfig.moc"
