/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ADDBUDDYSELECTOR_H
#define ADDBUDDYSELECTOR_H

#include <klocale.h>
#include <qlistview.h>
#include <qcombobox.h>
#include <kdebug.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qregexp.h>

#include "../buddy/buddy.h"
#include "../buddy/buddylist.h"
#include "../buddy/group.h"
#include "../buddy/grouplist.h"
#include "addbuddies.h"

class Buddy;
class BuddyList;
class Group;
class GroupList;
class QListViewItem;
class QComboBox;
class QPushButton;
class QLineEdit;
class QRegExp;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class AddBuddySelector : public AddForm {
    Q_OBJECT
public:
    AddBuddySelector( QWidget* parent = 0, const char* name = 0, WFlags fl = 0);
    ~AddBuddySelector();

    void setAllList();
    void setListByGroup(QString mGroupName);

    void setGroupList( const GroupList *GroupList );
    void setBuddyList( const BuddyList *BuddyList );
    void setSelectedBuddies(QString sBuddies);
    void setExceptList(QStringList &Except) {
        slExcept = Except;
    }

private:
    BuddyList *pBuddyList;
    GroupList *pGroupList;

    QPtrList<Buddy> m_BuddyList;
    QPtrList<Group> m_GroupList;

    QStringList slExcept;

private slots:
    void slotSelectGroup(const QString &sGroup);
    void slotSelectBuddies();
    void slotRemoveBuddies();
    void slotSearchBuddies();
    void slotSelectedOK();
    void slotNameChanged(const QString &sName);

signals:
    void updateList();
    void selectedBuddies( const QString &);
};

#endif
