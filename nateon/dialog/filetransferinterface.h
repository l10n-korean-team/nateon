/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FILETRANSFERFORM_H
#define FILETRANSFERFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>
#include <klocale.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QProgressBar;
class QPushButton;
class QGroupBox;
class QFrame;
class QListView;
class QListViewItem;

class FileTransferForm : public QDialog {
    Q_OBJECT

public:
    FileTransferForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~FileTransferForm();

    QLabel* fileNameLabel;
    QProgressBar* progressBar1;
    QPushButton* cancelButton;
    QLabel* kbStatusLabel;
    QPushButton* closeButton;
    QGroupBox* fileListGroupBox;
    QFrame* line1;
    QListView* fileListView;

protected:
    QGridLayout* FileTransferFormLayout;
    QSpacerItem* spacer5;
    QHBoxLayout* layout1;
    QSpacerItem* spacer1;
    QHBoxLayout* layout6;
    QHBoxLayout* layout3;
    QSpacerItem* spacer2;
    QHBoxLayout* layout4;
    QSpacerItem* spacer3;
    QSpacerItem* spacer4;
    QVBoxLayout* fileListGroupBoxLayout;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // FILETRANSFERFORM_H
