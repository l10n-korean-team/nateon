/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ADDFRIENDFORM_H
#define ADDFRIENDFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>
#include <klocale.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QTabWidget;
class QWidget;
class QLabel;
class QLineEdit;
class QComboBox;
class QTextEdit;
class QPushButton;
class QListView;
class QListViewItem;

class AddFriendForm : public QDialog {
    Q_OBJECT

public:
    AddFriendForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~AddFriendForm();

    QTabWidget* tabWidget;
    QWidget* tab1;
    QLabel* textLabel1;
    QLabel* textLabel2;
    QLabel* textLabel4;
    QLineEdit* lineeditID;
    QLabel* textLabel3;
    QComboBox* comboBox1;
    QTextEdit* texteditMessage;
    QPushButton* buttonRequire;
    QPushButton* buttonCancel;
    QWidget* tab2;
    QLabel* textLabel5;
    QLabel* textLabel10;
    QComboBox* comboBoxNo1;
    QLabel* textLabel6;
    QLineEdit* lineEditNo2;
    QLabel* textLabel7;
    QLineEdit* lineEditNo3;
    QLabel* textLabel8;
    QLineEdit* lineEditName1;
    QPushButton* pushButtonSearch1;
    QLabel* textLabel11;
    QLineEdit* lineEditName2;
    QLabel* textLabel12;
    QComboBox* comboBoxGender;
    QLabel* textLabel13;
    QLineEdit* lineEditAgeFrom;
    QLabel* textLabel14;
    QLineEdit* lineEditAgeTo;
    QPushButton* pushButtonSearch2;
    QListView* listViewResult;
    QLabel* textLabel9;
    QTextEdit* textEditMessage;
    QPushButton* pushButtonRequire;
    QPushButton* pushButtonCancel;

protected:
    QVBoxLayout* AddFriendFormLayout;
    QVBoxLayout* tab1Layout;
    QVBoxLayout* layout14;
    QHBoxLayout* layout13;
    QVBoxLayout* layout12;
    QVBoxLayout* layout5;
    QSpacerItem* spacer1;
    QVBoxLayout* layout11;
    QHBoxLayout* layout10;
    QHBoxLayout* layout7;
    QSpacerItem* spacer2;
    QSpacerItem* spacer3;
    QVBoxLayout* tab2Layout;
    QVBoxLayout* layout16;
    QHBoxLayout* layout17;
    QHBoxLayout* layout18;
    QHBoxLayout* layout19;
    QVBoxLayout* layout21;
    QSpacerItem* spacer6;
    QHBoxLayout* layout20;
    QSpacerItem* spacer4;
    QSpacerItem* spacer5;

protected slots:
    virtual void languageChange();
    void slotHostSelect( int nID );

private:
    QPixmap image0;

};

#endif // ADDFRIENDFORM_H
