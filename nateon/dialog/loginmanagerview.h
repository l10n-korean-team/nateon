/****************************************************************************
** Form interface generated from reading ui file 'loginmanagerview.ui'
**
** Created: 월  5월 3 23:52:47 2010
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef LOGINMANAGER_H
#define LOGINMANAGER_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>
#include <kconfig.h>
#include <kstandarddirs.h>
#include <kaboutdata.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QFrame;
class QPushButton;
class QTextEdit;
class QComboBox;
class QCheckBox;
class QButtonGroup;
class QHBox;

class LoginManager : public QDialog
{
    Q_OBJECT

public:
    LoginManager( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~LoginManager();
	void updateSessionList();

    QLabel* topTextLabel;
    QFrame* line1;
    QFrame* line2;
    QLabel* curTextLabel;
    QLabel* curPixmapLabel;
    QLabel* curInfotextLabel;
    QLabel* plusPixmapLabel;
    QLabel* otherTextLabel;
    QPushButton* allLogoutPushButton;
    QTextEdit* otherInfoTextEdit;
    QLabel* statusTextLabel;
    QComboBox* statusComboBox;
    QCheckBox* loginViewCheckBox;
    QPushButton* okPushButton;

protected:
    QHBoxLayout* LoginManagerLayout;
    QVBoxLayout* layout14;
    QHBoxLayout* layout11;
    QVBoxLayout* layout9;
    QSpacerItem* spacer4;
	QSpacerItem* spacer5;
    QHBoxLayout* layout7;
    QVBoxLayout* layout5;
    QHBoxLayout* layout4;
    QHBoxLayout* layout12;
    QHBoxLayout* layout13;
	QHBox*		 emptyBox;
	QPtrList<QHBox> listBoxes;

protected slots:
    virtual void languageChange();
    void okClicked();
	void allLogoutClicked();
	void logoutClicked(); 

private:
    QPixmap image0;
    QPixmap image1;
    QPixmap image2;
    QString sPicsPath;
	KConfig* config;

signals:
	void allLogout( LoginManager* ); 
	void otherLocLogout( QString & );
};

#endif // LOGINMANAGER_H
