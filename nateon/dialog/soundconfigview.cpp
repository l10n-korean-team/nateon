/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "soundconfigview.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qheader.h>
#include <kurllabel.h>
#include <qlineedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>


/*
 *  Constructs a SoundSelectDialog as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
SoundSelectDialog::SoundSelectDialog( QWidget* parent, const char* name, bool modal, WFlags fl )
        : QDialog( parent, name, modal, fl ) {
    if ( !name )
        setName( "SoundSelectDialog" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)1, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setSizeGripEnabled( TRUE );
    SoundSelectDialogLayout = new QVBoxLayout( this, 11, 6, "SoundSelectDialogLayout");

    soundGroupBox = new QGroupBox( this, "soundGroupBox" );
    soundGroupBox->setAlignment( int( QGroupBox::AlignAuto ) );
    soundGroupBox->setCheckable( TRUE );
    soundGroupBox->setChecked( TRUE );
    soundGroupBox->setColumnLayout(0, Qt::Vertical );
    soundGroupBox->layout()->setSpacing( 6 );
    soundGroupBox->layout()->setMargin( 11 );
    soundGroupBoxLayout = new QVBoxLayout( soundGroupBox->layout() );
    soundGroupBoxLayout->setAlignment( Qt::AlignTop );

    /*! QCheckListItem */

    soundListView = new KListView( soundGroupBox, "soundListView" );
    soundListView->addColumn( tr( "Column 1" ) );
    soundGroupBoxLayout->addWidget( soundListView );
    SoundSelectDialogLayout->addWidget( soundGroupBox );

    layout4 = new QHBoxLayout( 0, 0, 6, "layout4");
    spacer2 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer2 );

    soundDownKURLLabel = new KURLLabel( this, "soundDownKURLLabel" );
    layout4->addWidget( soundDownKURLLabel );
    SoundSelectDialogLayout->addLayout( layout4 );

    layout3 = new QHBoxLayout( 0, 0, 6, "layout3");

    filePathLineEdit= new QLineEdit( this, "filePathLineEdit" );
    filePathLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, filePathLineEdit->sizePolicy().hasHeightForWidth() ) );
    filePathLineEdit->setMinimumSize( QSize( 200, 0 ) );
    layout3->addWidget( filePathLineEdit );

    playButton = new QPushButton( this, "playButton" );
    layout3->addWidget( playButton );

    selectFileButton = new QPushButton( this, "selectFileButton" );
    layout3->addWidget( selectFileButton );
    SoundSelectDialogLayout->addLayout( layout3 );

    Layout1 = new QHBoxLayout( 0, 0, 6, "Layout1");
#if 0
    buttonHelp = new QPushButton( this, "buttonHelp" );
    buttonHelp->setAutoDefault( TRUE );
    Layout1->addWidget( buttonHelp );
#endif
    Horizontal_Spacing2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( Horizontal_Spacing2 );

    buttonOk = new QPushButton( this, "buttonOk" );
    buttonOk->setAutoDefault( TRUE );
    buttonOk->setDefault( TRUE );
    Layout1->addWidget( buttonOk );

    buttonCancel = new QPushButton( this, "buttonCancel" );
    buttonCancel->setAutoDefault( TRUE );
    Layout1->addWidget( buttonCancel );
    SoundSelectDialogLayout->addLayout( Layout1 );
    languageChange();
    resize( QSize(394, 218).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( buttonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );
    connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
SoundSelectDialog::~SoundSelectDialog() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void SoundSelectDialog::languageChange() {
    setCaption( trUtf8( "\xec\x86\x8c\xeb\xa6\xac\x20\x2d\x20\xea\xb3\xa0\xea\xb8\x89\xec\x84\xa4\xec\xa0\x95"
                        "" ) );
    soundGroupBox->setTitle( trUtf8( "\xec\x86\x8c\xeb\xa6\xac\x20\xec\x82\xac\xec\x9a\xa9\xed\x95\xa8" ) );
    soundListView->header()->setLabel( 0, tr( "Column 1" ) );
    soundListView->clear();
    QListViewItem * item = new QListViewItem( soundListView, 0 );
    item->setText( 0, tr( "New Item" ) );

    soundDownKURLLabel->setText( trUtf8( "\xec\x95\x8c\xeb\xa6\xbc\xec\x86\x8c\xeb\xa6\xac\x20\xeb\x8b\xa4\xec\x9a\xb4\xeb\xa1"
                                         "\x9c\xeb\x93\x9c" ) );
    soundDownKURLLabel->setURL( tr( "http://nateonweb.nate.com/bbs/skin/sound/list.php?BBSID=2" ) );
    playButton->setText( trUtf8( "\xeb\x93\xa4\xec\x96\xb4\xeb\xb3\xb4\xea\xb8\xb0" ) );
    selectFileButton->setText( trUtf8( "\xed\x8c\x8c\xec\x9d\xbc\xec\xb0\xbe\xea\xb8\xb0" ) );
    // buttonHelp->setText( tr( "&Help" ) );
    // buttonHelp->setAccel( QKeySequence( tr( "F1" ) ) );
    buttonOk->setText( tr( "&OK" ) );
    buttonOk->setAccel( QKeySequence( QString::null ) );
    buttonCancel->setText( tr( "&Cancel" ) );
    buttonCancel->setAccel( QKeySequence( QString::null ) );
}

#include "soundconfigview.moc"
