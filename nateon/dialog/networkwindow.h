/***************************************************************************
 *   author Michael Jarrett (JudgeBeavis@hotmail.com)                      *
 * 	  date April, 2004.                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef NETWORKWINDOW_H
#define NETWORKWINDOW_H

#include <qobject.h>
#include <kdialogbase.h>
#include <qpushbutton.h>
#include <qfiledialog.h>
#include <qdir.h>
#include <kdebug.h>
#include <kmessagebox.h>
#include <qfile.h>

class QCheckBox;
class KTextEdit;

/**
   @author Michael Jarrett (JudgeBeavis@hotmail.com)
   @date April, 2004.
*/
class NetworkWindow : public KDialogBase {
    Q_OBJECT
public:
    NetworkWindow(QWidget *parent = 0, const char *name = 0);

public slots:
    /**
     * Sets the number of lines that can be displayed in the log.
     * We will buffer this many log entries as well, though lessNetworkWindow
     * may be displayed if some entries are multi-line.
     */
    void setLogCapacity(int entries);

    /**
     * Adds an entry to the log.
     * This is a text message, formatted like QTextEdit expects in LogText mode.
     * The log will add date/time info to it, the rest is up to you.
     * Lines will not be broken unless you do so explicitly.
     * @param priority An integer representing the importance of a message.
     *                 any integer is valid, but I recommend -10 to +10, where
     *                 0 is the 'standard' log setting, and 10 are messages that
     *                 you shouldn't be able to hide ever.
     * @param msg  The message. Please i18n it before giving it to us.
     */
    void addLogEntry(int priority, const QString &msg);

    /**
     * Logs incoming messages from the server.
     * A shortcut to display an inbound server message, but with the abilityNetworkWindow
     * to filter separately. Also, any tag-like things are replaced with
     * entities so they can be displayed.
     * @param msg  Raw message buffer from the connection.
     */
    void addIncomingServerMessage(const QString &msg);

    /**
     * Logs outgoing traffic.
     * Logs outgoing messages to the server.
     * A shortcut to display an inbound server message, but with the ability
     * to filter separately. Also, any tag-like things are replaced with
     * entities so they can be displayed.
     * @param msg  Raw buffer of what you intend to send.
     */
    void addOutgoingServerMessage(const QString &msg);

protected:
    void addMessage(const QString &msg);

protected slots:
    void adjustFilters();
    void saveLog();
private:
    // Controls
    KTextEdit *m_pLogWidget; ///< The widget used to display the log
    QCheckBox *m_pFilterIncomingWidget, *m_pFilterOutgoingWidget;
    QPushButton *m_pSaveLog;

    // Data
    int m_nCapacity;
    bool m_bShowIncoming, m_bShowOutgoing;
};
#endif
