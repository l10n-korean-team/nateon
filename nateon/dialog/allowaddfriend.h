/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ALLOWADDFRIEND_H
#define ALLOWADDFRIEND_H

#include <qlabel.h>
#include <qpushbutton.h>

#include "allowdialoginterface.h"

class QLabel;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class AllowAddFriend : public AllowFriendForm {
    Q_OBJECT
public:
    AllowAddFriend(QWidget *parent = 0, const char *name = 0);

    ~AllowAddFriend();

    QString getUID() {
        return m_sUID;
    }
    QString getCMN() {
        return m_sCMN;
    }

    void setUID( QString sUID );
    void setCMN( QString sCMN ) {
        m_sCMN = sCMN;
    };
    void setMessage( QString sMessage );
	void cancel();

private:
    QString m_sUID;
    QString m_sCMN;

private slots:
    void slotAccept();
    void slotReject();

signals:
    void accept(QString &sCMN, QString &sUID);
    void reject(QString &sCMN, QString &sUID);
};

#endif
