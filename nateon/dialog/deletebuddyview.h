/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DELETEFORM_H
#define DELETEFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>
#include <qcheckbox.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QCheckBox;
class QPushButton;

class DeleteForm : public QDialog {
    Q_OBJECT

public:
    DeleteForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~DeleteForm();

    QLabel* idQueryLabel;
    QCheckBox* lockCheckBox;
    QPushButton* okButton;
    QPushButton* cancelButton;

    QString getUID() {
        return sUID;
    }
    QString getGID() {
        return sGID;
    }
    QString getHandle() {
        return sHandle;
    }

    bool isLocked() {
        return lockCheckBox->isChecked();
    }

    void setName( QString Name ) {
        sName = Name;
    }
    void setUID( QString UID ); /* { sUID = UID; } */
    void setGID( QString GID ) {
        sGID = GID;
    }
    void setHandle( QString Handle ) {
        sHandle = Handle;
    }

protected:
    QVBoxLayout* DeleteFormLayout;
    QHBoxLayout* layout2;
    QSpacerItem* spacer3;
    QHBoxLayout* layout1;
    QSpacerItem* spacer1;
    QSpacerItem* spacer2;

protected slots:
    virtual void languageChange();
    virtual void accept();

private:
    QPixmap image0;
    QString sName;
    QString sUID;
    QString sGID;
    QString sHandle;

signals:
    void deleteInfo( DeleteForm * );

};

#endif // DELETEFORM_H
