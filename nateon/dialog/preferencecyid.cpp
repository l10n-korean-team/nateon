/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "preferencecyid.h"
#include "../util/common.h"

extern nmconfig stConfig;

PreferenceCyID::PreferenceCyID(QWidget *parent, const char *name)
        : Preference(parent, name) {
    /*! -------------------------------------------------- */
    WStackPage_9 = new QWidget( widgetStack2 , "WStackPage_9" );
    WStackPageLayout_9 = new QVBoxLayout( WStackPage_9, 0, 6, "WStackPageLayout_9");

    NateSyncPixmapLabel = new QLabel( WStackPage_9, "NateSyncPixmapLabel" );
    // NateSyncPixmapLabel->setPixmap( XXXXXXXXXXXX(  ) );
    NateSyncPixmapLabel->setPixmap( QPixmap( sPicsPath + "sett_titl_10.bmp") );
    NateSyncPixmapLabel->setScaledContents( TRUE );
    WStackPageLayout_9->addWidget( NateSyncPixmapLabel );

    NateSync_Layout1 = new QHBoxLayout( 0, 0, 6, "NateSync_Layout1");

    NateSync_TextLabel1 = new QLabel( WStackPage_9, "NateSync_TextLabel1" );
    NateSync_TextLabel1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, NateSync_TextLabel1->sizePolicy().hasHeightForWidth() ) );
    NateSync_Layout1->addWidget( NateSync_TextLabel1 );

    NateSync_Line1 = new QFrame( WStackPage_9, "NateSync_Line1" );
    NateSync_Line1->setFrameShape( QFrame::HLine );
    NateSync_Line1->setFrameShadow( QFrame::Sunken );
    NateSync_Line1->setFrameShape( QFrame::HLine );
    NateSync_Layout1->addWidget( NateSync_Line1 );
    WStackPageLayout_9->addLayout( NateSync_Layout1 );

    NateSync_TextLabel2 = new QLabel( WStackPage_9, "NateSync_TextLabel2" );
    WStackPageLayout_9->addWidget( NateSync_TextLabel2 );

    NateSync_frame15 = new QFrame( WStackPage_9, "NateSync_frame15" );
    NateSync_frame15->setFrameShape( QFrame::StyledPanel );
    NateSync_frame15->setFrameShadow( QFrame::Raised );
    NateSync_frame15Layout = new QVBoxLayout( NateSync_frame15, 11, 6, "NateSync_frame15Layout");

    NateSync_layout170 = new QHBoxLayout( 0, 0, 6, "NateSync_layout170");

    NateSync_TextLabel3 = new QLabel( NateSync_frame15, "NateSync_TextLabel3" );
    NateSync_TextLabel3->setMinimumSize( QSize( 80, 20 ) );
    NateSync_TextLabel3->setMaximumSize( QSize( 80, 20 ) );
    NateSync_layout170->addWidget( NateSync_TextLabel3 );

    NateSync_NateIDLineEdit = new QLineEdit( NateSync_frame15, "NateSync_NateIDLineEdit" );
    NateSync_layout170->addWidget( NateSync_NateIDLineEdit );

    textLabel2_8 = new QLabel( NateSync_frame15, "textLabel2_8" );
    NateSync_layout170->addWidget( textLabel2_8 );

    NateSync_ComboBox1 = new QComboBox( FALSE, NateSync_frame15, "NateSync_ComboBox1" );
    NateSync_layout170->addWidget( NateSync_ComboBox1 );

    NateSync_SyncButton = new QPushButton( NateSync_frame15, "NateSync_SyncButton" );
    NateSync_layout170->addWidget( NateSync_SyncButton );
    NateSync_spacer9_4 = new QSpacerItem( 30, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    NateSync_layout170->addItem( NateSync_spacer9_4 );
    NateSync_frame15Layout->addLayout( NateSync_layout170 );

    NateSync_layout171 = new QHBoxLayout( 0, 0, 6, "NateSync_layout171");

    NateSync_PasswordTextLabel = new QLabel( NateSync_frame15, "NateSync_PasswordTextLabel" );
    NateSync_PasswordTextLabel->setMinimumSize( QSize( 80, 20 ) );
    NateSync_PasswordTextLabel->setMaximumSize( QSize( 80, 20 ) );
    NateSync_layout171->addWidget( NateSync_PasswordTextLabel );

    NateSync_NatePasswordLineEdit = new QLineEdit( NateSync_frame15, "NateSync_NatePasswordLineEdit" );
    NateSync_NatePasswordLineEdit->setMinimumSize( QSize( 250, 20 ) );
    NateSync_NatePasswordLineEdit->setMaximumSize( QSize( 250, 20 ) );
    NateSync_layout171->addWidget( NateSync_NatePasswordLineEdit );
    NateSync_spacer8_4 = new QSpacerItem( 174, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    NateSync_layout171->addItem( NateSync_spacer8_4 );
    NateSync_frame15Layout->addLayout( NateSync_layout171 );

    NateSync_layout172 = new QHBoxLayout( 0, 0, 6, "NateSync_layout172");

    NateSync_spacer7_4 = new QSpacerItem( 131, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    NateSync_layout172->addItem( NateSync_spacer7_4 );

    NateSync_findIDPasswordButton = new QPushButton( NateSync_frame15, "NateSync_findIDPasswordButton" );
    NateSync_layout172->addWidget( NateSync_findIDPasswordButton );

    NateSync_gaibButton = new QPushButton( NateSync_frame15, "NateSync_gaibButton" );
    NateSync_layout172->addWidget( NateSync_gaibButton );


    NateSync_frame15Layout->addLayout( NateSync_layout172 );
    WStackPageLayout_9->addWidget( NateSync_frame15 );
    NateSync_spacer101 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    WStackPageLayout_9->addItem( NateSync_spacer101 );
    widgetStack2 /* NateSync_widgetStack */->addWidget( WStackPage_9, 8 );
    /*!
      layout75->addWidget( widgetStack2 );
      PreferenceLayout->addLayout( layout75 );
    */
    /*! -------------------------------------------------- */
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_10.bmp"), trUtf8( "네이트" ) );

    /*! ------------------------------------------------ */

    // NateSync_TextLabel1->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xec\x97\xb0\xeb\x8f\x99" ) );

    /*
    NateSync_TextLabel2->setText( trUtf8( "\xec\x97\xb0\xeb\x8f\x99\xed\x95\x9c\x20\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xeb\x8b"
    									  "\xb7\xec\xbb\xb4\x20\xec\x95\x84\xec\x9d\xb4\xeb\x94\x94\xec\x9d\x98\x20\xec\x9d"
    									  "\xb4\xeb\xa9\x94\xec\x9d\xbc\x20\xeb\x93\xb1\xec\x9d\x84\x20\xec\x9e\x90\xec\x9c"
    									  "\xa0\xeb\xa1\xad\xea\xb2\x8c\x20\xec\x9d\xb4\xec\x9a\xa9\x20\xed\x95\xa0\x20\xec"
    									  "\x88\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    */
    // NateSync_TextLabel3->setText( trUtf8( "\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xeb\x8b\xb7\xec\xbb\xb4\x20\x49\x44" ) );

    NateSync_TextLabel1->setText( UTF8("네이트 연동") );
    NateSync_TextLabel2->setText( UTF8("연동한 네이트 아이디의 이메일 등을 자유롭게 이용 할 수 있습니다.") );
    NateSync_TextLabel3->setText( UTF8("네이트 ID") );
    textLabel2_8->setText( tr( "@" ) );
    NateSync_ComboBox1->clear();
    NateSync_ComboBox1->insertItem( tr( "nate.com" ) );
    NateSync_ComboBox1->insertItem( tr( "lycos.co.kr" ) );
    NateSync_ComboBox1->insertItem( tr( "netsgo.com" ) );
    NateSync_SyncButton->setText( trUtf8( "\xec\x97\xb0\xeb\x8f\x99\xed\x95\x98\xea\xb8\xb0" ) );
    NateSync_PasswordTextLabel->setText( trUtf8( "\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88\xed\x98\xb8" ) );
    NateSync_findIDPasswordButton->setText( trUtf8( "\x49\x44\x2f\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88\xed\x98\xb8\x20\xec\xb0\xbe\xea\xb8"
                                            "\xb0" ) );
    // NateSync_gaibButton->setText( trUtf8( "\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xeb\x8b\xb7\xec\xbb\xb4\x20\xed\x9a\x8c\xec\x9b"
    //									  "\x90\xea\xb0\x80\xec\x9e\x85" ) );
    NateSync_gaibButton->setText( UTF8("네이트 회원가입") );
    /*! ------------------------------------------------ */

    setTabOrder( NateSync_NateIDLineEdit, NateSync_ComboBox1 );
    setTabOrder( NateSync_ComboBox1, NateSync_NatePasswordLineEdit );
    setTabOrder( NateSync_NatePasswordLineEdit, NateSync_SyncButton );
    NateSync_SyncButton->setFocus();

    connect( NateSync_SyncButton, SIGNAL( clicked() ), SLOT( slotNateSync() ) );
    NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Password );
    connect( NateSync_findIDPasswordButton, SIGNAL( clicked() ), SLOT( slotFindIDPassword() ) );
    connect( NateSync_gaibButton, SIGNAL( clicked() ), SLOT( slotNateDotComGaIb() ) );
    // connect( NateSync_NatePasswordLineEdit, SIGNAL( returnPressed () ), NateSync_SyncButton, SLOT( clicked() ) );
}


PreferenceCyID::~PreferenceCyID() {
}

void PreferenceCyID::showSyncSetup() {
    widgetStack2->raiseWidget(8);
    NateSync_NateIDLineEdit->setFocus();
}

void PreferenceCyID::initialize() {
    Preference::initialize();

    if ( stConfig.usenatedotcom == TRUE ) {
        NateSync_ComboBox1->setEnabled( FALSE );
        NateSync_SyncButton->setText( UTF8("연동해제") );
        int nAt = (stConfig.nateid).find("@");
        if ( nAt != -1 ) {
            QString sID = (stConfig.nateid).left( nAt );
            NateSync_NateIDLineEdit->setText( sID );
        }
        NateSync_NateIDLineEdit->setEnabled ( FALSE );
        NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Normal );
        NateSync_NatePasswordLineEdit->setText( UTF8("비밀번호는 저장되지 않습니다.") );
        NateSync_NatePasswordLineEdit->setEnabled( FALSE );
    } else {
        NateSync_ComboBox1->setEnabled( TRUE );
        NateSync_SyncButton->setText( UTF8("연동하기") );
        NateSync_NateIDLineEdit->setEnabled ( TRUE );
        NateSync_NateIDLineEdit->clear();
        NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Password );
        NateSync_NatePasswordLineEdit->setEnabled( TRUE );
        NateSync_NatePasswordLineEdit->clear();
        NateSync_NateIDLineEdit->setFocus();
    }
}

void PreferenceCyID::accept() {
    Preference::accept();
    config->sync();
}

void PreferenceCyID::slotNateSync() {
    NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Password );
    if ( !stConfig.usenatedotcom ) {
        if ( ( NateSync_NateIDLineEdit->text().length()  < 1 ) ||
                ( NateSync_NatePasswordLineEdit->text().length()  < 1 ) ) {
            KMessageBox::information (this, QString::fromUtf8("아이디랑 패스워드를 입력하세요."), UTF8("연동하기") );
            return;
        }
        stConfig.usenatedotcom = true;
        stConfig.nateid = NateSync_NateIDLineEdit->text();
        /*! 사용자 인증이 성공하였습니다. */
        /*! 연동 설정이 실패하였습니다. */
        emit nateSync( NateSync_NateIDLineEdit->text(), NateSync_NatePasswordLineEdit->text() );
    } else {
        int result = KMessageBox::questionYesNo(this, QString::fromUtf8("네이트 아이디 연동설정 시 입력하신 ID와 비밀번호를 삭제하시겠습니다까?\n이용중이던 메일쓰기 및 통합메시지함은 연동해제와 동시에 자동종료되오니 주의하시기 바랍니다.\n또한, 삭제 하신 후 다시 사용하시려면 싸이월드 재인증 과정이 필요합니다."), UTF8("환경설정") );
        if ( result == KMessageBox::Yes ) {
            /*! 네이트 연동이 해제되었습니다. */
            stConfig.usenatedotcom = FALSE;
            NateSync_SyncButton->setText( UTF8("연동하기") );
            NateSync_NateIDLineEdit->setEnabled ( TRUE );
            NateSync_NateIDLineEdit->clear();
            NateSync_ComboBox1->setEnabled( TRUE );
            NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Password );
            NateSync_NatePasswordLineEdit->setEnabled( TRUE );
            NateSync_NatePasswordLineEdit->clear();
            NateSync_NatePasswordLineEdit->setFocus();


            /*! 네이트 연동 유무 */
            stConfig.usenatedotcom = false;
            /*! 싸이월드 ID */
            stConfig.nateid = QString::null;

            /*! 싸이월드 */
            config->setGroup( "Config_NateDotCom" );
            config->writeEntry( "NateDotCom_ID", stConfig.nateid );
            config->sync();

            emit nateSyncCancel();

            KMessageBox::information (this, QString::fromUtf8("네이트 연동이 해제되었습니다."), UTF8("환경설정") );
        }
    }
}

void PreferenceCyID::show() {
    initialize();
    Preference::show();
}

void PreferenceCyID::slotFindIDPassword() {
    LNMUtils::openURL( "http://helpdesk.nate.com/faq/exMemberInfo.asp?r_url=https://member.nate.com/sccustomer/join/nate/find/find_index.jsp" );
}

void PreferenceCyID::slotNateDotComGaIb() {
    LNMUtils::openURL( "http://helpdesk.nate.com/faq/exMemberInfo.asp?r_url=https://member.nate.com/sccustomer/join/nate/regist_index.jsp" );
}

void PreferenceCyID::slotCySyncAuthError( bool bErr ) {
    /*! 네이트 */
    config->setGroup( "Config_NateDotCom" );
    if ( bErr ) {
        stConfig.usenatedotcom = FALSE;
        NateSync_SyncButton->setText( UTF8("연동하기") );
        NateSync_NateIDLineEdit->setEnabled ( TRUE );
        NateSync_ComboBox1->setEnabled( TRUE );
        NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Password );
        NateSync_NatePasswordLineEdit->setEnabled( TRUE );
        NateSync_NatePasswordLineEdit->clear();
        NateSync_NatePasswordLineEdit->setFocus();
        KMessageBox::information (this, QString::fromUtf8("아이디/패스워드가 일치하지 않습니다."), UTF8("연동하기") );
    } else {
        stConfig.usenatedotcom = TRUE;
        NateSync_SyncButton->setText( UTF8("연동해제") );
        stConfig.nateid=NateSync_NateIDLineEdit->text();
        NateSync_NateIDLineEdit->setEnabled ( FALSE );
        NateSync_ComboBox1->setEnabled( FALSE );
        NateSync_NatePasswordLineEdit->setEchoMode ( QLineEdit::Normal );
        NateSync_NatePasswordLineEdit->setText( UTF8("비밀번호는 저장되지 않습니다.") );
        NateSync_NatePasswordLineEdit->setEnabled( FALSE );
        KMessageBox::information (this, QString::fromUtf8("사용자 인증이 성공하였습니다."), UTF8("연동하기") );
    }
    /*! 네이트 ID */
    config->writeEntry( "NateDotCom_ID", stConfig.nateid );
    config->sync();
}

void PreferenceCyID::slotCyCanceled() {
}

#include "preferencecyid.moc"
