/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CONFIRMSAVELOG_H
#define CONFIRMSAVELOG_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>

#include "linklabel.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QPushButton;
class LinkLabel;

class ConfirmSaveLog : public QDialog {
    Q_OBJECT

public:
    ConfirmSaveLog( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~ConfirmSaveLog();
#if 0
    static int dialog( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
#endif
    QLabel* textLabel1;
    LinkLabel* textLabel2;
    QPushButton* yesButton;
    QPushButton* noButton;
    QPushButton* cancelButton;

protected:
    QVBoxLayout* ConfirmSaveLogLayout;
    QHBoxLayout* layout1;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // CONFIRMSAVELOG_H
