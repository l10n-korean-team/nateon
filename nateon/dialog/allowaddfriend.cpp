/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "allowaddfriend.h"

AllowAddFriend::AllowAddFriend(QWidget *parent, const char *name)
        : AllowFriendForm(parent, name) {
    connect(acceptButton, SIGNAL( clicked() ), this, SLOT( slotAccept() ) );
    connect(rejectButton, SIGNAL( clicked() ), this, SLOT( slotReject() ) );
}


AllowAddFriend::~AllowAddFriend() {
}

void AllowAddFriend::setUID(QString sUID) {
    m_sUID = sUID;

    useridLabel->clear();
    useridLabel->setText(m_sUID + QString::fromUtf8(" 님이 온라인 친구 요청을 하셨습니다. 수락하시겠습니까?") );
}

void AllowAddFriend::slotAccept() {
    emit accept( m_sCMN, m_sUID );
    close();
}

void AllowAddFriend::slotReject() {
    emit reject( m_sCMN, m_sUID );
    close();
}

void AllowAddFriend::cancel() {
	close();
}

void AllowAddFriend::setMessage(QString sMessage) {
    sMessage.replace("%20", " ");
    sMessage.replace("%0D", "\r");
    sMessage.replace("%0A", "\n");

    messageLabel->setText( sMessage );
}


#include "allowaddfriend.moc"
