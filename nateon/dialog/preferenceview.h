/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PREFERENCEVIEW_H
#define PREFERENCEVIEW_H

#include <kapp.h>
#include <kconfig.h>
#include <kmessagebox.h>

#include <qwidgetstack.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qdir.h>
#include <qfiledialog.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qlistbox.h>
#include <qwidgetstack.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qdir.h>
#include <qfiledialog.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qlistbox.h>
#include <qvariant.h>
#include <qpushbutton.h>
#include <qlistbox.h>
#include <qwidgetstack.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qframe.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "preferenceinterface.h"

class SoundConfig;

/**
   설정창

   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class PreferenceView : public Preference {
    Q_OBJECT
public:
    PreferenceView ( QWidget *parent = 0, const char *name = 0, bool modal = FALSE );
    ~PreferenceView();

    virtual void showSyncSetup();
    virtual void accept();
    virtual void initialize();
    virtual void show();

    virtual void setOffline();
    virtual void setOnline();

protected:

public slots:

private slots:
    /*! 싸이연동 */
    void slotCySync();
    void slotCySyncAuthError( bool bErr );
    void slotCyCanceled();
    void slotFindIDPassword();
    void slotNateDotComGaIb();
    void slotWhatMiniHompy();

signals:
    void cySync(const QString &sID, const QString &sPassword);
    void updateCyInfo( const QString &sCommand );
};

#endif
