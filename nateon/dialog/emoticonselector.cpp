/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "emoticonselector.h"

EmoticonSelector::EmoticonSelector(QWidget *parent, const char *name)
        :MyDialog1(parent, name) {
    /*
      createIconSet();

      tabWidget->setTabIconSet( tabWidget->page(0), ptab1OffQIS );
      tabWidget->setTabIconSet( tabWidget->page(1), ptab2OffQIS );
      tabWidget->setTabIconSet( tabWidget->page(2), ptab3OffQIS );
      tabWidget->setTabIconSet( tabWidget->page(3), ptab4OffQIS );
      tabWidget->setTabIconSet( tabWidget->page(4), ptab5OffQIS );
    */

    // kTabWidget1->setCurrentPage( 1 );
    // kTabWidget1->showPage(TabPage1);

    connect(tabWidget, SIGNAL( currentChanged ( QWidget * ) ), this, SLOT( onTab( QWidget * ) ) );
#if 0
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton2, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton3, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton4, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton5, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton6, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton7, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton8, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton9, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton10, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton11, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton12, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton13, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton14, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton15, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton16, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton17, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton18, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton19, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton20, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton21, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton22, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
    connect(toolButton1, SIGNAL( clicked() ), SLOT( slot01() ) );
#endif

}

void EmoticonSelector::createIconSet() {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString         path;
    QImage          mTemp;
    QPixmap         mQP;

    path = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/emoticons/emoticon_tab.bmp" );

    QImage *m_StatusQI = new QImage(path);

    int m_nWidth = m_StatusQI->width() / 2;
    int m_nHeight = m_StatusQI->height() / 6;

    /// 탭이미지 선택 안된이미지
    mTemp = m_StatusQI->copy( 0, (m_nHeight * 0), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab1OffQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( 0, (m_nHeight * 1), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab2OffQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( 0, (m_nHeight * 2), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab3OffQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( 0, (m_nHeight * 3), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab4OffQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( 0, (m_nHeight * 5), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab5OffQIS = QIconSet(mQP);

    /// 탭이지 선택이 되었을때.
    mTemp = m_StatusQI->copy( m_nWidth, (m_nHeight * 0), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab1OnQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( m_nWidth, (m_nHeight * 1), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab2OnQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( m_nWidth, (m_nHeight * 2), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab3OnQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( m_nWidth, (m_nHeight * 3), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab4OnQIS = QIconSet(mQP);

    mTemp = m_StatusQI->copy( m_nWidth, (m_nHeight * 5), m_nWidth, m_nHeight);
    mQP.convertFromImage(mTemp);
    ptab5OnQIS = QIconSet(mQP);
}

void EmoticonSelector::Allofficons() {

    tabWidget->setTabIconSet( tabWidget->page(0), ptab1OffQIS );
    tabWidget->setTabIconSet( tabWidget->page(1), ptab2OffQIS );
    tabWidget->setTabIconSet( tabWidget->page(2), ptab3OffQIS );
    tabWidget->setTabIconSet( tabWidget->page(3), ptab4OffQIS );
    tabWidget->setTabIconSet( tabWidget->page(4), ptab5OffQIS );

}

void EmoticonSelector::onTab(QWidget * pWidget) {
    Q_UNUSED( pWidget );
    /*
      Allofficons();
      tabWidget->setTabIconSet( pWidget, ptab1OffQIS );
    */
}


#include "emoticonselector.moc"
