/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ADDFORM_H
#define ADDFORM_H

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>
#include <kstandarddirs.h>
#include <kaboutdata.h>

#include "shapewidget.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLabel;
class QLineEdit;
class QPushButton;
class QComboBox;
class QListView;
class QListViewItem;
class ShapeButton;
class ShapeWidget;
class ShapeForm;

class AddForm : public QDialog {
    Q_OBJECT

public:
    AddForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~AddForm();

    QLabel* textLabel1;
    QLineEdit* lineEdit1;
    ShapeButton* pushButton1; /*! 검색 */
    QLabel* textLabel2;
    QComboBox* comboBox1;
    QListView* listView1;
    ShapeButton* pushButton2; /*! 추가 */
    ShapeButton* pushButton3; /*! 삭제 */
    QLabel* textLabel3;
    QListView* listView2;
    ShapeButton* pushButton4; /*! 확인 */
    ShapeButton* pushButton5; /*! 취소 */

protected:
    QVBoxLayout* AddFormLayout;
    QVBoxLayout* layout9;
    QHBoxLayout* layout1;
    QSpacerItem* spacer1;
    QHBoxLayout* layout7;
    QVBoxLayout* layout3;
    QHBoxLayout* layout2;
    QVBoxLayout* layout5;
    QSpacerItem* spacer2;
    QSpacerItem* spacer3;
    QVBoxLayout* layout6;
    QHBoxLayout* layout8;
    QSpacerItem* spacer4;
    QSpacerItem* spacer5;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;

};

#endif // ADDFORM_H
