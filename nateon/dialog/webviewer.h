/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef WEBVIEWER_H
#define WEBVIEWER_H

// #include <ktextbrowser.h>
#include <qcursor.h>
#include <qtimer.h>
#include <kurl.h>
#include <kparts/browserextension.h>
#include <qvbox.h>
#include <qlineedit.h>
#include <kapp.h>
#include <kmenubar.h>
#include <klocale.h>
#include <kpopupmenu.h>
#include <khtml_part.h>
#include <qlayout.h>
#include <kio/jobclasses.h>
#include <kstandarddirs.h>
#include <krun.h>
#include <kcursor.h>

#include "webwindow.h"
#include "utils.h"

// class KTextBrowser;

class WebViewer: public WebForm1 {
    Q_OBJECT
public:
    WebViewer(QWidget *parent = 0, const char *name = 0);
    ~WebViewer();
    void openURL(const QString &url);

protected slots:
    void openURLRequest(const KURL &url, const KParts::URLArgs &args );
    void createNewWindow( const KURL &url, const KParts::URLArgs &args );
    void started(KIO::Job *);    // Started to download the requested URL
    void completed();            // Requested URL download completed
    void timerTimeout();

private:
    QVBoxLayout* MyDialog1Layout;
    // QLineEdit *location;
    QVBox * vbox;
    KHTMLPart *browser;
    QString sUrl;
    void closeEvent ( QCloseEvent * e );

signals:
    void newUrlRequested(const KURL &url, const KParts::URLArgs &args);
    void newWindowRequested(const KURL &, const KParts::URLArgs &, const KParts::WindowArgs &, KParts::ReadOnlyPart *&);
};

#endif
