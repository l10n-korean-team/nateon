/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "filetransfer.h"
#include "../network/sendfileinfo.h"
#include "../util/common.h"
#include "../util/sound.h"

extern nmconfig stConfig;

FileTransfer::FileTransfer(QWidget *parent, const char *name)
        : FileTransferForm(parent, name) {
    /// fileListGroupBox->hide(); <<=== 잠시 없앰.
    // layout()->setResizeMode(QLayout::Fixed);
    progressBar1->reset();
    progressBar1->setPercentageVisible( true );
	fileListView->setAllColumnsShowFocus( true );
    fileListView->clear();


    connect( fileListView, SIGNAL( selectionChanged () ), this, SLOT( slotUpdateInfo() ) );
    connect( this, SIGNAL( progressUpdate( int, int ) ), progressBar1, SLOT( setProgress( int, int ) ) );
    connect( cancelButton, SIGNAL( clicked() ), SLOT( slotCancelButton() ) );
}


FileTransfer::~FileTransfer() {
}

void FileTransfer::showlist() {
    fileListGroupBox->show();
    layout()->setResizeMode(QLayout::Fixed);
}

void FileTransfer::hidelist() {
    fileListGroupBox->hide();
    layout()->setResizeMode(QLayout::Fixed);
}

void FileTransfer::addListView(const QString & sCookie, const QString & sID, const QString & sFile, const int nSize, bool bSend) {
    kdDebug() << "Add File Cookie : " << sCookie << endl;

    QListViewItem* newItem = new QListViewItem(fileListView, sCookie, sID, sFile, QString::number(nSize) );
    fileListView->setCurrentItem ( newItem );
    fileListView->setSelected ( newItem, true );
    fileListView->setSorting ( -1 );
    progressBar1->setTotalSteps( nSize );
    progressBar1->reset();
    kbStatusLabel->setText( "0/" + QString::number(nSize) );
    if ( bSend )
        newItem->setText( 4, UTF8("보냄") );
    else
        newItem->setText( 4, UTF8("받음") );
    newItem->setText( 6, QString::fromUtf8("기다리는 중") );
    newItem->setText( 7, "0");
    cancelButton->setEnabled( false );
    cancelButton->setText( QString::fromUtf8("중지") );
}


QListViewItem *FileTransfer::getListViewItem( const QString & sFileCookie ) {
    QListViewItemIterator it( fileListView );
    QListViewItem *item = 0;
    while ( it.current() ) {
        item = it.current();
        if (item->text(0) == sFileCookie )
            return item;
        ++it;
    }
    return 0;
}


void FileTransfer::endProgressByByte( const QString& sFileCookie ) {
    QListViewItem *item = getListViewItem( sFileCookie );
    if ( item ) {
        item->setText( 6, QString::fromUtf8("전송완료") );
        /*!
         * 쪽지 받음 소리
         */
        if ( stConfig.usesound && stConfig.usefilesenddonesound ) {
            Sound::play( stConfig.filesenddonesoundpath );
        }
        cancelButton->setEnabled( FALSE );
        cancelButton->setText( QString::fromUtf8("완료") );
    }
}


void FileTransfer::updateProgressByByte(const QString & sFileCookie, const unsigned long nByte) {
    kdDebug() << "sFileCookie : [" << sFileCookie << "], Byte : " << nByte << endl;

    QListViewItem *item = getListViewItem( sFileCookie );
    if ( item ) {
        double nTSize = QString(item->text(3)).toLong();
        unsigned int nPercent = ((double)nByte * 100 ) / nTSize;
        item->setText(5, QString::number( nPercent ) + "%");
        emit progressUpdate( nByte, nTSize );
        progressBar1->update();

        if ( item->text(6) != QString::fromUtf8("전송취소됨") ) {
            item->setText( 6, QString::fromUtf8("전송중") );
            cancelButton->setEnabled( true );
            cancelButton->setText( QString::fromUtf8("중지") );
        }

        item->setText(7, QString::number( nByte ) );

        if (  fileListView->isSelected( item )  ) {
            kbStatusLabel->clear();
            kbStatusLabel->setText( QString::number( nByte ) + "/" + item->text(3) );
        }
    }
}

#include "filetransfer.moc"

void FileTransfer::slotUpdateInfo() {
    QListViewItem* pCurItem = fileListView->selectedItem();
    if ( pCurItem ) {
        fileNameLabel->setText( pCurItem->text(2) );
        kbStatusLabel->setText( pCurItem->text(7) + "/" + pCurItem->text(3) );
        QString sPercent;
        sPercent = (pCurItem->text(5)).replace("%", "");
		progressBar1->reset();
        emit progressUpdate( pCurItem->text(7).toInt(), pCurItem->text(3).toInt() );
        if ( ( pCurItem->text(6) == QString::fromUtf8("전송중") ) || ( pCurItem->text(6) == QString::fromUtf8("기다리는 중") ) ) {
            cancelButton->setEnabled( TRUE );
            cancelButton->setText( QString::fromUtf8("중지") );
        } else {
            cancelButton->setEnabled( FALSE );
            cancelButton->setText( QString::fromUtf8("완료") );
        }
    }
}

void FileTransfer::cancelTransfer(const QString & sCookie) {
    KMessageBox::information( this, UTF8("상대방이 파일전송을 취소하였습니다."), UTF8("전송 취소") );

    QListViewItem *item = getListViewItem( sCookie );

// 	QListViewItemIterator it( fileListView );
// 	while ( it.current() )
// 	{
// 		item = it.current();
// 		if (item->text(0) == sCookie )
// 			break;
// 		++it;
// 	}
    if ( item ) {
        item->setText( 6, QString::fromUtf8("전송취소됨") );
    }
    cancelButton->setEnabled( FALSE );
}

void FileTransfer::slotCancelButton() {
    QListViewItem* pCurItem = fileListView->selectedItem();
    if ( pCurItem ) {
        kdDebug() << "TTTTTTTTTTTTTTTTTTTTTTTTTT : " << pCurItem->text(2) << endl;
        /*!
         * File Cookie
         */
        emit cancelFileTransfer( pCurItem->text(0) );
        cancelButton->setEnabled( FALSE );
        pCurItem->setText( 6, QString::fromUtf8("전송취소됨") );
    }
}
