/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ADDFRIENDVIEW_H
#define ADDFRIENDVIEW_H

#include <kmessagebox.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qpushbutton.h>
#include <qcombobox.h>

#include "addfriendinterface.h"

class QLineEdit;
class QTextEdit;
class QPushButthon;
class QComboBox;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class AddFriendView : public AddFriendForm {
    Q_OBJECT
public:
    AddFriendView(QWidget *parent = 0, const char *name = 0);

    ~AddFriendView();

    void initalize();
    void clear();
    QString getID() {
        return lineeditID->text();
    };
    QString getDomain() {
        return comboBox1->currentText();
    };
    QString getMessage() {
        return texteditMessage->text();
    };

    QString getPhoneNo1() {
        return comboBoxNo1->currentText();
    }
    QString getPhoneNo2() {
        return lineEditNo2->text();
    }
    QString getPhoneNo3() {
        return lineEditNo3->text();
    }
    QString getName1() {
        return lineEditName1->text();
    }

    QString getName2() {
        return lineEditName2->text();
    };
    QString getGender() {
        if  (comboBoxGender->currentItem() == 0)
            return "M";
        else
            return "F";
    }
    QString getFromAge() {
        return lineEditAgeFrom->text();
    }
    QString getToAge() {
        return lineEditAgeTo->text();
    }

    QString getSelectedID();
    QString getMessageOnSearchTab() {
        return textEditMessage->text();
    }

private:
    void showSearchResult( bool bIsSuccess, QStringList& slResult );
    int getEmailAddrLen( unsigned char nCode );
    char getDecodedChar( int nIdx );
    QString getEmailAddr( QString sEncData );

    QString m_sSearchFriendResult;

private slots:
    void slotRequire();
    void slotTextCheck();
    void slotCancel();
    void slotSearchTextCheck();
    void slotSearchByPhoneNo();
    void slotSearchByAge();
    void slotTextCheckOnSearchTab();
    void slotRequireOnSearchTab();

public slots:
    void slotGotSearchResult( const QString &sResult );

signals:
    void addFriendRequire( AddFriendView* );
    void searchByPhoneNo( AddFriendView* );
    void searchByAge( AddFriendView* );
    void addFriendRequireOnSearchTab( AddFriendView* );
};

#endif
