/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "webviewer.h"

// 이 소스는 다음 URL을 참고 했습니다.
// http://developer.kde.org/~larrosa/tutorial/p4.html

WebViewer::WebViewer(QWidget *parent, const char *name)
        :WebForm1(parent, name),
        MyDialog1Layout(0),
        vbox(0),
        browser(0) {
    MyDialog1Layout = new QVBoxLayout( this, 0, -1, "MyDialog1Layout");
    MyDialog1Layout->setResizeMode( QLayout::Auto );

    vbox = new QVBox ( this );
    browser=new KHTMLPart( vbox );

    // connect signal for link clicks
    connect( browser->browserExtension(), SIGNAL( openURLRequest( const KURL &, const KParts::URLArgs & ) ), SLOT( openURLRequest(const KURL &, const KParts::URLArgs & ) ) );
    // createNewWindow( const KURL &, const KParts::URLArgs & )
    connect( browser->browserExtension(), SIGNAL( createNewWindow( const KURL &, const KParts::URLArgs & ) ), SLOT( createNewWindow( const KURL &, const KParts::URLArgs &) ) );

    // connect signals for download progress
    // connect( browser,SIGNAL(started(KIO::Job*)), this,SLOT(started(KIO::Job*)));
    connect( browser,SIGNAL(completed()), this,SLOT(completed()));

    browser->setPluginsEnabled(true);
    browser->setMetaRefreshEnabled(true);
    browser->enableMetaRefresh(true);
    browser->setJavaEnabled(true);
    browser->enableJScript(true);
    browser->setProgressInfoEnabled(true);
    browser->setURLCursor( KCursor::handCursor() );
    // browser->setOnlyLocalReferences(true);

    browser->startTimer(3000);

    MyDialog1Layout->addWidget(vbox);
    browser->browserExtension()->setURLDropHandlingEnabled( true );
}


WebViewer::~ WebViewer() {
    if ( browser ) {
        browser->closeURL();
        delete browser;
    }
    if ( vbox ) delete vbox;
    if ( MyDialog1Layout ) delete MyDialog1Layout;
}


void WebViewer::openURL(const QString & url) {
    sUrl = url;
    if (url != "")
        browser->openURL(url);
}



void WebViewer::openURLRequest( const KURL & url, const KParts::URLArgs &args ) {
    browser->browserExtension()->setURLArgs( args );
    browser->openURL( url.url() );
}


void WebViewer::createNewWindow( const KURL & url, const KParts::URLArgs & args ) {
    Q_UNUSED( args );
#ifdef NETDEBUG
    kdDebug() << "XXXXX [" << url.url() << "]" << endl;
#endif
    // LNMUtils::openURL( url );
    new KRun( url );
}


void WebViewer::started(KIO::Job *job) {
    job = job;
    setCursor(QCursor(Qt::BusyCursor));
}


void WebViewer::completed() {
    setCursor(QCursor(Qt::ArrowCursor));

    // work around a bug in the HTML widget where it doesn't scroll
    // to the correct location if the URL has a reference
    KURL url(sUrl);
    if (url.hasRef()) {
        QTimer::singleShot(1, this, SLOT(timerTimeout()));
    }

    setFocus();
}


void WebViewer::timerTimeout() {
    // jump to reference if present in url
    KURL url(sUrl);
    if (url.hasRef()) {
        if (!browser->gotoAnchor(url.encodedHtmlRef()))
            browser->gotoAnchor(url.htmlRef());
    }
}

void WebViewer::closeEvent(QCloseEvent * e) {
    WebForm1::closeEvent( e );
    // WebForm1::done();
}


#include "webviewer.moc"
