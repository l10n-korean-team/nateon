/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef EMOTICONSELECTOR_H
#define EMOTICONSELECTOR_H

#include <qiconset.h>
#include <qimage.h>
#include <kstandarddirs.h>
#include <qdialog.h>
#include <qtabwidget.h>
#include <kdebug.h>
#include <kaboutdata.h>

#include "emoticonwindow.h"

class QIconSet;
class QTabWidget;

class EmoticonSelector: public MyDialog1 {
    Q_OBJECT
public:
    EmoticonSelector(QWidget *parent = 0, const char *name = 0);


private:


    void createIconSet();
    void Allofficons();

    QIconSet ptab1OnQIS;
    QIconSet ptab2OnQIS;
    QIconSet ptab3OnQIS;
    QIconSet ptab4OnQIS;
    QIconSet ptab5OnQIS;

    QIconSet ptab1OffQIS;
    QIconSet ptab2OffQIS;
    QIconSet ptab3OffQIS;
    QIconSet ptab4OffQIS;
    QIconSet ptab5OffQIS;

private slots:
    void onTab( QWidget *pWidget);
};

#endif
