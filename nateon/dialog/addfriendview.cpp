/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "addfriendview.h"
#include <qlistview.h>
#include <qregexp.h>
#include <kdebug.h>

AddFriendView::AddFriendView(QWidget *parent, const char *name)
        : AddFriendForm(parent, name) {
    initalize();
}


AddFriendView::~AddFriendView() {
}


#include "addfriendview.moc"


void AddFriendView::initalize() {
    lineeditID->setText( QString::null );
    texteditMessage->setText( QString::null );
    buttonRequire->setEnabled( FALSE );
    connect( buttonRequire, SIGNAL( clicked() ), SLOT( slotRequire() ) );
    connect( lineeditID, SIGNAL( textChanged(const QString &) ), SLOT( slotTextCheck() ) );
    connect( texteditMessage, SIGNAL( textChanged() ), SLOT( slotTextCheck() ) );
    connect( buttonCancel, SIGNAL( clicked() ), SLOT( slotCancel() ) );

    comboBoxNo1->setCurrentItem(0);
    lineEditNo2->setText( QString::null );
    lineEditNo3->setText( QString::null );
    lineEditName1->setText( QString::null );
    pushButtonSearch1->setEnabled( FALSE );
    connect( pushButtonSearch1, SIGNAL( clicked() ), SLOT( slotSearchByPhoneNo() ) );

    lineEditName2->setText( QString::null );
    comboBoxGender->setCurrentItem(0);
    lineEditAgeFrom->setText( QString::null );
    lineEditAgeTo->setText( QString::null );
    pushButtonSearch2->setEnabled( FALSE );
    connect( pushButtonSearch2, SIGNAL( clicked() ), SLOT( slotSearchByAge() ) );

    listViewResult->clear();
    listViewResult->setSorting(-1);                             // 자동 정렬 금지
    listViewResult->setAllColumnsShowFocus( TRUE );             // 선택 시 열 전체 반전
    listViewResult->setSelectionMode( QListView::Single );      // single 선택 모드
    int nColNo = listViewResult->addColumn("", 100);            // 레코드별 수신 데이터 저장용 컬럼
    listViewResult->hideColumn(nColNo);                         // 수신 데이터 저장 컬럼은 표시하지 않음
    connect( listViewResult, SIGNAL( selectionChanged() ), SLOT( slotTextCheckOnSearchTab() ) );

    connect( lineEditNo2, SIGNAL( textChanged(const QString &) ), SLOT( slotSearchTextCheck() ) );
    connect( lineEditNo3, SIGNAL( textChanged(const QString &) ), SLOT( slotSearchTextCheck() ) );
    connect( lineEditName1, SIGNAL( textChanged(const QString &) ), SLOT( slotSearchTextCheck() ) );
    connect( lineEditName2, SIGNAL( textChanged(const QString &) ), SLOT( slotSearchTextCheck() ) );
    connect( lineEditAgeFrom, SIGNAL( textChanged(const QString &) ), SLOT( slotSearchTextCheck() ) );
    connect( lineEditAgeTo, SIGNAL( textChanged(const QString &) ), SLOT( slotSearchTextCheck() ) );

    textEditMessage->setText( QString::null );
    connect( textEditMessage, SIGNAL( textChanged() ), SLOT( slotTextCheckOnSearchTab() ) );

    pushButtonRequire->setEnabled( FALSE );
    connect( pushButtonRequire, SIGNAL( clicked() ), SLOT( slotRequireOnSearchTab() ) );
    
    connect( pushButtonCancel, SIGNAL( clicked() ), SLOT( slotCancel() ) );
}

void AddFriendView::slotRequire() {
    accept();
    emit addFriendRequire( this );
}


void AddFriendView::slotCancel() {
    clear();
    reject();
}

void AddFriendView::clear() {
    lineeditID->setText("");
    texteditMessage->setText("");
    comboBox1->setCurrentItem(0);
    lineeditID->setFocus();

    comboBoxNo1->setCurrentItem(0);
    lineEditNo2->setText("");
    lineEditNo3->setText("");
    lineEditName1->setText("");
    lineEditName2->setText("");
    comboBoxGender->setCurrentItem(0);
    lineEditAgeFrom->setText("");
    lineEditAgeTo->setText("");
    listViewResult->clear();
    textEditMessage->setText("");
}

void AddFriendView::slotTextCheck() {
    if ( texteditMessage->text(). stripWhiteSpace().length() == 0 ||
            lineeditID->text().stripWhiteSpace().length() == 0 )
        buttonRequire->setEnabled( FALSE );
    else
        buttonRequire->setEnabled( TRUE );
}

void AddFriendView::slotSearchTextCheck() {
    if ( getPhoneNo2().stripWhiteSpace().length() >= 3
            && getPhoneNo3().stripWhiteSpace().length() >= 4
            && getName1().stripWhiteSpace().length() != 0 )
        pushButtonSearch1->setEnabled( TRUE );
    else
        pushButtonSearch1->setEnabled( FALSE );
    
    if ( getName2().stripWhiteSpace().length() != 0
            && getFromAge().stripWhiteSpace().length() != 0
            && getToAge().stripWhiteSpace().length() != 0 )
        pushButtonSearch2->setEnabled( TRUE );
    else
        pushButtonSearch2->setEnabled( FALSE );
}

void AddFriendView::slotSearchByPhoneNo() {
    pushButtonRequire->setEnabled( FALSE );     // 친구 요청 버튼 비활성화
    pushButtonSearch1->setEnabled( FALSE );     // 검색 버튼 일시 비활성화
    pushButtonSearch2->setEnabled( FALSE );     // 검색 버튼 일시 비활성화
    listViewResult->clear();                    // 검색 결과 표시 내용 초기화
    m_sSearchFriendResult = "";                 // 데이터 수신용 변수 초기화
    emit searchByPhoneNo( this );               // 검색 개시
}

void AddFriendView::slotSearchByAge() {
    // 입력된 나이 범위가 정상적이지 않거나, 5살 초과 범위이면 검색 불가
    if ((getFromAge().toInt() > getToAge().toInt())
            || (getToAge().toInt() - getFromAge().toInt() > 5)) {
        KMessageBox::information(this, UTF8("나이 범위를 제대로 입력해 주십시오.\n나이차는 5살 초과가 될 수 없습니다."), UTF8("친구 검색"));
        return;
    }

    pushButtonRequire->setEnabled( FALSE );     // 친구 요청 버튼 비활성화
    pushButtonSearch1->setEnabled( FALSE );     // 검색 버튼 일시 비활성화
    pushButtonSearch2->setEnabled( FALSE );     // 검색 버튼 일시 비활성화
    listViewResult->clear();                    // 검색 결과 표시 내용 초기화
    m_sSearchFriendResult = "";                 // 데이터 수신용 변수 초기화
    emit searchByAge( this );                   // 검색 개시
}

void AddFriendView::slotGotSearchResult( const QString &sResult ) {
    /*!
    [수신 결과-정상]
    100
    1 50 3 N N
    0 10004000001 F111... 홍길동%20(aaa***@nate.com) M 25 %00 F %00
    1 10004000002 F222... 홍길동%20(bbb***@nate.com) M 26 %00 F 010-*111-111*
    3 10004000003 F333... 홍길동%20(ccc***@nate.com) M 26 %00 F %00
    -----
    (응답코드)
    1 50 (검색레코드수) N N
    (일련번호) (?) (암호화된 전체 내용) (이름%20(이메일)) (성별) (나이) %00 F (전화번호)
    ...

    [수신 결과-에러]
    201
    fromage%20and%20toage%20must%20be%20natural%20integer%20and%20the%20diffrence%20must%20be%20equals%20or%20less%20than%205
    -----
    (응답코드)
    (에러 메시지)
    */

    // 검색 결과가 여러 차례로 나뉘어 오는 경우가 있으므로 전체 내용 보관용 변수에 수신 내용을 추가
    m_sSearchFriendResult += sResult;

    // 결과 코드를 해석하기위해 QStringList로 분해
    QStringList slResult = QStringList::split( "\r\n", UTF8(m_sSearchFriendResult) );

    // 수신 데이터가 없는 경우
    if (slResult.isEmpty() == TRUE) {
        slResult.clear();
        slResult << "000" << UTF8("알 수 없는 에러가 발생하였습니다.");
        showSearchResult( FALSE, slResult );
        m_sSearchFriendResult = "";
        return;
    }

    // 에러가 발생한 경우(첫번재 레코드에는 에러 코드가, 두번째 레코드에는 에러 설명이 저장되어 있음)
    if (slResult.first() != "100") {
        showSearchResult( FALSE, slResult );
        m_sSearchFriendResult = "";
        return;
    }

    // 검색 레코드 개수를 얻기 위해 헤더 추출
    QStringList slTemp = QStringList::split( " ", *slResult.at(1) );

    // 입력된 조건으로 검색된 결과가 없는 경우
    if ( (*slTemp.at(2)).toLong() == 0 ) {
        slResult.clear();
        slResult << "100" << UTF8("검색 결과가 없습니다.");
        showSearchResult( FALSE, slResult );
        m_sSearchFriendResult = "";
        return;
    }

    // 헤더에 지정된 레코드 개수가 수신된 레코드 개수보다 큰 경우 추가 데이터 수신 대기
    if ( (*slTemp.at(2)).toULong() > slResult.count() - 2 ) {
        return;
    }

    // 검색 결과를 모두 수신한 경우, 결과를 화면에 표시
    showSearchResult( TRUE, slResult );
    m_sSearchFriendResult = "";
}

void AddFriendView::showSearchResult(bool bIsSuccess, QStringList& slResult) {
    // 각 입력 필드에 입력된 내용을 확인하여 검색 버튼 활성화
    slotSearchTextCheck();

    // 검색 결과가 없거나 에러가 발생한 경우
    if ( bIsSuccess == FALSE ) {
        QString sErrDesc = *slResult.at(1);
        sErrDesc.replace("%20", " ");
        KMessageBox::information(this, sErrDesc, UTF8("친구 검색"));
        return;
    }

    // 검색 결과를 리스트에 표시
    for ( QStringList::Iterator it = slResult.at(2); it != slResult.end(); ++it ) {
        // 레코드에서 필드 분리
        QStringList slField = QStringList::split(" ", *it);

        // 필드가 8개 보다 적은 경우 무시
        if (slField.count() < 8) continue;

        QListViewItem *item = new QListViewItem(listViewResult, listViewResult->lastItem());

        // 이름 & 이메일 분리
        QStringList slNameEmail = QStringList::split("%20", *slField.at(3));

        // 이름
        item->setText(0, *slNameEmail.at(0));

        // 이메일
        QString sEmail = *slNameEmail.at(1);
        sEmail.replace(QRegExp::QRegExp("[()]"), "");       // 이메일에서 앞뒤 괄호 제거
        item->setText(1, sEmail);

        // 전화번호
        if (*slField.at(8) != "%00")
            item->setText(2, *slField.at(8));

        // 성별
        if (*slField.at(4) == "M")
            item->setText(3, QString::fromUtf8("남자"));
        else
            item->setText(3, QString::fromUtf8("여자"));

        // 친구 요청 시 사용하기 위해 해당 레코드 전체 데이터 저장
        item->setText(4, *it);
    }
}

// 이메일 주소 길이 해독용 테이블
static const unsigned char eMailLen_tbl[32] = {
/*  0 */ 0xe7, 0xe6, 0xe5, 0xe4, 0xe3, 0xe2, 0xe1, 0xe0,
/*  8 */ 0xef, 0xee, 0xed, 0xec, 0xeb, 0xea, 0xe9, 0xe8,
/* 16 */ 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, 0xf0,
/* 24 */ 0xff, 0xfe, 0xfd, 0xfc, 0xfb, 0xfa, 0xf9, 0xf8
};

int AddFriendView::getEmailAddrLen( unsigned char nCode )
{
    int nLen = 0;

    // 이메일 주소는 최대 31자
    for (int i=0; i<32; i++)
    {
        if (nCode == eMailLen_tbl[i])
        {
            nLen = i;
            break;
        }
    }

    return nLen;
}

// 이메일 주소 문자 해독용 테이블
static const unsigned char eMailEncChar_tbl[256] = {
/* 0x00 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x08 */ 'g', 'w', ' ', ' ', ' ', '7', ' ', ' ',
/* 0x10 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x18 */ 'f', 'v', ' ', ' ', ' ', '6', ' ', ' ',
/* 0x20 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x28 */ 'e', 'u', ' ', ' ', ' ', '5', ' ', ' ',
/* 0x30 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x38 */ 'd', 't', ' ', ' ', ' ', '4', ' ', ' ',
/* 0x40 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x48 */ 'c', 's', ' ', ' ', ' ', '3', ' ', ' ',
/* 0x50 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x58 */ 'b', 'r', ' ', ' ', ' ', '2', ' ', ' ',
/* 0x60 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x68 */ 'a', 'q', ' ', ' ', ' ', '1', ' ', ' ',
/* 0x70 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x78 */ ' ', 'p', '@', ' ', ' ', '0', ' ', ' ',
/* 0x80 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x88 */ 'o', ' ', ' ', '_', ' ', ' ', ' ', ' ',
/* 0x90 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0x98 */ 'n', ' ', ' ', ' ', '.', ' ', ' ', ' ',
/* 0xA0 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xA8 */ 'm', ' ', ' ', ' ', '-', ' ', ' ', ' ',
/* 0xB0 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xB8 */ 'l', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xC0 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xC8 */ 'k', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xD0 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xD8 */ 'j', 'z', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xE0 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xE8 */ 'i', 'y', ' ', ' ', ' ', '9', ' ', ' ',
/* 0xF0 */ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
/* 0xF8 */ 'h', 'x', ' ', ' ', ' ', '8', ' ', ' '
};

char AddFriendView::getDecodedChar( int nIdx ) {
    if (nIdx < 0x00 || nIdx > 0xff)
        return 0x00;

    return eMailEncChar_tbl[nIdx];
}

QString AddFriendView::getEmailAddr( QString sEncData ) {
    QString sEmailAddr = "";
    QString sCode;
    const char *pos;
    char cCh, cTemp[8];
    int nEncDataLen, nDecLen, nIdx;

    // 입력 문자열 길이가 12자(메일 주소가 5자) 미만이면 에러
    nEncDataLen = sEncData.length();
    if (nEncDataLen < 12)
    {
        kdDebug() << "nEncDataLen:" << nEncDataLen << endl;
        return QString::QString("");
    }

    pos = sEncData.ascii();

    // 메일 주소 길이 얻기
    memcpy(cTemp, pos, 2);
    cTemp[2] = 0x00;
    if (sscanf(cTemp, "%x", &nIdx) == 0)
    {
        return QString::QString("");
    }

    // 메일 주소 길이가 5자 미만이면 에러
    nDecLen = getEmailAddrLen(nIdx);
    if (nDecLen < 5)
    {
        return QString::QString("");
    }

    // 메일 주소 얻기
    for (int i=0; i < nDecLen; i++)
    {
        memcpy(cTemp, pos + 2 + (i*2), 2);
        cTemp[2] = 0x00;
        if (sscanf(cTemp, "%x", &nIdx) == 0)
        {
            return QString::QString("");
        }
        cCh = getDecodedChar(nIdx);
        if (cCh == 0x00)
        {
            return QString::QString("");
        }
        sEmailAddr += cCh;
    }

    return sEmailAddr;
}

void AddFriendView::slotTextCheckOnSearchTab() {
    if ( listViewResult->selectedItem() == 0 ||
            textEditMessage->text().stripWhiteSpace().length() == 0 )
        pushButtonRequire->setEnabled( FALSE );
    else
        pushButtonRequire->setEnabled( TRUE );
}

void AddFriendView::slotRequireOnSearchTab() {
    accept();
    emit addFriendRequireOnSearchTab( this );
}

QString AddFriendView::getSelectedID() {
    // 검색 결과 중 현재 선택된 사용자
    QListViewItem *item = listViewResult->selectedItem();
    if (item == NULL)
        return QString::QString("");

    // 숨겨진 컬럼에서 서버에서 수신한 원본 데이터 얻기
    QString sData = item->text(4);

    QStringList slField = QStringList::split(" ", sData);
    if (slField.count() < 8)
        return QString::QString("");

    return getEmailAddr(*slField.at(2));
}
