/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ALLOWFRIENDFORM_H
#define ALLOWFRIENDFORM_H

#include <kstandarddirs.h>
#include <kglobal.h>
#include <kaboutdata.h>

#include <qvariant.h>
#include <qpixmap.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QFrame;
class QLabel;
class QPushButton;

class AllowFriendForm : public QDialog {
    Q_OBJECT

public:
    AllowFriendForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~AllowFriendForm();

    QFrame* frame3;
    QLabel* pixmapLabel1;
    QLabel* useridLabel;
    QFrame* frame4;
    QLabel* textLabel1_2;
    QLabel* messageLabel;
    QPushButton* profileButton;
    QPushButton* acceptButton;
    QPushButton* rejectButton;
    QPushButton* cancelButton;

protected:
    QVBoxLayout* AllowFriendFormLayout;
    QHBoxLayout* frame3Layout;
    QSpacerItem* spacer3;
    QSpacerItem* spacer4;
    QSpacerItem* spacer5;
    QVBoxLayout* frame4Layout;
    QVBoxLayout* layout5;
    QHBoxLayout* layout3;
    QSpacerItem* spacer2;
    QHBoxLayout* layout1;
    QSpacerItem* spacer1;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;
    QPixmap image1;
    QPixmap image2;

};

#endif // ALLOWFRIENDFORM_H
