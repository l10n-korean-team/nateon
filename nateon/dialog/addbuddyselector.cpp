/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "addbuddyselector.h"

AddBuddySelector::AddBuddySelector(QWidget* parent, const char* name, WFlags fl)
        : AddForm(parent, name, fl) {
    connect(pushButton5, SIGNAL( clicked() ), this, SLOT( close() )  );
    connect(comboBox1, SIGNAL( activated( const QString &) ), this, SLOT( slotSearchBuddies() ) );
    connect(this, SIGNAL( updateList() ), this, SLOT( slotSearchBuddies() ) );
    /// 버디 선택 버튼
    connect(pushButton2, SIGNAL( clicked() ), this, SLOT( slotSelectBuddies() ) );
    /// 선택 제거 버튼
    connect(pushButton3, SIGNAL( clicked() ), this, SLOT( slotRemoveBuddies() ) );
    /// 버디 검색 버튼
    connect(pushButton1, SIGNAL( clicked() ), this, SLOT( slotSearchBuddies() ) );
    /// OK 버튼
    connect(pushButton4, SIGNAL( clicked() ), this, SLOT( slotSelectedOK() ) );
    /// 이름(검색 문자열) 입력
    connect(lineEdit1, SIGNAL( textChanged ( const QString & ) ), this, SLOT( slotNameChanged( const QString & ) ) );

    listView2->addColumn("Email", 100);
    listView2->hideColumn(1);

    listView1->setSelectionMode(QListView::Extended);
    listView2->setSelectionMode(QListView::Extended);
    listView2->clear();
    slExcept.clear();
}


AddBuddySelector::~AddBuddySelector() {
}

void AddBuddySelector::setAllList() {
    m_BuddyList = *pBuddyList;
    QPtrListIterator<Buddy> iterator( m_BuddyList );
    Buddy *pBuddy;

    listView1->clear();
    lineEdit1->clear();
    comboBox1->setCurrentItem( 0 );
    bool bFound=false;
    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        /*! 목록 예외처리 추가 */
        if (slExcept.count() > 0) {
            bool bfind = false;
            for ( QStringList::Iterator it = slExcept.begin(); it != slExcept.end(); ++it ) {
                if (*it == pBuddy->getHandle() ) {
                    bfind = true;
                    break;
                }
            }
            if ( bfind ) {
                ++iterator;
                continue;
            }
        }

        bFound=false;
        QListViewItemIterator it( listView2 );
        for ( ; it.current(); ++it ) {
            if ( it.current()->text(1) == pBuddy->getUID() )
                bFound=true;
        }
        if (!bFound)
            /* QListViewItem *pbuddyItem = */ new QListViewItem(listView1, pBuddy->getName(), pBuddy->getUID() );
        ++iterator;
    }

}


void AddBuddySelector::setGroupList( const GroupList *mGroupList ) {
    pGroupList = const_cast<GroupList*> (mGroupList);
    m_GroupList = *pGroupList;

    QPtrListIterator<Group> iterator( m_GroupList );
    Group *pGroup;
    comboBox1->insertItem(UTF8("전체 보기"));
    while (iterator.current() != 0) {
        pGroup = iterator.current();
        comboBox1->insertItem( pGroup->getGName() );
        ++iterator;
    }

    return;
}

void AddBuddySelector::setBuddyList( const BuddyList *mBuddyList ) {
    pBuddyList = const_cast<BuddyList*> (mBuddyList);

    setAllList();

    return;
}


/*!
  친구추가, 쪽지 수신자 추가 다이얼로그에서
  그룹을 선택했을경우
*/
void AddBuddySelector::slotSelectGroup(const QString & sGroup) {
    emit updateList();
}

void AddBuddySelector::slotSelectBuddies() {
    if ( !listView1->firstChild() )
        return;

    QListViewItemIterator it( listView1 );

    for ( ; it.current(); ++it ) {
        if (listView1->isSelected ( it.current() ) )
            /* QListViewItem *pAddBuddyItem = */ new QListViewItem(listView2, it.current()->text(0), it.current()->text(1));
    }
    emit updateList();
}

void AddBuddySelector::slotRemoveBuddies() {
    if ( !listView2->firstChild() )
        return;

    for ( QListViewItemIterator it( listView2 ); it.current(); ) {
        if ( listView2->isSelected( it.current() ) ) {
            listView2->takeItem( it.current() );
            continue;
        }
        it++;
    }

    emit updateList();
}

void AddBuddySelector::slotSearchBuddies() {
    /// 검색 문자열
    QRegExp rx( ".*" + lineEdit1->text() + ".*" );

    /// 검색 그룹 한정
    if ( comboBox1->currentItem() ) { /// 특정 그룹
        Group *pGroup = pGroupList->getGroupByName( comboBox1->currentText() );
        if (pGroup)
            m_BuddyList = pGroup->getBuddyList();
        else
            m_BuddyList.clear();
    } else { /// 전체
        m_BuddyList = *pBuddyList;
    }

    QPtrListIterator<Buddy> iterator( m_BuddyList );
    Buddy *pBuddy;

    listView1->clear();
    bool bFound=false;
    while (iterator.current() != 0) {
        pBuddy = iterator.current();

        /// 차단된 친구 제외
        if ( slExcept.count() && slExcept.find( pBuddy->getHandle() ) == slExcept.end() ) {
            ++iterator;
            continue;
        }

        /// 이름에 검색 문자열이 포함된 경우
        if (rx.search( pBuddy->getName() ) > -1) {
            bFound=false;
            /// listView2에 있으면,
            if ( listView2->firstChild() ) {
                QListViewItemIterator it( listView2 );
                for ( ; it.current(); ++it ) {
                    if ( it.current()->text(1) == pBuddy->getUID() ) {
                        bFound=true;
                        break;
                    }
                }
            }
            if (!bFound) {
                /* QListViewItem *pbuddyItem = */
                new QListViewItem(listView1, pBuddy->getName(), pBuddy->getUID() );
            }

        }
        /// ID에 검색 문자열이 포함된 경우
        else if (rx.search( pBuddy->getUID() ) > -1) {
            bFound=false;
            /// listView2에 있으면,
            if ( listView2->firstChild() ) {
                QListViewItemIterator it( listView2 );
                for ( ; it.current(); ++it ) {
                    if ( it.current()->text(1) == pBuddy->getUID() )
                        bFound=true;
                }
            }
            if (!bFound) {
                /* QListViewItem *pbuddyItem = */
                new QListViewItem(listView1, pBuddy->getName(), pBuddy->getUID() );
            }
        }
        ++iterator;
    }
}

void AddBuddySelector::slotSelectedOK() {
    QString sBuddies;

    if ( listView2->firstChild() ) {
        QListViewItemIterator it( listView2 );
        bool nOne = true;
        for ( ; it.current(); ++it ) {
            if ( nOne )
                sBuddies = sBuddies + "\"" + it.current()->text(0) +"\" <" + it.current()->text(1) +">";
            else
                sBuddies = sBuddies + ";\"" + it.current()->text(0) +"\" <" + it.current()->text(1) +">";
            nOne = false;
        }
    }
    emit selectedBuddies(sBuddies);
    close();
}

void AddBuddySelector::slotNameChanged(const QString &sName) {
    emit updateList();
}

void AddBuddySelector::setSelectedBuddies(QString sBuddies) {
    listView2->clear();

    Buddy *pBuddy = 0;

    QRegExp rx("<?([^@\\s<]+@[^>\\s]+)>?");

    int pos = 0;
    // QStringList list;
    int two = 0;
    while ( pos >= 0 ) {
        pos = rx.search( sBuddies , pos );
        if ( pos > -1 ) {
            pBuddy = pBuddyList->getBuddyByID( rx.cap(1) );
            if (pBuddy)
                /* QListViewItem *pbuddyItem = */ new QListViewItem(listView2, pBuddy->getName(), pBuddy->getUID() );
            two = 1;
            pos  += rx.matchedLength();
        }
    }
}

#include "addbuddyselector.moc"

