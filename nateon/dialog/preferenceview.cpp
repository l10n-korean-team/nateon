/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"

#include "preferenceview.h"
#include "../util/common.h"

extern nmconfig stConfig;

PreferenceView::PreferenceView(QWidget * parent, const char * name, bool modal)
        : Preference(parent, name, modal) {
    WStackPage_9 = new QWidget( widgetStack2, "WStackPage_9" );
    WStackPageLayout_9 = new QVBoxLayout( WStackPage_9, 0, 6, "WStackPageLayout_9");

    pixmapLabel5 = new QLabel( WStackPage_9, "pixmapLabel5" );
    pixmapLabel5->setPixmap( QPixmap( sPicsPath + "sett_titl_09.bmp") );
    pixmapLabel5->setScaledContents( TRUE );
    WStackPageLayout_9->addWidget( pixmapLabel5 );


    layout26 = new QHBoxLayout( 0, 0, 6, "layout26");

    textLabel20 = new QLabel( WStackPage_9, "textLabel20" );
    textLabel20->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel20->sizePolicy().hasHeightForWidth() ) );
    layout26->addWidget( textLabel20 );

    line15 = new QFrame( WStackPage_9, "line15" );
    line15->setFrameShape( QFrame::HLine );
    line15->setFrameShadow( QFrame::Sunken );
    line15->setFrameShape( QFrame::HLine );
    layout26->addWidget( line15 );
    WStackPageLayout_9->addLayout( layout26 );

    textLabel21 = new QLabel( WStackPage_9, "textLabel21" );
    textLabel21->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_9->addWidget( textLabel21 );

    frame6 = new QFrame( WStackPage_9, "frame6" );
    frame6->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, frame6->sizePolicy().hasHeightForWidth() ) );
    frame6->setFrameShape( QFrame::StyledPanel );
    frame6->setFrameShadow( QFrame::Raised );
    frame6Layout = new QVBoxLayout( frame6, 11, 6, "frame6Layout");

    layout35 = new QVBoxLayout( 0, 0, 6, "layout35");

    layout33 = new QHBoxLayout( 0, 0, 6, "layout33");

    textLabel22 = new QLabel( frame6, "textLabel22" );
    textLabel22->setMinimumSize( QSize( 80, 20 ) );
    textLabel22->setMaximumSize( QSize( 80, 20 ) );
    layout33->addWidget( textLabel22 );

    cyIDLineEdit = new QLineEdit( frame6, "cyIDLineEdit" );
    cyIDLineEdit->setMinimumSize( QSize( 220, 20 ) );
    cyIDLineEdit->setMaximumSize( QSize( 220, 20 ) );

    layout33->addWidget( cyIDLineEdit );

    syncButton = new QPushButton( frame6, "syncButton" );
    syncButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    syncButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout33->addWidget( syncButton );
    spacer9 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout33->addItem( spacer9 );
    layout35->addLayout( layout33 );

    layout34 = new QHBoxLayout( 0, 0, 6, "layout34");

    textLabel23 = new QLabel( frame6, "textLabel23" );
    textLabel23->setMinimumSize( QSize( 80, 20 ) );
    textLabel23->setMaximumSize( QSize( 80, 20 ) );

    layout34->addWidget( textLabel23 );

    cyPasswordLineEdit = new QLineEdit( frame6, "cyPasswordLineEdit" );
    cyPasswordLineEdit->setMinimumSize( QSize( 220, 20 ) );
    cyPasswordLineEdit->setMaximumSize( QSize( 220, 20 ) );
    layout34->addWidget( cyPasswordLineEdit );
    spacer8 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout34->addItem( spacer8 );
    layout35->addLayout( layout34 );

    setTabOrder( cyIDLineEdit, cyPasswordLineEdit );
    setTabOrder( cyPasswordLineEdit, syncButton );

    layout32 = new QHBoxLayout( 0, 0, 6, "layout32");

    findIDPasswordButton = new QPushButton( frame6, "findIDPasswordButton" );
    findIDPasswordButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    findIDPasswordButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout32->addWidget( findIDPasswordButton );

    gaibButton = new QPushButton( frame6, "gaibButton" );
    gaibButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    gaibButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout32->addWidget( gaibButton );

    whatminihompyButton = new QPushButton( frame6, "whatminihompyButton" );
    whatminihompyButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    whatminihompyButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout32->addWidget( whatminihompyButton );
    spacer7 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout32->addItem( spacer7 );
    layout35->addLayout( layout32 );
    frame6Layout->addLayout( layout35 );
    WStackPageLayout_9->addWidget( frame6 );

    textLabel24 = new QLabel( WStackPage_9, "textLabel24" );
    textLabel24->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_9->addWidget( textLabel24 );

    layout27 = new QHBoxLayout( 0, 0, 6, "layout27");

    textLabel25 = new QLabel( WStackPage_9, "textLabel25" );
    textLabel25->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel25->sizePolicy().hasHeightForWidth() ) );
    layout27->addWidget( textLabel25 );

    line16 = new QFrame( WStackPage_9, "line16" );
    line16->setFrameShape( QFrame::HLine );
    line16->setFrameShadow( QFrame::Sunken );
    line16->setFrameShape( QFrame::HLine );
    layout27->addWidget( line16 );
    WStackPageLayout_9->addLayout( layout27 );

    textLabel26 = new QLabel( WStackPage_9, "textLabel26" );
    WStackPageLayout_9->addWidget( textLabel26 );

    layout28 = new QHBoxLayout( 0, 0, 6, "layout28");

    textLabel27 = new QLabel( WStackPage_9, "textLabel27" );
    layout28->addWidget( textLabel27 );

    gonggaeComboBox = new QComboBox( FALSE, WStackPage_9, "gonggaeComboBox" );
    gonggaeComboBox->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    gonggaeComboBox->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout28->addWidget( gonggaeComboBox );
    spacer4 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout28->addItem( spacer4 );
    WStackPageLayout_9->addLayout( layout28 );

    layout29 = new QHBoxLayout( 0, 0, 6, "layout29");

    textLabel28 = new QLabel( WStackPage_9, "textLabel28" );
    textLabel28->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel28->sizePolicy().hasHeightForWidth() ) );
    layout29->addWidget( textLabel28 );

    line17 = new QFrame( WStackPage_9, "line17" );
    line17->setFrameShape( QFrame::HLine );
    line17->setFrameShadow( QFrame::Sunken );
    line17->setFrameShape( QFrame::HLine );
    layout29->addWidget( line17 );
    WStackPageLayout_9->addLayout( layout29 );

    layout30 = new QHBoxLayout( 0, 0, 6, "layout30");

    textLabel29 = new QLabel( WStackPage_9, "textLabel29" );
    layout30->addWidget( textLabel29 );

    alarmMinimiComboBox = new QComboBox( FALSE, WStackPage_9, "alarmMinimiComboBox" );
    alarmMinimiComboBox->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    alarmMinimiComboBox->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout30->addWidget( alarmMinimiComboBox );
    spacer5 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout30->addItem( spacer5 );
    WStackPageLayout_9->addLayout( layout30 );

    layout31 = new QHBoxLayout( 0, 0, 6, "layout31");

    layout16 = new QVBoxLayout( 0, 0, 6, "layout16");

    alarmNewCheckBox = new QCheckBox( WStackPage_9, "alarmNewCheckBox" );
    alarmNewCheckBox->setChecked( TRUE );
    layout16->addWidget( alarmNewCheckBox );

    alarmMemoEtcCheckBox = new QCheckBox( WStackPage_9, "alarmMemoEtcCheckBox" );
    alarmMemoEtcCheckBox->setChecked( TRUE );
    layout16->addWidget( alarmMemoEtcCheckBox );
    layout31->addLayout( layout16 );
    spacer6 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout31->addItem( spacer6 );
    WStackPageLayout_9->addLayout( layout31 );
    widgetStack2->addWidget( WStackPage_9, 8 );

    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_09.bmp"), trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c" ) );

    /*!
     *
     */
    connect(syncButton, SIGNAL( clicked() ), SLOT( slotCySync() ) );
    cyPasswordLineEdit->setEchoMode ( QLineEdit::Password );
    connect( findIDPasswordButton , SIGNAL( clicked() ), SLOT( slotFindIDPassword() ) );
    connect( gaibButton , SIGNAL( clicked() ), SLOT( slotNateDotComGaIb() ) );
    connect( whatminihompyButton , SIGNAL( clicked() ), SLOT( slotWhatMiniHompy() ) );


    textLabel20->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xec\x97\xb0\xeb\x8f\x99" ) );
    textLabel21->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\xec\x9d\x98\x20\xec\x95\x84\xec\x9d"
                                  "\xb4\xeb\x94\x94\xec\x99\x80\x20\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88\xed\x98\xb8"
                                  "\xeb\xa5\xbc\x20\xec\x9e\x85\xeb\xa0\xa5\xed\x95\x98\xeb\xa9\xb4\x20\xeb\x84\xa4"
                                  "\xec\x9d\xb4\xed\x8a\xb8\xec\x98\xa8\xec\x97\x90\xec\x84\x9c\x20\xec\x8b\xb8\xec"
                                  "\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xec\x9d\xbc\xec\xb4\x8c\xec\xb9\x9c\xea\xb5"
                                  "\xac\xec\x99\x80\x20\xeb\x8c\x80\xed\x99\x94\xeb\xa5\xbc\x20\xed\x95\x98\xea\xb1"
                                  "\xb0\xeb\x82\x98\x20\xeb\xaf\xb8\xeb\x8b\x88\xed\x99\x88\xed\x94\xbc\xeb\xa5\xbc"
                                  "\x20\xec\x97\xb0\xeb\x8f\x99\xed\x95\xa0\x20\xec\x88\x98\x20\xec\x9e\x88\xec\x8a"
                                  "\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    textLabel22->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\x49\x44" ) );
    syncButton->setText( trUtf8( "\xec\x97\xb0\xeb\x8f\x99\xed\x95\x98\xea\xb8\xb0" ) );
    textLabel23->setText( trUtf8( "\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88\xed\x98\xb8" ) );
    findIDPasswordButton->setText( trUtf8( "\x49\x44\x2f\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88\xed\x98\xb8\x20\xec\xb0\xbe\xea\xb8"
                                           "\xb0" ) );
    gaibButton->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xed\x9a\x8c\xec\x9b\x90\xea\xb0"
                                 "\x80\xec\x9e\x85" ) );
    whatminihompyButton->setText( trUtf8( "\xeb\xaf\xb8\xeb\x8b\x88\xed\x99\x88\xed\x94\xbc\xeb\x9e\x80\x3f" ) );
    textLabel24->setText( trUtf8( "\xec\x9e\x85\xeb\xa0\xa5\xed\x95\x98\xec\x8b\xa0\x20\xec\x8b\xb8\xec\x9d\xb4\xec\x9b"
                                  "\x94\xeb\x93\x9c\x20\x49\x44\xec\x99\x80\x20\xeb\xb9\x84\xeb\xb0\x80\xeb\xb2\x88"
                                  "\xed\x98\xb8\xeb\x8a\x94\x20\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20"
                                  "\xec\x97\xb0\xeb\x8f\x99\xeb\xa7\x8c\xec\x9d\x84\x20\xec\x9c\x84\xed\x95\xb4\x20"
                                  "\xec\x82\xac\xec\x9a\xa9\xeb\x90\xa9\xeb\x8b\x88\xeb\x8b\xa4\x2e\x20\xec\x9c\x84"
                                  "\x20\xec\x82\xac\xed\x95\xad\xec\x97\x90\x20\xeb\x8f\x99\xec\x9d\x98\xed\x95\x98"
                                  "\xec\xa7\x80\x20\xec\x95\x8a\xec\x9d\x84\x20\xea\xb2\xbd\xec\x9a\xb0\x20\x22\xec"
                                  "\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xec\x97\xb0\xeb\x8f\x99\xed\x95"
                                  "\x98\xea\xb8\xb0\x22\xec\x84\xa4\xec\xa0\x95\xec\x9d\x84\x20\xec\xb7\xa8\xec\x86"
                                  "\x8c\xed\x95\xb4\xec\xa3\xbc\xec\x8b\x9c\xea\xb8\xb0\x20\xeb\xb0\x94\xeb\x9e\x8d"
                                  "\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    textLabel25->setText( trUtf8( "\xeb\xaf\xb8\xeb\x8b\x88\xed\x99\x88\xed\x94\xbc\x20\xea\xb3\xb5\xea\xb0\x9c\xec\x84"
                                  "\xa4\xec\xa0\x95" ) );
    textLabel26->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xec\x97\xb0\xeb\x8f\x99\xed\x95"
                                  "\x98\xea\xb8\xb0\xeb\xa5\xbc\x20\xec\x84\xa4\xec\xa0\x95\xed\x95\x98\xec\x8b\xa0"
                                  "\x20\xea\xb2\xbd\xec\x9a\xb0\x20\xeb\x82\xb4\x20\xeb\xaf\xb8\xeb\x8b\x88\xed\x99"
                                  "\x88\xed\x94\xbc\xec\x9d\x98\x20\xea\xb3\xb5\xea\xb0\x9c\x20\xec\x97\xac\xeb\xb6"
                                  "\x80\xeb\xa5\xbc\x20\xec\x84\xa4\xec\xa0\x95\xed\x95\x98\xec\x8b\xa4\x20\xec\x88"
                                  "\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    textLabel27->setText( trUtf8( "\xeb\x82\xb4\x20\xeb\xaf\xb8\xeb\x8b\x88\xed\x99\x88\xed\x94\xbc\xeb\xa5\xbc\x20" ) );
    gonggaeComboBox->clear();
    gonggaeComboBox->insertItem( trUtf8( "\xeb\xaa\xa8\xeb\x91\x90\xec\x97\x90\xea\xb2\x8c\x20\xea\xb3\xb5\xea\xb0\x9c" ) );
    gonggaeComboBox->insertItem( trUtf8( "\xec\xb9\x9c\xea\xb5\xac\xec\x97\x90\xea\xb2\x8c\xeb\xa7\x8c\x20\xea\xb3\xb5\xea\xb0"
                                         "\x9c" ) );
    gonggaeComboBox->insertItem( trUtf8( "\xeb\xaa\xa8\xeb\x91\x90\xec\x97\x90\xea\xb2\x8c\x20\xeb\xb9\x84\xea\xb3\xb5\xea\xb0"
                                         "\x9c" ) );
    textLabel28->setText( trUtf8( "\xec\x8b\xb8\xec\x9d\xb4\xec\x9b\x94\xeb\x93\x9c\x20\xec\x95\x8c\xeb\xa6\xbc\x20\xec"
                                  "\x84\xa4\xec\xa0\x95" ) );
    textLabel29->setText( trUtf8( "\xeb\xaf\xb8\xeb\x8b\x88\xed\x99\x88\xed\x94\xbc\x20\xec\x95\x8c\xeb\xa6\xbc" ) );
    alarmMinimiComboBox->clear();
    alarmMinimiComboBox->insertItem( trUtf8( "\xeb\x82\xb4\x20\xeb\xaf\xb8\xeb\x8b\x88\xeb\xaf\xb8\xeb\xa1\x9c\x20\xec\x95\x8c\xeb"
                                     "\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0" ) );
    alarmMinimiComboBox->insertItem( trUtf8( "\xec\x95\x8c\xeb\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0" ) );
    alarmNewCheckBox->setText( trUtf8( "\xec\x83\x88\xea\xb8\x80\x20\xeb\x93\xb1\xeb\xa1\x9d\x20\xec\x8b\x9c\x20\xec\x95\x8c"
                                       "\xeb\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0" ) );
    alarmMemoEtcCheckBox->setText( trUtf8( "\xec\xaa\xbd\xec\xa7\x80\x2c\x20\xec\x9d\xbc\xec\xb4\x8c\x20\xec\x8b\xa0\xec\xb2\xad"
                                           "\x2c\x20\xec\x83\x88\x20\xec\x84\xa0\xeb\xac\xbc\x20\xec\x95\x8c\xeb\xa0\xa4\xec"
                                           "\xa3\xbc\xea\xb8\xb0" ) );

}


PreferenceView::~PreferenceView() {
}

void PreferenceView::accept() {
    Preference::accept();
    /*! 싸이월드 */
    config->setGroup( "Config_Cyworld" );
    stConfig.minihompypublic = config->readNumEntry( "MiniHompy_Public", 1 );

    if ( gonggaeComboBox->currentItem() != stConfig.minihompypublic ) {
        stConfig.minihompypublic = gonggaeComboBox->currentItem();
        QString sCommand;
        /*! 미니홈피 공개 */
        switch ( gonggaeComboBox->currentItem() ) {
        case 0 :
            sCommand = "cyworld_open_mode=O";
            break;
        case 1 :
            sCommand = "cyworld_open_mode=B";
            break;
        case 2 :
            sCommand = "cyworld_open_mode=C";
            break;
        }
        emit updateCyInfo( sCommand );
        config->writeEntry( "MiniHompy_Public", stConfig.minihompypublic );
#ifdef NETDEBUG
        kdDebug() << "미니홈피 공개 : [" << stConfig.minihompypublic << "], 전송 프로토콜 : [" << sCommand << "]" << endl;
#endif
    }



    /*! 미니홈피 알림 방법*/
    if ( stConfig.alarmminihompy != alarmMinimiComboBox->currentItem() ) {
        stConfig.alarmminihompy = alarmMinimiComboBox->currentItem();
        config->writeEntry( "MiniHompy_Alarm", stConfig.alarmminihompy );
    }
    /*! 새글 알림 */
    if ( stConfig.alarmminihompynew != alarmNewCheckBox->isChecked() ) {
        stConfig.alarmminihompynew = alarmNewCheckBox->isChecked();
        config->writeEntry( "Use_MiniHompy_New", stConfig.alarmminihompynew );
        QString sCommand("cyworld_noti1=");
        if ( stConfig.alarmminihompynew )
            sCommand += "Y";
        else
            sCommand += "N";
        emit updateCyInfo( sCommand );
    }
    /*! 쪽지, 일촌신청, 새선물 알림 */
    if ( stConfig.alarmetc != alarmMemoEtcCheckBox->isChecked() ) {
        stConfig.alarmetc = alarmMemoEtcCheckBox->isChecked();
        config->writeEntry( "Use_MiniHompy_ETC", stConfig.alarmetc );
        QString sCommand("cyworld_noti2=");
        if ( stConfig.alarmetc )
            sCommand += "Y";
        else
            sCommand += "N";
        emit updateCyInfo( sCommand );
    }
    config->sync();
}

void PreferenceView::initialize() {
    Preference::initialize();

    /*! 싸이월드 */
    config->setGroup( "Config_Cyworld" );

    /*! 싸이월드 연동 유무 */
    // stConfig.usecyworld = config->readBoolEntry( "Use_Cyworld", false );
    /*! 싸이월드 Password */
    // stConfig.cypw = config->readEntry( "Cyworld_Password" );

    // stConfig.cyid = config->readEntry( "Cyworld_ID" );
    if ( stConfig.usecyworld ) {
        syncButton->setText( UTF8("연동해제") );
        /*! 싸이월드 ID */
        /*!
         * 수정필요. Cyworld ID 값을 서버로부터 받아야 함.
         * 현재 가지고 있는것은 Cyworld CMN 값만 가지고 있음.
         */
        // stConfig.cyid = config->readEntry( "Cyworld_ID" ); /*! <===== 잘못되었음 */
        cyIDLineEdit->setText( stConfig.cyid );
        cyIDLineEdit->setEnabled ( false );
        cyPasswordLineEdit->setEchoMode ( QLineEdit::Normal );
        cyPasswordLineEdit->setText( UTF8("비밀번호는 저장되지 않습니다.") );
        cyPasswordLineEdit->setEnabled( false );
        gonggaeComboBox->setEnabled( true );
        alarmMinimiComboBox->setEnabled( true );
        alarmNewCheckBox->setEnabled( true );
        alarmMemoEtcCheckBox->setEnabled( true );
    } else {
        syncButton->setText( UTF8("연동하기") );
        cyIDLineEdit->setEnabled ( true );
        cyIDLineEdit->clear();
        cyPasswordLineEdit->setEchoMode ( QLineEdit::Password );
        cyPasswordLineEdit->setEnabled( true );
        cyPasswordLineEdit->clear();
        gonggaeComboBox->setEnabled( false );
        alarmMinimiComboBox->setEnabled( false );
        alarmNewCheckBox->setEnabled( false );
        alarmMemoEtcCheckBox->setEnabled( false );
    }
    /*! 미니홈피 공개 */
    stConfig.minihompypublic = config->readNumEntry( "MiniHompy_Public", 1 );
    gonggaeComboBox->setCurrentItem( stConfig.minihompypublic );
    /*! 미니홈피 알림 방법*/
    stConfig.alarmminihompy = config->readNumEntry( "MiniHompy_Alarm", 0 );
    alarmMinimiComboBox->setCurrentItem( stConfig.alarmminihompy );
    /*! 새글 알림 */
    stConfig.alarmminihompynew = config->readBoolEntry( "Use_MiniHompy_New", true );
    alarmNewCheckBox->setChecked( stConfig.alarmminihompynew );
    /*! 쪽지, 일촌신청, 새선물 알림 */
    stConfig.alarmetc = config->readBoolEntry( "Use_MiniHompy_ETC", true );
    alarmMemoEtcCheckBox->setChecked( stConfig.alarmetc );
}


void PreferenceView::show() {
    initialize();
    Preference::show();
}

void PreferenceView::slotCySync() {
    cyPasswordLineEdit->setEchoMode ( QLineEdit::Password );
    if ( !stConfig.usecyworld ) {
        if ( ( cyIDLineEdit->text().length()  < 1 ) ||
                ( cyPasswordLineEdit->text().length()  < 1 ) ) {
            KMessageBox::information (this, QString::fromUtf8("아이디랑 패스워드를 입력하세요."), UTF8("연동하기") );
            // syncButton->setOn ( false );
            return;
        }
        emit cySync( cyIDLineEdit->text(), cyPasswordLineEdit->text() );
#if 0
        KMessageBox::information (this, QString::fromUtf8("아이디/패스워드가 일치하지 않습니다."), UTF8("연동하기") );
        KMessageBox::information (this, QString::fromUtf8("사용자 인증이 성공하였습니다."), UTF8("연동하기") );
#endif

    } else {
        int result = KMessageBox::questionYesNo(this, QString::fromUtf8("미니홈피 설정 시 입력하신 싸이월드ID와 비밀번호를\n삭제하시겠습니까?\n(삭제 하신 후 다시 사용하시려면 싸이월드 재인증\n과정이 필요합니다."), UTF8("환경설정") );
        if ( result == KMessageBox::Yes ) {
            emit cySync(QString::null, QString::null);
        }
    }
}


void PreferenceView::slotCySyncAuthError( bool bErr ) {
    /*! 싸이월드 */
    config->setGroup( "Config_Cyworld" );
    if ( bErr ) {
        stConfig.usecyworld = false;
        syncButton->setText( UTF8("연동하기") );
        cyIDLineEdit->setEnabled ( true );
        // cyIDLineEdit->clear();
        cyPasswordLineEdit->setEchoMode ( QLineEdit::Password );
        cyPasswordLineEdit->setEnabled( true );
        cyPasswordLineEdit->clear();
        KMessageBox::information (this, QString::fromUtf8("아이디/패스워드가 일치하지 않습니다."), UTF8("연동하기") );
        alarmMinimiComboBox->setEnabled( false );
        gonggaeComboBox->setEnabled( false );
        alarmNewCheckBox->setEnabled( false );
        alarmMemoEtcCheckBox->setEnabled( false );
        cyPasswordLineEdit->setFocus();
    } else {
        stConfig.usecyworld = true;
        syncButton->setText( UTF8("연동해제") );
        stConfig.cyid=cyIDLineEdit->text();
        cyIDLineEdit->setEnabled ( false );
        cyPasswordLineEdit->setEchoMode ( QLineEdit::Normal );
        cyPasswordLineEdit->setText( UTF8("비밀번호는 저장되지 않습니다.") );
        cyPasswordLineEdit->setEnabled( false );
        KMessageBox::information (this, QString::fromUtf8("사용자 인증이 성공하였습니다."), UTF8("연동하기") );
        alarmMinimiComboBox->setEnabled( true );
        gonggaeComboBox->setEnabled( true );
        alarmNewCheckBox->setEnabled( true );
        alarmMemoEtcCheckBox->setEnabled( true );
    }
    /*! 싸이월드 연동 유무 */
    // config->writeEntry( "Use_Cyworld", stConfig.usecyworld );
    /*! 싸이월드 ID */
    config->writeEntry( "Cyworld_ID", stConfig.cyid);
    config->sync();
}


void PreferenceView::showSyncSetup() {
    widgetStack2->raiseWidget(8);
    cyIDLineEdit->setFocus();
}

void PreferenceView::slotCyCanceled() {
    stConfig.usecyworld = false;
    syncButton->setText( UTF8("연동하기") );
    cyIDLineEdit->setEnabled ( true );
    cyIDLineEdit->clear();
    cyPasswordLineEdit->setEnabled( true );
    cyPasswordLineEdit->clear();
    alarmMinimiComboBox->setEnabled( false );
    gonggaeComboBox->setEnabled( false );
    alarmNewCheckBox->setEnabled( false );
    alarmMemoEtcCheckBox->setEnabled( false );

    /*! 싸이월드 */
    config->setGroup( "Config_Cyworld" );

    /*! 미니홈피 공개 설정 초기화 */
    stConfig.minihompypublic = 1;
    gonggaeComboBox->setCurrentItem( stConfig.minihompypublic );
    config->writeEntry( "MiniHompy_Public", stConfig.minihompypublic );

    /*! 싸이월드 알림 설정 */
    stConfig.alarmminihompy = 0;
    alarmMinimiComboBox->setCurrentItem( stConfig.alarmminihompy );
    config->writeEntry( "MiniHompy_Alarm", stConfig.alarmminihompy );

    /*! 싸이월드 새글 등록 알림 */
    stConfig.alarmminihompynew = true;
    alarmNewCheckBox->setChecked( stConfig.alarmminihompynew );
    config->writeEntry( "Use_MiniHompy_New", stConfig.alarmminihompynew );

    /*! 싸이월드 쪽지, 일촌신청, 새 선물, 댓글 등록 알려주기 */
    stConfig.alarmetc = true;
    alarmMemoEtcCheckBox->setChecked( stConfig.alarmetc );
    config->writeEntry( "Use_MiniHompy_ETC", stConfig.alarmetc );

    /*! 싸이월드 연동 유무 */
    stConfig.usecyworld = false;
    // config->writeEntry( "Use_Cyworld", stConfig.usecyworld );

    /*! 싸이월드 ID */
    stConfig.cyid = QString::null;
    config->writeEntry( "Cyworld_ID", stConfig.cyid);
    config->sync();
    KMessageBox::information (this, QString::fromUtf8("싸이월드 연동이 해제되었습니다."), UTF8("환경설정") );
}

void PreferenceView::setOffline() {
    Preference::setOffline();
    WStackPage_2->setEnabled( FALSE );
    // WStackPage_3->setEnabled( FALSE );
    WStackPage_4->setEnabled( FALSE );
    WStackPage_5->setEnabled( FALSE );
    WStackPage_9->setEnabled( FALSE );
    saveFileLineEdit->setEnabled( FALSE );
    saveFileButton->setEnabled( FALSE );
}

void PreferenceView::setOnline() {
    Preference::setOnline();
    WStackPage_2->setEnabled( TRUE );
    // WStackPage_3->setEnabled( TRUE );
    WStackPage_4->setEnabled( TRUE );
    WStackPage_5->setEnabled( TRUE );
    WStackPage_9->setEnabled( TRUE );
    saveFileLineEdit->setEnabled( TRUE );
    saveFileButton->setEnabled( TRUE );
}

void PreferenceView::slotFindIDPassword() {
    QString sURL("http://br.nate.com/index.php");
    sURL += "?code=A020";
    LNMUtils::openURL( sURL );
}

void PreferenceView::slotNateDotComGaIb() {
    QString sURL("http://br.nate.com/index.php");
    sURL += "?code=A021";
    LNMUtils::openURL( sURL );
}

void PreferenceView::slotWhatMiniHompy() {
    LNMUtils::openURL( "http://cyworld.com/main2/preview_minihp.asp" );
}

#include "preferenceview.moc"
