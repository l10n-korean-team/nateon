/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "addfriendinterface.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qheader.h>
#include <qlistview.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

static const unsigned char image0_data[] = {
    0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d,
    0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x10,
    0x08, 0x06, 0x00, 0x00, 0x00, 0x1f, 0xf3, 0xff, 0x61, 0x00, 0x00, 0x03,
    0x46, 0x49, 0x44, 0x41, 0x54, 0x38, 0x8d, 0x85, 0x93, 0xff, 0x6b, 0x94,
    0x75, 0x00, 0xc7, 0x5f, 0xf7, 0x3c, 0xcf, 0xdd, 0xcd, 0x7b, 0x6e, 0x9d,
    0xe7, 0x7d, 0xdb, 0x8d, 0xcd, 0x6d, 0xe7, 0xdc, 0xdc, 0xe6, 0x35, 0xd7,
    0x34, 0x0a, 0xc3, 0x4d, 0xa4, 0x44, 0x8d, 0x48, 0xc2, 0x88, 0x92, 0xa0,
    0x41, 0x7e, 0x01, 0x09, 0x6c, 0x0a, 0x92, 0x62, 0x6b, 0x14, 0x8b, 0xa6,
    0x46, 0xf4, 0x65, 0x54, 0x3f, 0xc4, 0x4a, 0x4a, 0x42, 0xc2, 0xe9, 0x74,
    0x4d, 0x9a, 0xc6, 0xd2, 0xdc, 0x5a, 0xad, 0x95, 0x73, 0x67, 0x18, 0xd5,
    0x6e, 0x6d, 0xed, 0x5b, 0x76, 0x9e, 0xdb, 0xdd, 0xed, 0xf9, 0xf2, 0x79,
    0xfa, 0xe1, 0xea, 0x97, 0x0a, 0x7a, 0xfd, 0x01, 0x2f, 0x78, 0xc3, 0xfb,
    0x65, 0x7b, 0xaa, 0xe1, 0x0d, 0x00, 0x74, 0x3d, 0x63, 0x25, 0x13, 0x71,
    0x52, 0xe9, 0x04, 0xb9, 0xae, 0x25, 0xe4, 0x7a, 0x97, 0x22, 0xcb, 0x0e,
    0x1b, 0xff, 0x83, 0x02, 0xb0, 0x73, 0xc7, 0x6a, 0xeb, 0x70, 0x73, 0x0b,
    0x25, 0xeb, 0x37, 0x61, 0xf9, 0xf2, 0x39, 0xd3, 0x37, 0x48, 0xf9, 0x54,
    0x3f, 0xfb, 0x0e, 0x34, 0x59, 0x77, 0xe4, 0x47, 0x98, 0xd3, 0x4c, 0xe6,
    0x17, 0x04, 0x29, 0x4d, 0x90, 0xd6, 0x4d, 0x16, 0x74, 0x93, 0x05, 0x43,
    0x10, 0x7b, 0xff, 0x63, 0x9b, 0x92, 0xc9, 0x24, 0xad, 0xc3, 0xcd, 0x2d,
    0x34, 0xbd, 0xfa, 0x1a, 0xa5, 0xc5, 0x85, 0x74, 0x8f, 0x4c, 0x90, 0x8e,
    0xde, 0xcf, 0xe9, 0x4b, 0x97, 0x69, 0x7e, 0x6e, 0x2f, 0x45, 0xe1, 0x30,
    0x86, 0x61, 0xa2, 0x4b, 0x4e, 0xf2, 0xcb, 0x6b, 0xf0, 0x47, 0x2a, 0xf1,
    0x17, 0x57, 0x61, 0x98, 0x02, 0x00, 0xb9, 0x30, 0x50, 0xd0, 0x54, 0xb3,
    0xb9, 0x9e, 0x2b, 0x1d, 0x27, 0xf1, 0xa8, 0x2a, 0xdd, 0xed, 0x6d, 0xec,
    0x7a, 0x70, 0x23, 0xd7, 0x9c, 0x79, 0x7c, 0x3b, 0x34, 0xc8, 0x93, 0xae,
    0x61, 0xd6, 0x56, 0x04, 0xf1, 0xa5, 0x6e, 0xe0, 0x9a, 0xf9, 0x9e, 0x2b,
    0x5d, 0xa7, 0xf8, 0x69, 0x7c, 0x9a, 0x50, 0xd9, 0x9d, 0x24, 0x63, 0x3f,
    0xbe, 0xa0, 0xa8, 0x8b, 0x2c, 0x2c, 0xc9, 0x01, 0xf7, 0x6e, 0xa1, 0xb6,
    0xae, 0x9e, 0xda, 0xba, 0x7a, 0x62, 0xe3, 0x73, 0x08, 0x4b, 0x60, 0xc9,
    0x76, 0x1e, 0xde, 0x7b, 0x8c, 0xc2, 0xf2, 0x3c, 0xec, 0xe9, 0x6b, 0x30,
    0x71, 0x99, 0x1d, 0x83, 0x57, 0x79, 0xf1, 0xf8, 0x59, 0xce, 0xb7, 0x4d,
    0xb2, 0x22, 0x50, 0x6b, 0x29, 0x2e, 0x5f, 0x25, 0x7d, 0x5d, 0x9d, 0x14,
    0xef, 0x3b, 0x4a, 0xeb, 0x0f, 0x1a, 0xc5, 0x0e, 0xb8, 0x9e, 0x56, 0x18,
    0x18, 0xb8, 0xc4, 0x3a, 0x69, 0x86, 0xfc, 0x22, 0x0f, 0x76, 0x69, 0x0e,
    0xdd, 0x90, 0x30, 0x17, 0x97, 0x12, 0x58, 0x67, 0xe7, 0xa8, 0x27, 0x87,
    0x86, 0xd6, 0x7e, 0x46, 0xd3, 0x32, 0x92, 0xac, 0xb8, 0x6d, 0xd1, 0x68,
    0x35, 0xbf, 0x1c, 0x69, 0x64, 0xa8, 0xfb, 0x13, 0xbe, 0xfa, 0x75, 0x8a,
    0x9e, 0x8e, 0x93, 0xdc, 0xd3, 0xdf, 0xce, 0x5b, 0xcf, 0xef, 0x42, 0xb2,
    0x3b, 0x39, 0x78, 0xa8, 0x8d, 0xd1, 0x49, 0x1b, 0x76, 0x35, 0xc2, 0x58,
    0x62, 0x31, 0x46, 0x61, 0x98, 0x3d, 0x1b, 0xf3, 0xb8, 0x39, 0x39, 0x82,
    0x04, 0xf0, 0xf8, 0xee, 0x46, 0xa2, 0xd1, 0x6a, 0xe6, 0x3f, 0x3c, 0xc2,
    0x58, 0xeb, 0x6e, 0x32, 0x1f, 0xbc, 0xcc, 0xbb, 0xcd, 0xfb, 0x89, 0x54,
    0xad, 0x61, 0x6c, 0x6c, 0x96, 0x73, 0x9f, 0x7e, 0x89, 0xa5, 0x2c, 0x41,
    0x56, 0x43, 0x34, 0x1f, 0xeb, 0xe5, 0xf3, 0xe1, 0x04, 0x15, 0x15, 0x7e,
    0x54, 0x29, 0x9d, 0x15, 0x68, 0x36, 0x07, 0x0f, 0x3d, 0xbd, 0x1f, 0xaf,
    0xcf, 0xc7, 0x7b, 0x8f, 0x04, 0x78, 0xe7, 0x60, 0x2d, 0x9e, 0xa0, 0x17,
    0xcb, 0x4c, 0xa3, 0x1b, 0x06, 0xdb, 0xb7, 0x6f, 0x23, 0xb2, 0xac, 0x84,
    0x89, 0xf8, 0x34, 0x23, 0xb1, 0x38, 0x21, 0x7f, 0x2e, 0x58, 0x20, 0x61,
    0x64, 0x05, 0x73, 0x0b, 0x26, 0x29, 0x5d, 0x60, 0xb9, 0x03, 0xdc, 0x1e,
    0x1f, 0x65, 0x43, 0x75, 0x0e, 0x5e, 0x25, 0x86, 0xd0, 0x33, 0x94, 0x16,
    0x87, 0x78, 0xf6, 0x99, 0x27, 0x10, 0xe9, 0x29, 0x64, 0x91, 0xe4, 0x50,
    0xe3, 0x06, 0x56, 0x2e, 0x5d, 0xc4, 0xec, 0x58, 0x02, 0x0d, 0x57, 0xf6,
    0x48, 0x73, 0x9a, 0xc0, 0x14, 0x16, 0xc1, 0xb2, 0x55, 0x74, 0x5f, 0x1d,
    0xa6, 0xaa, 0xec, 0x37, 0x34, 0xb9, 0x0b, 0x29, 0xf8, 0x3b, 0xd8, 0x83,
    0x08, 0x21, 0x30, 0x8d, 0x79, 0xbc, 0x6a, 0x82, 0x4d, 0xf7, 0x79, 0x60,
    0x64, 0x98, 0xf3, 0x5f, 0xcf, 0xe0, 0x0d, 0x47, 0xb3, 0x82, 0x94, 0x66,
    0x60, 0x9a, 0x16, 0xd5, 0x0f, 0x3c, 0x4a, 0xc7, 0x77, 0xfd, 0x94, 0x7c,
    0x31, 0xcd, 0x56, 0xf7, 0x1f, 0x68, 0x99, 0x5e, 0x0c, 0xd5, 0x83, 0xe4,
    0x70, 0x81, 0x30, 0x20, 0x93, 0x84, 0xc9, 0x59, 0x7a, 0x3f, 0x8b, 0xf3,
    0xf6, 0x90, 0xce, 0xca, 0xd5, 0x77, 0x21, 0xd7, 0xd4, 0x6c, 0x26, 0xb8,
    0xaa, 0xaa, 0x49, 0x33, 0x05, 0x42, 0xb2, 0x93, 0x57, 0x79, 0x37, 0x1f,
    0xf5, 0x7c, 0xc3, 0xec, 0xcf, 0x93, 0x94, 0xb9, 0x3c, 0xa8, 0xba, 0x85,
    0x2d, 0x71, 0x9b, 0x9c, 0x44, 0x8a, 0x5b, 0x37, 0x6e, 0xd1, 0xde, 0x19,
    0xe7, 0xa5, 0xde, 0x9b, 0x04, 0xd7, 0x6e, 0xc3, 0x2d, 0xfe, 0x9a, 0x90,
    0xd1, 0x05, 0xa6, 0x29, 0x30, 0x84, 0x40, 0x71, 0x7b, 0xa9, 0xdb, 0xf3,
    0x0a, 0x3d, 0xa7, 0xdb, 0x39, 0xf7, 0xfa, 0x09, 0x76, 0x6e, 0x5d, 0x0f,
    0x92, 0xc6, 0xd9, 0x81, 0x38, 0xd7, 0xc7, 0x13, 0xe4, 0x04, 0x0a, 0x58,
    0xd1, 0x70, 0x00, 0x57, 0xa8, 0x04, 0x71, 0xf1, 0xa2, 0x4d, 0xf9, 0xaf,
    0xc2, 0x24, 0xc5, 0x4e, 0xa8, 0xa8, 0x94, 0x0b, 0x9d, 0xf3, 0xb4, 0x9c,
    0xb8, 0x80, 0x2b, 0x90, 0x8f, 0x2b, 0x6f, 0x19, 0xcb, 0x1f, 0xdb, 0x42,
    0x8e, 0xbf, 0x00, 0xd9, 0xa9, 0x82, 0x2d, 0x1b, 0xaa, 0xed, 0xef, 0x9c,
    0xff, 0xc9, 0x50, 0xdf, 0x9b, 0x16, 0x80, 0x3f, 0xbc, 0x06, 0x5f, 0x60,
    0x39, 0x0e, 0xa7, 0x1b, 0x59, 0xb6, 0xff, 0x2b, 0xef, 0x3f, 0x01, 0x4b,
    0xd0, 0x53, 0x12, 0x37, 0x98, 0xa3, 0x76, 0x00, 0x00, 0x00, 0x00, 0x49,
    0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};


/*
 *  Constructs a AddFriendForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
AddFriendForm::AddFriendForm( QWidget* parent, const char* name, bool modal, WFlags fl )
        : QDialog( parent, name, modal, fl ) {
    QImage img;
    img.loadFromData( image0_data, sizeof( image0_data ), "PNG" );
    image0 = img;
    if ( !name )
        setName( "AddFriendForm" );
    setIcon( image0 );
    AddFriendFormLayout = new QVBoxLayout( this, 11, 6, "AddFriendFormLayout");

    tabWidget = new QTabWidget( this, "tabWidget" );

    tab1 = new QWidget( tabWidget, "tab1" );
    tab1Layout = new QVBoxLayout( tab1, 11, 6, "tab1Layout" );

    layout14 = new QVBoxLayout( 0, 0, 6, "layout14");

    textLabel1 = new QLabel( tab1, "textLabel1" );
    layout14->addWidget( textLabel1 );

    layout13 = new QHBoxLayout( 0, 0, 6, "layout13");

    layout12 = new QVBoxLayout( 0, 0, 6, "layout12");

    textLabel2 = new QLabel( tab1, "textLabel2" );
    layout12->addWidget( textLabel2 );

    layout5 = new QVBoxLayout( 0, 0, 6, "layout5");

    textLabel4 = new QLabel( tab1, "textLabel4" );
    layout5->addWidget( textLabel4 );
    spacer1 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout5->addItem( spacer1 );
    layout12->addLayout( layout5 );
    layout13->addLayout( layout12 );

    layout11 = new QVBoxLayout( 0, 0, 6, "layout11");

    layout10 = new QHBoxLayout( 0, 0, 6, "layout10");

    lineeditID = new QLineEdit( tab1, "lineeditID" );
    layout10->addWidget( lineeditID );

    textLabel3 = new QLabel( tab1, "textLabel3" );
    layout10->addWidget( textLabel3 );

    comboBox1 = new QComboBox( FALSE, tab1, "comboBox1" );
    comboBox1->setEditable( TRUE );
    comboBox1->setAutoCompletion( TRUE );
    layout10->addWidget( comboBox1 );
    layout11->addLayout( layout10 );

    texteditMessage = new QTextEdit( tab1, "texteditMessage" );
    texteditMessage->setWordWrap( QTextEdit::WidgetWidth );
    layout11->addWidget( texteditMessage );
    layout13->addLayout( layout11 );
    layout14->addLayout( layout13 );

    layout7 = new QHBoxLayout( 0, 0, 6, "layout7");
    spacer2 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout7->addItem( spacer2 );

    buttonRequire = new QPushButton( tab1, "buttonRequire" );
    layout7->addWidget( buttonRequire );

    buttonCancel = new QPushButton( tab1, "buttonCancel" );
    layout7->addWidget( buttonCancel );
    spacer3 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout7->addItem( spacer3 );
    layout14->addLayout( layout7 );
    tab1Layout->addLayout( layout14 );
    tabWidget->insertTab( tab1, QString::fromLatin1("") );

    tab2 = new QWidget( tabWidget, "tab2" );
    tab2Layout = new QVBoxLayout( tab2, 11, 6, "tab2Layout"); 

    layout16 = new QVBoxLayout( 0, 0, 6, "layout16"); 

    textLabel5 = new QLabel( tab2, "textLabel5" );
    layout16->addWidget( textLabel5 );

    layout17 = new QHBoxLayout( 0, 0, 6, "layout17"); 

    textLabel10 = new QLabel( tab2, "textLabel10" );
    textLabel10->setMinimumSize( QSize( 75, 0 ) );
    textLabel10->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );
    layout17->addWidget( textLabel10 );

    comboBoxNo1 = new QComboBox( FALSE, tab2, "comboBoxNo1" );
    layout17->addWidget( comboBoxNo1 );

    textLabel6 = new QLabel( tab2, "textLabel6" );
    layout17->addWidget( textLabel6 );

    lineEditNo2 = new QLineEdit( tab2, "lineEditNo2" );
    lineEditNo2->setMinimumSize( QSize( 48, 0 ) );
    lineEditNo2->setMaxLength( 4 );
    layout17->addWidget( lineEditNo2 );

    textLabel7 = new QLabel( tab2, "textLabel7" );
    layout17->addWidget( textLabel7 );

    lineEditNo3 = new QLineEdit( tab2, "lineEditNo3" );
    lineEditNo3->setMinimumSize( QSize( 48, 0 ) );
    lineEditNo3->setMaxLength( 4 );
    layout17->addWidget( lineEditNo3 );

    textLabel8 = new QLabel( tab2, "textLabel8" );
    layout17->addWidget( textLabel8 );

    lineEditName1 = new QLineEdit( tab2, "lineEditName1" );
    lineEditName1->setMinimumSize( QSize( 70, 0 ) );
    layout17->addWidget( lineEditName1 );

    pushButtonSearch1 = new QPushButton( tab2, "pushButtonSearch1" );
    layout17->addWidget( pushButtonSearch1 );
    layout16->addLayout( layout17 );

    layout18 = new QHBoxLayout( 0, 0, 6, "layout18"); 

    textLabel11 = new QLabel( tab2, "textLabel11" );
    textLabel11->setMinimumSize( QSize( 75, 0 ) );
    textLabel11->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );
    layout18->addWidget( textLabel11 );

    lineEditName2 = new QLineEdit( tab2, "lineEditName2" );
    lineEditName2->setMinimumSize( QSize( 70, 0 ) );
    layout18->addWidget( lineEditName2 );

    textLabel12 = new QLabel( tab2, "textLabel12" );
    layout18->addWidget( textLabel12 );

    comboBoxGender = new QComboBox( FALSE, tab2, "comboBoxGender" );
    layout18->addWidget( comboBoxGender );

    textLabel13 = new QLabel( tab2, "textLabel13" );
    layout18->addWidget( textLabel13 );

    lineEditAgeFrom = new QLineEdit( tab2, "lineEditAgeFrom" );
    layout18->addWidget( lineEditAgeFrom );

    textLabel14 = new QLabel( tab2, "textLabel14" );
    layout18->addWidget( textLabel14 );

    lineEditAgeTo = new QLineEdit( tab2, "lineEditAgeTo" );
    layout18->addWidget( lineEditAgeTo );

    pushButtonSearch2 = new QPushButton( tab2, "pushButtonSearch2" );
    layout18->addWidget( pushButtonSearch2 );
    layout16->addLayout( layout18 );

    listViewResult = new QListView( tab2, "listViewResult" );
    listViewResult->addColumn( tr( "Name" ) );
    listViewResult->addColumn( tr( "E-Mail" ) );
    listViewResult->addColumn( tr( "Mobile" ) );
    listViewResult->addColumn( tr( "Gender" ) );
    layout16->addWidget( listViewResult );

    layout19 = new QHBoxLayout( 0, 0, 6, "layout19"); 

    layout21 = new QVBoxLayout( 0, 0, 6, "layout21"); 

    textLabel9 = new QLabel( tab2, "textLabel9" );
    layout21->addWidget( textLabel9 );
    spacer6 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout21->addItem( spacer6 );
    layout19->addLayout( layout21 );

    textEditMessage = new QTextEdit( tab2, "textEditMessage" );
    textEditMessage->setWordWrap( QTextEdit::WidgetWidth );
    layout19->addWidget( textEditMessage );
    layout16->addLayout( layout19 );

    layout20 = new QHBoxLayout( 0, 0, 6, "layout20"); 
    spacer4 = new QSpacerItem( 120, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout20->addItem( spacer4 );

    pushButtonRequire = new QPushButton( tab2, "pushButtonRequire" );
    layout20->addWidget( pushButtonRequire );

    pushButtonCancel = new QPushButton( tab2, "pushButtonCancel" );
    layout20->addWidget( pushButtonCancel );
    spacer5 = new QSpacerItem( 120, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout20->addItem( spacer5 );
    layout16->addLayout( layout20 );
    tab2Layout->addLayout( layout16 );
    tabWidget->insertTab( tab2, QString::fromLatin1("") );
    AddFriendFormLayout->addWidget( tabWidget );
    languageChange();
    resize( QSize(533, 330).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( comboBox1, SIGNAL( activated ( int ) ), SLOT( slotHostSelect( int ) ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
AddFriendForm::~AddFriendForm() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void AddFriendForm::languageChange() {
    setCaption( UTF8( "친구추가" ) );
    textLabel1->setText( UTF8( "네이트온 친구로 추가를 원하는 친구의 아이디와 \n친구 요청 메시지를 입력하시고 [친구요청]버튼을 누르세요. \n(기본으로 \"기타\"에 추가 됩니다.)" ) );
    textLabel2->setText( UTF8( "<p align=\"right\">아이디 :</p>" ) );
    textLabel4->setText( UTF8( "<p align=\"right\">요청메시지 :</p>" ) );
    textLabel3->setText( tr( "@" ) );
    comboBox1->clear();
    comboBox1->insertItem( tr( "nate.com" ) );
    comboBox1->insertItem( tr( "cyworld.com" ) );
    comboBox1->insertItem( tr( "empal.com" ) );
    comboBox1->insertItem( tr( "lycos.co.kr" ) );
    comboBox1->insertItem( tr( "netsgo.com" ) );
    comboBox1->insertItem( tr( "hanmail.net" ) );
    comboBox1->insertItem( tr( "naver.com" ) );
    comboBox1->insertItem( tr( "yahoo.co.kr" ) );
    comboBox1->insertItem( tr( "paran.com" ) );
    comboBox1->insertItem( tr( "hotmail.com" ) );
    comboBox1->insertItem( tr( "dreamwiz.com" ) );
    comboBox1->insertItem( UTF8( "직접입력" ) );
    /*
    myList.append("nate.com");
    myList.append("cyworld.com");
    myList.append("empal.com");
    myList.append("lycos.co.kr");
    myList.append("netsgo.com");
    myList.append("hanmail.net");
    myList.append("naver.com");
    myList.append("yahoo.co.kr");
    myList.append("paran.com");
    myList.append("hotmail.com");
    myList.append("dreamwiz.com");
    */
    texteditMessage->setText( tr( "hello add me~!" ) );
    buttonRequire->setText( UTF8( "친구요청" ) );
    buttonCancel->setText( UTF8( "취소" ) );
    tabWidget->changeTab( tab1, UTF8( "친구 추가" ) );
    textLabel5->setText( UTF8( "친구 정보를 입력한 후 친구를 찾아보세요." ) );
    textLabel10->setText( UTF8( "핸드폰번호 :" ) );
    comboBoxNo1->clear();
    comboBoxNo1->insertItem( tr( "010" ) );
    comboBoxNo1->insertItem( tr( "011" ) );
    comboBoxNo1->insertItem( tr( "016" ) );
    comboBoxNo1->insertItem( tr( "017" ) );
    comboBoxNo1->insertItem( tr( "018" ) );
    comboBoxNo1->insertItem( tr( "019" ) );
    textLabel6->setText( tr( "-" ) );
    textLabel7->setText( tr( "-" ) );
    textLabel8->setText( UTF8( "이름" ) );
    pushButtonSearch1->setText( UTF8( "검색" ) );
    textLabel11->setText( UTF8( "이름 :" ) );
    textLabel12->setText( UTF8( "성별" ) );
    comboBoxGender->clear();
    comboBoxGender->insertItem( UTF8( "남" ) );
    comboBoxGender->insertItem( UTF8( "여" ) );
    textLabel13->setText( UTF8( "나이" ) );
    textLabel14->setText( tr( "~" ) );
    pushButtonSearch2->setText( UTF8( "검색" ) );
    listViewResult->header()->setLabel( 0, UTF8( "이름" ) );
    listViewResult->header()->setLabel( 1, UTF8( "이메일" ) );
    listViewResult->header()->setLabel( 2, UTF8( "휴대폰" ) );
    listViewResult->header()->setLabel( 3, UTF8( "성별" ) );
    textLabel9->setText( UTF8( "요청메시지 :" ) );
    textEditMessage->setText( tr( "hello add me~!" ) );
    pushButtonRequire->setText( UTF8( "친구요청" ) );
    pushButtonCancel->setText( UTF8( "취소" ) );
    tabWidget->changeTab( tab2, UTF8( "친구 검색" ) );
}

void AddFriendForm::slotHostSelect(int nID) {
    if ( nID == 9 )
        comboBox1->setCurrentText( QString::null );
}

#include "addfriendinterface.moc"

