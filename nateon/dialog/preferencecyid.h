/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PREFERENCECYID_H
#define PREFERENCECYID_H

#include <kapp.h>
#include <kconfig.h>
#include <kmessagebox.h>

#include <qwidgetstack.h>
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qdir.h>
#include <qfiledialog.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qlistbox.h>
#include <qvariant.h>
#include <qpushbutton.h>
#include <qlistbox.h>
#include <qwidgetstack.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qframe.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "preferenceinterface.h"
#include "utils.h"

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class PreferenceCyID : public Preference {
    Q_OBJECT
public:
    PreferenceCyID(QWidget *parent = 0, const char *name = 0);

    ~PreferenceCyID();

    virtual void showSyncSetup();
    virtual void show();
    virtual void initialize();
    virtual void accept();

    // QWidget* WStackPage_10;
    QLabel* NateSyncPixmapLabel;
    QLabel* NateSync_TextLabel1;
    QFrame* NateSync_Line1;
    QLabel* NateSync_TextLabel2;
    QFrame* NateSync_frame15;
    QLabel* NateSync_TextLabel3;
    QLineEdit* NateSync_NateIDLineEdit;
    QLabel* textLabel2_8;
    QComboBox* NateSync_ComboBox1;
    QPushButton* NateSync_SyncButton;
    QLabel* NateSync_PasswordTextLabel;
    QLineEdit* NateSync_NatePasswordLineEdit;
    QPushButton* NateSync_findIDPasswordButton;
    QPushButton* NateSync_gaibButton;

protected:
    QVBoxLayout* WStackPageLayout_10;
    QSpacerItem* NateSync_spacer101;
    QHBoxLayout* NateSync_Layout1;
    QVBoxLayout* NateSync_frame15Layout;
    QHBoxLayout* NateSync_layout170;
    QSpacerItem* NateSync_spacer9_4;
    QHBoxLayout* NateSync_layout171;
    QSpacerItem* NateSync_spacer8_4;
    QHBoxLayout* NateSync_layout172;
    QSpacerItem* NateSync_spacer7_4;

private slots:
    /*! 네이트 연동 */
    void slotNateSync();
    void slotFindIDPassword();
    void slotNateDotComGaIb();
    void slotCySyncAuthError( bool );
    void slotCyCanceled();

signals:
    void nateSync( const QString&, const QString& );
    void nateSyncCancel();
};

#endif
