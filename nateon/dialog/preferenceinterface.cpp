/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "preferenceinterface.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlistbox.h>
#include <qwidgetstack.h>
#include <qwidget.h>
#include <qlabel.h>
#include <qframe.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qcombobox.h>
#include <qtextedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include <kfiledialog.h>
#include "../util/common.h"

extern nmconfig stConfig;

/*
 *  Constructs a Preference as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
Preference::Preference( QWidget* parent, const char* name, bool modal, WFlags fl)
        : QDialog( parent, name, modal),
        config(0),
        pSoundSelect(0),
        pCurrentAccount(0),
        pEmoticonSelector(0),
        bOnline( TRUE ),
        sSelectPrivacyID( QString::null ),
        nSelectPrivacyType(0) {
    Q_UNUSED( fl );

    config = kapp->config();

    KStandardDirs   *dirs   = KGlobal::dirs();
    sPicsPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" );
    // KIconLoader *loader = KGlobal::iconLoader();

    if ( !name )
        setName( "Preference" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)5, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setMinimumSize( QSize( 600, 460 ) );
    setMaximumSize( QSize( 600, 460 ) );
    setIcon( QPixmap( sPicsPath + "hi16-app-knateon.png") );
    setSizeGripEnabled( FALSE );
    PreferenceLayout = new QVBoxLayout( this, 11, 6, "PreferenceLayout");

    layout75 = new QHBoxLayout( 0, 0, 6, "layout75");

    listBox = new QListBox( this, "listBox" );
    listBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)7, 0, 0, listBox->sizePolicy().hasHeightForWidth() ) );
    layout75->addWidget( listBox );

    widgetStack2 = new QWidgetStack( this, "widgetStack2" );
    widgetStack2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, widgetStack2->sizePolicy().hasHeightForWidth() ) );

    WStackPage = new QWidget( widgetStack2, "WStackPage" );
    WStackPageLayout = new QVBoxLayout( WStackPage, 0, 6, "WStackPageLayout");

    pixmapLabel1 = new QLabel( WStackPage, "pixmapLabel1" );
    pixmapLabel1->setPixmap( QPixmap( sPicsPath + "sett_titl_01.bmp") );
    pixmapLabel1->setScaledContents( TRUE );
    WStackPageLayout->addWidget( pixmapLabel1 );

    layout37 = new QHBoxLayout( 0, 0, 6, "layout37");

    textLabel1 = new QLabel( WStackPage, "textLabel1" );
    textLabel1->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1->sizePolicy().hasHeightForWidth() ) );
    layout37->addWidget( textLabel1 );

    line1 = new QFrame( WStackPage, "line1" );
    line1->setFrameShape( QFrame::HLine );
    line1->setFrameShadow( QFrame::Sunken );
    line1->setFrameShape( QFrame::HLine );
    layout37->addWidget( line1 );
    WStackPageLayout->addLayout( layout37 );

    layout39 = new QHBoxLayout( 0, 0, 6, "layout39");

    layout4 = new QVBoxLayout( 0, 0, 6, "layout4");

    autoRunCheckBox = new QCheckBox( WStackPage, "autoRunCheckBox" );
    autoRunCheckBox->setEnabled( FALSE );
    layout4->addWidget( autoRunCheckBox );

    hideCheckBox = new QCheckBox( WStackPage, "hideCheckBox" );
    layout4->addWidget( hideCheckBox );

    topCheckBox = new QCheckBox( WStackPage, "topCheckBox" );
    layout4->addWidget( topCheckBox );

    upgradeCheckBox = new QCheckBox( WStackPage, "upgradeCheckBox" );
    upgradeCheckBox->setChecked( TRUE );
    layout4->addWidget( upgradeCheckBox );

    emoticonCheckBox = new QCheckBox( WStackPage, "emoticonCheckBox" );
    emoticonCheckBox->setChecked( TRUE );
    layout4->addWidget( emoticonCheckBox );

    layout76 = new QHBoxLayout( 0, 0, 6, "layout76"); 

    textLabelDoubleClickAction = new QLabel( WStackPage, "textLabelDoubleClickAction" );
    layout76->addWidget( textLabelDoubleClickAction );

    buddyDoubleClickComboBox = new QComboBox( FALSE, WStackPage, "buddyDoubleClickComboBox" );
    layout76->addWidget( buddyDoubleClickComboBox );
    layout4->addLayout( layout76 );
    layout39->addLayout( layout4 );
    spacer10 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout39->addItem( spacer10 );
    WStackPageLayout->addLayout( layout39 );

    layout38 = new QHBoxLayout( 0, 0, 6, "layout38");

    textLabel1_2 = new QLabel( WStackPage, "textLabel1_2" );
    textLabel1_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1_2->sizePolicy().hasHeightForWidth() ) );
    layout38->addWidget( textLabel1_2 );

    line2 = new QFrame( WStackPage, "line2" );
    line2->setFrameShape( QFrame::HLine );
    line2->setFrameShadow( QFrame::Sunken );
    line2->setFrameShape( QFrame::HLine );
    layout38->addWidget( line2 );
    WStackPageLayout->addLayout( layout38 );

    layout40 = new QHBoxLayout( 0, 0, 6, "layout40");

    textLabel2 = new QLabel( WStackPage, "textLabel2" );
    layout40->addWidget( textLabel2 );
    spacer11 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout40->addItem( spacer11 );
    WStackPageLayout->addLayout( layout40 );

    layout5 = new QHBoxLayout( 0, 0, 6, "layout5");

    saveFileLineEdit = new QLineEdit( WStackPage, "saveFileLineEdit" );
    layout5->addWidget( saveFileLineEdit );

    saveFileButton = new QPushButton( WStackPage, "saveFileButton" );
//    saveFileButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
//    saveFileButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    connect( saveFileButton, SIGNAL( clicked() ), this, SLOT( slotOpenSaveFile() ) );
    layout5->addWidget( saveFileButton );
    WStackPageLayout->addLayout( layout5 );

    layout400 = new QHBoxLayout( 0, 0, 6, "layout400");
    textLabel100 = new QLabel( WStackPage, "textLabel100" );
    textLabel100->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel100->sizePolicy().hasHeightForWidth() ) );
    textLabel100->setText( UTF8("웹 브라우져") );
    layout400->addWidget( textLabel100 );

    line100 = new QFrame( WStackPage, "line100" );
    line100->setFrameShape( QFrame::HLine );
    line100->setFrameShadow( QFrame::Sunken );
    line100->setFrameShape( QFrame::HLine );
    layout400->addWidget( line100 );
    WStackPageLayout->addLayout( layout400 );

    groupBox100 = new QGroupBox( WStackPage, "groupBox100" );
    groupBox100->setColumnLayout(0, Qt::Vertical );
    groupBox100->layout()->setSpacing( 6 );
    groupBox100->layout()->setMargin( 11 );
    dwgroupBox2Layout = new QVBoxLayout( groupBox100->layout() );
    dwgroupBox2Layout->setAlignment( Qt::AlignTop );
    // groupBox100->setGeometry( QRect( 0, 260, 460, 50 ) );
    groupBox100->setCheckable( TRUE );
    groupBox100->setChecked( FALSE );
    groupBox100->setTitle( UTF8( "사용자 웹 브라우저 설정 사용" ) );
    connect( groupBox100, SIGNAL( toggled ( bool ) ), SLOT( slotUseDefaultWebBrowser( bool ) ) );

    layout100 = new QHBoxLayout( 0, 0, 6, "layout9");
    lineEdit100 = new QLineEdit( groupBox100, "lineEdit1" );
    // lineEdit100->setGeometry( QRect( 10, 20, 400, 25 ) );
    layout100->addWidget( lineEdit100 );
    pushButton100 = new QPushButton( groupBox100, "pushButton1" );
    // pushButton100->setGeometry( QRect( 415, 20, 40, 25 ) );
    pushButton100->setText( UTF8("변경") );
    layout100->addWidget( pushButton100 );
    dwgroupBox2Layout->addLayout( layout100 );
    labelBrowser = new QLabel( groupBox100, "labelBrowser" );
    labelBrowser->setText( UTF8("예) 새창으로 띄우기 : /usr/bin/firefox -new-window") );
    dwgroupBox2Layout->addWidget( labelBrowser );
    WStackPageLayout->addWidget( groupBox100 );

    connect( pushButton100, SIGNAL( clicked() ), SLOT( slotOpenDefaultBrowserSelector() ) );

    // languageChange();
    // resize( QSize(600, 480).expandedTo(minimumSizeHint()) );
    // clearWState( WState_Polished );

    spacer12 = new QSpacerItem( 20, 95, QSizePolicy::Minimum, QSizePolicy::Expanding );
    WStackPageLayout->addItem( spacer12 );
    widgetStack2->addWidget( WStackPage, 0 );

    WStackPage_2 = new QWidget( widgetStack2, "WStackPage_2" );
    WStackPageLayout_2 = new QVBoxLayout( WStackPage_2, 0, 6, "WStackPageLayout_2");

    pixmapLabel2 = new QLabel( WStackPage_2, "pixmapLabel2" );
    pixmapLabel2->setPixmap( QPixmap( sPicsPath + "sett_titl_02.bmp") );
    pixmapLabel2->setScaledContents( TRUE );
    WStackPageLayout_2->addWidget( pixmapLabel2 );

    layout42 = new QHBoxLayout( 0, 0, 6, "layout42");

    textLabel1_3 = new QLabel( WStackPage_2, "textLabel1_3" );
    textLabel1_3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1_3->sizePolicy().hasHeightForWidth() ) );
    layout42->addWidget( textLabel1_3 );

    line3 = new QFrame( WStackPage_2, "line3" );
    line3->setFrameShape( QFrame::HLine );
    line3->setFrameShadow( QFrame::Sunken );
    line3->setFrameShape( QFrame::HLine );
    layout42->addWidget( line3 );
    WStackPageLayout_2->addLayout( layout42 );

    layout7 = new QHBoxLayout( 0, 0, 6, "layout7");

    textLabel2_2 = new QLabel( WStackPage_2, "textLabel2_2" );
    layout7->addWidget( textLabel2_2 );
    spacer2 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout7->addItem( spacer2 );

    nickEmoticonButton = new ShapeButton( WStackPage_2, sPicsPath + "chw_emoticon.bmp" );
    nickEmoticonButton->setMouseOverShape( sPicsPath + "chw_emoticon_ov.bmp" );
    nickEmoticonButton->setPressedShape( sPicsPath + "chw_emoticon_down.bmp" );
    nickEmoticonButton->setMinimumSize( QSize( 30, 20 ) );
    nickEmoticonButton->setMaximumSize( QSize( 30, 20 ) );

    layout7->addWidget( nickEmoticonButton );
    WStackPageLayout_2->addLayout( layout7 );

    nickLineEdit = new QLineEdit( WStackPage_2, "nickLineEdit" );
    WStackPageLayout_2->addWidget( nickLineEdit );

    layout43 = new QHBoxLayout( 0, 0, 6, "layout43");

    textLabel3 = new QLabel( WStackPage_2, "textLabel3" );
    textLabel3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel3->sizePolicy().hasHeightForWidth() ) );
    layout43->addWidget( textLabel3 );

    line4 = new QFrame( WStackPage_2, "line4" );
    line4->setFrameShape( QFrame::HLine );
    line4->setFrameShadow( QFrame::Sunken );
    line4->setFrameShape( QFrame::HLine );
    layout43->addWidget( line4 );
    WStackPageLayout_2->addLayout( layout43 );

    layout47 = new QHBoxLayout( 0, 0, 6, "layout47");

    timeAwayCheckBox = new QCheckBox( WStackPage_2, "timeAwayCheckBox" );
    timeAwayCheckBox->setChecked( TRUE );
    layout47->addWidget( timeAwayCheckBox );

    lineEdit10 = new QLineEdit( WStackPage_2, "lineEdit10" );
    lineEdit10->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, lineEdit10->sizePolicy().hasHeightForWidth() ) );
    lineEdit10->setMinimumSize( QSize( 44, 22 ) );
    lineEdit10->setMaximumSize( QSize( 44, 22 ) );
    lineEdit10->setMaxLength ( 3 );
    connect( lineEdit10, SIGNAL(textChanged ( const QString & ) ), SLOT( slotCheckNum(  const QString & ) ) );
    layout47->addWidget( lineEdit10 );

    textLabel1_6 = new QLabel( WStackPage_2, "textLabel1_6" );
    layout47->addWidget( textLabel1_6 );
    spacer15 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout47->addItem( spacer15 );
    WStackPageLayout_2->addLayout( layout47 );

    layout44 = new QHBoxLayout( 0, 0, 6, "layout44");

    textLabel4 = new QLabel( WStackPage_2, "textLabel4" );
    textLabel4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel4->sizePolicy().hasHeightForWidth() ) );
    layout44->addWidget( textLabel4 );

    line5 = new QFrame( WStackPage_2, "line5" );
    line5->setFrameShape( QFrame::HLine );
    line5->setFrameShadow( QFrame::Sunken );
    line5->setFrameShape( QFrame::HLine );
    layout44->addWidget( line5 );
    WStackPageLayout_2->addLayout( layout44 );

    textLabel5 = new QLabel( WStackPage_2, "textLabel5" );
    textLabel5->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_2->addWidget( textLabel5 );

    layout45 = new QHBoxLayout( 0, 0, 6, "layout45");
    spacer13 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout45->addItem( spacer13 );

    profileButton = new QPushButton( WStackPage_2, "profileButton" );
    profileButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    profileButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout45->addWidget( profileButton );
    WStackPageLayout_2->addLayout( layout45 );
    spacer14 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    WStackPageLayout_2->addItem( spacer14 );
    widgetStack2->addWidget( WStackPage_2, 1 );

    WStackPage_3 = new QWidget( widgetStack2, "WStackPage_3" );
    WStackPageLayout_3 = new QVBoxLayout( WStackPage_3, 0, 6, "WStackPageLayout_3");

    pixmapLabel1_3 = new QLabel( WStackPage_3, "pixmapLabel1_3" );
    pixmapLabel1_3->setPixmap( QPixmap( sPicsPath + "sett_titl_03.bmp") );
    pixmapLabel1_3->setScaledContents( TRUE );
    WStackPageLayout_3->addWidget( pixmapLabel1_3 );

    layout48 = new QHBoxLayout( 0, 0, 6, "layout48");

    textLabel1_4 = new QLabel( WStackPage_3, "textLabel1_4" );
    textLabel1_4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1_4->sizePolicy().hasHeightForWidth() ) );
    layout48->addWidget( textLabel1_4 );

    line6 = new QFrame( WStackPage_3, "line6" );
    line6->setFrameShape( QFrame::HLine );
    line6->setFrameShadow( QFrame::Sunken );
    line6->setFrameShape( QFrame::HLine );
    layout48->addWidget( line6 );
    WStackPageLayout_3->addLayout( layout48 );

    dwgroupBox1 = new QGroupBox( WStackPage_3, "dwgroupBox1" );
    dwgroupBox1->setColumnLayout(0, Qt::Vertical );
    dwgroupBox1->layout()->setSpacing( 6 );
    dwgroupBox1->layout()->setMargin( 11 );
    dwgroupBox1Layout = new QVBoxLayout( dwgroupBox1->layout() );
    dwgroupBox1Layout->setAlignment( Qt::AlignTop );

    layout9 = new QVBoxLayout( 0, 0, 6, "layout9");

    alarmConnectCheckBox = new QCheckBox( dwgroupBox1, "alarmConnectCheckBox" );
    alarmConnectCheckBox->setChecked( TRUE );
    layout9->addWidget( alarmConnectCheckBox );

    alarmChatCheckBox = new QCheckBox( dwgroupBox1, "alarmChatCheckBox" );
    alarmChatCheckBox->setChecked( TRUE );
    layout9->addWidget( alarmChatCheckBox );
    dwgroupBox1Layout->addLayout( layout9 );
    WStackPageLayout_3->addWidget( dwgroupBox1 );

    groupBox2 = new QGroupBox( WStackPage_3, "groupBox2" );
    groupBox2->setColumnLayout(0, Qt::Vertical );
    groupBox2->layout()->setSpacing( 6 );
    groupBox2->layout()->setMargin( 11 );
    groupBox2Layout = new QHBoxLayout( groupBox2->layout() );
    groupBox2Layout->setAlignment( Qt::AlignTop );

    layout23 = new QVBoxLayout( 0, 0, 6, "layout23");

    alarmMailCheckBox = new QCheckBox( groupBox2, "alarmMailCheckBox" );
    alarmMailCheckBox->setChecked( TRUE );
    layout23->addWidget( alarmMailCheckBox );

    layout22 = new QHBoxLayout( 0, 0, 6, "layout22");

    alarmMemoCheckBox = new QCheckBox( groupBox2, "alarmMemoCheckBox" );
    alarmMemoCheckBox->setChecked( TRUE );
    layout22->addWidget( alarmMemoCheckBox );

    alarmMemoComboBox = new QComboBox( FALSE, groupBox2, "alarmMemoComboBox" );
    alarmMemoComboBox->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    alarmMemoComboBox->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    connect( alarmMemoCheckBox, SIGNAL( toggled( bool ) ), alarmMemoComboBox, SLOT( setEnabled ( bool ) ) );

    layout22->addWidget( alarmMemoComboBox );
    layout23->addLayout( layout22 );
    groupBox2Layout->addLayout( layout23 );
    spacer31 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    groupBox2Layout->addItem( spacer31 );
    WStackPageLayout_3->addWidget( groupBox2 );

    layout49 = new QHBoxLayout( 0, 0, 6, "layout49");

    textLabel2_3 = new QLabel( WStackPage_3, "textLabel2_3" );
    textLabel2_3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel2_3->sizePolicy().hasHeightForWidth() ) );
    layout49->addWidget( textLabel2_3 );

    line7 = new QFrame( WStackPage_3, "line7" );
    line7->setFrameShape( QFrame::HLine );
    line7->setFrameShadow( QFrame::Sunken );
    line7->setFrameShape( QFrame::HLine );
    layout49->addWidget( line7 );
    WStackPageLayout_3->addLayout( layout49 );

    layout50 = new QHBoxLayout( 0, 0, 6, "layout50");

    useAlarmCheckBox = new QCheckBox( WStackPage_3, "useAlarmCheckBox" );
    useAlarmCheckBox->setChecked( TRUE );
    layout50->addWidget( useAlarmCheckBox );
    spacer16 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout50->addItem( spacer16 );

    extendOptionButton = new QPushButton( WStackPage_3, "extendOptionButton" );
    extendOptionButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    extendOptionButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    connect( useAlarmCheckBox, SIGNAL( toggled( bool ) ), extendOptionButton, SLOT( setEnabled( bool ) ));
    connect( extendOptionButton, SIGNAL( clicked() ), SLOT( slotShowSoundSelectDialog() ) );
    connect( useAlarmCheckBox, SIGNAL( toggled( bool ) ), SLOT( slotUseAllSound( bool ) ) );

    layout50->addWidget( extendOptionButton );
    WStackPageLayout_3->addLayout( layout50 );
    spacer17 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    WStackPageLayout_3->addItem( spacer17 );
    widgetStack2->addWidget( WStackPage_3, 2 );

    WStackPage_4 = new QWidget( widgetStack2, "WStackPage_4" );
    WStackPageLayout_4 = new QVBoxLayout( WStackPage_4, 0, 6, "WStackPageLayout_4");

    pixmapLabel2_2 = new QLabel( WStackPage_4, "pixmapLabel2_2" );
    pixmapLabel2_2->setPixmap( QPixmap( sPicsPath + "sett_titl_04.bmp") );
    pixmapLabel2_2->setScaledContents( TRUE );
    WStackPageLayout_4->addWidget( pixmapLabel2_2 );

    layout51 = new QHBoxLayout( 0, 0, 6, "layout51");

    textLabel3_2 = new QLabel( WStackPage_4, "textLabel3_2" );
    textLabel3_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel3_2->sizePolicy().hasHeightForWidth() ) );
    layout51->addWidget( textLabel3_2 );

    line8 = new QFrame( WStackPage_4, "line8" );
    line8->setFrameShape( QFrame::HLine );
    line8->setFrameShadow( QFrame::Sunken );
    line8->setFrameShape( QFrame::HLine );
    layout51->addWidget( line8 );
    WStackPageLayout_4->addLayout( layout51 );

    groupBox3 = new QGroupBox( WStackPage_4, "groupBox3" );
    groupBox3->setColumnLayout(0, Qt::Vertical );
    groupBox3->layout()->setSpacing( 6 );
    groupBox3->layout()->setMargin( 11 );
    groupBox3Layout = new QVBoxLayout( groupBox3->layout() );
    groupBox3Layout->setAlignment( Qt::AlignTop );

    layout72 = new QHBoxLayout( 0, 0, 6, "layout72");

    viewAwayMessageCheckBox = new QCheckBox( groupBox3, "viewAwayMessageCheckBox" );
    layout72->addWidget( viewAwayMessageCheckBox );
    spacer33 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout72->addItem( spacer33 );
    groupBox3Layout->addLayout( layout72 );

    connect( viewAwayMessageCheckBox, SIGNAL( toggled ( bool ) ), SLOT( slotEditEnable( bool ) ) );

    layout71 = new QHBoxLayout( 0, 0, 6, "layout71");

    viewBusyMessageCheckBox = new QCheckBox( groupBox3, "viewBusyMessageCheckBox" );
    layout71->addWidget( viewBusyMessageCheckBox );
    spacer32 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout71->addItem( spacer32 );

    connect( viewBusyMessageCheckBox, SIGNAL( toggled ( bool ) ), SLOT( slotEditEnable( bool ) ) );

    /*! 부재중 메시지 이모티콘 */
    pixmapLabel3 = new ShapeButton( groupBox3, sPicsPath + "chw_emoticon.bmp" );
    pixmapLabel3->setMouseOverShape(sPicsPath + "chw_emoticon_ov.bmp");
    pixmapLabel3->setPressedShape(sPicsPath + "chw_emoticon_down.bmp");
    pixmapLabel3->setMinimumSize( QSize( 30, 20 ) );
    pixmapLabel3->setMaximumSize( QSize( 30, 20 ) );
    pixmapLabel3->setDisabled();

    layout71->addWidget( pixmapLabel3 );

    connect( pixmapLabel3, SIGNAL( clicked() ), SLOT( slotEmoticonDialog() ) );

    /*! 부재중 메시지 폰트 */
    pixmapLabel4 = new ShapeButton( groupBox3, sPicsPath + "chw_font.bmp" );
    pixmapLabel4->setMouseOverShape(sPicsPath + "chw_font_ov.bmp");
    pixmapLabel4->setPressedShape(sPicsPath + "chw_font_down.bmp");
    pixmapLabel4->setMinimumSize( QSize( 30, 20 ) );
    pixmapLabel4->setMaximumSize( QSize( 30, 20 ) );
    pixmapLabel4->setDisabled();
    layout71->addWidget( pixmapLabel4 );
    connect( pixmapLabel4, SIGNAL( clicked() ), SLOT( slotFontDialog() ) );

    groupBox3Layout->addLayout( layout71 );

    textEdit1 = new QTextEdit( groupBox3, "textEdit1" );
    textEdit1->setMaximumSize( QSize( 32767, 40 ) );
    textEdit1->setEnabled( FALSE );
    textEdit1->setPaletteBackgroundColor("#C0C0C0");

    groupBox3Layout->addWidget( textEdit1 );
    WStackPageLayout_4->addWidget( groupBox3 );

    groupBox4 = new QGroupBox( WStackPage_4, "groupBox4" );
    groupBox4->setColumnLayout(0, Qt::Vertical );
    groupBox4->layout()->setSpacing( 6 );
    groupBox4->layout()->setMargin( 11 );
    groupBox4Layout = new QVBoxLayout( groupBox4->layout() );
    groupBox4Layout->setAlignment( Qt::AlignTop );

    layout73 = new QHBoxLayout( 0, 0, 6, "layout73");

    chatSaveFileComboBox = new QComboBox( FALSE, groupBox4, "chatSaveFileComboBox" );
    chatSaveFileComboBox->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    chatSaveFileComboBox->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout73->addWidget( chatSaveFileComboBox );
    spacer35 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout73->addItem( spacer35 );
    groupBox4Layout->addLayout( layout73 );

    textLabel5_2 = new QLabel( groupBox4, "textLabel5_2" );
    textLabel5_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    groupBox4Layout->addWidget( textLabel5_2 );
    WStackPageLayout_4->addWidget( groupBox4 );

    groupBox5 = new QGroupBox( WStackPage_4, "groupBox5" );
    groupBox5->setColumnLayout(0, Qt::Vertical );
    groupBox5->layout()->setSpacing( 6 );
    groupBox5->layout()->setMargin( 11 );
    groupBox5Layout = new QHBoxLayout( groupBox5->layout() );
    groupBox5Layout->setAlignment( Qt::AlignTop );

    useEmoticonCheckBox = new QCheckBox( groupBox5, "useEmoticonCheckBox" );
    useEmoticonCheckBox->setChecked( TRUE );
    useEmoticonCheckBox->setEnabled( FALSE );
    groupBox5Layout->addWidget( useEmoticonCheckBox );
    spacer34 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    groupBox5Layout->addItem( spacer34 );
    WStackPageLayout_4->addWidget( groupBox5 );

    layout52 = new QHBoxLayout( 0, 0, 6, "layout52");

    textLabel6 = new QLabel( WStackPage_4, "textLabel6" );
    textLabel6->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel6->sizePolicy().hasHeightForWidth() ) );
    layout52->addWidget( textLabel6 );

    line9 = new QFrame( WStackPage_4, "line9" );
    line9->setFrameShape( QFrame::HLine );
    line9->setFrameShadow( QFrame::Sunken );
    line9->setFrameShape( QFrame::HLine );
    layout52->addWidget( line9 );
    WStackPageLayout_4->addLayout( layout52 );

    groupBox6 = new QGroupBox( WStackPage_4, "groupBox6" );
    groupBox6->setColumnLayout(0, Qt::Vertical );
    groupBox6->layout()->setSpacing( 6 );
    groupBox6->layout()->setMargin( 11 );
    groupBox6Layout = new QVBoxLayout( groupBox6->layout() );
    groupBox6Layout->setAlignment( Qt::AlignTop );

    layout74 = new QHBoxLayout( 0, 0, 6, "layout74");

    replayCheckBox = new QCheckBox( groupBox6, "replayCheckBox" );
    layout74->addWidget( replayCheckBox );
    spacer36 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout74->addItem( spacer36 );

    allReplyCheckBox = new QCheckBox( groupBox6, "allReplyCheckBox" );
    allReplyCheckBox->setChecked( TRUE );
    layout74->addWidget( allReplyCheckBox );
    spacer37 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout74->addItem( spacer37 );

    forwardCheckBox = new QCheckBox( groupBox6, "forwardCheckBox" );
    forwardCheckBox->setChecked( TRUE );
    layout74->addWidget( forwardCheckBox );
    spacer38 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout74->addItem( spacer38 );
    groupBox6Layout->addLayout( layout74 );
    WStackPageLayout_4->addWidget( groupBox6 );
    widgetStack2->addWidget( WStackPage_4, 3 );

    WStackPage_5 = new QWidget( widgetStack2, "WStackPage_5" );
    WStackPageLayout_5 = new QVBoxLayout( WStackPage_5, 0, 6, "WStackPageLayout_5");

    pixmapLabel1_4 = new QLabel( WStackPage_5, "pixmapLabel1_4" );
    pixmapLabel1_4->setPixmap( QPixmap( sPicsPath + "sett_titl_05.bmp") );
    pixmapLabel1_4->setScaledContents( TRUE );
    WStackPageLayout_5->addWidget( pixmapLabel1_4 );

    layout53 = new QHBoxLayout( 0, 0, 6, "layout53");

    textLabel1_5 = new QLabel( WStackPage_5, "textLabel1_5" );
    textLabel1_5->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel1_5->sizePolicy().hasHeightForWidth() ) );
    layout53->addWidget( textLabel1_5 );

    line10 = new QFrame( WStackPage_5, "line10" );
    line10->setFrameShape( QFrame::HLine );
    line10->setFrameShadow( QFrame::Sunken );
    line10->setFrameShape( QFrame::HLine );
    layout53->addWidget( line10 );
    WStackPageLayout_5->addLayout( layout53 );

    textLabel2_4 = new QLabel( WStackPage_5, "textLabel2_4" );
    textLabel2_4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, textLabel2_4->sizePolicy().hasHeightForWidth() ) );
    textLabel2_4->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_5->addWidget( textLabel2_4 );

    frame3 = new QFrame( WStackPage_5, "frame3" );
    frame3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)7, 0, 0, frame3->sizePolicy().hasHeightForWidth() ) );
    frame3->setFrameShape( QFrame::StyledPanel );
    frame3->setFrameShadow( QFrame::Raised );
    frame3Layout = new QVBoxLayout( frame3, 11, 6, "frame3Layout");

    layout59 = new QVBoxLayout( 0, 0, 6, "layout59");

    allowUserCheckBox = new QCheckBox( frame3, "allowUserCheckBox" );
    allowUserCheckBox->setChecked( TRUE );
    connect( allowUserCheckBox, SIGNAL( toggled( bool ) ), SLOT( lockOther( bool ) ) );
    layout59->addWidget( allowUserCheckBox );

    layout58 = new QHBoxLayout( 0, 0, 6, "layout58");

    textLabel1_7 = new QLabel( frame3, "textLabel1_7" );
    layout58->addWidget( textLabel1_7 );
    spacer24 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout58->addItem( spacer24 );

    textLabel2_5 = new QLabel( frame3, "textLabel2_5" );
    layout58->addWidget( textLabel2_5 );
    layout59->addLayout( layout58 );

    layout57 = new QHBoxLayout( 0, 0, 6, "layout57");

    allowUserListBox = new QListBox( frame3, "allowUserListBox" );
    connect( allowUserListBox, SIGNAL( rightButtonClicked ( QListBoxItem *, const QPoint & ) ), SLOT( slotAllowRightClicked( QListBoxItem *, const QPoint & ) ) );
    connect( allowUserListBox, SIGNAL(  highlighted( int ) ), SLOT( slotAllowListBoxClicked( int ) ) );
    layout57->addWidget( allowUserListBox );

    layout56 = new QVBoxLayout( 0, 0, 6, "layout56");
    spacer22 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout56->addItem( spacer22 );

    layout13 = new QVBoxLayout( 0, 0, 6, "layout13");

    lockButton = new QPushButton( frame3, "lockButton" );
    lockButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    lockButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout13->addWidget( lockButton );

    unlockButton = new QPushButton( frame3, "unlockButton" );
    unlockButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    unlockButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout13->addWidget( unlockButton );
    layout56->addLayout( layout13 );
    spacer23 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout56->addItem( spacer23 );
    layout57->addLayout( layout56 );

    lockedUserListBox = new QListBox( frame3, "lockedUserListBox" );
    connect( lockedUserListBox, SIGNAL( rightButtonClicked ( QListBoxItem *, const QPoint & ) ), SLOT( slotLockedRightClicked( QListBoxItem *, const QPoint & ) ) );
    connect( lockedUserListBox, SIGNAL(  highlighted( int ) ), SLOT( slotLockedListBoxClicked( int ) ) );
    layout57->addWidget( lockedUserListBox );
    layout59->addLayout( layout57 );
    frame3Layout->addLayout( layout59 );
    WStackPageLayout_5->addWidget( frame3 );

    layout54 = new QHBoxLayout( 0, 0, 6, "layout54");

    receiveFromFriendCheckBox = new QCheckBox( WStackPage_5, "receiveFromFriendCheckBox" );
    receiveFromFriendCheckBox->setChecked( TRUE );
    layout54->addWidget( receiveFromFriendCheckBox );
    spacer19 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout54->addItem( spacer19 );
    WStackPageLayout_5->addLayout( layout54 );

    layout55 = new QHBoxLayout( 0, 0, 6, "layout55");

    profileViewButton = new QPushButton( WStackPage_5, "profileViewButton" );
    profileViewButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    profileViewButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    // connect( profileViewButton, SIGNAL( clicked() ), SLOT(  ) );

    layout55->addWidget( profileViewButton );
    spacer20 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout55->addItem( spacer20 );
    WStackPageLayout_5->addLayout( layout55 );
    widgetStack2->addWidget( WStackPage_5, 4 );

    WStackPage_6 = new QWidget( widgetStack2, "WStackPage_6" );
    WStackPageLayout_6 = new QVBoxLayout( WStackPage_6, 0, 6, "WStackPageLayout_6");

    pixmapLabel2_3 = new QLabel( WStackPage_6, "pixmapLabel2_3" );
    pixmapLabel2_3->setPixmap( QPixmap( sPicsPath + "sett_titl_06.bmp") );
    pixmapLabel2_3->setScaledContents( TRUE );
    WStackPageLayout_6->addWidget( pixmapLabel2_3 );

    layout62 = new QHBoxLayout( 0, 0, 6, "layout62");

    textLabel4_2 = new QLabel( WStackPage_6, "textLabel4_2" );
    textLabel4_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel4_2->sizePolicy().hasHeightForWidth() ) );
    layout62->addWidget( textLabel4_2 );

    line11 = new QFrame( WStackPage_6, "line11" );
    line11->setFrameShape( QFrame::HLine );
    line11->setFrameShadow( QFrame::Sunken );
    line11->setFrameShape( QFrame::HLine );
    layout62->addWidget( line11 );
    WStackPageLayout_6->addLayout( layout62 );

    textLabel5_3 = new QLabel( WStackPage_6, "textLabel5_3" );
    textLabel5_3->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_6->addWidget( textLabel5_3 );

    encryptChatCheckBox = new QCheckBox( WStackPage_6, "encryptChatCheckBox" );
    encryptChatCheckBox->setChecked( TRUE );
    WStackPageLayout_6->addWidget( encryptChatCheckBox );
    spacer26 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    WStackPageLayout_6->addItem( spacer26 );
    widgetStack2->addWidget( WStackPage_6, 5 );

    WStackPage_7 = new QWidget( widgetStack2, "WStackPage_7" );
    WStackPageLayout_7 = new QVBoxLayout( WStackPage_7, 0, 6, "WStackPageLayout_7");

    pixmapLabel3_2 = new QLabel( WStackPage_7, "pixmapLabel3_2" );
    pixmapLabel3_2->setPixmap( QPixmap( sPicsPath + "sett_titl_07.bmp") );
    pixmapLabel3_2->setScaledContents( TRUE );
    WStackPageLayout_7->addWidget( pixmapLabel3_2 );

    textLabel6_2 = new QLabel( WStackPage_7, "textLabel6_2" );
    textLabel6_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_7->addWidget( textLabel6_2 );

    layout63 = new QHBoxLayout( 0, 0, 6, "layout63");

    textLabel7 = new QLabel( WStackPage_7, "textLabel7" );
    textLabel7->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel7->sizePolicy().hasHeightForWidth() ) );
    layout63->addWidget( textLabel7 );

    line12 = new QFrame( WStackPage_7, "line12" );
    line12->setFrameShape( QFrame::HLine );
    line12->setFrameShadow( QFrame::Sunken );
    line12->setFrameShape( QFrame::HLine );
    layout63->addWidget( line12 );
    WStackPageLayout_7->addLayout( layout63 );

    frame4 = new QFrame( WStackPage_7, "frame4" );
    frame4->setFrameShape( QFrame::StyledPanel );
    frame4->setFrameShadow( QFrame::Raised );
    frame4Layout = new QVBoxLayout( frame4, 11, 6, "frame4Layout");

    useProxyCheckBox = new QCheckBox( frame4, "useProxyCheckBox" );
    connect( useProxyCheckBox, SIGNAL( toggled( bool ) ), SLOT( slotUseProxy( bool ) ) );
    // useProxyCheckBox->setEnabled( FALSE );
    frame4Layout->addWidget( useProxyCheckBox );

    layout25 = new QVBoxLayout( 0, 0, 6, "layout25");

    layout24 = new QHBoxLayout( 0, 0, 6, "layout24");

    textLabel8 = new QLabel( frame4, "textLabel8" );
    layout24->addWidget( textLabel8 );

    typeComboBox = new QComboBox( FALSE, frame4, "typeComboBox" );
    typeComboBox->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    typeComboBox->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    connect( typeComboBox, SIGNAL( activated ( int ) ), SLOT( slotProxyType( int ) ) );

    // typeComboBox->setEnabled( FALSE );
    layout24->addWidget( typeComboBox );
    spacer3 = new QSpacerItem( 296, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout24->addItem( spacer3 );
    layout25->addLayout( layout24 );

    layout18 = new QHBoxLayout( 0, 0, 6, "layout18");

    textLabel9 = new QLabel( frame4, "textLabel9" );
    layout18->addWidget( textLabel9 );

    serverLineEdit = new QLineEdit( frame4, "serverLineEdit" );
    // serverLineEdit->setEnabled( FALSE );
    layout18->addWidget( serverLineEdit );

    textLabel11 = new QLabel( frame4, "textLabel11" );
    layout18->addWidget( textLabel11 );

    portLineEdit = new QLineEdit( frame4, "portLineEdit" );
    // portLineEdit->setEnabled( FALSE );
    layout18->addWidget( portLineEdit );
    layout25->addLayout( layout18 );

    layout19 = new QHBoxLayout( 0, 0, 6, "layout19");

    textLabel10 = new QLabel( frame4, "textLabel10" );
    layout19->addWidget( textLabel10 );

    userLineEdit = new QLineEdit( frame4, "userLineEdit" );
    // userLineEdit->setEnabled( FALSE );
    layout19->addWidget( userLineEdit );

    textLabel12 = new QLabel( frame4, "textLabel12" );
    layout19->addWidget( textLabel12 );

    passwordLineEdit = new QLineEdit( frame4, "passwordLineEdit" );
    // passwordLineEdit->setEnabled( FALSE );
    layout19->addWidget( passwordLineEdit );
    layout25->addLayout( layout19 );
    frame4Layout->addLayout( layout25 );
    WStackPageLayout_7->addWidget( frame4 );

    layout64 = new QHBoxLayout( 0, 0, 6, "layout64");

    textLabel13 = new QLabel( WStackPage_7, "textLabel13" );
    textLabel13->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel13->sizePolicy().hasHeightForWidth() ) );
    layout64->addWidget( textLabel13 );

    line13 = new QFrame( WStackPage_7, "line13" );
    line13->setFrameShape( QFrame::HLine );
    line13->setFrameShadow( QFrame::Sunken );
    line13->setFrameShape( QFrame::HLine );
    layout64->addWidget( line13 );
    WStackPageLayout_7->addLayout( layout64 );

    layout65 = new QHBoxLayout( 0, 0, 6, "layout65");

    textLabel14 = new QLabel( WStackPage_7, "textLabel14" );
    layout65->addWidget( textLabel14 );

    p2pPortLineEdit = new QLineEdit( WStackPage_7, "p2pPortLineEdit" );
    p2pPortLineEdit->setEnabled( true );
    layout65->addWidget( p2pPortLineEdit );
    spacer27 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout65->addItem( spacer27 );
    WStackPageLayout_7->addLayout( layout65 );

    textLabel15 = new QLabel( WStackPage_7, "textLabel15" );
    textLabel15->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_7->addWidget( textLabel15 );
    spacer28 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    WStackPageLayout_7->addItem( spacer28 );
    widgetStack2->addWidget( WStackPage_7, 6 );

    WStackPage_8 = new QWidget( widgetStack2, "WStackPage_8" );
    WStackPageLayout_8 = new QVBoxLayout( WStackPage_8, 0, 6, "WStackPageLayout_8");

    pixmapLabel4_2 = new QLabel( WStackPage_8, "pixmapLabel4_2" );
    pixmapLabel4_2->setPixmap( QPixmap( sPicsPath + "sett_titl_08.bmp") );
    pixmapLabel4_2->setScaledContents( TRUE );
    WStackPageLayout_8->addWidget( pixmapLabel4_2 );

    layout69 = new QHBoxLayout( 0, 0, 6, "layout69");

    textLabel16 = new QLabel( WStackPage_8, "textLabel16" );
    textLabel16->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, textLabel16->sizePolicy().hasHeightForWidth() ) );
    layout69->addWidget( textLabel16 );

    line14 = new QFrame( WStackPage_8, "line14" );
    line14->setFrameShape( QFrame::HLine );
    line14->setFrameShadow( QFrame::Sunken );
    line14->setFrameShape( QFrame::HLine );
    layout69->addWidget( line14 );
    WStackPageLayout_8->addLayout( layout69 );

    textLabel17 = new QLabel( WStackPage_8, "textLabel17" );
    textLabel17->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, textLabel17->sizePolicy().hasHeightForWidth() ) );
    textLabel17->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter ) );
    WStackPageLayout_8->addWidget( textLabel17 );

    layout68 = new QHBoxLayout( 0, 0, 6, "layout68");

    listBox4 = new QListBox( WStackPage_8, "listBox4" );
    listBox4->setEnabled( false );
    layout68->addWidget( listBox4 );

    frame5 = new QFrame( WStackPage_8, "frame5" );
    frame5->setFrameShape( QFrame::StyledPanel );
    frame5->setFrameShadow( QFrame::Raised );
    frame5Layout = new QVBoxLayout( frame5, 11, 6, "frame5Layout");

    textLabel18 = new QLabel( frame5, "textLabel18" );
    textLabel18->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, textLabel18->sizePolicy().hasHeightForWidth() ) );
    textLabel18->setFrameShape( QLabel::StyledPanel );
    textLabel18->setFrameShadow( QLabel::Raised );
    frame5Layout->addWidget( textLabel18 );

    textLabel19 = new QLabel( frame5, "textLabel19" );
    textLabel19->setAlignment( int( QLabel::AlignTop | QLabel::AlignLeft ) );
    frame5Layout->addWidget( textLabel19 );

    startLoginCheckBox = new QCheckBox( frame5, "startLoginCheckBox" );
    startLoginCheckBox->setEnabled( false );
    frame5Layout->addWidget( startLoginCheckBox );

    layout67 = new QHBoxLayout( 0, 0, 6, "layout67");

    installButton = new QPushButton( frame5, "installButton" );
    installButton->setEnabled( false );
    installButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    installButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    layout67->addWidget( installButton );

    upgradeButton = new QPushButton( frame5, "upgradeButton" );
    upgradeButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    upgradeButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    upgradeButton->setEnabled( FALSE );
    layout67->addWidget( upgradeButton );

    removeButton = new QPushButton( frame5, "removeButton" );
    removeButton->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    removeButton->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );

    removeButton->setEnabled( FALSE );
    layout67->addWidget( removeButton );
    frame5Layout->addLayout( layout67 );
    layout68->addWidget( frame5 );
    WStackPageLayout_8->addLayout( layout68 );
    widgetStack2->addWidget( WStackPage_8, 7 );

    layout75->addWidget( widgetStack2 );
    PreferenceLayout->addLayout( layout75 );

    layout13_2 = new QHBoxLayout( 0, 0, 6, "layout13_2");
    Horizontal_Spacing2 = new QSpacerItem( 403, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout13_2->addItem( Horizontal_Spacing2 );

    buttonOk = new QPushButton( this, "buttonOk" );
    buttonOk->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    buttonOk->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    buttonOk->setAutoDefault( TRUE );
    buttonOk->setDefault( TRUE );
    layout13_2->addWidget( buttonOk );

    buttonCancel = new QPushButton( this, "buttonCancel" );
    buttonCancel->setAutoDefault( TRUE );
    buttonCancel->setPaletteForegroundColor( QColor( 83, 83, 83 ) );
    buttonCancel->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    layout13_2->addWidget( buttonCancel );

    buttonHelp = new QPushButton( this, "buttonHelp" );
    buttonHelp->setAutoDefault( TRUE );
    buttonHelp->setPaletteForegroundColor( QColor( 51, 102, 153 ) );
    buttonHelp->setPaletteBackgroundColor( QColor( 255, 255, 255 ) );
    layout13_2->addWidget( buttonHelp );

    PreferenceLayout->addLayout( layout13_2 );

    languageChange();
    resize( QSize(600, 520).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( buttonOk, SIGNAL( clicked() ),  SLOT( accept() ) );
    connect( buttonCancel, SIGNAL( clicked() ), SLOT( reject() ) );
    connect( buttonHelp, SIGNAL( clicked() ),  SLOT( help() ) );
    connect( listBox, SIGNAL( highlighted(int) ), widgetStack2, SLOT( raiseWidget(int) ) );
    connect( lockButton, SIGNAL( clicked() ), SLOT( slotAddLock() ) );
    connect( unlockButton, SIGNAL( clicked() ), SLOT( slotAddUnlock() ) );
    connect( nickEmoticonButton, SIGNAL( clicked() ), SLOT( slotEmoticonDialog() ) );

    pPrivacyUnlockedRightClick = new QPopupMenu( this );
    pPrivacyUnlockedRightClick->insertItem( UTF8("프로필 보기"),  this, SLOT( slotShowProfile() ), 0, 1);
    pPrivacyUnlockedRightClick->insertItem( UTF8("친구 목록에 표시"),  this, SLOT( slotAddEtcList() ), 0, 2);
    pPrivacyUnlockedRightClick->insertItem( UTF8("차단된 친구 목록으로 이동"),  this, SLOT( slotAddLock() ), 0, 3);
    pPrivacyUnlockedRightClick->insertItem( UTF8("삭제"),  this, SLOT( slotDelete() ), 0, 4);

    pPrivacyLockedRightClick = new QPopupMenu( this );
    pPrivacyLockedRightClick->insertItem( UTF8("프로필 보기"),  this, SLOT( slotShowProfile() ), 0, 1);
    pPrivacyLockedRightClick->insertItem( UTF8("친구 목록에 표시"),  this, SLOT( slotAddEtcList() ), 0, 2);
    pPrivacyLockedRightClick->insertItem( UTF8("허용된 친구 목록으로 이동"),  this, SLOT( slotAddUnlock() ), 0, 3);
    pPrivacyLockedRightClick->insertItem( UTF8("삭제"),  this, SLOT( slotDelete() ), 0, 4);
}

/*
 *  Destroys the object and frees any allocated resources
 */
Preference::~Preference() {
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void Preference::languageChange() {
    /*!
     * 대화 암호화 기능 없음
     */
    encryptChatCheckBox->setChecked( false );
    encryptChatCheckBox->setEnabled(false);
    /*!
     * 자동 업그레이드 기능 없음
     */
    upgradeCheckBox->setChecked( false );
    upgradeCheckBox->setEnabled( false );
    /*!
     * 메일체크 기능 없음
     */
    alarmMailCheckBox->setChecked( false );
    alarmMailCheckBox->setEnabled( false );


    setCaption( trUtf8( "\xed\x99\x98\xea\xb2\xbd\xec\x84\xa4\xec\xa0\x95" ) );
    listBox->clear();
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_01.bmp"), trUtf8( "\xea\xb8\xb0\xeb\xb3\xb8" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_02.bmp"), trUtf8( "\xea\xb0\x9c\xec\x9d\xb8" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_03.bmp"), trUtf8( "\xec\x95\x8c\xeb\xa6\xbc\x2f\xec\x86\x8c\xeb\xa6\xac" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_04.bmp"), trUtf8( "\xeb\xa9\x94\xec\x8b\x9c\xec\xa7\x80" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_05.bmp"), trUtf8( "\xed\x94\x84\xeb\x9d\xbc\xec\x9d\xb4\xeb\xb2\x84\xec\x8b\x9c" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_06.bmp"), trUtf8( "\xeb\xa9\x94\xec\x8b\xa0\xec\xa0\x80\x20\xeb\xb3\xb4\xec\x95\x88" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_07.bmp"), trUtf8( "\xec\x97\xb0\xea\xb2\xb0\x28\xeb\xb0\xa9\xed\x99\x94\xeb\xb2\xbd\x29" ) );
    listBox->insertItem( QPixmap(sPicsPath + "sett_icon_08.bmp"), trUtf8( "\xed\x94\x8c\xeb\x9f\xac\xea\xb7\xb8\xec\x9d\xb8" ) );
    listBox->setCurrentItem( 0 );
    textLabel1->setText( trUtf8( "\xec\x9d\xbc\xeb\xb0\x98" ) );
    autoRunCheckBox->setText( trUtf8( "\x4b\x44\x45\x20\xec\x8b\xa4\xed\x96\x89\x20\xec\x8b\x9c\x20\xec\x9e\x90\xeb\x8f\x99"
                                      "\xec\x8b\xa4\xed\x96\x89" ) );
    hideCheckBox->setText( trUtf8( "\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xec\x98\xa8\x20\xec\x8b\x9c\xec\x9e\x91\x20\xec"
                                   "\x8b\x9c\x20\xec\xb0\xbd\x20\xec\x88\xa8\xea\xb8\xb0\xea\xb8\xb0\x28\x4b\x44\x45"
                                   "\x20\xec\x8b\x9c\xec\x9e\x91\x20\xec\x8b\x9c\xeb\xa7\x8c\x20\xec\xa0\x81\xec\x9a"
                                   "\xa9\x29" ) );
    topCheckBox->setText( trUtf8( "\xed\x95\xad\xec\x83\x81\x20\xec\x9c\x84\xec\x97\x90\x20\xeb\xb3\xb4\xec\x9d\xb4\xea"
                                  "\xb8\xb0" ) );
    upgradeCheckBox->setText( trUtf8( "\xec\x83\x88\x20\xeb\xb2\x84\xec\xa0\x84\x20\xec\x9e\x90\xeb\x8f\x99\x20\xec\x97\x85"
                                      "\xea\xb7\xb8\xeb\xa0\x88\xec\x9d\xb4\xeb\x93\x9c\x20\xeb\xb0\x9b\xea\xb8\xb0" ) );
    emoticonCheckBox->setText( trUtf8( "\xec\xb9\x9c\xea\xb5\xac\xeb\xaa\xa9\xeb\xa1\x9d\xec\x97\x90\x20\xec\x9d\xb4\xeb\xaa"
                                       "\xa8\xed\x8b\xb0\xec\xbd\x98\x20\xeb\xb3\xb4\xec\x9d\xb4\xea\xb8\xb0" ) );
    textLabelDoubleClickAction->setText( trUtf8( "\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xec\x98\xa8\x20\xec\xb9\x9c\xea\xb5\xac\xeb\xa5"
                                                 "\xbc\x20\xeb\x8d\x94\xeb\xb8\x94\xed\x81\xb4\xeb\xa6\xad\xed\x95\x98\xeb\xa9\xb4"
                                                 "" ) );
    buddyDoubleClickComboBox->clear();
    buddyDoubleClickComboBox->insertItem( trUtf8( "\xeb\x8c\x80\xed\x99\x94\x20\xed\x95\x98\xea\xb8\xb0" ) );
    buddyDoubleClickComboBox->insertItem( trUtf8( "\xec\xaa\xbd\xec\xa7\x80\x20\xeb\xb3\xb4\xeb\x82\xb4\xea\xb8\xb0" ) );
    textLabel1_2->setText( trUtf8( "\xed\x8c\x8c\xec\x9d\xbc\x20\xec\xa0\x84\xec\x86\xa1" ) );
    textLabel2->setText( trUtf8( "\xeb\x8b\xa4\xec\x9d\x8c\x20\xed\x8f\xb4\xeb\x8d\x94\xec\x97\x90\x20\xeb\xb0\x9b\xec"
                                 "\x9d\x80\x20\xed\x8c\x8c\xec\x9d\xbc\x20\xec\xa0\x80\xec\x9e\xa5" ) );
    saveFileButton->setText( trUtf8( "\xeb\xb3\x80\xea\xb2\xbd" ) );
    textLabel1_3->setText( trUtf8( "\xeb\x82\xb4\x20\xeb\x8c\x80\xed\x99\x94\xeb\xaa\x85\x20\xec\x84\xa4\xec\xa0\x95" ) );
    textLabel2_2->setText( trUtf8( "\xec\xb9\x9c\xea\xb5\xac\xec\x97\x90\xea\xb2\x8c\x20\xeb\xb3\xb4\xec\x97\xac\xec\xa4"
                                   "\x84\x20\xeb\x8c\x80\xed\x99\x94\xeb\xaa\x85\xec\x9d\x84\x20\xec\x9e\x85\xeb\xa0"
                                   "\xa5\xed\x95\x98\xec\x84\xb8\xec\x9a\x94\x2e" ) );
    nickLineEdit->setText( trUtf8( "\xed\x98\x84\xec\x9e\xac\x20\xec\x82\xac\xec\x9a\xa9\xec\xa4\x91\xec\x9d\xb8\x20\xeb"
                                   "\x8c\x80\xed\x99\x94\xeb\xaa\x85" ) );
    textLabel3->setText( trUtf8( "\xec\x83\x81\xed\x83\x9c" ) );
    timeAwayCheckBox->setText( trUtf8( "\xeb\x8b\xa4\xec\x9d\x8c\x20\xec\x8b\x9c\xea\xb0\x84\xec\x9d\xb4\x20\xec\xa7\x80\xeb"
                                       "\x82\x98\xeb\xa9\xb4\x20\xec\x9e\x90\xeb\xa6\xac\x20\xeb\xb9\x84\xec\x9b\x80\xec"
                                       "\x9c\xbc\xeb\xa1\x9c\x20\xed\x91\x9c\xec\x8b\x9c" ) );
    lineEdit10->setText( tr( "10" ) );
    textLabel1_6->setText( trUtf8( "\xeb\xb6\x84" ) );
    textLabel4->setText( trUtf8( "\xea\xb0\x9c\xec\x9d\xb8\x20\xec\xa0\x95\xeb\xb3\xb4\x20\xec\x84\xa4\xec\xa0\x95" ) );
    textLabel5->setText( trUtf8( "\xea\xb0\x9c\xec\x9d\xb8\xec\xa0\x95\xeb\xb3\xb4\xec\x9d\x98\x20\xeb\xb3\x80\xea\xb2"
                                 "\xbd\xec\x9d\x84\x20\xec\x9b\x90\xed\x95\x98\xeb\xa9\xb4\x20\xec\x95\x84\xeb\x9e"
                                 "\x98\x20\x5b\xea\xb0\x9c\xec\x9d\xb8\xec\xa0\x95\xeb\xb3\xb4\xec\x84\xa4\xec\xa0"
                                 "\x95\x5d\xec\x9d\x84\x20\xed\x81\xb4\xeb\xa6\xad\xed\x95\x98\xec\x97\xac\x20\xec"
                                 "\xa0\x95\xeb\xb3\xb4\xeb\xa5\xbc\x20\xec\x88\x98\xec\xa0\x95\xed\x95\xa0\x20\xec"
                                 "\x88\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    profileButton->setText( trUtf8( "\xea\xb0\x9c\xec\x9d\xb8\x20\xec\xa0\x95\xeb\xb3\xb4\x20\xec\x84\xa4\xec\xa0\x95" ) );
    textLabel1_4->setText( trUtf8( "\xec\x95\x8c\xeb\xa6\xbc" ) );
    dwgroupBox1->setTitle( trUtf8( "\xec\xa0\x91\xec\x86\x8d\x20\xeb\xb0\x8f\x20\xeb\x8c\x80\xed\x99\x94\xec\x9a\x94\xec"
                                   "\xb2\xad" ) );
    alarmConnectCheckBox->setText( trUtf8( "\xec\xb9\x9c\xea\xb5\xac\x20\xec\xa0\x91\xec\x86\x8d\x20\xec\x8b\x9c\x20\xec\x95\x8c"
                                           "\xeb\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0" ) );
    alarmChatCheckBox->setText( trUtf8( "\xeb\x8c\x80\xed\x99\x94\x20\xec\x9a\x94\xec\xb2\xad\xec\x8b\x9c\x20\xec\x95\x8c\xeb"
                                        "\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0\x20\xed\x91\x9c\xec\x8b\x9c" ) );
    groupBox2->setTitle( trUtf8( "\xeb\xa9\x94\xec\x9d\xbc\x2c\x20\xec\xaa\xbd\xec\xa7\x80\x20\xec\x88\x98\xec\x8b\xa0"
                                 "" ) );
    alarmMailCheckBox->setText( trUtf8( "\xec\x83\x88\x20\xeb\xa9\x94\xec\x9d\xbc\x20\xec\x88\x98\xec\x8b\xa0\xec\x8b\x9c\x20"
                                        "\xec\x95\x8c\xeb\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0" ) );
    alarmMemoCheckBox->setText( trUtf8( "\xec\x83\x88\x20\xec\xaa\xbd\xec\xa7\x80\x20\xec\x88\x98\xec\x8b\xa0\x20\xec\x8b\x9c"
                                        "" ) );
    alarmMemoComboBox->clear();
    alarmMemoComboBox->insertItem( trUtf8( "\xec\xaa\xbd\xec\xa7\x80\xec\xb0\xbd\x20\xeb\xb0\x94\xeb\xa1\x9c\x20\xeb\xb3\xb4\xec"
                                           "\x97\xac\xec\xa3\xbc\xea\xb8\xb0" ) );
    alarmMemoComboBox->insertItem( trUtf8( "\xec\x95\x8c\xeb\xa0\xa4\xec\xa3\xbc\xea\xb8\xb0" ) );
    textLabel2_3->setText( trUtf8( "\xec\x86\x8c\xeb\xa6\xac" ) );
    useAlarmCheckBox->setText( trUtf8( "\xec\x95\x8c\xeb\x9e\x8c\x20\xec\x8b\x9c\x20\xec\x86\x8c\xeb\xa6\xac\x20\xec\x82\xac"
                                       "\xec\x9a\xa9" ) );
    extendOptionButton->setText( trUtf8( "\xea\xb3\xa0\xea\xb8\x89\xec\x84\xa4\xec\xa0\x95" ) );
    textLabel3_2->setText( trUtf8( "\xeb\x8c\x80\xed\x99\x94" ) );
    groupBox3->setTitle( trUtf8( "\xeb\xb6\x80\xec\x9e\xac\xec\xa4\x91\x20\xeb\xa9\x94\xec\x8b\x9c\xec\xa7\x80" ) );
    viewAwayMessageCheckBox->setText( trUtf8( "\xec\x9e\x90\xeb\xa6\xac\xeb\xb9\x84\xec\x9b\x80\x20\xec\x83\x81\xed\x83\x9c\xec\x9d"
                                      "\xbc\x20\xeb\x95\x8c\x20\xeb\xb6\x80\xec\x9e\xac\xec\xa4\x91\x20\xeb\xa9\x94\xec"
                                      "\x8b\x9c\xec\xa7\x80\x20\xeb\xb3\xb4\xec\x9d\xb4\xea\xb8\xb0" ) );
    viewBusyMessageCheckBox->setText( trUtf8( "\xeb\x8b\xa4\xeb\xa5\xb8\x20\xec\x9a\xa9\xeb\xac\xb4\x20\xec\xa4\x91\xec\x9d\xbc\xeb"
                                      "\x95\x8c\x20\xeb\xb6\x80\xec\x9e\xac\xec\xa4\x91\x20\xeb\xa9\x94\xec\x8b\x9c\xec"
                                      "\xa7\x80\x20\xeb\xb3\xb4\xec\x9d\xb4\xea\xb8\xb0" ) );
    groupBox4->setTitle( trUtf8( "\xeb\x8c\x80\xed\x99\x94\x20\xeb\x82\xb4\xec\x9a\xa9\x20\xec\xa0\x80\xec\x9e\xa5" ) );
    chatSaveFileComboBox->clear();
    chatSaveFileComboBox->insertItem( trUtf8( "\xec\xa0\x80\xec\x9e\xa5\x20\xec\x97\xac\xeb\xb6\x80\x20\xed\x99\x95\xec\x9d\xb8\xed"
                                      "\x95\x98\xea\xb8\xb0" ) );
    chatSaveFileComboBox->insertItem( trUtf8( "\xec\x9e\x90\xeb\x8f\x99\x20\xec\xa0\x80\xec\x9e\xa5\xed\x95\x98\xea\xb8\xb0" ) );
    chatSaveFileComboBox->insertItem( trUtf8( "\xec\x9e\x90\xeb\x8f\x99\x20\xec\xa0\x80\xec\x9e\xa5\xed\x95\x98\xec\xa7\x80\x20\xec"
                                      "\x95\x8a\xea\xb8\xb0" ) );
    textLabel5_2->setText( trUtf8( "\xeb\x8c\x80\xed\x99\x94\x20\xeb\x82\xb4\xec\x9a\xa9\xec\x9d\x80\x20\xeb\xa1\x9c\xec"
                                   "\xbb\xac\x20\x50\x43\xec\x97\x90\x20\xec\xa0\x80\xec\x9e\xa5\xeb\x90\x98\xeb\x8b"
                                   "\x88\x20\xea\xb3\xb5\xea\xb3\xb5\xec\x9e\xa5\xec\x86\x8c\xec\x97\x90\xec\x84\x9c"
                                   "\x20\xec\x82\xac\xec\x9a\xa9\xed\x95\xa0\x20\xeb\x95\x8c\xeb\x8a\x94\x20\xec\xa3"
                                   "\xbc\xec\x9d\x98\xed\x95\xb4\xec\xa3\xbc\xec\x84\xb8\xec\x9a\x94\x2e\x20\xec\xa0"
                                   "\x80\xec\x9e\xa5\xeb\x90\x9c\x20\xeb\x8c\x80\xed\x99\x94\x20\xeb\x82\xb4\xec\x9a"
                                   "\xa9\xec\x9d\x80\x20\xed\x86\xb5\xed\x95\xa9\xeb\xa9\x94\xec\x8b\x9c\xec\xa7\x80"
                                   "\xed\x95\xa8\x20\x3e\x20\xeb\x8c\x80\xed\x99\x94\xed\x95\xa8\xec\x97\x90\xec\x84"
                                   "\x9c\x20\xeb\xb3\xbc\x20\xec\x88\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb"
                                   "\x8b\xa4\x2e" ) );
    groupBox5->setTitle( trUtf8( "\xec\x9d\xb4\xeb\xaa\xa8\xed\x8b\xb0\xec\xbd\x98\x20\xec\x82\xac\xec\x9a\xa9" ) );
    useEmoticonCheckBox->setText( trUtf8( "\xea\xb8\xb0\xeb\xb3\xb8\x20\xec\x9d\xb4\xeb\xaa\xa8\xed\x8b\xb0\xec\xbd\x98\x20\xec"
                                          "\x82\xac\xec\x9a\xa9" ) );
    textLabel6->setText( trUtf8( "\xec\xaa\xbd\xec\xa7\x80" ) );
    groupBox6->setTitle( trUtf8( "\xec\xaa\xbd\xec\xa7\x80\x20\xec\x93\xb8\x20\xeb\x95\x8c\x20\xeb\xb0\x9b\xec\x9d\x80"
                                 "\x20\xeb\x82\xb4\xec\x9a\xa9\x20\xed\x91\x9c\xec\x8b\x9c\x20\xec\x97\xac\xeb\xb6"
                                 "\x80\x20\xec\x84\xa4\xec\xa0\x95" ) );
    replayCheckBox->setText( trUtf8( "\xeb\x8b\xb5\xec\x9e\xa5" ) );
    allReplyCheckBox->setText( trUtf8( "\xec\xa0\x84\xec\xb2\xb4\xeb\x8b\xb5\xec\x9e\xa5" ) );
    forwardCheckBox->setText( trUtf8( "\xec\xa0\x84\xeb\x8b\xac" ) );
    textLabel1_5->setText( trUtf8( "\xeb\x8c\x80\xed\x99\x94\xec\x83\x81\xeb\x8c\x80\x20\xea\xb4\x80\xeb\xa6\xac" ) );
    textLabel2_4->setText( trUtf8( "\xeb\x8c\x80\xed\x99\x94\x2f\xec\xaa\xbd\xec\xa7\x80\xea\xb0\x80\x20\xec\xb0\xa8\xeb"
                                   "\x8b\xa8\xeb\x90\x9c\x20\xec\xb9\x9c\xea\xb5\xac\x20\xeb\xaa\xa9\xeb\xa1\x9d\xec"
                                   "\x97\x90\x20\xeb\x93\xb1\xeb\xa1\x9d\xeb\x90\x9c\x20\xec\x82\xac\xeb\x9e\x8c\xeb"
                                   "\x93\xa4\xec\x9d\x80\x20\xeb\x82\x98\xec\x9d\x98\x20\xec\x98\xa8\xeb\x9d\xbc\xec"
                                   "\x9d\xb8\x20\xec\xa0\x95\xeb\xb3\xb4\xeb\xa5\xbc\x20\xec\x95\x8c\x20\xec\x88\x98"
                                   "\x20\xec\x97\x86\xec\x9c\xbc\xeb\xa9\xb0\x20\xeb\x82\x98\xec\x97\x90\xea\xb2\x8c"
                                   "\x20\xec\xaa\xbd\xec\xa7\x80\xeb\xa5\xbc\x20\xeb\xb3\xb4\xeb\x82\xb4\xea\xb1\xb0"
                                   "\xeb\x82\x98\x20\xeb\x8c\x80\xed\x99\x94\x20\xec\x9a\x94\xec\xb2\xad\xec\x9d\x84"
                                   "\x20\xed\x95\xa0\x20\xec\x88\x98\x20\xec\x97\x86\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b"
                                   "\xa4\x2e" ) );
    allowUserCheckBox->setText( trUtf8( "\xed\x97\x88\xec\x9a\xa9\xeb\x90\x9c\x20\xeb\x8c\x80\xed\x99\x94\x20\xec\x83\x81\xeb"
                                        "\x8c\x80\xec\x97\x90\xea\xb2\x8c\xeb\xa7\x8c\x20\xeb\x8c\x80\xed\x99\x94\x20\xec"
                                        "\x9a\x94\xec\xb2\xad\x20\xeb\xb0\x9b\xea\xb8\xb0" ) );
    textLabel1_7->setText( trUtf8( "\xed\x97\x88\xec\x9a\xa9\xeb\x90\x9c\x20\xec\xb9\x9c\xea\xb5\xac\x20\xeb\xaa\xa9\xeb"
                                   "\xa1\x9d" ) );
    textLabel2_5->setText( trUtf8( "\xec\xb0\xa8\xeb\x8b\xa8\xeb\x90\x9c\x20\xec\xb9\x9c\xea\xb5\xac\x20\xeb\xaa\xa9\xeb"
                                   "\xa1\x9d" ) );
    allowUserListBox->clear();
    allowUserListBox->insertItem( tr( "New Item" ) );
    lockButton->setText( trUtf8( "\xec\xb0\xa8\xeb\x8b\xa8\x3e\x3e" ) );
    unlockButton->setText( trUtf8( "\x3c\x3c\xed\x97\x88\xec\x9a\xa9" ) );
    lockedUserListBox->clear();
    lockedUserListBox->insertItem( tr( "New Item" ) );
    receiveFromFriendCheckBox->setText( trUtf8( "\xec\xb9\x9c\xea\xb5\xac\xec\x97\x90\xea\xb2\x8c\xeb\xa7\x8c\x20\xec\xaa\xbd\xec\xa7"
                                        "\x80\x20\xeb\xb0\x9b\xea\xb8\xb0" ) );
    profileViewButton->setText( trUtf8( "\xed\x94\x84\xeb\xa1\x9c\xed\x95\x84\x20\xeb\xb3\xb4\xea\xb8\xb0" ) );
    textLabel4_2->setText( trUtf8( "\xeb\x8c\x80\xed\x99\x94\x20\xec\x95\x94\xed\x98\xb8\xed\x99\x94" ) );
    textLabel5_3->setText( trUtf8( "\xec\x82\xac\xec\x83\x9d\xed\x99\x9c\x20\xeb\xb3\xb4\xed\x98\xb8\xeb\xa5\xbc\x20\xec"
                                   "\x9c\x84\xed\x95\xb4\x20\xeb\x8c\x80\xed\x99\x94\xeb\xa5\xbc\x20\xec\x95\x94\xed"
                                   "\x98\xb8\xed\x99\x94\xed\x95\x98\xec\x97\xac\x20\xec\xa0\x84\xec\x86\xa1\xed\x95"
                                   "\x98\xeb\x8a\x94\x20\xea\xb8\xb0\xeb\x8a\xa5\xec\x9e\x85\xeb\x8b\x88\xeb\x8b\xa4"
                                   "\x2e\x20\xeb\x8c\x80\xed\x99\x94\x20\xec\x95\x94\xed\x98\xb8\xed\x99\x94\xeb\xa5"
                                   "\xbc\x20\xec\x82\xac\xec\x9a\xa9\xed\x95\x98\xec\xa7\x80\x20\xec\x95\x8a\xec\x9d"
                                   "\x80\x20\xea\xb2\xbd\xec\x9a\xb0\x20\xea\xb3\xa0\xea\xb0\x9d\xeb\x8b\x98\xec\x9d"
                                   "\x98\x20\xeb\x8c\x80\xed\x99\x94\xea\xb0\x80\x20\xeb\x85\xb8\xec\xb6\x9c\xeb\x90"
                                   "\xa0\x20\xec\x88\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    encryptChatCheckBox->setText( trUtf8( "\xeb\xaa\xa8\xeb\x93\xa0\x20\xeb\x8c\x80\xed\x99\x94\x20\xeb\x82\xb4\xec\x9a\xa9\xec"
                                          "\x9d\x84\x20\xec\x95\x94\xed\x98\xb8\xed\x99\x94\xed\x95\xa9\xeb\x8b\x88\xeb\x8b"
                                          "\xa4\x2e" ) );
    textLabel6_2->setText( trUtf8( "\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xec\x98\xa8\xec\x9d\x80\x20\xea\xb8\xb0\xeb\xb3"
                                   "\xb8\xec\xa0\x81\xec\x9c\xbc\xeb\xa1\x9c\x20\xeb\x84\xa4\xed\x8a\xb8\xec\x9b\x8c"
                                   "\xed\x81\xac\x20\xed\x99\x98\xea\xb2\xbd\xec\x9d\x84\x20\xec\x9e\x90\xeb\x8f\x99"
                                   "\xec\x9c\xbc\xeb\xa1\x9c\x20\xea\xb2\x80\xec\x82\xac\xed\x95\x98\xec\x97\xac\x20"
                                   "\xec\x97\xb0\xea\xb2\xb0\xed\x95\xb4\xec\xa4\x8d\xeb\x8b\x88\xeb\x8b\xa4\x2e\x20"
                                   "\xea\xb8\xb0\xeb\xb3\xb8\x20\xec\x97\xb0\xea\xb2\xb0\x20\xec\x84\xa4\xec\xa0\x95"
                                   "\xec\x97\x90\x20\xeb\xac\xb8\xec\xa0\x9c\xea\xb0\x80\x20\xec\x9e\x88\xeb\x8a\x94"
                                   "\x20\xea\xb2\xbd\xec\x9a\xb0\x20\xec\x82\xac\xec\x9a\xa9\xec\x9e\x90\x20\xed\x99"
                                   "\x98\xea\xb2\xbd\xec\x97\x90\x20\xec\xa0\x81\xed\x95\xa9\xed\x95\x98\xeb\x8f\x84"
                                   "\xeb\xa1\x9d\x20\xec\x84\xa4\xec\xa0\x95\xed\x95\x98\xec\x8b\xad\xec\x8b\x9c\xec"
                                   "\x98\xa4\x2e" ) );
    textLabel7->setText( trUtf8( "\xeb\x84\xa4\xed\x8a\xb8\xec\x9b\x8c\xed\x81\xac\x20\xec\x97\xb0\xea\xb2\xb0\xec\x84"
                                 "\xa4\xec\xa0\x95" ) );
    useProxyCheckBox->setText( trUtf8( "\xed\x94\x84\xeb\xa1\x9d\xec\x8b\x9c\x20\xec\x84\x9c\xeb\xb2\x84\x20\xec\x82\xac\xec"
                                       "\x9a\xa9" ) );
    textLabel8->setText( trUtf8( "\xec\xa2\x85\xeb\xa5\x98" ) );
    typeComboBox->clear();
    typeComboBox->insertItem( trUtf8( "\x53\x4f\x43\x4b\x53\x20\xeb\xb2\x84\xec\xa0\x84\x20\x34" ) );
    typeComboBox->insertItem( trUtf8( "\x53\x4f\x43\x4b\x53\x20\xeb\xb2\x84\xec\xa0\x84\x20\x35" ) );
    typeComboBox->insertItem( trUtf8( "\x48\x54\x54\x50\x20\xed\x94\x84\xeb\xa1\x9d\xec\x8b\x9c" ) );
    textLabel9->setText( trUtf8( "\xec\x84\x9c\xeb\xb2\x84" ) );
    textLabel11->setText( trUtf8( "\xed\x8f\xac\xed\x8a\xb8" ) );
    textLabel10->setText( trUtf8( "\xec\x82\xac\xec\x9a\xa9\xec\x9e\x90" ) );
    textLabel12->setText( trUtf8( "\xec\x95\x94\xed\x98\xb8" ) );
    textLabel13->setText( trUtf8( "\x50\x32\x50\x20\xec\x97\xb0\xea\xb2\xbd\x20\xed\x8f\xac\xed\x8a\xb8\x20\xec\x84\xa4"
                                  "\xec\xa0\x95" ) );
    textLabel14->setText( trUtf8( "\x50\x32\x50\x20\xec\x97\xb0\xea\xb2\xb0\x20\xed\x8f\xac\xed\x8a\xb8" ) );
    p2pPortLineEdit->setText( tr( "5004" ) );
    textLabel15->setText( trUtf8( "\xeb\xb0\xa9\xed\x99\x94\xeb\xb2\xbd\xec\x9c\xbc\xeb\xa1\x9c\x20\xeb\x84\xa4\xed\x8a"
                                  "\xb8\xec\x9b\x8c\xed\x81\xac\xea\xb0\x80\x20\xeb\xa7\x89\xed\x98\x80\xec\x9e\x88"
                                  "\xeb\x8a\x94\x20\xea\xb2\xbd\xec\x9a\xb0\x20\xec\x97\xb4\xeb\xa0\xa4\xec\x9e\x88"
                                  "\xeb\x8a\x94\x20\xed\x8a\xb9\xec\xa0\x95\xed\x8f\xac\xed\x8a\xb8\xeb\xa5\xbc\x20"
                                  "\xec\x84\xa0\xed\x83\x9d\xed\x95\x98\xeb\xa9\xb4\x20\x50\x32\x50\x20\xed\x8c\x8c"
                                  "\xec\x9d\xbc\xec\xa0\x84\xec\x86\xa1\x20\xeb\x93\xb1\xec\x97\x90\x20\x50\x32\x50"
                                  "\xec\x97\xb0\xea\xb2\xb0\xec\x9d\x84\x20\xec\x82\xac\xec\x9a\xa9\xed\x95\xa0\x20"
                                  "\xec\x88\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    textLabel16->setText( trUtf8( "\xed\x94\x8c\xeb\x9f\xac\xea\xb7\xb8\xec\x9d\xb8" ) );
    textLabel17->setText( trUtf8( "\xeb\x84\xa4\xec\x9d\xb4\xed\x8a\xb8\xec\x98\xa8\x20\xed\x94\x8c\xeb\xa1\x9c\xea\xb7"
                                  "\xb8\xec\x9d\xb8\xec\x9d\x84\x20\xec\x82\xac\xec\x9a\xa9\xed\x95\x98\xec\x8b\x9c"
                                  "\xeb\xa9\xb4\x20\xea\xb8\xb0\xeb\xb3\xb8\x20\xea\xb8\xb0\xeb\x8a\xa5\x20\xec\x99"
                                  "\xb8\xec\x97\x90\x20\xed\x99\x95\xec\x9e\xa5\xeb\x90\x9c\x20\xea\xb8\xb0\xeb\x8a"
                                  "\xa5\xeb\x93\xa4\xec\x9d\x84\x20\xec\x9d\xb4\xec\x9a\xa9\xed\x95\x98\xec\x8b\xa4"
                                  "\x20\xec\x88\x98\x20\xec\x9e\x88\xec\x8a\xb5\xeb\x8b\x88\xeb\x8b\xa4\x2e" ) );
    listBox4->clear();
    listBox4->insertItem( trUtf8( "이이템" ) );
    textLabel18->setText( trUtf8( "\xed\x94\x8c\xeb\x9f\xac\xea\xb7\xb8\xec\x9d\xb8\x20\xec\xa0\x95\xeb\xb3\xb4" ) );
    textLabel19->setText( trUtf8( "\xed\x86\xb5\xed\x95\xa9\xeb\xa9\x94\xec\x84\xb8\xec\xa7\x80\xed\x95\xa8" ) );
    startLoginCheckBox->setText( trUtf8( "\xeb\xa1\x9c\xea\xb7\xb8\xec\x9d\xb8\x20\xec\x8b\x9c\x20\xec\x9e\x90\xeb\x8f\x99\x20"
                                         "\xec\x8b\xa4\xed\x96\x89" ) );
    installButton->setText( trUtf8( "설치" ) );
    upgradeButton->setText( trUtf8( "업그레이드" ) );
    removeButton->setText( trUtf8( "제거" ) );

    // buttonOk->setText( tr( "&OK" ) );
    buttonOk->setText( UTF8( "확인" ) );
    buttonOk->setAccel( QKeySequence( QString::null ) );
    // buttonCancel->setText( tr( "&Cancel" ) );
    buttonCancel->setText( UTF8( "취소" ) );
    buttonCancel->setAccel( QKeySequence( QString::null ) );
    // buttonHelp->setText( tr( "&Help" ) );
    buttonHelp->setText( UTF8( "도움말" ) );
    buttonHelp->setAccel( QKeySequence( tr( "F1" ) ) );

    WStackPage_8->setEnabled( FALSE );
}

void Preference::slotSetNick(const QString &sNick) {
    nickLineEdit->setText( sNick );
}

void Preference::slotSetDownPath(const QString &sPath) {
    saveFileLineEdit->setText( sPath );
}

void Preference::slotOpenSaveFile() {
    QString sPath = KFileDialog::getExistingDirectory(
                        saveFileLineEdit->text(),
                        this,
                        UTF8("디렉토리 선택")
                    );
    if ( sPath.length() ) {
        saveFileLineEdit->setText( sPath );
        if ( sPath != stConfig.filedownloadpath ) {
            /*! 기본 설정 */
            config->setGroup( "Config_General" );

            /*! P2P 파일 저장 디렉토리 */
            stConfig.filedownloadpath = sPath;
            config->writeEntry( "Down_Path", stConfig.filedownloadpath );
            config->sync();
        }
    }
}

void Preference::slotShowSoundSelectDialog() {
    if ( !pSoundSelect ) {
        pSoundSelect = new SoundConfig( this );
        connect( useAlarmCheckBox, SIGNAL( toggled( bool ) ), pSoundSelect->soundGroupBox, SLOT( setChecked ( bool ) ) );
        connect( pSoundSelect->soundGroupBox, SIGNAL( toggled( bool ) ), useAlarmCheckBox, SLOT( setChecked ( bool ) ) );
    }
    pSoundSelect->show();
}

void Preference::slotAddLock() {
    int nId = allowUserListBox->currentItem();
    QString sTemp;

    if ( nId != -1) {
        QRegExp rx("[^<(]+@[^>)]+");
        rx.search( allowUserListBox-> currentText () );
        QString sEmail = rx.cap(0);

        const BuddyList* pBuddyList = pCurrentAccount->getBuddyList();
        Buddy *pBuddy = pBuddyList->getBuddyByID( sEmail );

        if ( pBuddy ) {
            pBuddy->setBL( TRUE );
            emit sendLockBuddy( sEmail );
        }

        sTemp = allowUserListBox->text ( nId );
        allowUserListBox->removeItem ( nId );
        lockedUserListBox->insertItem( sTemp );
        lockedUserListBox->sort( true );

        QListBoxItem *pListBoxItem = lockedUserListBox->findItem( UTF8("다른 모든 사용자") );
        if ( pListBoxItem ) {
            lockedUserListBox->setTopItem ( lockedUserListBox->index( pListBoxItem ) );
            allowUserCheckBox->setChecked( TRUE );
        } else {
            allowUserCheckBox->setChecked( FALSE );
        }

        emit refreshBuddyList();
    }
}

void Preference::slotAddUnlock() {
    int nId = lockedUserListBox->currentItem();
    QString sTemp;

    if ( nId != -1) {
        QRegExp rx("[^<(]+@[^>)]+");
        rx.search( lockedUserListBox-> currentText () );
        QString sEmail = rx.cap(0);

        const BuddyList* pBuddyList = pCurrentAccount->getBuddyList();
        Buddy *pBuddy = pBuddyList->getBuddyByID( sEmail );

        if ( pBuddy ) {
            pBuddy->setAL( TRUE );
            emit sendUnlockBuddy( sEmail );
        }

        sTemp = lockedUserListBox->text ( nId );
        lockedUserListBox->removeItem ( nId );
        allowUserListBox->insertItem( sTemp );
        allowUserListBox->sort( true );

        QListBoxItem *pListBoxItem = allowUserListBox->findItem( UTF8("다른 모든 사용자") );
        if ( pListBoxItem ) {
            allowUserListBox->setTopItem ( allowUserListBox->index( pListBoxItem ) );
            allowUserCheckBox->setChecked( FALSE );
        } else {
            allowUserCheckBox->setChecked( TRUE );
        }
        emit refreshBuddyList();
    }
}

void Preference::setPrivacyList() {
    const BuddyList* pBuddyList;
    QStringList slLockedBuddy;
    QStringList slUnlockedBuddy;
    Buddy *pBuddy;

    slLockedBuddy.clear();
    slUnlockedBuddy.clear();

    allowUserListBox->clear();
    lockedUserListBox->clear();

    /*! 기본 설정 */
    config->setGroup( "Config_Privacy" );
    /*! KDE 실행시 자동실행 */
    stConfig.allowother = config->readBoolEntry( "Allow_Other", true );

    if ( stConfig.allowother ) {
        allowUserListBox->insertItem( UTF8( "다른 모든 사용자" ) );
        allowUserCheckBox->setChecked( false );
    } else {
        lockedUserListBox->insertItem( UTF8( "다른 모든 사용자" ) );
        allowUserCheckBox->setChecked( true );
    }

    pBuddyList = pCurrentAccount->getBuddyList();
    QPtrListIterator<Buddy> iterator( *pBuddyList );
    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        QString sBuddy( pBuddy->getName() );
        sBuddy += "(";
        sBuddy += pBuddy->getUID();
        sBuddy += ")";

        /*! 차단 = 1011 (ADDB BL) */
        /*! BL(Block List) == 1 */
        if ( /* pBuddy->getBuddyFlag()[2] == '1' */  pBuddy->isBL() == true )
            // lockedUserListBox->insertItem( sBuddy );
            slLockedBuddy.append( sBuddy );
        /*! 해제 = 1101 (ADDB AL) */
        else
            // allowUserListBox->insertItem( sBuddy );
            slUnlockedBuddy.append( sBuddy );
        ++iterator;
    }
    slLockedBuddy.sort();
    lockedUserListBox->insertStringList( slLockedBuddy );
    slUnlockedBuddy.sort();
    allowUserListBox->insertStringList( slUnlockedBuddy );
}

void Preference::updatePrivacy() {
    const BuddyList* pBuddyList;
    Buddy *pBuddy;
    QStringList slUnlock;
    QStringList slLock;

    slUnlock.clear();
    slLock.clear();

    pBuddyList = pCurrentAccount->getBuddyList();
    QPtrListIterator<Buddy> iterator( *pBuddyList );
    while (iterator.current() != 0) {
        pBuddy = iterator.current();
        QString sBuddy( pBuddy->getName() );
        sBuddy += "(";
        sBuddy += pBuddy->getUID();
        sBuddy += ")";

        /*! 차단 = 1011 (ADDB BL) */
        /*! BL == 1 */
        if ( pBuddy->isBL() == true ) {
            QListBoxItem *pListBoxItem = allowUserListBox->findItem( sBuddy );
            if ( pListBoxItem ) {
                slUnlock.append( pBuddy->getHandle() + " " + pBuddy->getUID() );
                pBuddy->setBL( false );
            }
        }
        /*! 해제 = 1101 (ADDB AL) */
        else {
            QListBoxItem *pListBoxItem = lockedUserListBox->findItem( sBuddy );
            if ( pListBoxItem ) {
                slLock.append( pBuddy->getHandle() + " " + pBuddy->getUID() );
                pBuddy->setBL( true );
            }
        }
        ++iterator;
    }

    bool bUpdate = false;

    if ( slUnlock.count() ) {
        emit unlockList( slUnlock );
        bUpdate = true;
    }

    if ( slLock.count() ) {
        emit lockList( slLock );
        bUpdate = true;
    }

    if ( bUpdate )
        emit refreshBuddyList();
}

void Preference::initialize() {
    /*! 프로필 설정 */
    QString sProfile( "Profile_" );
    /*! 닉네임 설정 */
    sProfile += pCurrentAccount->getMyNateID();
    config->setGroup( sProfile );
    slotSetNick( pCurrentAccount->getMyNickName() );

    /*! 기본 설정 */
    config->setGroup( "Config_General" );
    /*! KDE 실행시 자동실행 */
    stConfig.autorun = config->readBoolEntry( "Auto_Run", false );
    autoRunCheckBox->setChecked( stConfig.autorun );
    /*! 시작시 창 숨김 */
    stConfig.hidestart = config->readBoolEntry( "Hide_Start", false );
    hideCheckBox->setChecked( stConfig.hidestart );
    /*! 항상 위에 보이기 */
    stConfig.alwaystop = config->readBoolEntry( "Always_Top", false );
    topCheckBox->setChecked( stConfig.alwaystop );
    /*! 친구 목록에 이모티콘 사용 */
    stConfig.viewemoticonlist = config->readBoolEntry( "Use_Emoticon_BuddyList", true );
    emoticonCheckBox->setChecked( stConfig.viewemoticonlist );
    /*! 친구 더블클릭시 기본 행동 */
    stConfig.buddydoubleclickaction = config->readNumEntry( "Buddy_DoubleClick_Action", 0 );
    buddyDoubleClickComboBox->setCurrentItem( stConfig.buddydoubleclickaction );
    /*! 받은 파일 저장 위치 */
    config->setGroup( "Config_General" );
    stConfig.filedownloadpath = config->readEntry( "Down_Path", QDir::homeDirPath() );
    if ( !QDir( stConfig.filedownloadpath ).exists() )
        stConfig.filedownloadpath = QDir::homeDirPath();
    slotSetDownPath( stConfig.filedownloadpath );

    /*! 개인 */
    config->setGroup( "Config_Personal" );
    /*! 자리비움 사용 유무 */
    stConfig.checkawaytime = config->readBoolEntry( "Use_Away_Time", false );
    timeAwayCheckBox->setChecked( stConfig.checkawaytime );
    /*! 자리비움 시간 */
    stConfig.awaytime = config->readNumEntry( "Away_Time", 10 );
    lineEdit10->setText( QString::number(stConfig.awaytime) );

    /*! 알림/소리 */
    config->setGroup( "Config_Alarm_Sound" );
    /*! 친구 접속 알림 */
    stConfig.alarmbuddyconnect = config->readBoolEntry( "Use_Alarm_Buddy_Connect", false );
    alarmConnectCheckBox->setChecked( stConfig.alarmbuddyconnect );
    /*! 대화 시작 알림 */
    stConfig.alarmrequirechat = config->readBoolEntry( "Use_Alarm_Chat_Start", false );
    alarmChatCheckBox->setChecked( stConfig.alarmrequirechat );
    /*! 새 쪽지 수신 알림 사용 유무 */
    stConfig.usealarmreceivenewmemo = config->readBoolEntry("Use_Alarm_New_Memo", false );
    alarmMemoCheckBox->setChecked( stConfig.usealarmreceivenewmemo );
    /*! 새 쪽지 수신시 규칙 */
    stConfig.receivenewmemo = config->readNumEntry( "Action_Receive_Memo", 0 );
    alarmMemoComboBox->setCurrentItem ( stConfig.receivenewmemo );
    /*! 알람시 소리 사용 */
    // stConfig.usealarmwithsound = config->readBoolEntry( "Use_Alarm_With_Sound", false );
    // useAlarmCheckBox->setChecked( stConfig.usealarmwithsound );
    stConfig.useallsound= config->readBoolEntry( "UseSound", false );
    useAlarmCheckBox->setChecked( stConfig.useallsound );

    /*! 메시지 */
    config->setGroup( "Config_Message" );
    /*! 부재중 메시지 사용유무 */
    stConfig.showawaymemo = config->readBoolEntry("Use_Away_Message", false );
    viewAwayMessageCheckBox->setChecked( stConfig.showawaymemo );
    /*! 다른용무중 부재중 메시지 보이기 */
    stConfig.showawaymemo_busy = config->readBoolEntry("Use_Away_Message_Busy", false );
    viewBusyMessageCheckBox->setChecked( stConfig.showawaymemo_busy );

    if ( stConfig.showawaymemo || stConfig.showawaymemo_busy ) {
        pixmapLabel3->setEnabled();
        pixmapLabel4->setEnabled();
        textEdit1->setEnabled( TRUE );
        textEdit1->setPaletteBackgroundColor("#FFFFFF");
    } else {
        pixmapLabel3->setDisabled();
        pixmapLabel4->setDisabled();
        textEdit1->setEnabled( FALSE );
        textEdit1->setPaletteBackgroundColor("#C0C0C0");
    }
    /*! 부재중 메시지 */
    stConfig.awaymemo = config->readEntry( "Away_Message" );
    textEdit1->setText( stConfig.awaymemo );

    // config->writeEntry( "Away_Message_Font", font );
    textEdit1->setFont( config->readFontEntry( "Away_Message_Font" ) );

    /*! 대화 저장 방법 */
    stConfig.savechatlog = config->readNumEntry( "Save_Chat_Log", 2 );
    chatSaveFileComboBox->setCurrentItem( stConfig.savechatlog );
    /*! 이모티콘 사용 유무 */
    stConfig.useemoticon = config->readBoolEntry( "Use_Chat_Emoticon", true );
    useEmoticonCheckBox->setChecked( stConfig.useemoticon );
    /*! 답장에 메모 첨부  */
    stConfig.replymemoattach = config->readBoolEntry( "Attach_Reply_Memo", false );
    replayCheckBox->setChecked( stConfig.replymemoattach );
    /*! 모두에게답장시 메모 첨부 */
    stConfig.allreplymemoattach = config->readBoolEntry( "Attach_All_Reply_Memo", false );
    allReplyCheckBox->setChecked( stConfig.allreplymemoattach );
    /*! 전달시 메모 첨부 */
    stConfig.forwardmemoattach = config->readBoolEntry( "Attach_Forward_Memo", false );
    forwardCheckBox->setChecked( stConfig.forwardmemoattach );

    /*! 프라이버시 */
    config->setGroup( "Config_Privacy" );
    /*! 허용된 사용자에게만 쪽지를 받음 */
    stConfig.allowonlypermitchat = config->readBoolEntry( "Allow_Only_Permit_Chat", false );
    allowUserCheckBox->setChecked( stConfig.allowonlypermitchat );
    /*! 친구에게만 대화요청 받음 */
    stConfig.allowonlyfriendmemo = config->readBoolEntry( "Allow_Only_Friend_Memo", false );
    receiveFromFriendCheckBox->setChecked( stConfig.allowonlyfriendmemo );

    /*! 연결(방화벽) */
    config->setGroup( "Config_Connection" );
    /*! 프록시 사용 여부 */
    stConfig.useproxy = config->readBoolEntry( "Use_Proxy", false );
    useProxyCheckBox->setChecked( stConfig.useproxy );
    /*! 프록시 종류 */
    stConfig.proxytype = config->readNumEntry( "Proxy_Type", 0 );
    typeComboBox->setCurrentItem( stConfig.proxytype );
    if ( stConfig.useproxy ) {
        setProxyEnabled( stConfig.proxytype );
        typeComboBox->setEnabled( TRUE );
    } else {
        setProxyDisabled();
        typeComboBox->setEnabled( FALSE );
    }
    /*! 프록시 서버 */
    stConfig.proxyserver = config->readEntry( "Proxy_Server" );
    serverLineEdit->setText( stConfig.proxyserver );
    /*! 프록시 포트 */
    stConfig.proxyport = config->readNumEntry( "Proxy_Port" );
    portLineEdit->setText( QString::number( stConfig.proxyport ) );
    /*! 프록시 ID */
    stConfig.proxyid = config->readEntry( "Proxy_ID" );
    userLineEdit->setText( stConfig.proxyid );
    /*! 프록시 패스워드 */
    stConfig.proxypassword = config->readEntry( "Proxy_Password" );
    passwordLineEdit->setText( stConfig.proxypassword );
    /*! P2P 포트 */
    stConfig.p2pport = config->readNumEntry( "P2P_Port", 5004 );
    p2pPortLineEdit->setText( QString::number( stConfig.p2pport ) );

    /*! 차단/해제 목록 */
    setPrivacyList();
}

void Preference::showChangeNick() {
    widgetStack2->raiseWidget(1);
    nickLineEdit->setFocus();
}

void Preference::accept() {
    if ( bOnline ) {
        /*! 프로필 설정 */
        QString sProfile("Profile_");
        sProfile += pCurrentAccount->getMyNateID();
        config->setGroup( sProfile );
        /*! 닉네임 설정 */

        if ( nickLineEdit->isModified() ) {
            /*!
            	KConfig로 저장하고,
            	DP서버로 수정된 Nick 전송.
            	emit으로 Nick변경된것 적용.
            */
            QString sNick = nickLineEdit->text();
            QString sStripNick = sNick.stripWhiteSpace();
            if ( sStripNick.length() > 0 ) {
                config->writeEntry( "Nick", sNick );
                pCurrentAccount->setMyNickName( sNick );
                emit changeNick( sNick );
            }
        }
    }
    /*! 기본 설정 */
    config->setGroup( "Config_General" );
    /*! KDE 실행시 자동실행 */
    stConfig.autorun = autoRunCheckBox->isChecked();
    config->writeEntry( "Auto_Run", stConfig.autorun );
    /*! 시작시 창 숨김 */
    stConfig.hidestart = hideCheckBox->isChecked();
    config->writeEntry( "Hide_Start", stConfig.hidestart );
    /*! 항상 위에 보이기 */
    if ( stConfig.alwaystop != topCheckBox->isChecked() ) {
        stConfig.alwaystop = topCheckBox->isChecked();
        emit alwaysTop( stConfig.alwaystop );
    }
    config->writeEntry( "Always_Top", stConfig.alwaystop );
    /*! 친구 더블클릭시 기본 행동 */
    stConfig.buddydoubleclickaction = (short int)buddyDoubleClickComboBox->currentItem();
    config->writeEntry( "Buddy_DoubleClick_Action", stConfig.buddydoubleclickaction );

    if ( bOnline ) {
        /*! 친구 목록에 이모티콘 사용 */
        stConfig.viewemoticonlist = emoticonCheckBox->isChecked();
        config->writeEntry( "Use_Emoticon_BuddyList", stConfig.viewemoticonlist );
        emit emoticonShow( stConfig.viewemoticonlist );
        /*! 받은 파일 저장 위치 */
        /*! 끝날때 저장하지 않고 다이얼로그에서 Path 선택하면 바로 바로 저장 */
        // stConfig.filedownloadpath = saveFileLineEdit->text();
        // config->writeEntry( "Down_Path", stConfig.filedownloadpath );

        /*! 개인 */
        config->setGroup( "Config_Personal" );

        if ( stConfig.checkawaytime != timeAwayCheckBox->isChecked() ) {
            /*! 자리비움 사용 유무 */
            stConfig.checkawaytime = timeAwayCheckBox->isChecked();
            config->writeEntry( "Use_Away_Time", stConfig.checkawaytime );
            emit updateAwayTime();
        }

        QString sAwayTime( lineEdit10->text() );
        QString sAwayTime2 = sAwayTime.stripWhiteSpace();
        if ( sAwayTime2.length() > 0 ) {
            if ( stConfig.awaytime != ( lineEdit10->text() ).toInt() ) {
                /*! 자리비움 시간 */
                stConfig.awaytime = (short unsigned int)( lineEdit10->text() ).toInt();
                config->writeEntry( "Away_Time", stConfig.awaytime );
                emit updateAwayTime();
            }
        }
        /*! 기본 브라우저 설정 */
        config->setGroup( "WebBrowser" );
        stConfig.usedefaultwebbrowser = groupBox100->isChecked();
        config->writeEntry( "UseUserDefault", stConfig.usedefaultwebbrowser );
        stConfig.defaultwebbrowser = lineEdit100->text();
        QString sTemp = ( stConfig.defaultwebbrowser ).stripWhiteSpace();
        if ( sTemp == UTF8("설정안됨") || sTemp.isEmpty() ) {
            stConfig.defaultwebbrowser = UTF8("설정안됨");
            lineEdit100->setText( stConfig.defaultwebbrowser );
            config->writeEntry( "UserDefault",  stConfig.defaultwebbrowser );
            stConfig.usedefaultwebbrowser = FALSE;
            groupBox100->setChecked( stConfig.usedefaultwebbrowser );
            config->writeEntry( "UseUserDefault", stConfig.usedefaultwebbrowser );
        }
	else {
	    config->writeEntry( "UserDefault", stConfig.defaultwebbrowser );
	}

        if ( stConfig.usedefaultwebbrowser == TRUE  )
            emit changeWebBrowser( stConfig.defaultwebbrowser );
        else
            emit changeWebBrowser( UTF8("none") );

        /*! 알림/소리 */
        config->setGroup( "Config_Alarm_Sound" );
        /*! 친구 접속 소리 */
        stConfig.alarmbuddyconnect = alarmConnectCheckBox->isChecked();
        config->writeEntry( "Use_Alarm_Buddy_Connect", stConfig.alarmbuddyconnect );
        /*! 대화 시작 소리 */
        stConfig.alarmrequirechat = alarmChatCheckBox->isChecked();
        config->writeEntry( "Use_Alarm_Chat_Start", stConfig.alarmrequirechat );
        /*! 새 쪽지 수신 알림 사용 유무 */
        stConfig.usealarmreceivenewmemo = alarmMemoCheckBox->isChecked();
        config->writeEntry( "Use_Alarm_New_Memo", stConfig.usealarmreceivenewmemo );
        /*! 새 쪽지 수신시 규칙 */
        stConfig.receivenewmemo = (short int)alarmMemoComboBox->currentItem();
        config->writeEntry( "Action_Receive_Memo", stConfig.receivenewmemo );
        /*! 알람시 소리 사용 */
        // stConfig.usealarmwithsound = useAlarmCheckBox->isChecked();
        // config->writeEntry( "Use_Alarm_With_Sound", stConfig.usealarmwithsound );
        stConfig.useallsound= useAlarmCheckBox->isChecked();
        config->writeEntry( "UseSound", stConfig.useallsound );

        /*! 메시지 */
        config->setGroup( "Config_Message" );
        /*! 부재중 메시지 사용유무 */
        stConfig.showawaymemo = viewAwayMessageCheckBox->isChecked();
        config->writeEntry( "Use_Away_Message", stConfig.showawaymemo );
        /*! 다른용무중 부재중 메시지 보이기 */
        stConfig.showawaymemo_busy = viewBusyMessageCheckBox->isChecked();
        config->writeEntry( "Use_Away_Message_Busy", stConfig.showawaymemo_busy );
        /*!
         * 부재중 메시지가 없으면
         * "입력된 부재 중 메시지가 없습니다."
         * 메시지보여야 함
         */
        if ( stConfig.showawaymemo || stConfig.showawaymemo_busy ) {
            QString sTemp( textEdit1->text() );
            QString sMsg = sTemp.stripWhiteSpace();
            if ( sMsg.length() < 1 ) {
                KMessageBox::information (this, QString::fromUtf8("입력된 부재 중 메시지가 없습니다."), UTF8("환경설정") );
                widgetStack2->raiseWidget(3);
                textEdit1->clear();
                textEdit1->setFocus();
                return;
            }
            /*! 부재중 메시지 */
            stConfig.awaymemo = sMsg;
        }

        config->writeEntry( "Away_Message", stConfig.awaymemo );
        /*! 대화 저장 방법 */
        stConfig.savechatlog = (short int)chatSaveFileComboBox->currentItem();
        config->writeEntry( "Save_Chat_Log", stConfig.savechatlog );
        /*! 이모티콘 사용 유무 */
        stConfig.useemoticon = useEmoticonCheckBox->isChecked();
        config->writeEntry( "Use_Chat_Emoticon", stConfig.useemoticon );
        /*! 답장에 메모 첨부  */
        stConfig.replymemoattach = replayCheckBox->isChecked();
        config->writeEntry( "Attach_Reply_Memo", stConfig.replymemoattach );
        /*! 모두에게답장시 메모 첨부 */
        stConfig.allreplymemoattach = allReplyCheckBox->isChecked();
        config->writeEntry( "Attach_All_Reply_Memo", stConfig.allreplymemoattach );
        /*! 전달시 메모 첨부 */
        stConfig.forwardmemoattach = forwardCheckBox->isChecked();
        config->writeEntry( "Attach_Forward_Memo", stConfig.forwardmemoattach );

        /*! 프라이버시 */
        config->setGroup( "Config_Privacy" );
        /*! 허용된 사용자에게만 쪽지를 받음 */
        if ( stConfig.allowonlypermitchat != allowUserCheckBox->isChecked() ) {
            stConfig.allowonlypermitchat = allowUserCheckBox->isChecked();
            config->writeEntry( "Allow_Only_Permit_Chat", stConfig.allowonlypermitchat );
            emit onlyPermitChat( stConfig.allowonlypermitchat );
        }

        /*! 친구에게만 대화요청 받음 */
        if ( stConfig.allowonlyfriendmemo != receiveFromFriendCheckBox->isChecked() ) {
            stConfig.allowonlyfriendmemo = receiveFromFriendCheckBox->isChecked();
            config->writeEntry( "Allow_Only_Friend_Memo", stConfig.allowonlyfriendmemo );
            emit onlyFriendMemo( stConfig.allowonlyfriendmemo );
        }

        /*! 허용된 친구목록 - 다른 모든 사용자  */
        QListBoxItem *pListBoxItem = lockedUserListBox->findItem( UTF8("다른 모든 사용자") );
        if ( pListBoxItem )
            stConfig.allowother = false;
        else
            stConfig.allowother = true;
        config->writeEntry( "Allow_Other", stConfig.allowother );
    }

    /*! 연결(방화벽) */
    config->setGroup( "Config_Connection" );
    /*! 프록시 사용 여부 */
    stConfig.useproxy = useProxyCheckBox->isChecked();
    config->writeEntry( "Use_Proxy", stConfig.useproxy );
    /*! 프록시 종류 */
    stConfig.proxytype = (short unsigned int)typeComboBox->currentItem();
    config->writeEntry( "Proxy_Type", stConfig.proxytype );
    /*! 프록시 서버 */
    stConfig.proxyserver = serverLineEdit->text();
    config->writeEntry( "Proxy_Server", stConfig.proxyserver );
    /*! 프록시 포트 */
    stConfig.proxyport = (short unsigned int)( portLineEdit->text() ).toInt();
    config->writeEntry( "Proxy_Port", stConfig.proxyport );
    /*! 프록시 ID */
    stConfig.proxyid = userLineEdit->text();
    config->writeEntry( "Proxy_ID", stConfig.proxyid );
    /*! 프록시 패스워드 */
    stConfig.proxypassword = passwordLineEdit->text();
    config->writeEntry( "Proxy_Password", stConfig.proxypassword );
    /*! P2P 포트 */
    stConfig.p2pport = (short unsigned int)( p2pPortLineEdit->text() ).toInt();
    config->writeEntry( "P2P_Port", stConfig.p2pport );
    config->sync();

    if ( bOnline ) {
        /*! 프라이버시 설정 적용 */
        // updatePrivacy();

        if ( pEmoticonSelector )
            if ( pEmoticonSelector->isShown() )
                pEmoticonSelector->hide();
    }
    QDialog::accept();
}

void Preference::show() {
    initialize();
    QDialog::show();
}

void Preference::showMemoReplyRole() {
    widgetStack2->raiseWidget(3);
}

void Preference::showSyncSetup() {
}

void Preference::slotEmoticonDialog() {
    if ( !pEmoticonSelector ) {
        pEmoticonSelector = new EmoticonSelector( this, "EmoticonSelector" );
        connect(pEmoticonSelector->toolButton1, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton2, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton3, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton4, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton5, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton6, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton7, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton8, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton9, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton10, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton11, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton12, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton13, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton14, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton15, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton16, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton17, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton18, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton19, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton20, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton21, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton22, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton23, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton24, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton25, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton26, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton27, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton28, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton29, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton30, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton31, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton32, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton33, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton34, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton35, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton36, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton37, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton38, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton39, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton40, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton41, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton42, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton43, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton44, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton45, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton46, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton47, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton48, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton49, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton50, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton51, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton52, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton53, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton54, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton55, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton56, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton57, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton58, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton59, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton60, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton61, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton62, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton63, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton64, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton65, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton66, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton67, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton68, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton69, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton70, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton71, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton72, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton73, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton74, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton75, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton76, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton77, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton78, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton79, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton80, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton81, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton82, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton83, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton84, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton85, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton86, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton87, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton88, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton89, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton90, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton91, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton92, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton93, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton94, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton95, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton96, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton97, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton98, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton99, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton100, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton101, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton102, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton103, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton104, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton105, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton106, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton107, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton108, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton109, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton110, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton111, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton112, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton113, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton114, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton115, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton116, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton117, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton118, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton119, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton120, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton121, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton122, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton123, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton124, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton125, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton126, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton127, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton128, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton129, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton130, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton131, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton132, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton133, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton134, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton135, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton136, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton137, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton138, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton139, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton140, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton141, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton142, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton143, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton144, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton145, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton146, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton147, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton148, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton149, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton150, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton151, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton152, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton153, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton154, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton155, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton156, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton157, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton158, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton159, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton160, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton161, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton162, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton163, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton164, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton165, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton166, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton167, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton168, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton169, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton170, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton171, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton172, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton173, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton174, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton175, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton176, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton177, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton178, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton179, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton180, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton181, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton182, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton183, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton184, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton185, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton186, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton187, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton188, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton189, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton190, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton191, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton192, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton193, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton194, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton195, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton196, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton197, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton198, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton199, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton200, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton201, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton202, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton203, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton204, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
        connect(pEmoticonSelector->toolButton205, SIGNAL( clicked(const QString &) ), SLOT( slotPutEmoticon(const QString &) ) );
    }

    int screen = kapp->desktop()->screenNumber();
    QRect screenSize = kapp->desktop()->screenGeometry(screen);

    if ( ( x() + ( width() / 2) ) > ( screenSize.width() / 2 ) )
        pEmoticonSelector->move( x() - pEmoticonSelector->width(), y()/* + ( height() - pEmoticonSelector->height() )*/ );
    else
        pEmoticonSelector->move( x() + width(), y()/* + ( height() - pEmoticonSelector->height() )*/ );
    pEmoticonSelector->setModal( false );
    if ( pEmoticonSelector->isShown() )
        pEmoticonSelector->hide();
    else
        pEmoticonSelector->show();
}

void Preference::slotPutEmoticon(const QString & sText) {
    if ( widgetStack2->id (widgetStack2->visibleWidget () ) == 1) {
        nickLineEdit->insert( sText );
        nickLineEdit->setActiveWindow ();
    } else if ( widgetStack2->id (widgetStack2->visibleWidget () ) == 3) {
        textEdit1->insert( sText );
        textEdit1->setActiveWindow ();
    }
}

void Preference::showMessageTab() {
    widgetStack2->raiseWidget(3);
    chatSaveFileComboBox->setFocus();
}

void Preference::slotCheckNum(const QString & sNum) {
    QString sTest( sNum );
    bool ok = FALSE;
    /* int dec = */
    sTest.toInt( &ok, 10 );

    if ( sTest.length() != 0)
        if ( !ok )
            lineEdit10->setText("10");
}

void Preference::help() {
    LNMUtils::openURL( "http://nateonweb.nate.com/help/guide_demo_pc_v3_015.html" );
}

void Preference::slotEditEnable(bool bTrue) {
    Q_UNUSED( bTrue );

    if ( viewAwayMessageCheckBox->isChecked () ||
            viewBusyMessageCheckBox->isChecked () ) {
        pixmapLabel3->setEnabled();
        pixmapLabel4->setEnabled();
        textEdit1->setEnabled( TRUE );
        textEdit1->setPaletteBackgroundColor("#FFFFFF");
    } else {
        pixmapLabel3->setDisabled();
        pixmapLabel4->setDisabled();
        textEdit1->setEnabled( FALSE );
        textEdit1->setPaletteBackgroundColor("#C0C0C0");
    }
}

void Preference::slotFontDialog() {
    bool ok;
    QFont font = QFontDialog::getFont( &ok, QFont( UTF8("굴림"), 10 ), this );

    if ( ok ) {
        textEdit1->setFont( font );
        config->setGroup( "Config_Message" );
        config->writeEntry( "Away_Message_Font", font );
    }
}

void Preference::lockOther(bool bAllow) {
    if ( bAllow ) {
        QListBoxItem *pListBoxItem = allowUserListBox->findItem( UTF8("다른 모든 사용자") );
        if ( pListBoxItem ) {
            allowUserListBox->removeItem ( allowUserListBox->index( pListBoxItem ) );
            lockedUserListBox->insertItem( UTF8("다른 모든 사용자") );
            lockedUserListBox->sort( true );
            // lockedUserListBox->setTopItem ( lockedUserListBox->index( pListBoxItem ) );
        }
    } else {
        QListBoxItem *pListBoxItem = lockedUserListBox->findItem( UTF8("다른 모든 사용자") );
        if ( pListBoxItem ) {
            lockedUserListBox->removeItem ( lockedUserListBox->index( pListBoxItem ) );
            allowUserListBox->insertItem( UTF8("다른 모든 사용자") );
            allowUserListBox->sort( true );
            // allowUserListBox->setTopItem ( allowUserListBox->index( pListBoxItem ) );
        }
    }
}

void Preference::slotProxyType(int nID) {
    switch ( nID ) {
    case 0:
        setProxyEnabled( 0 );
        break;
    case 1:
        setProxyEnabled( 1 );
        break;
    case 2:
        KMessageBox::information(this, UTF8("HTTP 프록시는 차후 지원될 예정입니다."), UTF8("프록시 종류 설정") );
        setProxyDisabled();
        // setProxyEnabled( 2 );
        break;
    }
}

void Preference::setProxyEnabled( int nType ) {
    /*! 프록시 서버 */
    serverLineEdit->setEnabled( TRUE );
    /*! 프록시 포트 */
    portLineEdit->setEnabled( TRUE );
    switch ( nType  ) {
    case 0:
        /*! 프록시 ID */
        userLineEdit->setEnabled( FALSE );
        /*! 프록시 패스워드 */
        passwordLineEdit->setEnabled( FALSE );
        break;
    case 1:
        /*! 프록시 ID */
        userLineEdit->setEnabled( TRUE );
        /*! 프록시 패스워드 */
        passwordLineEdit->setEnabled( TRUE );
        break;
    case 2:
        /*! 프록시 ID */
        userLineEdit->setEnabled( TRUE );
        /*! 프록시 패스워드 */
        passwordLineEdit->setEnabled( TRUE );
        break;
    }

}

void Preference::setProxyDisabled() {
    /*! 프록시 서버 */
    serverLineEdit->setEnabled( FALSE );
    /*! 프록시 포트 */
    portLineEdit->setEnabled( FALSE );
    /*! 프록시 ID */
    userLineEdit->setEnabled( FALSE );
    /*! 프록시 패스워드 */
    passwordLineEdit->setEnabled( FALSE );
}

void Preference::slotUseProxy(bool bUse) {
    if ( bUse ) {
        setProxyEnabled( typeComboBox->currentItem() );
        typeComboBox->setEnabled( TRUE );
    } else {
        setProxyDisabled();
        typeComboBox->setEnabled( FALSE );
    }
}

#if 0
void Preference::setPrivaryListProfile() {
    QString sName;
    sName = pBuddy->getName();

    pPrivacyUnlockedRightClick->changeItem ( 1, );
}
#endif

void Preference::slotShowProfile() {
    const BuddyList* pBuddyList = pCurrentAccount->getBuddyList();
    Buddy *pBuddy = pBuddyList->getBuddyByID( sSelectPrivacyID );
    if ( pBuddy ) {
        QString sURL("http://br.nate.com/index.php");
        sURL += "?code=F009";
        sURL += "&t=";
        sURL += pCurrentAccount->getMyTicket();
        sURL += "&param=";
        sURL += pBuddy->getHandle();;

#ifdef NETDEBUG
        kdDebug() << "Profile : [" << sURL << "]" <<endl;
#endif

        LNMUtils::openURL( sURL );
    }
}

void Preference::slotAddEtcList() {
    Group *pGroup = pCurrentAccount->getGroupList()->getGroupByID( "0" );
    if ( pGroup ) {
        const BuddyList* pBuddyList = pCurrentAccount->getBuddyList();
        Buddy *pBuddy = pBuddyList->getBuddyByID( sSelectPrivacyID );
        if ( pBuddy ) {
            pGroup->addBuddy( pBuddy );
            pBuddy->setFL( TRUE );
        }
        emit addEtc( sSelectPrivacyID );
    }
}

void Preference::slotDelete() {
    emit sendRealDelete( sSelectPrivacyID );
    int nID;
    switch ( nSelectPrivacyType ) {
    case 1:
        nID = allowUserListBox->currentItem();
        if ( nID != -1 )
            allowUserListBox->removeItem ( nID );
        break;
    case 2:
        nID = lockedUserListBox->currentItem();
        if ( nID != -1 )
            lockedUserListBox->removeItem ( nID );
        break;
    }

}

void Preference::slotAllowRightClicked(QListBoxItem * pItem, const QPoint & pPoint) {
    if ( pItem->text() == UTF8("다른 모든 사용자"))
        return;
    QString sProfile;
    sProfile = pItem->text();
    sProfile += UTF8("님의 프로필 보기");
    pPrivacyUnlockedRightClick->changeItem ( 1, sProfile );

    QRegExp rx("[^<(]+@[^>)]+");
    rx.search( allowUserListBox-> currentText () );
    QString sEmail = rx.cap(0);

    sSelectPrivacyID = sEmail;
    nSelectPrivacyType = 1;
#ifdef NETDEBUG
    kdDebug() << "Email:[" << sEmail << "]" << endl;
#endif

    const BuddyList* pBuddyList = pCurrentAccount->getBuddyList();
    Buddy *pBuddy = pBuddyList->getBuddyByID( sEmail );

    bool bDelete = FALSE;
    if ( pBuddy->isFL() ) {
        pPrivacyUnlockedRightClick->setItemEnabled ( 2, FALSE );
    } else {
        pPrivacyUnlockedRightClick->setItemEnabled ( 2, TRUE );
        if ( !pBuddy->isRL() ) {
            bDelete = TRUE;
        }
    }
    pPrivacyUnlockedRightClick->setItemEnabled ( 4, bDelete );

    pPrivacyUnlockedRightClick->popup( pPoint );
}

void Preference::slotLockedRightClicked(QListBoxItem * pItem, const QPoint & pPoint) {
    if ( pItem->text() == UTF8("다른 모든 사용자"))
        return;
    QString sProfile;
    sProfile = pItem->text();
    sProfile += UTF8("님의 프로필 보기");
    pPrivacyLockedRightClick->changeItem ( 1, sProfile );

    QRegExp rx("[^<(]+@[^>)]+");
    rx.search( lockedUserListBox-> currentText () );
    QString sEmail = rx.cap(0);

    const BuddyList* pBuddyList = pCurrentAccount->getBuddyList();
    Buddy *pBuddy = pBuddyList->getBuddyByID( sEmail );

#ifdef NETDEBUG
    kdDebug() << "Email:[" << sEmail << "]" << endl;
#endif

    sSelectPrivacyID = sEmail;
    nSelectPrivacyType = 2;
    bool bDelete = FALSE;
    if ( pBuddy->isFL() ) {
        pPrivacyLockedRightClick->setItemEnabled ( 2, FALSE );
    } else {
        pPrivacyLockedRightClick->setItemEnabled ( 2, TRUE );
        if ( !pBuddy->isRL() ) {
            bDelete = TRUE;
        }
    }
    pPrivacyLockedRightClick->setItemEnabled ( 4, bDelete );

    pPrivacyLockedRightClick->popup( pPoint );
}

void Preference::slotAllowListBoxClicked( int nID ) {
    Q_UNUSED( nID );
    lockedUserListBox->clearSelection();
}

void Preference::slotLockedListBoxClicked( int nID ) {
    Q_UNUSED( nID );
    allowUserListBox->clearSelection();
}

void Preference::keyPressEvent(QKeyEvent * e) {
    if ( e->key() != Key_Return ) { /*! Only Return */
        QDialog::keyPressEvent(e);
    }
}

void Preference::setDefaultWebBrowser(const QString & sDefault) {
    stConfig.defaultwebbrowser = sDefault;
    lineEdit100->setText( stConfig.defaultwebbrowser );
    // kdDebug() << "Default : [" << stConfig.defaultwebbrowser << "]" << endl;
    // config->sync();
    return;
}

void Preference::setUseDefaultWebBrowser(bool bUse) {
    stConfig.usedefaultwebbrowser = bUse;
    groupBox100->setChecked( stConfig.usedefaultwebbrowser );
    // config->sync();
    return;
}

bool Preference::isUseDefaultWebBrowser() {
    return groupBox100->isChecked();
}

QString Preference::getDefaultWebBrowser() {
    return lineEdit100->text();
}

void Preference::slotOpenDefaultBrowserSelector() {
    QString sFileName = QFileDialog::getSaveFileName( QDir::homeDirPath(),
                        "All(*)",
                        this,
                        UTF8("웹 브라우저 선택"),
                        UTF8("기본으로 설정할 웹 브라우저를 선택하십시오.") );

    if ( sFileName != QString::null ) {
        setDefaultWebBrowser( sFileName );
    }

    return;
}

void Preference::slotUseDefaultWebBrowser(bool bUse) {
    setUseDefaultWebBrowser( bUse );
    return;
}

void Preference::slotUseAllSound(bool bUse) {
    /*! 알림/소리 */
    config->setGroup( "Config_Alarm_Sound" );
    stConfig.useallsound= bUse;
    config->writeEntry( "UseSound", stConfig.useallsound );

    config->setGroup("Sound");
    stConfig.usesound = bUse;
    stConfig.usebuddyconnectsound = bUse;
    stConfig.usememorecievesound = bUse;
    stConfig.usefilesenddonesound = bUse;
    stConfig.usemyloginsound = bUse;
    stConfig.usestartchatsound = bUse;
    stConfig.useminihompynewsound = bUse;

    config->writeEntry( "UseSound", stConfig.usesound );
    config->writeEntry( "IsOnFriendConnect", stConfig.usebuddyconnectsound );
    config->writeEntry( "IsOnReceiveMemo", stConfig.usememorecievesound );
    config->writeEntry( "IsOnFileTransferDone", stConfig.usefilesenddonesound );
    config->writeEntry( "IsOnLogin", stConfig.usemyloginsound );
    config->writeEntry( "IsOnChatRequest", stConfig.usestartchatsound );
    config->writeEntry( "IsOnMiniHompyNew", stConfig.useminihompynewsound );

    KStandardDirs   *dirs   = KGlobal::dirs();
    QString sSoundPath = dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/sound/" );

    stConfig.buddyconnectsoundpath = config->readEntry("FriendConnect", sSoundPath + "buddy_login.wav");
    stConfig.memorecievesoundpath = config->readEntry("ReceiveMemo", sSoundPath + "recv_memo.wav");
    stConfig.filesenddonesoundpath = config->readEntry("FileTransferDone", sSoundPath + "trans_comp.wav");
    stConfig.myloginsoundpath = config->readEntry("Login", sSoundPath + "login.wav");
    stConfig.startchatsoundpath = config->readEntry( "ChatRequest", sSoundPath + "recv_chat.wav" );
    stConfig.minihompynewsoundpath = config->readEntry( "MiniHompyNew", sSoundPath + "minihompy.wav" );

    config->writeEntry( "FriendConnect", stConfig.buddyconnectsoundpath );
    config->writeEntry( "ReceiveMemo", stConfig.memorecievesoundpath );
    config->writeEntry( "FileTransferDone", stConfig.filesenddonesoundpath );
    config->writeEntry( "Login", stConfig.myloginsoundpath );
    config->writeEntry( "ChatRequest", stConfig.startchatsoundpath );
    config->writeEntry( "MiniHompyNew", stConfig.minihompynewsoundpath );
    config->sync();
}

#include "preferenceinterface.moc"

