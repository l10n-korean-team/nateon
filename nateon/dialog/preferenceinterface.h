/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PREFERENCE_H
#define PREFERENCE_H

#include <klocale.h>
#include <kstandarddirs.h>
#include <qvariant.h>
#include <qdialog.h>
#include <qfontdialog.h>
#include <kmessagebox.h>
#include <qpopupmenu.h>
#include <kiconloader.h>
#include <kaboutdata.h>

#include "shapewidget.h"
#include "../currentaccount.h"
#include "soundconfig.h"
#include "../buddy/buddy.h"
#include "../buddy/buddylist.h"
#include "../buddy/group.h"
#include "../buddy/grouplist.h"
#include "./emoticonselector.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QListBox;
class QListBoxItem;
class QWidgetStack;
class QWidget;
class QLabel;
class QFrame;
class QCheckBox;
class QLineEdit;
class QPushButton;
class QGroupBox;
class QComboBox;
class QTextEdit;
class ShapeButton;

class Preference : public QDialog {
    Q_OBJECT

public:
    Preference( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0);
    ~Preference();

    KConfig* config;
    QListBox* listBox;
    QWidgetStack* widgetStack2;
    QWidget* WStackPage;
    QLabel* pixmapLabel1;
    QLabel* textLabel1;
    QLabel* labelBrowser;
    QFrame* line1;
    QCheckBox* autoRunCheckBox;
    QCheckBox* hideCheckBox;
    QCheckBox* topCheckBox;
    QCheckBox* upgradeCheckBox;
    QCheckBox* emoticonCheckBox;
    QLabel* textLabelDoubleClickAction;
    QComboBox* buddyDoubleClickComboBox;
    QLabel* textLabel1_2;
    QFrame* line2;
    QLabel* textLabel2;
    QLineEdit* saveFileLineEdit;
    QPushButton* saveFileButton;
    QWidget* WStackPage_2;
    QLabel* pixmapLabel2;
    QLabel* textLabel1_3;
    QFrame* line3;
    QLabel* textLabel2_2;
    ShapeButton* nickEmoticonButton;
    QLineEdit* nickLineEdit;
    QLabel* textLabel3;
    QFrame* line4;
    QCheckBox* timeAwayCheckBox;
    QLineEdit* lineEdit10;
    QLabel* textLabel1_6;
    QLabel* textLabel4;
    QFrame* line5;
    QLabel* textLabel5;
    QPushButton* profileButton;
    QWidget* WStackPage_3;
    QLabel* pixmapLabel1_3;
    QLabel* textLabel1_4;
    QFrame* line6;
    QGroupBox* dwgroupBox1;
    QCheckBox* alarmConnectCheckBox;
    QCheckBox* alarmChatCheckBox;
    QGroupBox* groupBox2;
    QCheckBox* alarmMailCheckBox;
    QCheckBox* alarmMemoCheckBox;
    QComboBox* alarmMemoComboBox;
    QLabel* textLabel2_3;
    QFrame* line7;
    QCheckBox* useAlarmCheckBox;
    QPushButton* extendOptionButton;
    QWidget* WStackPage_4;
    QLabel* pixmapLabel2_2;
    QLabel* textLabel3_2;
    QFrame* line8;
    QGroupBox* groupBox3;
    QCheckBox* viewAwayMessageCheckBox;
    QCheckBox* viewBusyMessageCheckBox;
    ShapeButton* pixmapLabel3;
    ShapeButton* pixmapLabel4;
    QTextEdit* textEdit1;
    QGroupBox* groupBox4;
    QComboBox* chatSaveFileComboBox;
    QLabel* textLabel5_2;
    QGroupBox* groupBox5;
    QCheckBox* useEmoticonCheckBox;
    QLabel* textLabel6;
    QFrame* line9;
    QGroupBox* groupBox6;
    QCheckBox* replayCheckBox;
    QCheckBox* allReplyCheckBox;
    QCheckBox* forwardCheckBox;
    QWidget* WStackPage_5;
    QLabel* pixmapLabel1_4;
    QLabel* textLabel1_5;
    QFrame* line10;
    QLabel* textLabel2_4;
    QFrame* frame3;
    QCheckBox* allowUserCheckBox;
    QLabel* textLabel1_7;
    QLabel* textLabel2_5;
    QListBox* allowUserListBox;
    QPushButton* lockButton;
    QPushButton* unlockButton;
    QListBox* lockedUserListBox;
    QCheckBox* receiveFromFriendCheckBox;
    QPushButton* profileViewButton;
    QWidget* WStackPage_6;
    QLabel* pixmapLabel2_3;
    QLabel* textLabel4_2;
    QFrame* line11;
    QLabel* textLabel5_3;
    QCheckBox* encryptChatCheckBox;
    QWidget* WStackPage_7;
    QLabel* pixmapLabel3_2;
    QLabel* textLabel6_2;
    QLabel* textLabel7;
    QFrame* line12;
    QFrame* frame4;
    QCheckBox* useProxyCheckBox;
    QLabel* textLabel8;
    QComboBox* typeComboBox;
    QLabel* textLabel9;
    QLineEdit* serverLineEdit;
    QLabel* textLabel11;
    QLineEdit* portLineEdit;
    QLabel* textLabel10;
    QLineEdit* userLineEdit;
    QLabel* textLabel12;
    QLineEdit* passwordLineEdit;
    QLabel* textLabel13;
    QFrame* line13;
    QLabel* textLabel14;
    QLineEdit* p2pPortLineEdit;
    QLabel* textLabel15;
    QWidget* WStackPage_8;
    QLabel* pixmapLabel4_2;
    QLabel* textLabel16;
    QFrame* line14;
    QLabel* textLabel17;
    QListBox* listBox4;
    QFrame* frame5;
    QLabel* textLabel18;
    QLabel* textLabel19;
    QCheckBox* startLoginCheckBox;
    QPushButton* installButton;
    QPushButton* upgradeButton;
    QPushButton* removeButton;
    QLabel* pixmapLabel5;
    QLabel* textLabel20;
    QFrame* line15;
    QLabel* textLabel21;
    QFrame* frame6;
    QLabel* textLabel22;
    QLineEdit* cyIDLineEdit;
    QPushButton* syncButton;
    QLabel* textLabel23;
    QLineEdit* cyPasswordLineEdit;
    QPushButton* findIDPasswordButton;
    QPushButton* gaibButton;
    QPushButton* whatminihompyButton;
    QLabel* textLabel24;
    QLabel* textLabel25;
    QFrame* line16;
    QLabel* textLabel26;
    QLabel* textLabel27;
    QComboBox* gonggaeComboBox;
    QLabel* textLabel28;
    QFrame* line17;
    QLabel* textLabel29;
    QComboBox* alarmMinimiComboBox;
    QCheckBox* alarmNewCheckBox;
    QCheckBox* alarmMemoEtcCheckBox;
    QPushButton* buttonOk;
    QPushButton* buttonCancel;
    QPushButton* buttonHelp;

    QPopupMenu *pPrivacyUnlockedRightClick;
    QPopupMenu *pPrivacyLockedRightClick;

    /*! 싸이월드 / 네이트 연동 */
    QWidget* WStackPage_9;

    void setPrivacyList();
    void updatePrivacy();
    void showChangeNick();
    void showMessageTab();

    void setCurrentAccount ( CurrentAccount* ca ) {
        pCurrentAccount = ca;
    }
    void showMemoReplyRole();

    virtual void accept();
    virtual void initialize();
    virtual void show();
    virtual void showSyncSetup();

    virtual void setOffline() {
        bOnline = FALSE;
    };
    virtual void setOnline() {
        bOnline = TRUE;
    };
    bool isOnline() {
        return bOnline;
    }
    void setProxyEnabled( int nType );
    void setProxyDisabled();
    // void setPrivaryListProfile( const QString & );

    void setDefaultWebBrowser( const QString &sDefault );
    void setUseDefaultWebBrowser( bool bUse );
    bool isUseDefaultWebBrowser();
    QString getDefaultWebBrowser();

protected:
    void keyPressEvent ( QKeyEvent * e );

    QVBoxLayout* PreferenceLayout;
    QHBoxLayout* layout75;
    QVBoxLayout* WStackPageLayout;
    QSpacerItem* spacer12;
    QHBoxLayout* layout37;
    QHBoxLayout* layout39;
    QSpacerItem* spacer10;
    QVBoxLayout* layout4;
    QHBoxLayout* layout76;
    QHBoxLayout* layout38;
    QHBoxLayout* layout40;

    QHBoxLayout* layout400;
    QGroupBox* groupBox100;
    QPushButton* pushButton100;
    QLineEdit* lineEdit100;
    QLabel* textLabel100;
    QFrame* line100;

    QSpacerItem* spacer11;
    QHBoxLayout* layout5;
    QVBoxLayout* WStackPageLayout_2;
    QSpacerItem* spacer14;
    QHBoxLayout* layout42;
    QHBoxLayout* layout7;
    QSpacerItem* spacer2;
    QHBoxLayout* layout43;
    QHBoxLayout* layout47;
    QSpacerItem* spacer15;
    QHBoxLayout* layout44;
    QHBoxLayout* layout45;
    QSpacerItem* spacer13;
    QVBoxLayout* WStackPageLayout_3;
    QSpacerItem* spacer17;
    QHBoxLayout* layout48;
    QVBoxLayout* dwgroupBox1Layout;
    QVBoxLayout* dwgroupBox2Layout;
    QHBoxLayout* layout100;
    QVBoxLayout* layout9;
    QHBoxLayout* groupBox2Layout;
    QSpacerItem* spacer31;
    QVBoxLayout* layout23;
    QHBoxLayout* layout22;
    QHBoxLayout* layout49;
    QHBoxLayout* layout50;
    QSpacerItem* spacer16;
    QVBoxLayout* WStackPageLayout_4;
    QHBoxLayout* layout51;
    QVBoxLayout* groupBox3Layout;
    QHBoxLayout* layout72;
    QSpacerItem* spacer33;
    QHBoxLayout* layout71;
    QSpacerItem* spacer32;
    QVBoxLayout* groupBox4Layout;
    QHBoxLayout* layout73;
    QSpacerItem* spacer35;
    QHBoxLayout* groupBox5Layout;
    QSpacerItem* spacer34;
    QHBoxLayout* layout52;
    QVBoxLayout* groupBox6Layout;
    QHBoxLayout* layout74;
    QSpacerItem* spacer36;
    QSpacerItem* spacer37;
    QSpacerItem* spacer38;
    QVBoxLayout* WStackPageLayout_5;
    QHBoxLayout* layout53;
    QVBoxLayout* frame3Layout;
    QVBoxLayout* layout59;
    QHBoxLayout* layout58;
    QSpacerItem* spacer24;
    QHBoxLayout* layout57;
    QVBoxLayout* layout56;
    QSpacerItem* spacer22;
    QSpacerItem* spacer23;
    QVBoxLayout* layout13;
    QHBoxLayout* layout54;
    QSpacerItem* spacer19;
    QHBoxLayout* layout55;
    QSpacerItem* spacer20;
    QVBoxLayout* WStackPageLayout_6;
    QSpacerItem* spacer26;
    QHBoxLayout* layout62;
    QVBoxLayout* WStackPageLayout_7;
    QSpacerItem* spacer28;
    QHBoxLayout* layout63;
    QVBoxLayout* frame4Layout;
    QVBoxLayout* layout25;
    QHBoxLayout* layout24;
    QSpacerItem* spacer3;
    QHBoxLayout* layout18;
    QHBoxLayout* layout19;
    QHBoxLayout* layout64;
    QHBoxLayout* layout65;
    QSpacerItem* spacer27;
    QVBoxLayout* WStackPageLayout_8;
    QHBoxLayout* layout69;
    QHBoxLayout* layout68;
    QVBoxLayout* frame5Layout;
    QHBoxLayout* layout67;
    QHBoxLayout* layout26;
    QVBoxLayout* frame6Layout;
    QVBoxLayout* layout35;
    QHBoxLayout* layout33;
    QSpacerItem* spacer9;
    QHBoxLayout* layout34;
    QSpacerItem* spacer8;
    QHBoxLayout* layout32;
    QSpacerItem* spacer7;
    QHBoxLayout* layout27;
    QHBoxLayout* layout28;
    QSpacerItem* spacer4;
    QHBoxLayout* layout29;
    QHBoxLayout* layout30;
    QSpacerItem* spacer5;
    QHBoxLayout* layout31;
    QSpacerItem* spacer6;
    QVBoxLayout* layout16;
    QHBoxLayout* layout13_2;
    QSpacerItem* Horizontal_Spacing2;
    QString         sPicsPath;

    /*! 싸이월드 / 네이트 연동 */
    QVBoxLayout* WStackPageLayout_9;
    SoundConfig* pSoundSelect;
    CurrentAccount* pCurrentAccount;

private:
    EmoticonSelector *pEmoticonSelector;
    bool bOnline;
    QString sSelectPrivacyID;
    int nSelectPrivacyType;

protected slots:
    virtual void languageChange();
    void slotSetNick ( const QString &sNick );
    void slotSetDownPath ( const QString &sPath );

    void slotOpenSaveFile();
    void slotShowSoundSelectDialog();
    void slotAddLock();
    void slotAddUnlock();

    void slotEmoticonDialog();
    void slotPutEmoticon(const QString & sText);
    void slotCheckNum(  const QString & sNum );
    void help();
    void slotEditEnable( bool bTrue );
    void slotFontDialog();
    void lockOther( bool bAllow );
    void slotProxyType( int nID );
    void slotUseProxy( bool bUse );

    void slotShowProfile();
    void slotAddEtcList();
    void slotDelete();

    void slotAllowRightClicked( QListBoxItem *pItem, const QPoint & pPoint );
    void slotLockedRightClicked( QListBoxItem *pItem, const QPoint & pPoint );

    void slotAllowListBoxClicked( int nID );
    void slotLockedListBoxClicked( int nID );
    void slotOpenDefaultBrowserSelector();
    void slotUseDefaultWebBrowser( bool bUse );
    void slotUseAllSound( bool bUse );

signals:
    void changeNick ( QString );
    void unlockList( QStringList& );
    void lockList( QStringList& );
    void alwaysTop( bool );
    void refreshBuddyList();
    void onlyPermitChat( bool );
    void onlyFriendMemo( bool );
    void updateAwayTime();
    void sendRealDelete( const QString &sID );
    void addEtc( const QString &sID );
    void sendLockBuddy( const QString &sID );
    void sendUnlockBuddy( const QString &sID );
    void emoticonShow( bool );
    void changeWebBrowser( const QString &sBrowser );
};

#endif // PREFERENCE_H
