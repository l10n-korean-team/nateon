/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SOUNDSELECTDIALOG_H
#define SOUNDSELECTDIALOG_H

#include <qvariant.h>
#include <qdialog.h>
#include <klistview.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QGroupBox;
class KListView;
class QListViewItem;
class KURLLabel;
class QLineEdit;
class QPushButton;

class SoundSelectDialog : public QDialog {
    Q_OBJECT

public:
    SoundSelectDialog( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~SoundSelectDialog();

    QGroupBox* soundGroupBox;
    KListView* soundListView;
    KURLLabel* soundDownKURLLabel;
    QLineEdit* filePathLineEdit;
    QPushButton* playButton;
    QPushButton* selectFileButton;
    //    QPushButton* buttonHelp;
    QPushButton* buttonOk;
    QPushButton* buttonCancel;

protected:
    QVBoxLayout* SoundSelectDialogLayout;
    QVBoxLayout* soundGroupBoxLayout;
    QHBoxLayout* layout4;
    QSpacerItem* spacer2;
    QHBoxLayout* layout3;
    QHBoxLayout* Layout1;
    QSpacerItem* Horizontal_Spacing2;

protected slots:
    virtual void languageChange();
};

#endif // SOUNDSELECTDIALOG_H
