/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "knateoncommon.h"
#include "logoutview.h"

LogoutView::LogoutView(QWidget *parent, const char *name)
        : loginviewinterface(parent, name) {
    KStandardDirs   *dirs   = KGlobal::dirs();
    QString         sPicsPath( dirs->findResource( "data", QString( KGlobal::instance()->aboutData()->appName() ) + "/pics/" ) );

    layout6 = new QVBoxLayout( 0, 11, 6, "layout6");
    spacer10 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout6->addItem( spacer10 );

    layout2 = new QHBoxLayout( 0, 0, 6, "layout2");
    spacer4 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout2->addItem( spacer4 );

    textLabel1 = new QLabel( this, "textLabel1" );
    textLabel1->setPaletteForegroundColor( QColor( 34, 102, 153 ) );
    QFont textLabel1_font(  textLabel1->font() );
    textLabel1_font.setBold( TRUE );
    textLabel1->setFont( textLabel1_font );

    layout2->addWidget( textLabel1 );
    spacer5 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout2->addItem( spacer5 );
    layout6->addLayout( layout2 );
    spacer1 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout6->addItem( spacer1 );

    layout3 = new QHBoxLayout( 0, 0, 6, "layout3");
    spacer6 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout3->addItem( spacer6 );

    pushButton1 = new ShapeButton( this, sPicsPath + "rogin_bt_login_nor.bmp" );
    pushButton1->setMouseOverShape( sPicsPath + "rogin_bt_login_ov.bmp" );
    pushButton1->setPressedShape( sPicsPath + "rogin_bt_login_down.bmp" );
    pushButton1->setMinimumSize( QSize( 96, 37 ) );
    pushButton1->setMaximumSize( QSize( 96, 37 ) );
    pushButton1->setCursor(Qt::PointingHandCursor);

    layout3->addWidget( pushButton1 );
    spacer7 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout3->addItem( spacer7 );
    layout6->addLayout( layout3 );
    spacer3 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed );
    layout6->addItem( spacer3 );

    layout4 = new QHBoxLayout( 0, 0, 6, "layout4");
    spacer8 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer8 );

    textLabel2 = new LinkLabel( this, "textLabel2" );
    textLabel2->setText( UTF8( "<qt><nobr><u>다른 아이디로 로그인</u></nobr></qt>" ) );
    /*!
      textLabel2->setMinimumSize( QSize( 130, 0 ) );
      textLabel2->setMaximumSize( QSize( 130, 32767 ) );
    */
    textLabel2->setCursor(Qt::PointingHandCursor);
    textLabel2->setPaletteForegroundColor( QColor( 255, 255, 255 ) );
    textLabel2->setMouseOverColor( QColor( 140, 140, 140) );
    textLabel2->setMouseLeaveColor( QColor( 255, 255, 255 ) );
    textLabel2->setAutoResize ( true ); // adjustSize();

    layout4->addWidget( textLabel2 );
    spacer9 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer9 );
    layout6->addLayout( layout4 );
    spacer11 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout6->addItem( spacer11 );

    loginviewinterfaceLayout->addLayout( layout6 );
    /*
      layout26 = new QVBoxLayout( 0, 0, 6, "layout26");
      spacer34 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
      layout26->addItem( spacer34 );
      loginviewinterfaceLayout->addLayout( layout26 );
    */
    textLabel1->setText( UTF8( "xxx@nate.com" ) );


}


LogoutView::~LogoutView() {
}


#include "logoutview.moc"
