/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <qwidget.h>

/**
   @author Doo-Hyun Jang
*/
class Account : public QWidget {
    Q_OBJECT
public:
    Account();

    virtual ~Account();

    enum PROXYTYPE { TYPE_NONE = 0, TYPE_SOCKS4, TYPE_SOCKS5, TYPE_HTTP };

    virtual void copyAccount( const Account *account );

    bool getUseProxy() const;
    const QString& getProxyPassword() const;
    const int& getProxyPort() const;
    const QString& getProxyServer() const;
    PROXYTYPE getProxyType() const; // const int& getProxyType() const;
    const QString& getProxyUID() const;
    const QString getID() const;
    const QString& getPassword() const;
    const QString getDPip() const;
    const QString getAuthTicket() const;
    const int getDPport() const;
    const int& getIdleTime() const {
        return idleTime_;
    }
    bool getUseIdleTimer() const {
        return useIdleTimer_;
    }
    const QString getOTP() const;

    void setLoginInformation(QString sID, QString sPassword);
    void setOTP( QString sOTP );
    void setDPInformation(QString sIP, int nPort);
    void setIdleTime( int idleTime );
    void setUseIdleTimer( bool useIdleTimer ) {
        useIdleTimer_ = useIdleTimer;
    }

    void setProxyType( PROXYTYPE eType ) {
        m_proxyType = eType;
    }
    void setProxyServer( QString sServer ) {
        m_sProxyServer = sServer;
    }
    void setProxyPort( int nPort ) {
        m_nProxyPort = nPort;
    }
    void setProxyID( QString sID ) {
        m_sProxyUID = sID;
    }
    void setProxyPassword( QString sPassword ) {
        m_sProxyPassword = sPassword;
    }
    void setAuthTicket( QString sTicket ) {
        m_sAuthTicket = sTicket;
    }

private:
    bool m_bUseProxy;

    PROXYTYPE m_proxyType;
    QString m_sProxyServer;
    int m_nProxyPort;
    QString m_sProxyUID;
    QString m_sProxyPassword;

    QString m_sID;
    QString m_sPassword;
    QString m_sAuthTicket;
    QString m_sOTP;

    QString m_sDPip;
    int m_nDPport;
    int idleTime_;
    bool useIdleTimer_;

signals:
    void changedTimerSettings();
};
#endif
