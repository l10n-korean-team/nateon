/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include <qdragobject.h>

#include "buddylistview.h"
#include "contactlist.h"
#include "contactroot.h"
#include "contactbase.h"

BuddyListView::BuddyListView(QWidget* parent, const char* name/*=0*/) :
        KListView(parent, name) {
    setAcceptDrops( TRUE );
}

BuddyListView::~BuddyListView() {
}

void BuddyListView::contentsMousePressEvent(QMouseEvent* e) {
    QPoint vp = contentsToViewport(e->pos());
    ContactList* item = (ContactList*)itemAt(vp);
    if (item && item->hasHompy() && item->richText() &&
            item->parent()) {
        item->richText()->adjustSize();

        //printf("korone>> e->pos() x:%d y:%d\n",
        //	e->pos().x(), e->pos().y());
        int marginX = treeStepSize() * item->depth();
        //printf("marginX:%d\n", marginX);

        // 18: hompy button size
        int hompyX1 = marginX + item->richText()->width() - 18;
        int hompyX2 = hompyX1 + 18;

        //printf("korone>> x1:%d x2:%d\n", hompyX1, hompyX2);
        if (e->pos().x() >= hompyX1 && e->pos().x() <= hompyX2) {
            emit clickedHompy(item);
            return;
        }
    }

    KListView::contentsMousePressEvent(e);
}

void BuddyListView::contentsDropEvent(QDropEvent * e) {
    e->setPoint( contentsToViewport( e->pos() ) );
    ContactBase *c = dynamic_cast<ContactBase*>( KListView::itemAt( e->pos() ) );
    if ( c ) {
        if ( c->getType() == ContactBase::Group ) {
            ContactRoot *cr = dynamic_cast<ContactRoot*>( c );
            if ( cr ) {
                const QString sGroupName( cr->getName() );
                emit moveBuddies( sGroupName );
                kdDebug() << "1. BuddyListView::contentsDropEvent : " << sGroupName << endl;
            }
        } else if ( c->getType() == ContactBase::Buddy ) {
            ContactList *cl = dynamic_cast<ContactList*>( c );
            if ( cl ) {
                ContactRoot *cr = dynamic_cast<ContactRoot*>( cl->getGroupItem() );
                if ( cr ) {
                    const QString sGroupName( cr->getName() );
                    emit moveBuddies( sGroupName );
                    kdDebug() << "2. BuddyListView::contentsDropEvent : " << sGroupName << endl;
                }
            }
        }
    }
}

void BuddyListView::contentsDragEnterEvent(QDragEnterEvent * e) {
    if ( QTextDrag::canDecode( e )
            || QImageDrag::canDecode( e )
            || QUriDrag::canDecode( e ) ) {
        e->accept();
    }

    // Give the user some feedback...
    QString t;
    const char *f;
    for ( int i=0; (f=e->format( i )); i++ ) {
        if ( *(f) ) {
            if ( !t.isEmpty() )
                t += "\n";
            t += f;
        }
    }
    KListView::contentsDragEnterEvent( e );
}

void BuddyListView::contentsDragMoveEvent(QDragMoveEvent * e) {
    e->acceptAction( e->action() == QDropEvent::Copy );
    KListView::contentsDragMoveEvent( e );
}

#include "buddylistview.moc"
