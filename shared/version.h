#ifndef VERSION_H
#define VERSION_H

namespace AutoVersion {

//Date Version Types
static const char DATE[] = "12";
static const char MONTH[] = "06";
static const char YEAR[] = "2009";
static const double UBUNTU_VERSION_STYLE = 9.06;

//Software Status
static const char STATUS[] = "Beta";
static const char STATUS_SHORT[] = "b";

//Standard Version Type
static const long MAJOR = 1;
static const long MINOR = 1;
static const long BUILD = 0;
static const long REVISION = 301;

//Miscellaneous Version Types
static const long BUILDS_COUNT = 26;
#define RC_FILEVERSION 1,0,1,244
#define RC_FILEVERSION_STRING "1, 0, 1, 244\0"
static const char FULLVERSION_STRING[] = "1.0.1.244";

//SVN Version
static const char SVN_REVISION[] = "244";
static const char SVN_DATE[] = "2009-06-12T11:10:50.217246Z";

//These values are to keep track of your versioning state, don't modify them.
static const long BUILD_HISTORY = 6;


}
#endif //VERSION_h
