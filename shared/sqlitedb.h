/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef SQLITEDB_H
#define SQLITEDB_H

#include <kdebug.h>
#include <qobject.h>
#include <qstringlist.h>
#include <qfile.h>
#include <quuid.h>
#include <qdatetime.h>
#include <qmap.h>
#include <qvaluelist.h>
#include <qregexp.h>

#include <sqlite3.h>

typedef QMap<int, int> rowIdMap;
typedef QValueList<QStringList> rowList;

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class SQLiteDB : public QObject {
    Q_OBJECT
public:
    SQLiteDB( QObject *parent = 0, const char *name = 0 );
    ~SQLiteDB();
    int callback( void *NotUsed, int argc, char **argv, char **azColName );
    void execOne( const QString &sFile, const QString &sCommand );
    void execAll( const QString &sFile, QStringList &slCommand );
    void createMemoDB( const QString &sPath );
    void createChatDB( const QString &sPath );
    void createUserDB( const QString &sPath );
    void createP2PDB( const QString &sPath );

    /*!                       User                Data      */
    void saveChatBox( QString sCHATUSERS, QString sBODYDATA );
    void saveMemoInbox( QString sUUID, QString sSUSER, QString sRUSER, QString sBODYDATA, QString sIsUnread );
    void saveMemoInbox(QMap<QString, QString> &mData);
    void saveMemoOutbox( const QString & sSUSER, QString sRUSER, QString sBODYDATA);
    void deleteMemoInbox( const QString &sUUID );
    rowList getRecords( const QString & sFile, const QString & sQuery );
    int getNewMemoCount();

private:
    sqlite3 *db;
    QString sMemoFolders;
    QString sMemoInbox;
    QString sMemoOutbox;
    QString sChatFolders;
    QString sChatCount;
    QString sChatBox;
    QString sUserDB;
    QString sP2PDB;

    int nChatMID;
    int nMemoMID;
    rowList browseRecs;
    rowIdMap idmap;
};

#endif
