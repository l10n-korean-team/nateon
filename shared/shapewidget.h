//
// Description:
//  Interfaces for ShapeWidget and deriver classes ShapeForm and ShapeButton
//
// Author: Valentin Heinitz, <heinitz(at)freenet(dot)de>, (C) 2005
//
// Copyright: This is GPL software,
//            See License.txt file that comes with this distribution
//
//
#ifndef SHAPEWIDGET_H
#define SHAPEWIDGET_H

#include <qwidget.h>
#include <qimage.h>
#include <qtimer.h>

class QString;

/*!
  Widget wich take a shape of nontransparent areas of an image.
*/
class ShapeWidget : public QWidget {
public:
    ShapeWidget(QWidget *);
    ShapeWidget( QWidget *parent, const char *name, WFlags f );
    ShapeWidget(QWidget *, const QImage  &, long=-1);
    ShapeWidget(QWidget *, const QString &, long=-1);
    void setShape( const QImage  &, long =-1);
    void setShape( const QString &, long =-1);

    static void setWidgetShape( QWidget *, const QString &, long =-1);
    static void setWidgetShape( QWidget *, const QImage  &, long =-1);

};

/*!
  ShapeForm - Widget for use as the QApplication's main widget.
  Since the mainWidget may have no window manager panel,
  ShapeForm can be moved by clicking with mouse at any visible
  position and dragging.
*/
class ShapeForm : public ShapeWidget {
    Q_OBJECT
public:
    ShapeForm(QWidget *);
    ShapeForm(QWidget *, const QImage  &, long=-1);
    ShapeForm(QWidget *, const QString &, long=-1);
protected:
    /*!
      Reimplemented for moving widget when clicked
    */
    virtual void mouseMoveEvent( QMouseEvent *);
    virtual void mousePressEvent( QMouseEvent *);
    virtual void mouseReleaseEvent( QMouseEvent *);
private:
    QPoint _mpos;
    bool _mpressed;
};

/*!
  ShapeButton. Switches between two pixmaps if clicked by mouse.
  Provides parametrized signal clicked(int id). The id which is sent by the
  signal can be set with setId(int).
*/
class ShapeButton : public ShapeWidget {
    Q_OBJECT
public:
    ShapeButton(QWidget *);
    ShapeButton(QWidget *, const QImage  &, long=-1);
    ShapeButton(QWidget *, const QString &, long=-1);
    void setShape( const QImage  &, long =-1);
    void setShape( const QString &, long =-1);
    void setPressedShape( const QImage  &, long =-1);
    void setPressedShape( const QString &, long =-1);
    void setMouseOverShape( const QImage  &, long =-1);
    void setMouseOverShape( const QString &, long =-1);
    void setId(int id) {
        _id=id;
    }
    void setDisabled();
    void setEnabled();
protected:
    /*!
      Reimplemented for setting pressed/released-shape and emitting clicked()
    */
    virtual void mousePressEvent( QMouseEvent *);
    virtual void mouseReleaseEvent( QMouseEvent *);
    virtual void enterEvent( QEvent * );
    virtual void leaveEvent( QEvent * );
signals:
    void clicked( int );
    void clicked();

private:
    int _id;
    bool bEnable;
    QImage _rimg;
    QImage _pimg;
    QImage _oimg;
};

#endif // SHAPEWIDGET_H
