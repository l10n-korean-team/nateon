/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LINKLABEL_H
#define LINKLABEL_H

#include <qlabel.h>

/**
   @author Doo-Hyun Jang <ring0320@nate.com>
*/
class LinkLabel : public QLabel {
    Q_OBJECT
public:
    LinkLabel(QWidget *parent = 0, const char *name = 0);
    ~LinkLabel();

    void setMouseOverColor( QColor color );
    void setMouseLeaveColor( QColor color );

    void setDisabled();
    void setEnabled();

    void init();

protected:
    virtual void mousePressEvent( QMouseEvent *);
    virtual void mouseReleaseEvent( QMouseEvent *);
    virtual void enterEvent( QEvent * );
    virtual void leaveEvent( QEvent * );

signals:
    void clicked();

private:
    bool bEnable;
    QColor _ov;
    QColor _lv;
};

#endif
