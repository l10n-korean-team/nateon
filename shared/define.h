
#ifndef NATEON_DEFINE_H
#define NATEON_DEFINE_H

#include <qdict.h>
#include <qstring.h>

typedef QDict<QString> NMStringDict;

enum MEMO_EVENT { NW,RE, AL, FW };

#define PVER_VERSION "3.0"
#define PVER_LANG_OS "ko.linuxm"
#define LSIN_ENC "UTF8"

#endif
