/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef LNMUTILS_H
#define LNMUTILS_H

#include <krun.h>
#include <qfile.h>
#include <qdir.h>
#include <qobject.h>
#include <qstringlist.h>
#include <kdebug.h>
#include <qhttp.h>
#include <qurloperator.h>
#include <qnetwork.h>
#include <qregexp.h>

class KAboutData;

class LNMUtils : public QObject {
    Q_OBJECT
public:
    LNMUtils(QObject *parent = 0, const char *name = 0);

    ~LNMUtils();
    static void openURL( const QString &sURL );
    static void sendMail( const QString &sEMails );
    static void openBridgeURL( const QString &BrCode, const QString &BrTicket, const QString &BrParam);
    static void openBridgeURL( const QString &BrCode, const QString &BrTicket, const QString &BrParam, bool BrToolBarYN, unsigned int BrWidth, unsigned int BrHeight );
    static KAboutData *getAboutData( const char *about );
};

#endif
