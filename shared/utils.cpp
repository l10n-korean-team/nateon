/***************************************************************************
 *   Copyright (C) 2008 by SK Communications.                              *
 *   http://kldp.net/projects/nateon/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kaboutdata.h>
#include <stdlib.h>
#include "utils.h"
#include "../nateon/util/common.h"
#include "version.h"

extern nmconfig stConfig;

LNMUtils::LNMUtils(QObject *parent, const char *name)
        : QObject(parent, name) {
}


LNMUtils::~LNMUtils() {
}

#include "utils.moc"

// extern char version[];

static char version[1000];

KAboutData *
LNMUtils::getAboutData( const char *about ) {
    using namespace AutoVersion;

    memset(version, 0x00, 1000);
    snprintf( version, 1000, "%d.%d.%d.%d %s", MAJOR, MINOR, BUILD, REVISION, STATUS );

    KAboutData *aboutData;
    aboutData = new KAboutData("nateon", I18N_NOOP("네이트온 메신저"),
                               I18N_NOOP(version),
                               I18N_NOOP(about),
                               KAboutData::License_Custom,
                               I18N_NOOP("(C) 2007 SK Communications, Inc."), 0, "http://nateonweb.nate.com");
    /*!
      개발자 정보
    */
    aboutData->addAuthor( I18N_NOOP("장두현"), I18N_NOOP("프로젝트 관리자"), "dh.jang@gmail.com", "http://kldp.net/projects/nateon/" );
    aboutData->addAuthor( I18N_NOOP("kakapapa"), I18N_NOOP("개발자"), "kakapapa@nate.com" );
    aboutData->addAuthor( I18N_NOOP("YoungHo Park"), I18N_NOOP("개발자"), "amesianx@nate.com" );
    aboutData->addAuthor( I18N_NOOP("박준규 (segfault87)"), I18N_NOOP("프로젝트 관리자"), "mastermind@planetmono.org", "http://planetmono.org" );
    aboutData->addAuthor( I18N_NOOP("Byoung Ywan Cho (korone)"), I18N_NOOP("개발자"), "korone@gmail.com", "http://korone.net" );
    aboutData->addAuthor( I18N_NOOP("김태훈 (kth321)"), I18N_NOOP("개발자"), "kth3321@gmail.com", "http://onestep.tistory.com/" );
    aboutData->addAuthor( I18N_NOOP("라병영 (la9527)"), I18N_NOOP("개발자"), "la9527@yahoo.co.kr" );
    aboutData->addAuthor( I18N_NOOP("강준석 (zingle)"), I18N_NOOP("개발자"), "zingle@gmail.com" );
    aboutData->addAuthor( I18N_NOOP("임인택 (litnsio2)"), I18N_NOOP("개발자"), "masterhand@gmail.com", "http://janbyul.com" );
    aboutData->addAuthor( I18N_NOOP("김동진 (tobetter)"), I18N_NOOP("개발자"), "tobetter@gmail.com" );
    aboutData->addAuthor( I18N_NOOP("신홍중 (whitepoo)"), I18N_NOOP("개발자"), "sjtetris@daum.net" );
    aboutData->addAuthor( I18N_NOOP("정재훈 (dicastyle)"), I18N_NOOP("개발자"), "dicastyle@nate.com" );
    aboutData->addAuthor( I18N_NOOP("조병탁 (jbtstyle)"), I18N_NOOP("개발자"), "jbtstyle@gmail.com" );
    aboutData->addAuthor( I18N_NOOP("전창현 (expectedtime)"), I18N_NOOP("개발자"), "cch870906@gmail.com" );
    aboutData->addAuthor( I18N_NOOP("노연진 (nyjin)"), I18N_NOOP("개발자"), "zacronan@naver.com" );
    aboutData->addAuthor( I18N_NOOP("유경진 (kyoji)"), I18N_NOOP("개발자"), "yookyoji@hotmail.com" );
    aboutData->addAuthor( I18N_NOOP("최수현 (soohyunc)"), I18N_NOOP("개발자"), "s.choi@hackers.org.uk", "http://www.cs.ucl.ac.uk/staff/S.Choi" );
    aboutData->addAuthor( I18N_NOOP("이지석 (morgana)"), I18N_NOOP("개발자"), "morgana@naver.com", "http://morgana.tistory.com" );
    aboutData->addAuthor( I18N_NOOP("박영호 (amesianx)"), I18N_NOOP("개발자"), "amesianx@nate.com" );
    aboutData->addAuthor( I18N_NOOP("박청규 (cheongpark)"), I18N_NOOP("개발자"), "ck0park@gmail.com" );
    aboutData->addAuthor( I18N_NOOP("윤석태 (luciferX2)"), I18N_NOOP("개발자"), "luciferX2@gmail.com" );
    aboutData->addAuthor( I18N_NOOP("신근태 (killme)"), I18N_NOOP("개발자"), "keuntae.shin@gmail.com" );

    /*!
      도움주신 분
    */
    aboutData->addCredit( I18N_NOOP("한혜진"), I18N_NOOP("기획"), "imyayayaya@gmail.com" );
    aboutData->addCredit( I18N_NOOP("이은아"), I18N_NOOP("기획"), "ena27@nate.com" );
    aboutData->addCredit( I18N_NOOP("이승명"), I18N_NOOP("디자이너"), "smdesign@nate.com", "http://www.leesm.com" );
    aboutData->addCredit( I18N_NOOP("박상완"), I18N_NOOP("버그트래킹서버 제공, 버그 수정"), "nakyup@gmail.com", "http://people.sarang.net" );
    aboutData->addCredit( I18N_NOOP("강정희"), I18N_NOOP("automake 부분"), "keizie@gmail.com" );
    aboutData->addCredit( I18N_NOOP("김가현"), I18N_NOOP("설치 도움"), "kfmes@kfmes.com", "http://kfmes.com" );
    aboutData->addCredit( I18N_NOOP("A Lee"), I18N_NOOP("Debian 패키징"), "alee@debian.org" );
    aboutData->addCredit( I18N_NOOP("Hyunsik Choi"), I18N_NOOP("Gentoo 패키징"), "c0d3h4ck@gmail.com" );
    aboutData->addCredit( I18N_NOOP("Barosl LEE"), I18N_NOOP("(k)ubuntu 패키징"), "", "http://barosl.com" );
    aboutData->addCredit( I18N_NOOP("황윤성 (hys545)"), I18N_NOOP("Asianux 패키징"), "hys545@dreamwiz.com" );
    aboutData->addCredit( I18N_NOOP("시노 삐 (sinovino)"), I18N_NOOP("(k)ubuntu 패키징"), "sinovino@sinovino.org" );
    aboutData->addCredit( I18N_NOOP("노상균 (freesky)"), I18N_NOOP("아치리눅스(Arch Linux) 패키징"), "freesky80@gmail.com" );
    aboutData->addCredit( I18N_NOOP("정하영 (sixt06)"), I18N_NOOP("openSUSE 패키징"), "sixt06@gmail.com" );
    aboutData->addCredit( I18N_NOOP("이상현 (sanghyun)"), I18N_NOOP("Fedoa 패키징"), "SangHyun.0925@gmail.com" );
    aboutData->addCredit( I18N_NOOP("나동희 (acsungcode)"), I18N_NOOP("Ubuntu 패키징"), "corona10@gmail.com", "http://acsungcode.tistory.com/" );
    aboutData->addCredit( I18N_NOOP("Hyun Seung Bum (orion203)"), I18N_NOOP("CentOS 패키징"), "orion_203@naver.com" );

    return aboutData;
}

// #define NETDEBUG 1

void LNMUtils::openURL( const QString &sURL ) {
    QString sCommand;
    if ( stConfig.usedefaultwebbrowser ) {
        sCommand = stConfig.defaultwebbrowser;
        sCommand += " ";
        sCommand += "'";
        sCommand += sURL;
        sCommand += "'";
#ifdef NETDEBUG
        kdDebug() << stConfig.defaultwebbrowser << " : "<< sCommand << endl;
#endif
        /*!
         * Bugs 304875 - modified by luciferX2@gmail.com, 20081103
         */
        //system( sCommand.data() );
        KRun::runCommand(sCommand);
    } else {
#ifdef NETDEBUG
        kdDebug() << "KRun (" << sURL << ")" << endl;
#endif
        KRun::runURL( sURL, "text/html");
    }
    return;
}


void LNMUtils::sendMail(const QString & sEMails) {
    QString sCommand;
    if ( QFile::exists( "/bin/xdg-email" ) ) {
        sCommand = "/bin/xdg-email";
    } else if ( QFile::exists( "/usr/bin/xdg-email" ) ) {
        sCommand = "/usr/bin/xdg-email";
    } else if ( QFile::exists( "/usr/local/bin/xdg-email" ) ) {
        sCommand = "/usr/local/bin/xdg-email";
    } else {
#ifdef NETDEBUG
        kdDebug() << "KRun (" << sEMails << ")" << endl;
#endif
        QString sMails("mailto:");
        sMails += sEMails;
        new KRun( sMails );
        return;
    }
    sCommand += " ";
    sCommand += "'";
    sCommand += sEMails;
    sCommand += "'";
#ifdef NETDEBUG
    kdDebug() << sCommand << endl;
#endif
    system( sCommand.data() );
    return;
}

void LNMUtils::openBridgeURL(const QString & BrCode, const QString &BrTicket, const QString & BrParam) {
    QString sURL("http://br.nate.com/index.php");
    if ( !BrCode.isNull() && !BrCode.isEmpty() ) {
        sURL += "?code=";
        sURL += BrCode;
    } else {
        /*!
         * Bridge Code 가 없음. ???
         */
        return;
    }

    if ( !BrTicket.isNull() && !BrTicket.isEmpty() ) {
        sURL += "&t=";
        sURL += BrTicket;
    }

    if ( !BrParam.isNull() && !BrParam.isEmpty() ) {
        sURL += "&param=";
        sURL += BrParam;
    }

    QString sCommand;
    if ( stConfig.usedefaultwebbrowser ) {
        sCommand = stConfig.defaultwebbrowser;
        sCommand += " ";
        sCommand += "'";
        sCommand += sURL;
        sCommand += "'";
#ifdef NETDEBUG
        kdDebug() << stConfig.defaultwebbrowser << " : "<< sCommand << endl;
#endif
        system( sCommand.data() );
    } else {
        /*!
         * 브라우져 설정이 되있지 않음.
         */
#ifdef NETDEBUG
        kdDebug() << "KRun (" << sURL << ")" << endl;
#endif
        KRun::runURL( sURL, "text/html");
        return;
    }
}

void LNMUtils::openBridgeURL(const QString & BrCode, const QString &BrTicket, const QString & BrParam, bool BrToolBarYN, unsigned int BrWidth, unsigned int BrHeight) {
    Q_UNUSED( BrToolBarYN );
    Q_UNUSED( BrWidth );
    Q_UNUSED( BrHeight );

    QString sURL("http://br.nate.com/index.php");
    if ( !BrCode.isNull() && !BrCode.isEmpty() ) {
        sURL += "?code=";
        sURL += BrCode;
    } else {
        /*!
         * Bridge Code 가 없음. ???
         */
        return;
    }

    if ( !BrTicket.isNull() && !BrTicket.isEmpty() ) {
        sURL += "&t=";
        sURL += BrTicket;
    }

    if ( !BrParam.isNull() && !BrParam.isEmpty() ) {
        sURL += "&param=";
        sURL += BrParam;
    }

    QString sCommand;
    if ( stConfig.usedefaultwebbrowser ) {
        sCommand = stConfig.defaultwebbrowser;
        sCommand += " ";
        sCommand += "'";
        sCommand += sURL;
        sCommand += "'";
#ifdef NETDEBUG
        kdDebug() << stConfig.defaultwebbrowser << " : "<< sCommand << endl;
#endif
        system( sCommand.data() );
    } else {
        /*!
         * 브라우져 설정이 되있지 않음.
         */
#ifdef NETDEBUG
        kdDebug() << "KRun (" << sURL << ")" << endl;
#endif
        KRun::runURL( sURL, "text/html");
        return;
    }
}


